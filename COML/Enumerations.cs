﻿// Decompiled with JetBrains decompiler
// Type: COML.Enumerations
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML
{
  public class Enumerations
  {
    public enum enumRace
    {
      African,
      Coloured,
      Indian,
      White,
      Other,
    }

    public enum enumGender
    {
      Male,
      Female,
    }

    public enum enumMaterialType
    {
      Doc,
      Pdf,
      PowerPoint,
      Url,
      UrlVideo,
      Excel,
    }

    public enum enumTrueFalse
    {
      False,
      True,
    }

    public enum enumLogType
    {
      Sms,
      Email,
      Page,
      Material,
      Answer,
      Chat,
      GeneralChat,
      GraduateChat,
    }

    public enum LogLevel
    {
      Off,
      Trace,
      Debug,
      Info,
      Warning,
      Error,
      Fatal,
    }

    public enum enumSystemFile
    {
      LearnerCV = 1,
      LearnerBusinessPlan = 2,
      InstructorCV = 3,
      InstructorQualification = 4,
      LearnerCV2 = 5,
      LearnerBusinessPlanDoc1 = 6,
      LearnerBusinessPlanDoc2 = 7,
      LearnerBusinessPlanDoc3 = 8,
      ResourceFile = 9,
    }

    public enum enumTypeFile
    {
      Pdf = 1,
      Word = 2,
      Pic = 3,
    }

    public enum enumUserType
    {
      Admin,
      Assessor,
      Mentor,
      Sponsor,
      Learner,
      AssessorAndMentor,
      Moderator,
    }

    public enum ControllerType
    {
      ProcessNote,
    }

    public enum ControllerState
    {
      Initialised,
      Running,
      Stopped,
      Uninitialised,
      Unknown,
    }
  }
}
