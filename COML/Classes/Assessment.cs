﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.Assessment
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System.Collections.Generic;

namespace COML.Classes
{
  public class Assessment
  {
    public int ID { get; set; }

    public int Attempts { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public int SubModuleID { get; set; }

    public string SubModuleCode { get; set; }

    public string SubModuleName { get; set; }

    public List<Assessment.MultipleChoiceQuestion> MultipleChoice { get; set; }

    public List<Assessment.TrueFalseQuestion> TrueOrFalse { get; set; }

    public bool IsAssessed { get; set; }

    public class MultipleChoiceQuestion
    {
      public int ID { get; set; }

      public string Question { get; set; }

      public int Answer { get; set; }

      public List<Assessment.MultipleChoiceAnswer> Answers { get; set; }
    }

    public class MultipleChoiceAnswer
    {
      public int ID { get; set; }

      public string Answer { get; set; }
    }

    public class TrueFalseQuestion
    {
      public int ID { get; set; }

      public string Question { get; set; }

      public bool Answer { get; set; }
    }
  }
}
