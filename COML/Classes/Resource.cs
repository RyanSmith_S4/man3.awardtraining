﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.Resource
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class Resource
  {
    public int ID { get; set; }

    public string Description { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public UserFile Logo { get; set; }

    public string Link { get; set; }

    public string ContactPerson { get; set; }

    public string ContactPersonCell { get; set; }

    public string ContactPersonEmail { get; set; }
  }
}
