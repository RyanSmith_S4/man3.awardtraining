﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.IncorrectMultipleChoiceQuestion
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System.Collections.Generic;

namespace COML.Classes
{
  public class IncorrectMultipleChoiceQuestion
  {
    public int MultipleChoiceID { get; set; }

    public int MultipleChoiceSubID { get; set; }

    public string Question { get; set; }

    public bool IsCorrect { get; set; }

    public bool IsEnabled { get; set; }

    public string Answer { get; set; }

    public List<string> OtherAnswers { get; set; }
  }
}
