﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.StudentStats
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class StudentStats
  {
    public int CurrentModuleID { get; set; }

    public int CurrentSubModuleID { get; set; }

    public bool IsGraduate { get; set; }

    public bool IsChatEnabled { get; set; }

    public bool IsLocked { get; set; }
  }
}
