﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.TrueFalseAnswer
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class TrueFalseAnswer
  {
    public bool IsCorrect { get; set; }

    public bool IsEnabled { get; set; }

    public string Question { get; set; }

    public bool Answer { get; set; }
  }
}
