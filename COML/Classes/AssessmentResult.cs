﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.AssessmentResult
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System.Collections.Generic;

namespace COML.Classes
{
  public class AssessmentResult
  {
    public int Attempt { get; set; }

    public bool Passed { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public int TotalMark { get; set; }

    public int StudentMark { get; set; }

    public int OverallMark { get; set; }

    public int OverallTotal { get; set; }

    public List<IncorrectMultipleChoiceQuestion> MultipleChoice { get; set; }

    public List<TrueFalseAnswer> TrueFalse { get; set; }
  }
}
