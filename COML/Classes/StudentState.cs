﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.StudentState
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class StudentState : Student
  {
    public bool IsComplete { get; set; }

    public bool IsSuccessful { get; set; }

    public int Attempts { get; set; }
  }
}
