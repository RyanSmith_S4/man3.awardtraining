﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.Module
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System.Collections.Generic;

namespace COML.Classes
{
  public class Module
  {
    public int ID { get; set; }

    public string Code { get; set; }

    public int Order { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public List<SubModule> SubModules { get; set; }

    public bool IsAvailable { get; set; }

    public bool IsCompleted { get; set; }
  }
}
