﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.SubModule
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System.Collections.Generic;

namespace COML.Classes
{
  public class SubModule
  {
    public int ID { get; set; }

    public int ModuleID { get; set; }

    public int Attempts { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public int Order { get; set; }

    public List<SubModuleMaterial> Materials { get; set; }

    public bool HasAssessment { get; set; }

    public bool IsAvailable { get; set; }

    public bool IsCurrent { get; set; }

    public bool IsLocked { get; set; }

    public bool IsComplete { get; set; }

    public bool IsStarted { get; set; }

    public bool IsNotYetCompetent { get; set; }

    public bool IsWait { get; set; }
  }
}
