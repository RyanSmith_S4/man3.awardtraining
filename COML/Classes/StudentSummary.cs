﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.StudentSummary
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class StudentSummary
  {
    public int StudentID { get; set; }

    public string UserID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string FsaCode { get; set; }

    public string Province { get; set; }

    public DateTime LastActivity { get; set; }

    public string IdentityNumber { get; set; }

    public Enumerations.enumGender Gender { get; set; }

    public string EmailAddress { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public int Progress { get; set; }

    public bool IsLocked { get; set; }

    public bool IsComplete { get; set; }

    public bool IsEnabled { get; set; }
  }
}
