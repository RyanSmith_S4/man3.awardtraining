﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.GenericLog
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class GenericLog
  {
    public DateTime TimeStamp { get; set; }

    public Enumerations.enumLogType Type { get; set; }

    public string Description { get; set; }
  }
}
