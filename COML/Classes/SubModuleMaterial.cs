﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.SubModuleMaterial
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class SubModuleMaterial
  {
    public int ID { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public Enumerations.enumMaterialType Type { get; set; }

    public string Path { get; set; }
  }
}
