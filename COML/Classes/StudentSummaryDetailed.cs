﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.StudentSummaryDetailed
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class StudentSummaryDetailed
  {
    public int StudentID { get; set; }

    public int Age { get; set; }

    public string ReferredFriendUserID { get; set; }

    public string ReferredFriend { get; set; }

    public string UserID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Nickname { get; set; }

    public string FsaCode { get; set; }

    public string HomeLanguage { get; set; }

    public string Province { get; set; }

    public DateTime LastActivity { get; set; }

    public string IdentityNumber { get; set; }

    public string HighestQualification { get; set; }

    public string HighestQualificationTitle { get; set; }

    public string PostalAddress1 { get; set; }

    public string PostalAddress2 { get; set; }

    public string PostalAddress3 { get; set; }

    public string PostalCity { get; set; }

    public string PostalCode { get; set; }

    public Enumerations.enumGender Gender { get; set; }

    public string EmailAddress { get; set; }

    public string TelephoneNumber { get; set; }

    public bool CurrentlyEmployed { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public int Progress { get; set; }

    public string CurrentModule { get; set; }

    public string CurrentSubModule { get; set; }

    public bool IsLocked { get; set; }

    public bool IsComplete { get; set; }

    public string Race { get; set; }
  }
}
