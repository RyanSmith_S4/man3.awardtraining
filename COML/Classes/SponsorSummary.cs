﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.SponsorSummary
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class SponsorSummary
  {
    public int SponsortID { get; set; }

    public string Company { get; set; }

    public string UserID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public DateTime LastActivity { get; set; }

    public string EmailAddress { get; set; }

    public string UserEmailAddress { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public bool IsLocked { get; set; }
  }
}
