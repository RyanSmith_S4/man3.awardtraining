﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.Student
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;
using System.Collections.Generic;

namespace COML.Classes
{
  public class Student
  {
    public int ID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Nickname { get; set; }

    public string IdentityNumber { get; set; }

    public int Age { get; set; }

    public Enumerations.enumGender SelectGender { get; set; }

    public string HomeLanguage { get; set; }

    public Enumerations.enumRace SelectedRace { get; set; }

    public bool Employed { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public string EmailAddress { get; set; }

    public DateTime LastActivity { get; set; }

    public string PostalAddress1 { get; set; }

    public string PostalAddress2 { get; set; }

    public string PostalAddress3 { get; set; }

    public string PostalAddressCity { get; set; }

    public string PostalAddressCode { get; set; }

    public string PostalAddressProvince { get; set; }

    public string TelephoneCell { get; set; }

    public string HighestQualification { get; set; }

    public string HighestQualificationTitle { get; set; }

    public string UserID { get; set; }

    public string FsaCode { get; set; }

    public bool IsEnabled { get; set; }

    public bool IsLocked { get; set; }

    public string ReferralUser { get; set; }

    public string ReferralCode { get; set; }

    public int ReferralStudent { get; set; }

    public bool IsChatEnabled { get; set; }

    public List<UserAssessedFile> Files { get; set; }
  }
}
