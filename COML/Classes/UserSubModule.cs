﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.UserSubModule
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System.Collections.Generic;

namespace COML.Classes
{
  public class UserSubModule
  {
    public int ID { get; set; }

    public int ModuleID { get; set; }

    public int SubModuleID { get; set; }

    public int Attempt { get; set; }

    public string Name { get; set; }

    public bool Passed { get; set; }

    public bool IsAssessed { get; set; }

    public bool IsComplete { get; set; }

    public bool IsSuccessful { get; set; }

    public List<UserSubModuleMultipleChoice> UserMultipleChoiceAnswers { get; set; }
  }
}
