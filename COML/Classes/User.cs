﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.User
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class User
  {
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserID { get; set; }

    public string FSACode { get; set; }
  }
}
