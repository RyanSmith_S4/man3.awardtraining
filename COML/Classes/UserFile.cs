﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.UserFile
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class UserFile
  {
    public int ID { get; set; }

    public string Name { get; set; }

    public Enumerations.enumSystemFile SystemFile { get; set; }

    public Enumerations.enumTypeFile TypeFile { get; set; }

    public string Filepath { get; set; }

    public DateTime DateCreated { get; set; }
  }
}
