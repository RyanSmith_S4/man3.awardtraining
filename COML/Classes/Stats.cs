﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.Stats
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class Stats
  {
    public int TotalLearners { get; set; }

    public int TotalLearnersLocked { get; set; }

    public int TotalLearnersInactive { get; set; }

    public int TotalLearnersActive { get; set; }

    public int LearnersLately { get; set; }
  }
}
