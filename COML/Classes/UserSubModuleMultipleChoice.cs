﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.UserSubModuleMultipleChoice
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML.Classes
{
  public class UserSubModuleMultipleChoice
  {
    public int ID { get; set; }

    public int UserSubModuleID { get; set; }

    public int MultipleChoiceID { get; set; }

    public int UserAnswer { get; set; }

    public int Answer { get; set; }
  }
}
