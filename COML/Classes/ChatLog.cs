﻿// Decompiled with JetBrains decompiler
// Type: COML.Classes.ChatLog
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Classes
{
  public class ChatLog
  {
    public string UserName { get; set; }

    public Enumerations.enumUserType UserType { get; set; }

    public string Message { get; set; }

    public DateTime DateTimeCreated { get; set; }
  }
}
