﻿// Decompiled with JetBrains decompiler
// Type: COML.Exceptions.ConfigSettingNotSupportedException
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Exceptions
{
  [Serializable]
  public class ConfigSettingNotSupportedException : Exception
  {
    public ConfigSettingNotSupportedException(string paErrorMessage)
      : base(paErrorMessage)
    {
    }

    public ConfigSettingNotSupportedException(string paErrorMessage, Exception paInnerException)
      : base(paErrorMessage, paInnerException)
    {
    }
  }
}
