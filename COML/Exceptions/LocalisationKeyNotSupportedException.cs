﻿// Decompiled with JetBrains decompiler
// Type: COML.Exceptions.LocalisationKeyNotSupportedException
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;

namespace COML.Exceptions
{
  [Serializable]
  public class LocalisationKeyNotSupportedException : Exception
  {
    public LocalisationKeyNotSupportedException(string paErrorMessage)
      : base(paErrorMessage)
    {
    }

    public LocalisationKeyNotSupportedException(string paErrorMessage, Exception paInnerException)
      : base(paErrorMessage, paInnerException)
    {
    }
  }
}
