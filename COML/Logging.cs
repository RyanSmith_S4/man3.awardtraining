﻿// Decompiled with JetBrains decompiler
// Type: COML.Logging
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using COML.Exceptions;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;

namespace COML
{
  public class Logging
  {
    private static Logger meLogger = LogManager.GetCurrentClassLogger();

    public static string GetExceptionMessage(Exception paException)
    {
      if (paException == null)
        return string.Empty;
      return string.Format("(HRESULT:0x{0:X8}).", (object) paException.HResult);
    }

    public static void Log(
      string paMessage,
      Enumerations.LogLevel paLogLevel,
      Exception paException = null)
    {
      NLog.LogLevel info = NLog.LogLevel.Info;
      NLog.LogLevel logLevel;
      switch (paLogLevel)
      {
        case Enumerations.LogLevel.Off:
          return;
        case Enumerations.LogLevel.Trace:
          logLevel = NLog.LogLevel.Trace;
          break;
        case Enumerations.LogLevel.Debug:
          logLevel = NLog.LogLevel.Debug;
          break;
        case Enumerations.LogLevel.Info:
          logLevel = NLog.LogLevel.Info;
          break;
        case Enumerations.LogLevel.Warning:
          logLevel = NLog.LogLevel.Warn;
          break;
        case Enumerations.LogLevel.Error:
          logLevel = NLog.LogLevel.Error;
          break;
        case Enumerations.LogLevel.Fatal:
          logLevel = NLog.LogLevel.Fatal;
          break;
        default:
          throw new LogLevelNotSupportedException(string.Format("Log Level not supported", (object) paLogLevel.ToString()));
      }
      Logging.meLogger.Log(new LogEventInfo()
      {
        TimeStamp = DateTime.Now,
        Level = logLevel,
        Exception = paException,
        Message = paMessage + (paException == null ? string.Empty : " " + Logging.GetExceptionMessage(paException))
      });
    }

    public static void RemoveLogger(string paName)
    {
      try
      {
        LoggingConfiguration configuration = LogManager.Configuration;
        Target targetByName = configuration.FindTargetByName(paName);
        if (targetByName == null)
          return;
        foreach (LoggingRule loggingRule in (IEnumerable<LoggingRule>) configuration.LoggingRules)
        {
          if (loggingRule.Targets.Contains(targetByName))
          {
            configuration.LoggingRules.Remove(loggingRule);
            configuration.Reload();
            break;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log("Unexpected Error", Enumerations.LogLevel.Error, ex);
      }
    }
  }
}
