﻿// Decompiled with JetBrains decompiler
// Type: COML.Settings
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

using System;
using System.Configuration;

namespace COML
{
  public class Settings
  {
    public static bool SendSmses
    {
      get
      {
        try
        {
          return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSmser"]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return false;
        }
      }
    }

    public static string SmsApiKey
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (SmsApiKey)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static bool VerifyIDNumbers
    {
      get
      {
        try
        {
          return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableVerifyID"]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return false;
        }
      }
    }

    public static string VerifyIDUsername
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (VerifyIDUsername)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string VerifyIDAPIKey
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings["VerifyApiKey"];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static bool MailerEnabled
    {
      get
      {
        try
        {
          return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableMailer"]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return false;
        }
      }
    }

    public static bool InactivityMailerEnabled
    {
      get
      {
        try
        {
          return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableInactivityMailer"]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return false;
        }
      }
    }

    public static string MailerHost
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings["MailerSmtp"];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static int MailerPort
    {
      get
      {
        try
        {
          return Convert.ToInt32(ConfigurationManager.AppSettings["MailerSmtpPort"]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return 0;
        }
      }
    }

    public static string MailerFromAddress
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (MailerFromAddress)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string MailerFromPassword
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings["MailerPassword"];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string AdminEmail
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (AdminEmail)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string AppealEmail
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (AppealEmail)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string SupportEmail
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (SupportEmail)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string EnrolmentsEmail
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (EnrolmentsEmail)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static bool AutoAssignLearnersToAssessor
    {
      get
      {
        try
        {
          return bool.Parse(ConfigurationManager.AppSettings["AutoAssignToGraham"]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return false;
        }
      }
    }

    public static string FeedbackEmail
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (FeedbackEmail)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static string TechEmail
    {
      get
      {
        try
        {
          return ConfigurationManager.AppSettings[nameof (TechEmail)];
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return "";
        }
      }
    }

    public static int ProcessLearners
    {
      get
      {
        try
        {
          return Convert.ToInt32(ConfigurationManager.AppSettings[nameof (ProcessLearners)]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return 0;
        }
      }
    }

    public static int LogProcessing
    {
      get
      {
        try
        {
          return Convert.ToInt32(ConfigurationManager.AppSettings[nameof (LogProcessing)]);
        }
        catch (Exception ex)
        {
          Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          return 0;
        }
      }
    }
  }
}
