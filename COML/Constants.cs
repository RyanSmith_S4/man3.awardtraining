﻿// Decompiled with JetBrains decompiler
// Type: COML.Constants
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML
{
  public class Constants
  {
    public const string EXECUTION_SWITCH_WINL_MODE_GUI = "-gui";
    public const string EXECUTION_SWITCH_WINL_MODE_SERVICE = "-service";
    public const string EXECUTION_WINL_MODE_GUI = "GUI";
    public const string EXECUTION_WINL_MODE_SERVICE = "SERVICE";
    public const string EXECUTION_WINL_SERVICE_TITLE = "SAB";
  }
}
