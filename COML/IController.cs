﻿// Decompiled with JetBrains decompiler
// Type: COML.IController
// Assembly: COML, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 743178F3-7629-4369-96BD-B04FA50ACDFA
// Assembly location: C:\Users\S4\Desktop\man\bin\COML.dll

namespace COML
{
  public abstract class IController
  {
    protected Enumerations.ControllerState meStatus = Enumerations.ControllerState.Uninitialised;

    public abstract bool Init(params object[] paInitParameters);

    public abstract bool Start();

    public abstract bool Stop();

    public Enumerations.ControllerState Status
    {
      get
      {
        return this.meStatus;
      }
    }
  }
}
