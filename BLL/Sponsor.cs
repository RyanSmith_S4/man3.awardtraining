﻿// Decompiled with JetBrains decompiler
// Type: BLL.Sponsor
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;
using System.Collections.Generic;

namespace BLL
{
  public class Sponsor
  {
    public static bool AddSponsor(COML.Classes.Sponsor paSponsor)
    {
      return DAL.Core.Sponsor.AddSponsor(paSponsor);
    }

    public static bool AddSponsorFsaCode(int paSponsorID, string paCode)
    {
      return DAL.Core.Sponsor.AddSponsorFsaCode(paSponsorID, paCode);
    }

    public static List<SponsorSummary> GetSponsors()
    {
      return DAL.Core.Sponsor.GetSponsors();
    }

    public static List<SponsorSummary> GetStudentsByFsaCode(string paWord)
    {
      return DAL.Core.Sponsor.GetStudentsByFsaCode(paWord);
    }

    public static COML.Classes.Sponsor GetSponsorByID(int paSponsorID)
    {
      return DAL.Core.Sponsor.GetSponsorByID(paSponsorID);
    }

    public static int GetSponsorIDByUserID(string paSponsorID)
    {
      return DAL.Core.Sponsor.GetSponsorIDByUserID(paSponsorID);
    }

    public static COML.Classes.Sponsor GetSponsorByUserID(string paSponsorID)
    {
      return DAL.Core.Sponsor.GetSponsorByUserID(paSponsorID);
    }

    public static bool RemoveSponsorFsaCode(int paSponsorID, string paCode)
    {
      return DAL.Core.Sponsor.RemoveSponsorFsaCode(paSponsorID, paCode);
    }

    public static bool LockSponsor(int paSponsorID)
    {
      return DAL.Core.Sponsor.LockSponsor(paSponsorID);
    }

    public static bool UnlockSponsor(int paSponsorID)
    {
      return DAL.Core.Sponsor.UnlockSponsor(paSponsorID);
    }

    public static bool IsLocked(string paSponsorID)
    {
      return DAL.Core.Sponsor.IsLocked(paSponsorID);
    }

    public static List<StudentSummaryDetailed> GetStudentsBySponsor(
      string paSponsorID)
    {
      return DAL.Core.Sponsor.GetStudentsBySponsor(paSponsorID);
    }
  }
}
