﻿// Decompiled with JetBrains decompiler
// Type: BLL.Student
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;
using System;
using System.Collections.Generic;

namespace BLL
{
  public class Student
  {
    public static bool DisableChat(int paStudentID)
    {
      return DAL.Admin.Student.DisableChat(paStudentID);
    }

    public static bool EnableChat(int paStudentID)
    {
      return DAL.Admin.Student.EnableChat(paStudentID);
    }

    public static bool DisableAccount(int paStudentID)
    {
      return DAL.Admin.Student.DisableAccount(paStudentID);
    }

    public static bool ClearNotifications(string paStudentID)
    {
      return DAL.Admin.Student.ClearNotifications(paStudentID);
    }

    public static bool DisableInactiveAccounts()
    {
      return DAL.Admin.Student.DisableInactiveAccounts();
    }

    public static bool EnableAccount(int paStudentID)
    {
      return DAL.Admin.Student.EnableAccount(paStudentID);
    }

    public static bool RegisterStudent(COML.Classes.Student paStudent)
    {
      bool flag = DAL.Registration.Student.RegisterStudent(paStudent);
      if (flag)
        flag = DAL.Registration.Module.RegisterStudentModule(paStudent.UserID);
      return flag;
    }

    public static bool IDNumberExists(string paIDNumber)
    {
      return DAL.Registration.Student.IDNumberExists(paIDNumber);
    }

    public static bool IsLocked(string paStudentID)
    {
      return DAL.Registration.Student.IsLocked(paStudentID);
    }

    public static bool UpdateStudentFsaCode(int paStudentID, string paFsaCode)
    {
      return DAL.Registration.Student.UpdateStudentFsaCode(paStudentID, paFsaCode);
    }

    public static COML.Classes.Student GetStudent(string paUserID)
    {
      return DAL.Core.Assessment.Student.GetStudent(paUserID);
    }

    public static COML.Classes.Student GetStudentByID(int paStudentID)
    {
      return DAL.Core.Assessment.Student.GetStudentByID(paStudentID);
    }

    public static bool UpdateStudentByID(COML.Classes.Student paStudent)
    {
      return DAL.Registration.Student.UpdateStudentByID(paStudent);
    }

    public static COML.Classes.Student GetStudentRecordByID(int paStudentID)
    {
      return DAL.Core.Assessment.Student.GetStudentRecordByID(paStudentID);
    }

    public static bool StudentIsFriend(int paStudentID, string paUserID)
    {
      return DAL.Core.Assessment.Student.StudentIsFriend(paStudentID, paUserID);
    }

    public static COML.Classes.Student GetStudentByIDForSponsor(
      int paStudentID,
      int paSponsorID)
    {
      return DAL.Core.Assessment.Student.GetStudentByIDForSponsor(paStudentID, paSponsorID);
    }

    public static List<StudentSummary> GetStudents()
    {
      return DAL.Admin.Student.GetStudents();
    }

    public static List<StudentSummary> GetAllStudents()
    {
      return DAL.Admin.Student.GetAllStudents();
    }

    public static List<StudentSummary> GetStudentsOrderedByName()
    {
      return DAL.Admin.Student.GetStudentsOrderedByName();
    }

    public static List<StudentSummaryDetailed> GetAllStudentsDetailed()
    {
      return DAL.Admin.Student.GetAllStudentsDetailed();
    }

    public static List<StudentSummaryDetailed> GetStudentsDetailed()
    {
      return DAL.Admin.Student.GetStudentsDetailed();
    }

    public static List<StudentSummaryDetailed> GetStudentsActiveInPeriod(
      DateTime paDateTimeStart,
      DateTime paDateTimeStop)
    {
      return DAL.Admin.Student.GetStudentsActiveInPeriod(paDateTimeStart, paDateTimeStop);
    }

    public static List<StudentSummaryDetailed> GetInactiveStudentsDetailed()
    {
      return DAL.Admin.Student.GetInactiveStudentsDetailed();
    }

    public static List<StudentSummaryDetailed> GetLockedStudentsDetailed()
    {
      return DAL.Admin.Student.GetLockedStudentsDetailed();
    }

    public static List<StudentSummary> GetStudentsByIDNumber(string paWord)
    {
      return DAL.Admin.Student.GetStudentsByIDNumber(paWord);
    }

    public static bool UnlockStudent(int paStudentID)
    {
      return DAL.Admin.Student.UnlockStudent(paStudentID);
    }

    public static void RemoveStudents()
    {
      DAL.Core.Assessment.Student.RemoveStudents();
    }

    public static List<StudentSummary> GetStudentsForSponsor(int paSponsorID)
    {
      return DAL.Admin.Student.GetStudentsForSponsor(paSponsorID);
    }

    public static List<StudentSummary> GetStudentsByIDNumberForSponsor(
      int paSponsorID,
      string paWord)
    {
      return DAL.Admin.Student.GetStudentsByIDNumberForSponsor(paSponsorID, paWord);
    }

    public static List<StudentSummary> GetStudentsByIDNumberAndCodeForSponsor(
      int paSponsorID,
      string paWord,
      string paCode)
    {
      return DAL.Admin.Student.GetStudentsByIDNumberAndCodeForSponsor(paSponsorID, paWord, paCode);
    }

    public static bool SaveStudentFile(
      string paStudentID,
      int paSystemFile,
      int paTypeFile,
      string paFileName,
      string paFileLocation)
    {
      return DAL.Core.Assessment.Student.SaveStudentFile(paStudentID, paSystemFile, paTypeFile, paFileName, paFileLocation);
    }

    public static UserFile GetStudentFile(string paStudentID, int paFileID)
    {
      return DAL.Core.Assessment.Student.GetStudentFile(paStudentID, paFileID);
    }

    public static List<UserAssessedFile> GetStudentsNonAssessedFiles(
      string paStudentID)
    {
      return DAL.Core.Assessment.Student.GetStudentsNonAssessedFiles(paStudentID);
    }

    public static List<UserAssessedFile> GetStudentsAssessedFiles(
      string paStudentID)
    {
      return DAL.Core.Assessment.Student.GetStudentsAssessedFiles(paStudentID);
    }

    public static StudentStats GetStudentStats(string paStudentID)
    {
      return DAL.Core.Assessment.Student.GetStudentStats(paStudentID);
    }

    public static List<StudentSummary> GetStudentReferredStudents(
      string paStudentID)
    {
      return DAL.Admin.Student.GetStudentReferredStudents(paStudentID);
    }
  }
}
