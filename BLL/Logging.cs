﻿// Decompiled with JetBrains decompiler
// Type: BLL.Logging
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;
using System;
using System.Collections.Generic;

namespace BLL
{
  public class Logging
  {
    public static bool SavePageLog(string paUserID, string paPage)
    {
      return DAL.Logging.Logging.SavePageLog(paUserID, paPage);
    }

    public static bool SaveInstructorPageLog(string paUserID, string paPage)
    {
      return DAL.Logging.Logging.SaveInstructorPageLog(paUserID, paPage);
    }

    public static bool SaveSponsorPageLog(string paUserID, string paPage)
    {
      return DAL.Logging.Logging.SaveSponsorPageLog(paUserID, paPage);
    }

    public static bool SaveMaterialLog(string paUserID, int paSubModuleMaterialID)
    {
      return DAL.Logging.Logging.SaveMaterialLog(paUserID, paSubModuleMaterialID);
    }

    public static bool SaveSystemLog(int paStudentID, string paAction, string paSystemUsername)
    {
      return DAL.Logging.Logging.SaveSystemLog(paStudentID, paSystemUsername + " - " + paAction);
    }

    public static List<GenericLog> GetStudentLogs(int paStudentID)
    {
      return DAL.Logging.Logging.GetStudentLogs(paStudentID);
    }

    public static List<GenericLog> GetSponsorLogs(int paSponsorID)
    {
      return DAL.Logging.Logging.GetSponsorLogs(paSponsorID);
    }

    public static List<GenericLog> GetMentorLogs(int paInstructorID)
    {
      return DAL.Logging.Logging.GetMentorLogs(paInstructorID);
    }

    public static List<GenericLog> GetAssessorLogs(int paInstructorID)
    {
      return DAL.Logging.Logging.GetAssessorLogs(paInstructorID);
    }

    public static bool SaveChatLog(string paUserID, string paMessage, List<string> paRoles)
    {
      return DAL.Logging.Logging.SaveChatLog(paUserID, paMessage, paRoles);
    }

    public static List<ChatLog> GetChatLogs(DateTime paDatetime)
    {
      return DAL.Logging.Logging.GetChatLogs(paDatetime);
    }

    public static bool SaveGeneralChatLog(string paUserID, string paMessage, List<string> paRoles)
    {
      return DAL.Logging.Logging.SaveGeneralChatLog(paUserID, paMessage, paRoles);
    }

    public static List<ChatLog> GetGeneralChatLogs(DateTime paDatetime)
    {
      return DAL.Logging.Logging.GetGeneralChatLogs(paDatetime);
    }

    public static bool CanSendVerificationCode(string paUserID, DateTime paDateTime)
    {
      return DAL.Logging.Logging.CanSendVerificationCode(paUserID, paDateTime);
    }

    public static bool SaveResetPasswordCode(string paUserID, string paCode)
    {
      return DAL.Logging.Logging.SaveResetPasswordCode(paUserID, paCode);
    }

    public static bool GetResetPasswordCode(string paEmail, string paUrlEmail, string paCode)
    {
      return DAL.Logging.Logging.GetResetPasswordCode(paEmail, paUrlEmail, paCode);
    }

    public static bool IsGeneralChatActive(string paUserID)
    {
      return DAL.Logging.Logging.IsGeneralChatActive(paUserID);
    }

    public static bool IsGraduateChatActive(string paUserID)
    {
      return DAL.Logging.Logging.IsGraduateChatActive(paUserID);
    }
  }
}
