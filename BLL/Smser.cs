﻿// Decompiled with JetBrains decompiler
// Type: BLL.Smser
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace BLL
{
  public class Smser
  {
    public static void SendRegisterSms(string paNumber, string paName, string paUserID)
    {
      if (!Settings.SendSmses)
        return;
      try
      {
        /*
        StringBuilder stringBuilder = new StringBuilder();
        string smsApiKey = Settings.SmsApiKey;
        string str = "Hi " + paName + ". Thank you for registering with Start a Business.";
        stringBuilder.AppendFormat("?api_service_key={0}", (object) HttpUtility.UrlEncode(smsApiKey));
        stringBuilder.AppendFormat("&msg_senderid={0}", (object) HttpUtility.UrlEncode("StartABusiness"));
        stringBuilder.AppendFormat("&msg_to={0}", (object) HttpUtility.UrlEncode(paNumber));
        stringBuilder.AppendFormat("&msg_text={0}", (object) HttpUtility.UrlEncode(str));
        stringBuilder.AppendFormat("&msg_dr={0}", (object) "0");
        stringBuilder.AppendFormat("&msg_clientref={0}", (object) HttpUtility.UrlEncode("ABC123"));
        stringBuilder.AppendFormat("&output={0}", (object) HttpUtility.UrlEncode("pipe"));
        HttpWebResponse response = (HttpWebResponse) WebRequest.Create("https://api.doluna.net/sms/send" + (object) stringBuilder).GetResponse();
        DAL.Logging.Logging.SaveSmsLog(paUserID, str);
        */

           string message = "Hi " + paName + ". Thank you for registering with Start a Business.";
           Mailer.SendEmail("award1@smsgw1.gsm.co.za", "SMS", "SMS", new Attachment((Stream)Smser.CreateCSV(paNumber, message), "sms.csv"));
         }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static string SendVerificationSms(string paNumber, string paUserID)
    {
      string str1 = "";
      if (Settings.SendSmses)
      {
        try
        {
          DateTime now = DateTime.Now;
          if (BLL.Logging.CanSendVerificationCode(paUserID, now))
          {
            StringBuilder stringBuilder = new StringBuilder();
            string smsApiKey = Settings.SmsApiKey;
            string str2 = "%7Botp%7Cassessment%7C3000%7CE TEN%7C6%7D";
            stringBuilder.AppendFormat("?api_service_key={0}", (object) HttpUtility.UrlEncode(smsApiKey));
            stringBuilder.AppendFormat("&msg_senderid={0}", (object) HttpUtility.UrlEncode("StartABusiness"));
            stringBuilder.AppendFormat("&msg_to={0}", (object) HttpUtility.UrlEncode(paNumber));
            stringBuilder.AppendFormat("&msg_text={0}", (object) HttpUtility.UrlEncode(str2));
            stringBuilder.AppendFormat("&msg_dr={0}", (object) "0");
            stringBuilder.AppendFormat("&msg_clientref={0}", (object) HttpUtility.UrlEncode("ABC123"));
            stringBuilder.AppendFormat("&output={0}", (object) HttpUtility.UrlEncode("pipe"));
            StreamReader streamReader = new StreamReader(WebRequest.Create("https://api.doluna.net/sms/send" + (object) stringBuilder).GetResponse().GetResponseStream());
            string[] strArray = streamReader.ReadToEnd().Split('|');
            streamReader.Close();
            if (strArray[0] == "OK")
            {
              str1 = strArray[2].ToString();
              DAL.Logging.Logging.SaveSmsLog(paUserID, "Verification SMS sent to student's phone.");
            }
            else
              str1 = "";
          }
          else
            str1 = "";
        }
        catch (Exception ex)
        {
          COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        }
      }
      return str1;
    }

    public static bool VerifySms(string paRefNo, string paPin, string paUserID)
    {
      bool flag = false;
      if (Settings.SendSmses)
      {
        try
        {
          StringBuilder stringBuilder = new StringBuilder();
          string smsApiKey = Settings.SmsApiKey;
          stringBuilder.AppendFormat("?api_service_key={0}", (object) HttpUtility.UrlEncode(smsApiKey));
          stringBuilder.AppendFormat("&trans_ref={0}", (object) HttpUtility.UrlEncode(paRefNo));
          stringBuilder.AppendFormat("&pin={0}", (object) HttpUtility.UrlEncode(paPin));
          stringBuilder.AppendFormat("&template={0}", (object) HttpUtility.UrlEncode("ETEN"));
          stringBuilder.AppendFormat("&output={0}", (object) HttpUtility.UrlEncode("pipe"));
          StreamReader streamReader = new StreamReader(WebRequest.Create("https://api.doluna.net/sms/checkpin" + (object) stringBuilder).GetResponse().GetResponseStream());
          string[] strArray = streamReader.ReadToEnd().Split('|');
          streamReader.Close();
          flag = strArray[0] == "VERIFIED";
          DAL.Logging.Logging.SaveSmsLog(paUserID, "Student sending code:" + paPin + " to verification server.");
        }
        catch (Exception ex)
        {
          COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        }
      }
      return flag;
    }

    public static void SendResultSMS(
      string paName,
      string paNumber,
      string paCode,
      string paModule,
      bool paPass,
      int paStudentMark,
      int paTotalMark,
      int paAttempt,
      string paUserID,
      int paOverallMark,
      int paOverallTotal)
    {
      if (!Settings.SendSmses)
        return;
      try
      {
        string str1 = "https://api.doluna.net/sms/send";
        StringBuilder stringBuilder = new StringBuilder();
        string smsApiKey = Settings.SmsApiKey;
        string str2 = "Hi " + paName + ", ";
        if (paPass)
        {
          if (paPass)
            str2 = str2 + "you have been found competent at " + paModule + " - " + paCode + ".";
        }
        else
        {
          string str3 = str2 + "you are not yet competent at " + paModule + " - " + paCode + ". ";
          if (paAttempt < 2)
            str2 = str3 + "You achieved: " + (object) paStudentMark + "/" + (object) paTotalMark + ". " + "You have 1 attempt left.";
          else
            str2 = str3 + "You achieved: " + (object) paOverallMark + "/" + (object) paOverallTotal + ". " + "You have no attempts left.";
        }
        stringBuilder.AppendFormat("?api_service_key={0}", (object) HttpUtility.UrlEncode(smsApiKey));
        stringBuilder.AppendFormat("&msg_senderid={0}", (object) HttpUtility.UrlEncode("StartABusiness"));
        stringBuilder.AppendFormat("&msg_to={0}", (object) HttpUtility.UrlEncode(paNumber));
        stringBuilder.AppendFormat("&msg_text={0}", (object) HttpUtility.UrlEncode(str2));
        stringBuilder.AppendFormat("&msg_dr={0}", (object) "0");
        stringBuilder.AppendFormat("&msg_clientref={0}", (object) HttpUtility.UrlEncode("ABC123"));
        stringBuilder.AppendFormat("&output={0}", (object) HttpUtility.UrlEncode("pipe"));
        HttpWebResponse response = (HttpWebResponse) WebRequest.Create(str1 + (object) stringBuilder).GetResponse();
        DAL.Logging.Logging.SaveSmsLog(paUserID, str2);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendResetPasswordSms(
      string paName,
      string paNumber,
      string paPassword,
      string paUserID)
    {
      if (!Settings.SendSmses)
        return;
      try
      {
        StringBuilder stringBuilder = new StringBuilder();
        string smsApiKey = Settings.SmsApiKey;
        string str = "Hi " + paName + ", " + "your new Start a Business password is: " + paPassword + ".";
        stringBuilder.AppendFormat("?api_service_key={0}", (object) HttpUtility.UrlEncode(smsApiKey));
        stringBuilder.AppendFormat("&msg_senderid={0}", (object) HttpUtility.UrlEncode("StartABusiness"));
        stringBuilder.AppendFormat("&msg_to={0}", (object) HttpUtility.UrlEncode(paNumber));
        stringBuilder.AppendFormat("&msg_text={0}", (object) HttpUtility.UrlEncode(str));
        stringBuilder.AppendFormat("&msg_dr={0}", (object) "0");
        stringBuilder.AppendFormat("&msg_clientref={0}", (object) HttpUtility.UrlEncode("ABC123"));
        stringBuilder.AppendFormat("&output={0}", (object) HttpUtility.UrlEncode("pipe"));
        HttpWebResponse response = (HttpWebResponse) WebRequest.Create("https://api.doluna.net/sms/send" + (object) stringBuilder).GetResponse();
        DAL.Logging.Logging.SaveSmsLog(paUserID, str);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

        public static MemoryStream CreateCSV(string number, string message)
        {
            string str = number + "," + message;
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter((Stream)memoryStream);
            streamWriter.Write(str);
            streamWriter.Flush();
            memoryStream.Position = 0L;
            return memoryStream;
        }

    }
}
