﻿// Decompiled with JetBrains decompiler
// Type: BLL.Core.Resource
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using System.Collections.Generic;

namespace BLL.Core
{
  public class Resource
  {
    public static List<COML.Classes.Resource> GetResources()
    {
      return DAL.Admin.Resource.GetResources();
    }

    public static List<COML.Classes.Resource> GetResourcesBySearch(string paWord)
    {
      return DAL.Admin.Resource.GetResourcesBySearch(paWord);
    }

    public static COML.Classes.Resource GetResource(int paID)
    {
      return DAL.Admin.Resource.GetResource(paID);
    }

    public static bool RemoveResource(int paID)
    {
      return DAL.Admin.Resource.RemoveResource(paID);
    }

    public static bool SaveResource(COML.Classes.Resource paResource, string paUserID)
    {
      return DAL.Admin.Resource.SaveResource(paResource, paUserID);
    }
  }
}
