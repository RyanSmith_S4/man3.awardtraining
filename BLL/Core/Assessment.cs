﻿// Decompiled with JetBrains decompiler
// Type: BLL.Core.Assessment
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;

namespace BLL.Core
{
  public class Assessment
  {
    public static COML.Classes.Assessment GetModuleAssessment(
      int paModuleID,
      int paSubModuleID,
      string paUserID)
    {
      return DAL.Core.Assessment.Assessment.GetModuleAssessment(paModuleID, paSubModuleID, paUserID);
    }

    public static bool AddUserMultipleChoiceResult(
      int paModuleID,
      int paSubModuleID,
      int paMultipleChoiceID,
      int paMultipleChoiceAnswer,
      string paUserID,
      bool paFinish)
    {
      return DAL.Core.Assessment.Assessment.AddUserMultipleChoiceResult(paModuleID, paSubModuleID, paMultipleChoiceID, paMultipleChoiceAnswer, paUserID, paFinish);
    }

    public static bool AddUserTrueFalseResult(
      int paModuleID,
      int paSubModuleID,
      int paTrueFalseID,
      bool paTrueFalseAnswer,
      string paUserID,
      bool paFinish)
    {
      return DAL.Core.Assessment.Assessment.AddUserTrueFalseResult(paModuleID, paSubModuleID, paTrueFalseID, paTrueFalseAnswer, paUserID, paFinish);
    }

    public static AssessmentResult MarkUserAssessment(
      int paModuleID,
      int paSubModuleID,
      string paUserID)
    {
      return DAL.Core.Assessment.Assessment.MarkUserAssessment(paModuleID, paSubModuleID, paUserID);
    }
  }
}
