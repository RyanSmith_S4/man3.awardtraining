﻿// Decompiled with JetBrains decompiler
// Type: BLL.Core.Admin
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;
using System.Collections.Generic;

namespace BLL.Core
{
  public class Admin
  {
    public static Stats GetStats()
    {
      return DAL.Admin.Admin.GetStats();
    }

    public static List<User> GetUsers()
    {
      return DAL.Admin.Admin.GetUsers();
    }

    public static List<string> GetFSACodes()
    {
      return DAL.Admin.Admin.GetFSACodes();
    }
  }
}
