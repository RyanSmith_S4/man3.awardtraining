﻿// Decompiled with JetBrains decompiler
// Type: BLL.Instructor
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;
using System.Collections.Generic;

namespace BLL
{
  public class Instructor
  {
    public static bool IDNumberExists(string paIDNumber)
    {
      return DAL.Core.Instructor.IDNumberExists(paIDNumber);
    }

    public static int RegisterInstructor(COML.Classes.Instructor paInstructor)
    {
      return DAL.Core.Instructor.RegisterInstructor(paInstructor);
    }

    public static COML.Classes.Instructor GetInstructorByID(int paInstructorID)
    {
      return DAL.Core.Instructor.GetInstructorByID(paInstructorID);
    }

    public static COML.Classes.Instructor GetInstructorByUserID(string paInstructorID)
    {
      return DAL.Core.Instructor.GetInstructorByUserID(paInstructorID);
    }

    public static COML.Classes.Instructor GetAssessorByUserID(string paInstructorID)
    {
      return DAL.Core.Instructor.GetAssessorByUserID(paInstructorID);
    }

    public static COML.Classes.Instructor GetAssessorByID(int paInstructorID)
    {
      return DAL.Core.Instructor.GetAssessorByID(paInstructorID);
    }

    public static COML.Classes.Instructor GetMentorByID(int paInstructorID)
    {
      return DAL.Core.Instructor.GetMentorByID(paInstructorID);
    }

    public static bool SaveInstructorFile(
      int paInstructorID,
      int paSystemFile,
      int paTypeFile,
      string paFileName,
      string paFileLocation)
    {
      return DAL.Core.Instructor.SaveInstructorFile(paInstructorID, paSystemFile, paTypeFile, paFileName, paFileLocation);
    }

    public static bool DeregisterInstructor(int paInstructorID)
    {
      return DAL.Core.Instructor.DeregisterInstructor(paInstructorID);
    }

    public static List<MentorSummary> GetMentors()
    {
      return DAL.Core.Instructor.GetMentors();
    }

    public static List<AssessorSummary> GetAssessors()
    {
      return DAL.Core.Instructor.GetAssessors();
    }

    public static List<MentorSummary> GetMentorsByIDNumber(string paWord)
    {
      return DAL.Core.Instructor.GetMentorByIdNumber(paWord);
    }

    public static List<AssessorSummary> GetAssessorsByIDNumber(string paWord)
    {
      return DAL.Core.Instructor.GetAssessorByIdNumber(paWord);
    }

    public static UserFile GetInstructorFile(int paInstructorID, int paFileID)
    {
      return DAL.Core.Instructor.GetIntructorFile(paInstructorID, paFileID);
    }

    public static UserFile GetAssessorStudentFile(
      string paInstructorID,
      int paStudentID,
      int paFileID)
    {
      return DAL.Core.Instructor.GetAssessorStudentFile(paInstructorID, paStudentID, paFileID);
    }

    public static bool LockAssessor(int paInstructorID)
    {
      return DAL.Core.Instructor.LockAssessor(paInstructorID);
    }

    public static bool UnlockAssessor(int paInstructorID)
    {
      return DAL.Core.Instructor.UnlockAssessor(paInstructorID);
    }

    public static bool ApproveAssessor(int paInstructorID)
    {
      return DAL.Core.Instructor.ApproveAssessor(paInstructorID);
    }

    public static bool LockMentor(int paInstructorID)
    {
      return DAL.Core.Instructor.LockMentor(paInstructorID);
    }

    public static bool UnlockMentor(int paInstructorID)
    {
      return DAL.Core.Instructor.UnlockMentor(paInstructorID);
    }

    public static bool ApproveMentor(int paInstructorID)
    {
      return DAL.Core.Instructor.ApproveMentor(paInstructorID);
    }

    public static bool IsLocked(string paInstructorID)
    {
      return DAL.Core.Instructor.IsLocked(paInstructorID);
    }

    public static bool UnassignAssessorStudent(int paInstructorID, int paStudentID)
    {
      return DAL.Core.Instructor.UnassignAssessorStudent(paInstructorID, paStudentID);
    }

    public static List<COML.Classes.Student> GetStudentsNotAssignedToAssessors()
    {
      return DAL.Core.Instructor.GetStudentsNotAssignedToAssessors();
    }

    public static List<COML.Classes.Student> GetStudentsByIDNumberAndCodeForAssessor(
      string paWord,
      string paCode)
    {
      return DAL.Core.Instructor.GetStudentsByIDNumberAndCodeForAssessor(paWord, paCode);
    }

    public static List<COML.Classes.Student> GetAssignedStudentsByIDNumberForAssessor(
      string paUserID,
      string paWord)
    {
      return DAL.Core.Instructor.GetAssignedStudentsByIDNumberForAssessor(paUserID, paWord);
    }

    public static bool AssignLearnerToAssessor(int paInstructorID, int paStudentID)
    {
      return DAL.Core.Instructor.AssignLearnerToAssessor(paInstructorID, paStudentID);
    }

    public static StudentState GetStudentsNonAssessedFiles(
      string paUserID,
      int paStudentID)
    {
      return DAL.Core.Instructor.GetStudentsNonAssessedFiles(paUserID, paStudentID);
    }

    public static bool IsMyLearner(string paUserID, int paStudentID)
    {
      return DAL.Core.Instructor.IsMyLearner(paUserID, paStudentID);
    }

    public static bool MarkFileOK(string paUserID, int paStudentID, int paFileID)
    {
      return DAL.Core.Instructor.MarkFileOK(paUserID, paStudentID, paFileID);
    }

    public static bool MarkFileNOK(string paUserID, int paStudentID, int paFileID)
    {
      return DAL.Core.Instructor.MarkFileNOK(paUserID, paStudentID, paFileID);
    }

    public static bool FinalizeStudentOK(string paUserID, int paStudentID)
    {
      return DAL.Core.Instructor.FinalizeStudentOK(paUserID, paStudentID);
    }

    public static bool FinalizeStudentNOK(string paUserID, int paStudentID)
    {
      return DAL.Core.Instructor.FinalizeStudentNOK(paUserID, paStudentID);
    }

    public static bool StudentIsFriend(int paStudentID, string paUserID)
    {
      return DAL.Core.Instructor.StudentIsFriend(paStudentID, paUserID);
    }

    public static List<StudentSummary> GetStudentReferredStudents(
      string paStudentID)
    {
      return DAL.Core.Instructor.GetStudentReferredStudents(paStudentID);
    }
  }
}
