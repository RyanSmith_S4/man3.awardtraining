﻿// Decompiled with JetBrains decompiler
// Type: BLL.Mailer
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using TheArtOfDev.HtmlRenderer.Core;
using TheArtOfDev.HtmlRenderer.Core.Entities;
using TheArtOfDev.HtmlRenderer.WinForms;

namespace BLL
{
  public class Mailer
  {
    public static void SendEmail(string paToAddress, string paSubject, string paMessage, Attachment attachment = null)
    {
      if (!Settings.MailerEnabled)
        return;
      new SmtpClient(Settings.MailerHost, Settings.MailerPort)
      {
        UseDefaultCredentials = false,
        Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
      }.Send(new MailMessage()
      {
        From = new MailAddress(Settings.MailerFromAddress, "SMTP Test"),
        To = {
          paToAddress
        },
        Subject = paSubject,
        IsBodyHtml = true,
        Body = paMessage
      });
    }

    public static void SendRegistrationEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID,
      string paPassword)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str1 = "Award Training - Registration";
        string str2 = "Award Training - Registration - " + paIDNumber;
        string str3 = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>";
        string str4 = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + " - Email: " + paToAddress + "</p>";
        string input = str3 + "<p>Welcome! You have successfully registered at Award Training for the MAN3 course :-) </p>" + "<p>We hope you have as much FUN working through the 11 modules as we did creating them! </p>" + "<p>Below are your details: </p>" + "<p>Username: " + paToAddress + "</p>" + "<p>Password: " + paPassword + "</p>" + "<p>Website: <a href=\"man3.awardtraining.org\">man3.awardtraining.org</a></p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>All the best from the Award Training Team!</p>";
        string str5 = str4 + "<p>Welcome! You have successfully registered at Award Training for the MAN3 course :-) </p>" + "<p>We hope you have as much FUN working through the 11 modules as we did creating them! </p>" + "<p>Below are your details: </p>" + "<p>Username: " + paToAddress + "</p>" + "<p>Password: " + paPassword + "</p>" + "<p>Website: <a href=\"man3.awardtraining.org\">man3.awardtraining.org</a></p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>All the best from the Award Training Team!</p>";
        SmtpClient smtpClient = new SmtpClient(Settings.MailerHost, Settings.MailerPort);
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Credentials = (ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword);
        MailMessage message1 = new MailMessage();
        message1.From = new MailAddress(Settings.MailerFromAddress, "Award Training - Registration");
        message1.To.Add(paToAddress);
        message1.Subject = str1;
        message1.IsBodyHtml = true;
        message1.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>";
        MailMessage message2 = new MailMessage();
        message2.From = new MailAddress(Settings.MailerFromAddress, "Award Training - Registration - " + paIDNumber);
        message2.To.Add(adminEmail);
        message2.Subject = str2;
        message2.IsBodyHtml = true;
        message2.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + str5 + "<body>";
        smtpClient.Send(message1);
        smtpClient.Send(message2);
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendFirstInactivityEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      if (!Settings.InactivityMailerEnabled)
        return;
      try
      {
        string str = "Start A Business - Inactivity";
        string input = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>" + "<p>We notice your account has been inactive for over 30 days.</p>" + "<p>We encourage you to finish what you started and complete your business plan.</p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>We are committed to entrepreneurial success.</p>" + "<br>" + "<p>Kind Regards,</p>" + "<p>The Start a Business™ Team!</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Inactivity"),
          To = {
            paToAddress
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendSecondInactivityEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      if (!Settings.InactivityMailerEnabled)
        return;
      try
      {
        string str = "Start A Business - Inactivity";
        string input = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>" + "<p>We notice your account has been inactive for over 60 days.</p>" + "<p>It appears as if you will not be making use of the free training at Start a Business™</p>" + "<p>Your account will be disabled should it remain inactive after 90 days.</p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>We are committed to entrepreneurial success.</p>" + "<br>" + "<p>Kind Regards,</p>" + "<p>The Start a Business™ Team!</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Inactivity"),
          To = {
            paToAddress
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendFinalInactivityEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      if (!Settings.InactivityMailerEnabled)
        return;
      try
      {
        string str = "Start A Business - Inactivity";
        string input = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>" + "<p>Your account has been inactive for over 90 days and has been disabled.</p>" + "<p>Should you wish to have your account reactivated, please contact us at admin@award.co.za.</p>" + "<p>We are committed to entrepreneurial success.</p>" + "<br>" + "<p>Kind Regards,</p>" + "<p>The Start a Business™ Team!</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Inactivity"),
          To = {
            paToAddress
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendAppealEmail(
      string paIDNumber,
      string paEmail,
      string paMessage,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str = "APPEAL - Award Training";
        string input = "You have received an appeal from: " + paIDNumber + " - " + paEmail + "<br>" + paMessage;
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "APPEAL - Award Training"),
          To = {
            adminEmail
          },
          Subject = str,
          IsBodyHtml = true,
          Body = input
        });
        string paMessage1 = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage1);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendTechEmail(
      string paIDNumber,
      string paEmail,
      string paMessage,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str = "APPEAL - Award Training";
        string input = "You have received an appeal from: " + paIDNumber + " - " + paEmail + "<br>" + paMessage;
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "APPEAL - Award Training"),
          To = {
            adminEmail
          },
          Subject = str,
          IsBodyHtml = true,
          Body = input
        });
        string paMessage1 = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage1);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendFeedbackEmail(
      string paIDNumber,
      string paEmail,
      string paMessage,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string supportEmail = Settings.SupportEmail;
        string str = "FEEDBACK - Award Training";
        string input = "You have received feedback from: " + paIDNumber + " - " + paEmail + "<br>" + paMessage;
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "FEEDBACK - Award Training"),
          To = {
            supportEmail
          },
          Subject = str,
          IsBodyHtml = true,
          Body = input
        });
        string paMessage1 = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage1);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendResultEmail(
      string paEmail,
      string paCode,
      string paSubModule,
      string IdNumber,
      string paStudentName,
      bool paPass,
      int paStudentMark,
      int paTotalMark,
      int paAttempt,
      string paUserID,
      int paMark,
      int paTotal,
      string paPercentage)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str1 = "Award Training - Assessment Results For: " + paSubModule + " - " + paCode;
        string input = "<p>Hi " + paStudentName + " - I.D. No:" + IdNumber + "</p>";
        if (paPass)
        {
          if (paPass)
            input = input + "<p>Congratulations!!!</p>" + "<p>You have been found Competent in " + paSubModule + " - " + paCode + "</p>" + "<p>You achieved: " + (object) paMark + " / " + (object) paTotal + " questions correct. </p>";
        }
        else
        {
          string str2 = input + "<p>You have been found Not Yet Competent in: " + paSubModule + " - " + paCode + "</p>";
          string str3;
          if (paAttempt < 2)
            str3 = str2 + "<p>You achieved: " + (object) paStudentMark + "/" + (object) paTotalMark + " questions correct. Should you disagree with the result, you have the right to <a href=\"admin@award.co.za\">admin@award.co.za</a>.</p>" + "<p>Please revise the material supplied and then <u>take your 2nd and final assessment</u>. Should you not be found competent on your 2nd attempt, you may continue to the next unit standard.</p>";
          else
            str3 = str2 + "<p>You achieved: " + (object) paStudentMark + " / " + (object) paTotalMark + " questions correct on your second attempt. </p>" + "<p>Your total marks for both attempts are: " + (object) paMark + " / " + (object) paTotal + " which is " + paPercentage + ". You should be aiming for 80% for future assessments.</p>" + "<p>You have no attempts left. Please continue to the next unit standard.</p>";
          input = str3 + "<p>From: Award Training ™ </p>";
        }
        SmtpClient smtpClient = new SmtpClient(Settings.MailerHost, Settings.MailerPort);
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Credentials = (ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword);
        smtpClient.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Award Training - Assessment Results For: " + paSubModule + " - " + paCode),
          To = {
            paEmail
          },
          Subject = str1,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        if (paPass || paAttempt >= 2)
          smtpClient.Send(new MailMessage()
          {
            From = new MailAddress(Settings.MailerFromAddress, "Award Training - Assessment Results For: " + paSubModule + " - " + paCode),
            To = {
              adminEmail
            },
            Subject = str1 + " - ID Number: " + IdNumber,
            IsBodyHtml = true,
            Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
          });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendResetPasswordLink(string paEmail, string paUserID, string paMessage)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string str1 = "Award Training - Reset Password Link";
        string str2 = "<p>You have requested a password reset from our website. If you did make the request, click the link below, otherwise ignore this email.</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Award Training - Reset Password Link"),
          To = {
            paEmail
          },
          Subject = str1,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + str2 + paMessage + "<p> From: AWard Training </p><body>"
        });
        string paMessage1 = Regex.Replace(paMessage, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage1);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static bool SendContactEmail(
      string paFullname,
      string paEmail,
      string paMessage,
      string paTelephone)
    {
      bool flag = false;
      if (Settings.MailerEnabled)
      {
        try
        {
          string supportEmail = Settings.SupportEmail;
          string str1 = "CONTACT - Award Training";
          string str2 = "<p>You have received feedback from: " + paFullname + "</p>" + "<p>Date/Time: " + string.Format("{0:d/M/yyyy HH:mm:ss}", (object) DateTime.Now) + "</p>" + "<p>Telephone: " + paTelephone + "</p>" + "<p>Email: " + paEmail + "</p>" + "<p>Message: " + paMessage + "</p>";
          new SmtpClient(Settings.MailerHost, Settings.MailerPort)
          {
            UseDefaultCredentials = false,
            Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
          }.Send(new MailMessage()
          {
            From = new MailAddress(Settings.MailerFromAddress, "CONTACT - Award Training"),
            To = {
              supportEmail
            },
            Subject = str1,
            IsBodyHtml = true,
            Body = str2
          });
          flag = true;
        }
        catch (Exception ex)
        {
          COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          flag = false;
        }
      }
      return flag;
    }

    public static void SendSponsorRegistrationEmail(
      string paToAddress,
      string paFirstName,
      string paLastName,
      string paUserID,
      string paUsername,
      string paPassword,
      string paCompany)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str1 = "Start A Business - Sponsor Registration - " + paCompany;
        string str2 = "SPONSOR REGISTRATION - Start A Business - " + paCompany;
        string input = "<p>Hi " + paFirstName.ToUpper() + "</p>" + "<p>Welcome! You have successfully been registered at Start A Business™ :-) </p>" + "<p>Your username is: " + paUsername + "</p>" + "<p>Your password is: " + paPassword + "</p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>All the best from the Start a Business ™ Team!</p>";
        SmtpClient smtpClient = new SmtpClient(Settings.MailerHost, Settings.MailerPort);
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Credentials = (ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword);
        MailMessage message1 = new MailMessage();
        message1.From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Sponsor Registration - " + paCompany);
        message1.To.Add(paToAddress);
        message1.Subject = str1;
        message1.IsBodyHtml = true;
        message1.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>";
        MailMessage message2 = new MailMessage();
        message2.From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Sponsor Registration - " + paCompany);
        message2.To.Add(adminEmail);
        message2.Subject = str2;
        message2.IsBodyHtml = true;
        message2.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>";
        smtpClient.Send(message1);
        smtpClient.Send(message2);
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendInstructorRegistrationEmail(
      string paToAddress,
      string paFirstName,
      string paLastName,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str1 = "Start A Business - Registration";
        string str2 = "ASSESSOR/MENTOR REGISTRATION - Start A Business";
        string input = "<p>Hi " + paFirstName.ToUpper() + "</p>" + "<p>Welcome! You have successfully been registered at Start A Business™ :-) </p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>All the best from the Start a Business ™ Team!</p>";
        SmtpClient smtpClient = new SmtpClient(Settings.MailerHost, Settings.MailerPort);
        smtpClient.UseDefaultCredentials = false;
        smtpClient.Credentials = (ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword);
        MailMessage message1 = new MailMessage();
        message1.From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Registration");
        message1.To.Add(paToAddress);
        message1.Subject = str1;
        message1.IsBodyHtml = true;
        message1.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>";
        MailMessage message2 = new MailMessage();
        message2.From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Registration");
        message2.To.Add(adminEmail);
        message2.Subject = str2;
        message2.IsBodyHtml = true;
        message2.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>";
        smtpClient.Send(message1);
        smtpClient.Send(message2);
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendCertificateEmail(
      string paStudentID,
      string paStudentFirstName,
      string paStudentLastName,
      string paStudentIDNumber,
      string paStudentEmail)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string certificate = Mailer.GenerateCertificate(paStudentID, paStudentFirstName + " " + paStudentLastName, paStudentIDNumber);
        if (certificate == "")
        {
          COML.Logging.Log("Error generating certificate file path.", Enumerations.LogLevel.Error, (Exception) null);
        }
        else
        {
          string str = "Start A Business - Certification";
          string input = "<p>Hi " + paStudentFirstName.ToUpper() + "</p>" + "<p>Congratulations you have successfully completed Start A Business™ :-) </p>" + "<p>Please find your attached certification.</p>" + "<p>If you have any queries, please feel free to contact us at admin@award.co.za.</p>" + "<p>All the best for startign YOUR business:-)</p>";
          SmtpClient smtpClient = new SmtpClient(Settings.MailerHost, Settings.MailerPort);
          smtpClient.UseDefaultCredentials = false;
          smtpClient.Credentials = (ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword);
          Attachment attachment = new Attachment(certificate);
          MailMessage message = new MailMessage();
          message.Attachments.Add(attachment);
          message.From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Certification");
          message.To.Add(paStudentEmail);
          message.Subject = str;
          message.IsBodyHtml = true;
          message.Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>";
          smtpClient.Send(message);
          string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
          DAL.Logging.Logging.SaveEmailLog(paStudentID, paMessage);
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static string GenerateCertificate(
      string paUserID,
      string paStudentName,
      string StudentIDNumber)
    {
      try
      {
        HtmlString htmlString = new HtmlString("<div style=\"position:relative;width:100%;height:100%;\"><div style=\"font-size:17px;color:white;margin-top:265px;margin-left:200px;height:17px;width:400px;\">" + paStudentName + "</div><div style=\"font-size:17px;color:white;margin-top:38px;margin-left:80px;height:17px;width:100px;\">" + StudentIDNumber + "</div></div>");
        Image image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "/Images/Certificate.png");
        HtmlRender.RenderToImage(image, htmlString.ToHtmlString(), (PointF) new Point(50, 50), (CssData) null, (EventHandler<HtmlStylesheetLoadEventArgs>) null, (EventHandler<HtmlImageLoadEventArgs>) null);
        image.Save(AppDomain.CurrentDomain.BaseDirectory + "/Files/Users/Certificates/" + paUserID + ".png", ImageFormat.Png);
        return AppDomain.CurrentDomain.BaseDirectory + "/Files/Users/Certificates/" + paUserID + ".png";
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return "";
    }

    public static void SendSubmissionEmail(
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      try
      {
        string adminEmail = Settings.AdminEmail;
        string str = "Start A Business - Learner Document Uploaded";
        string input = "<p>Hi Admin,</p>" + "<p>" + paStudentName.ToUpper() + " (I.D. No: " + paIDNumber + ") has uploaded documentation to be assessed.</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Learner Document Uploaded"),
          To = {
            adminEmail
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }
  }
}
