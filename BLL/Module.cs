﻿// Decompiled with JetBrains decompiler
// Type: BLL.Module
// Assembly: BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 183A9E4D-A2A8-4479-AA93-BD6609C09A1B
// Assembly location: C:\Users\S4\Desktop\man\bin\BLL.dll

using COML.Classes;
using System.Collections.Generic;

namespace BLL
{
  public class Module
  {
    public static List<COML.Classes.Module> GetModules()
    {
      return DAL.Core.Training.Module.GetModules();
    }

    public static List<COML.Classes.Module> GetModulesForUser(string paUserID)
    {
      return DAL.Core.Training.Module.GetModulesForUser(paUserID);
    }

    public static bool GetUserSubModuleWait(string paUserID, int paSubModuleID)
    {
      return DAL.Core.Training.Module.GetUserSubModuleWait(paUserID, paSubModuleID);
    }

    public static bool GetUserSubModuleUploadsViewable(string paUserID, int paSubModuleID)
    {
      return DAL.Core.Training.Module.GetUserSubModuleUploadsViewable(paUserID, paSubModuleID);
    }

    public static List<COML.Classes.Module> GetModulesForUserByID(int paStudentID)
    {
      return DAL.Core.Training.Module.GetModulesForUserByID(paStudentID);
    }

    public static COML.Classes.Module GetSubModuleForUser(
      string paUserID,
      int paModuleID,
      int paSubModuleID)
    {
      return DAL.Core.Training.Module.GetSubModuleForUser(paUserID, paModuleID, paSubModuleID);
    }

    public static SubModuleMaterial GetMaterial(int paMaterialID)
    {
      return DAL.Core.Training.Module.GetMaterial(paMaterialID);
    }

    public static void SetSubModuleComplete(string paUserID, int paModuleID, int paSubModuleID)
    {
      DAL.Core.Training.Module.SetSubModuleComplete(paUserID, paModuleID, paSubModuleID);
    }

    public static List<COML.Classes.Module> GetModulesForUserByIDforSponsor(
      int paStudentID,
      int paSponsorID)
    {
      return DAL.Core.Training.Module.GetModulesForUserByIDforSponsor(paStudentID, paSponsorID);
    }

    public static bool IsStudentComplete(COML.Classes.Student paStudent)
    {
      return DAL.Core.Training.Module.IsStudentComplete(paStudent);
    }

    public static COML.Classes.Module GetStudentCurrentModule(string paStudentID)
    {
      return DAL.Core.Training.Module.GetStudentCurrentModule(paStudentID);
    }
  }
}
