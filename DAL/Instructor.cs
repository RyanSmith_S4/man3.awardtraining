﻿// Decompiled with JetBrains decompiler
// Type: DAL.Instructor
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;
using System.Collections.Generic;

namespace DAL
{
  public class Instructor
  {
    public Instructor()
    {
      this.AssignedAssessors = (ICollection<AssignedAssessor>) new HashSet<AssignedAssessor>();
      this.AssignedMentors = (ICollection<AssignedMentor>) new HashSet<AssignedMentor>();
      this.InstructorFiles = (ICollection<InstructorFile>) new HashSet<InstructorFile>();
    }

    public int ID { get; set; }

    public string UserID { get; set; }

    public string FirstNames { get; set; }

    public string LastNames { get; set; }

    public string Nickname { get; set; }

    public string IdentityNumber { get; set; }

    public int Age { get; set; }

    public string Gender { get; set; }

    public string Race { get; set; }

    public string HomeLanguage { get; set; }

    public string PostalAddress1 { get; set; }

    public string PostalAddress2 { get; set; }

    public string PostalAddress3 { get; set; }

    public string PostalCity { get; set; }

    public string PostalCode { get; set; }

    public string PostalProvince { get; set; }

    public string EmailAddress { get; set; }

    public string AssignedEmailAddress { get; set; }

    public string TelephoneCell { get; set; }

    public string HighestQualification { get; set; }

    public string HighestQualificationTitle { get; set; }

    public bool CurrentlyEmployed { get; set; }

    public bool IsAssessor { get; set; }

    public string AssessorCode { get; set; }

    public bool IsMentor { get; set; }

    public string MentorCode { get; set; }

    public bool IsApproved { get; set; }

    public bool IsLocked { get; set; }

    public bool IsEnabled { get; set; }

    public DateTime DateTime_Created { get; set; }

    public DateTime LastActivity { get; set; }

    public DateTime? DateTime_Approved { get; set; }

    public bool? IsChatEnabled { get; set; }

    public virtual AspNetUser AspNetUser { get; set; }

    public virtual ICollection<AssignedAssessor> AssignedAssessors { get; set; }

    public virtual ICollection<AssignedMentor> AssignedMentors { get; set; }

    public virtual ICollection<InstructorFile> InstructorFiles { get; set; }
  }
}
