﻿// Decompiled with JetBrains decompiler
// Type: DAL.UserSubModuleFile
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;

namespace DAL
{
  public class UserSubModuleFile
  {
    public int ID { get; set; }

    public int UserSubModuleID { get; set; }

    public int UserFileID { get; set; }

    public int Attempt { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public bool IsCorrect { get; set; }

    public bool IsAssessed { get; set; }

    public DateTime? DateTimeAssessed { get; set; }

    public string InstructorNote { get; set; }

    public bool IsEnabled { get; set; }

    public int? InstructorID { get; set; }

    public virtual UserFile UserFile { get; set; }

    public virtual UserSubModule UserSubModule { get; set; }
  }
}
