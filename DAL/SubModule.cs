﻿// Decompiled with JetBrains decompiler
// Type: DAL.SubModule
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Collections.Generic;

namespace DAL
{
  public class SubModule
  {
    public SubModule()
    {
      this.SubModuleModuleMaterials = (ICollection<SubModuleModuleMaterial>) new HashSet<SubModuleModuleMaterial>();
      this.SubModuleMultipleChoiceQuestions = (ICollection<SubModuleMultipleChoiceQuestion>) new HashSet<SubModuleMultipleChoiceQuestion>();
      this.SubModuleTrueFalseQuestions = (ICollection<SubModuleTrueFalseQuestion>) new HashSet<SubModuleTrueFalseQuestion>();
      this.UserSubModules = (ICollection<UserSubModule>) new HashSet<UserSubModule>();
    }

    public int ID { get; set; }

    public int ModuleID { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public int Order { get; set; }

    public bool IsEnabled { get; set; }

    public virtual Module Module { get; set; }

    public virtual ICollection<SubModuleModuleMaterial> SubModuleModuleMaterials { get; set; }

    public virtual ICollection<SubModuleMultipleChoiceQuestion> SubModuleMultipleChoiceQuestions { get; set; }

    public virtual ICollection<SubModuleTrueFalseQuestion> SubModuleTrueFalseQuestions { get; set; }

    public virtual ICollection<UserSubModule> UserSubModules { get; set; }
  }
}
