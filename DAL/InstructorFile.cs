﻿// Decompiled with JetBrains decompiler
// Type: DAL.InstructorFile
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

namespace DAL
{
  public class InstructorFile
  {
    public int ID { get; set; }

    public int UserFileID { get; set; }

    public int InstructorID { get; set; }

    public virtual UserFile UserFile { get; set; }

    public virtual Instructor Instructor { get; set; }
  }
}
