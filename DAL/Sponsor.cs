﻿// Decompiled with JetBrains decompiler
// Type: DAL.Sponsor
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;
using System.Collections.Generic;

namespace DAL
{
  public class Sponsor
  {
    public Sponsor()
    {
      this.SponsorFsaCodes = (ICollection<SponsorFsaCode>) new HashSet<SponsorFsaCode>();
    }

    public int ID { get; set; }

    public string UserID { get; set; }

    public string Company { get; set; }

    public string FirstNames { get; set; }

    public string LastNames { get; set; }

    public string EmailAddress { get; set; }

    public string UserEmailAddress { get; set; }

    public string TelephoneCell { get; set; }

    public bool IsLocked { get; set; }

    public bool IsEnabled { get; set; }

    public DateTime DateTime_Created { get; set; }

    public DateTime LastActivity { get; set; }

    public virtual AspNetUser AspNetUser { get; set; }

    public virtual ICollection<SponsorFsaCode> SponsorFsaCodes { get; set; }
  }
}
