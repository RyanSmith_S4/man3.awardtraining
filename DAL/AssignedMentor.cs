﻿// Decompiled with JetBrains decompiler
// Type: DAL.AssignedMentor
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;

namespace DAL
{
  public class AssignedMentor
  {
    public int ID { get; set; }

    public int StudentID { get; set; }

    public int InstructorID { get; set; }

    public bool IsEnabled { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public virtual Student Student { get; set; }

    public virtual Instructor Instructor { get; set; }
  }
}
