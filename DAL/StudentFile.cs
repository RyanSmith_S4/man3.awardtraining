﻿// Decompiled with JetBrains decompiler
// Type: DAL.StudentFile
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

namespace DAL
{
  public class StudentFile
  {
    public int ID { get; set; }

    public int UserFileID { get; set; }

    public int StudentID { get; set; }

    public virtual UserFile UserFile { get; set; }

    public virtual Student Student { get; set; }
  }
}
