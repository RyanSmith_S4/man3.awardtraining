﻿// Decompiled with JetBrains decompiler
// Type: DAL.SubModuleMultipleChoiceQuestion
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

namespace DAL
{
  public class SubModuleMultipleChoiceQuestion
  {
    public int SubModuleID { get; set; }

    public int MultipleChoiceID_Attempt1 { get; set; }

    public int MultipleChoiceID_Attempt2 { get; set; }

    public bool IsEnabled { get; set; }

    public virtual MultipleChoiceQuestion MultipleChoiceQuestion { get; set; }

    public virtual MultipleChoiceQuestion MultipleChoiceQuestion1 { get; set; }

    public virtual SubModule SubModule { get; set; }
  }
}
