﻿// Decompiled with JetBrains decompiler
// Type: DAL.Core.Assessment.Assessment
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Core.Assessment
{
  public class Assessment
  {
    public static COML.Classes.Assessment GetModuleAssessment(
      int paModuleID,
      int paSubModuleID,
      string paUserID)
    {
      COML.Classes.Assessment assessment1 = new COML.Classes.Assessment();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.Assessment assessment2 = trainingEntities.Modules.Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, (Expression<Func<DAL.Module, int>>) (m => m.ID), (Expression<Func<DAL.SubModule, int>>) (sm => sm.ModuleID), (m, sm) => new
          {
            m = m,
            sm = sm
          }).Where(data => data.m.IsEnabled && data.m.ID == paModuleID && data.sm.IsEnabled && data.sm.ID == paSubModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType26<DAL.Module, DAL.SubModule>, COML.Classes.Assessment>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.set_ID)), )))); //unable to render the statement
          ParameterExpression parameterExpression2;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.UserSubModule loCurrentAttempt = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
          {
            um = um,
            usm = usm
          }).Where(data => data.um.IsEnabled && data.um.ModuleID == paModuleID && data.usm.IsEnabled && data.usm.SubModuleID == paSubModuleID && data.um.UserID == paUserID && !data.um.IsComplete && data.um.IsStarted && !data.usm.IsComplete && data.usm.IsStarted).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, COML.Classes.UserSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserSubModule.set_ID)), )))); //unable to render the statement
          if (loCurrentAttempt == null || loCurrentAttempt.Attempt > 2)
          {
            assessment1.Attempts = loCurrentAttempt.Attempt;
            return assessment1;
          }
          assessment2.MultipleChoice = new List<COML.Classes.Assessment.MultipleChoiceQuestion>();
          if (loCurrentAttempt.Attempt == 1)
          {
            List<int> loCurrentMcAnswers = trainingEntities.UserSubModuleMultipleChoiceAnswers.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (usmmc, smmc) => new
            {
              usmmc = usmmc,
              smmc = smmc
            }).Where(data => data.usmmc.IsEnabled && data.smmc.IsEnabled && data.smmc.SubModuleID == loCurrentAttempt.SubModuleID && data.usmmc.UserSubModuleID == loCurrentAttempt.ID && data.usmmc.Attempt == loCurrentAttempt.Attempt).Select(data => data.smmc.MultipleChoiceID_Attempt1).ToList<int>();
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.Assessment.MultipleChoiceQuestion> list1 = trainingEntities.MultipleChoiceQuestions.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (mc, smmc) => new
            {
              mc = mc,
              smmc = smmc
            }).Where(data => data.mc.IsEnabled && data.smmc.IsEnabled && data.smmc.SubModuleID == loCurrentAttempt.SubModuleID && !loCurrentMcAnswers.Contains(data.mc.ID)).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType28<DAL.MultipleChoiceQuestion, SubModuleMultipleChoiceQuestion>, COML.Classes.Assessment.MultipleChoiceQuestion>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment.MultipleChoiceQuestion)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.MultipleChoiceQuestion.set_ID)), )))); //unable to render the statement
            foreach (COML.Classes.Assessment.MultipleChoiceQuestion multipleChoiceQuestion in list1)
            {
              COML.Classes.Assessment.MultipleChoiceQuestion lpQuestion = multipleChoiceQuestion;
              lpQuestion.Answers = new List<COML.Classes.Assessment.MultipleChoiceAnswer>();
              ParameterExpression parameterExpression4;
              // ISSUE: method reference
              // ISSUE: method reference
              List<COML.Classes.Assessment.MultipleChoiceAnswer> list2 = trainingEntities.MultipleChoiceSubQuestions.Where<MultipleChoiceSubQuestion>((Expression<Func<MultipleChoiceSubQuestion, bool>>) (mca => mca.IsEnabled && mca.MultipleChoiceQuestionID == lpQuestion.ID)).Select<MultipleChoiceSubQuestion, COML.Classes.Assessment.MultipleChoiceAnswer>(Expression.Lambda<Func<MultipleChoiceSubQuestion, COML.Classes.Assessment.MultipleChoiceAnswer>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment.MultipleChoiceAnswer)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.MultipleChoiceAnswer.set_ID)), )))); //unable to render the statement
              lpQuestion.Answers = list2;
            }
            assessment2.MultipleChoice = list1;
          }
          else
          {
            List<int> list1 = trainingEntities.UserSubModuleMultipleChoiceAnswers.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (usmmc, smmc) => new
            {
              usmmc = usmmc,
              smmc = smmc
            }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmmc.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              usm = usm
            }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
            {
              \u003C\u003Eh__TransparentIdentifier1 = data,
              um = um
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.um.ModuleID == paModuleID && data.um.UserID == paUserID && !data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsCorrect).Select(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.MultipleChoiceID_Attempt2).ToList<int>();
            List<int> loCurrentMcAnswers = trainingEntities.UserSubModuleMultipleChoiceAnswers.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt2), (usmmc, smmc) => new
            {
              usmmc = usmmc,
              smmc = smmc
            }).Where(data => data.usmmc.IsEnabled && data.smmc.IsEnabled && data.smmc.SubModuleID == loCurrentAttempt.SubModuleID && data.usmmc.UserSubModuleID == loCurrentAttempt.ID && data.usmmc.Attempt == loCurrentAttempt.Attempt).Select(data => data.smmc.MultipleChoiceID_Attempt2).ToList<int>();
            List<int> loQuestions = list1.Where<int>((Func<int, bool>) (p => !loCurrentMcAnswers.Any<int>((Func<int, bool>) (p2 => p2 == p)))).ToList<int>();
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.Assessment.MultipleChoiceQuestion> list2 = trainingEntities.MultipleChoiceQuestions.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt2), (mc, smmc) => new
            {
              mc = mc,
              smmc = smmc
            }).Where(data => data.mc.IsEnabled && data.smmc.SubModuleID == loCurrentAttempt.SubModuleID && data.smmc.IsEnabled && loQuestions.Contains(data.mc.ID)).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType28<DAL.MultipleChoiceQuestion, SubModuleMultipleChoiceQuestion>, COML.Classes.Assessment.MultipleChoiceQuestion>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment.MultipleChoiceQuestion)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.MultipleChoiceQuestion.set_ID)), )))); //unable to render the statement
            foreach (COML.Classes.Assessment.MultipleChoiceQuestion multipleChoiceQuestion in list2)
            {
              COML.Classes.Assessment.MultipleChoiceQuestion lpQuestion = multipleChoiceQuestion;
              lpQuestion.Answers = new List<COML.Classes.Assessment.MultipleChoiceAnswer>();
              ParameterExpression parameterExpression4;
              // ISSUE: method reference
              // ISSUE: method reference
              List<COML.Classes.Assessment.MultipleChoiceAnswer> list3 = trainingEntities.MultipleChoiceSubQuestions.Where<MultipleChoiceSubQuestion>((Expression<Func<MultipleChoiceSubQuestion, bool>>) (mca => mca.IsEnabled && mca.MultipleChoiceQuestionID == lpQuestion.ID)).Select<MultipleChoiceSubQuestion, COML.Classes.Assessment.MultipleChoiceAnswer>(Expression.Lambda<Func<MultipleChoiceSubQuestion, COML.Classes.Assessment.MultipleChoiceAnswer>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment.MultipleChoiceAnswer)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.MultipleChoiceAnswer.set_ID)), )))); //unable to render the statement
              lpQuestion.Answers = list3;
            }
            assessment2.MultipleChoice = list2;
          }
          assessment2.TrueOrFalse = new List<COML.Classes.Assessment.TrueFalseQuestion>();
          if (loCurrentAttempt.Attempt == 1)
          {
            List<int> loCurrentTfAnswers = trainingEntities.UserSubModuleTrueFalseAnswers.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt1), (usmtf, smtf) => new
            {
              usmtf = usmtf,
              smtf = smtf
            }).Where(data => data.usmtf.IsEnabled && data.smtf.IsEnabled && data.smtf.SubModuleID == loCurrentAttempt.SubModuleID && data.usmtf.UserSubModuleID == loCurrentAttempt.ID && data.usmtf.Attempt == loCurrentAttempt.Attempt).Select(data => data.smtf.SubTrueFalseID_Attempt1).ToList<int>();
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.Assessment.TrueFalseQuestion> list = trainingEntities.TrueFalseQuestions.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<DAL.TrueFalseQuestion, int>>) (tf => tf.ID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt1), (tf, smtf) => new
            {
              tf = tf,
              smtf = smtf
            }).Where(data => data.tf.IsEnabled && data.smtf.IsEnabled && data.smtf.SubModuleID == loCurrentAttempt.SubModuleID && !loCurrentTfAnswers.Contains(data.tf.ID)).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType31<DAL.TrueFalseQuestion, SubModuleTrueFalseQuestion>, COML.Classes.Assessment.TrueFalseQuestion>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment.TrueFalseQuestion)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.TrueFalseQuestion.set_ID)), )))); //unable to render the statement
            assessment2.TrueOrFalse = list;
          }
          else
          {
            List<int> list1 = trainingEntities.UserSubModuleTrueFalseAnswers.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt1), (usmtf, smtf) => new
            {
              usmtf = usmtf,
              smtf = smtf
            }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmtf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              usm = usm
            }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
            {
              \u003C\u003Eh__TransparentIdentifier1 = data,
              um = um
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.um.ModuleID == paModuleID && data.um.UserID == paUserID && !data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsCorrect).Select(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubTrueFalseID_Attempt2).ToList<int>();
            List<int> loCurrentTfAnswers = trainingEntities.UserSubModuleTrueFalseAnswers.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt2), (usmtf, smtf) => new
            {
              usmtf = usmtf,
              smtf = smtf
            }).Where(data => data.usmtf.IsEnabled && data.smtf.IsEnabled && data.smtf.SubModuleID == loCurrentAttempt.SubModuleID && data.usmtf.UserSubModuleID == loCurrentAttempt.ID && data.usmtf.Attempt == loCurrentAttempt.Attempt).Select(data => data.smtf.SubTrueFalseID_Attempt2).ToList<int>();
            List<int> loQuestions = list1.Where<int>((Func<int, bool>) (p => !loCurrentTfAnswers.Any<int>((Func<int, bool>) (p2 => p2 == p)))).ToList<int>();
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.Assessment.TrueFalseQuestion> list2 = trainingEntities.TrueFalseQuestions.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<DAL.TrueFalseQuestion, int>>) (tf => tf.ID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt2), (tf, smtf) => new
            {
              tf = tf,
              smtf = smtf
            }).Where(data => data.tf.IsEnabled && data.smtf.IsEnabled && data.smtf.SubModuleID == loCurrentAttempt.SubModuleID && loQuestions.Contains(data.tf.ID)).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType31<DAL.TrueFalseQuestion, SubModuleTrueFalseQuestion>, COML.Classes.Assessment.TrueFalseQuestion>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Assessment.TrueFalseQuestion)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Assessment.TrueFalseQuestion.set_ID)), )))); //unable to render the statement
            assessment2.TrueOrFalse = list2;
          }
          if (assessment2.TrueOrFalse.Count<COML.Classes.Assessment.TrueFalseQuestion>() == 0 && assessment2.MultipleChoice.Count<COML.Classes.Assessment.MultipleChoiceQuestion>() == 0)
          {
            if (loCurrentAttempt.Attempt == 1)
              assessment1.IsAssessed = loCurrentAttempt.IsSuccessful || loCurrentAttempt.IsComplete;
            else if (loCurrentAttempt.Attempt == 2)
              assessment1.IsAssessed = loCurrentAttempt.IsSuccessful || loCurrentAttempt.IsComplete;
          }
          else
            assessment1.IsAssessed = false;
          assessment1.IsAssessed = loCurrentAttempt.IsAssessed;
          assessment1 = assessment2;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetModuleAssessment: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return assessment1;
    }

    public static bool AddUserMultipleChoiceResult(
      int paModuleID,
      int paSubModuleID,
      int paMultipleChoiceID,
      int paMultipleChoiceAnswer,
      string paUserID,
      bool paFinish)
    {
      bool flag1 = false;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            COML.Classes.UserSubModule loCurrentAttempt = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
            {
              um = um,
              usm = usm
            }).Where(data => data.um.IsEnabled && data.usm.IsEnabled && data.um.ModuleID == paModuleID && data.usm.SubModuleID == paSubModuleID && data.um.UserID == paUserID && !data.um.IsComplete && data.um.IsStarted && !data.usm.IsComplete && data.usm.IsStarted).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, COML.Classes.UserSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserSubModule.set_ID)), )))); //unable to render the statement
            if (trainingEntities.UserSubModuleMultipleChoiceAnswers.Where<UserSubModuleMultipleChoiceAnswer>((Expression<Func<UserSubModuleMultipleChoiceAnswer, bool>>) (umc => umc.MultipleChoiceQuestionID == paMultipleChoiceID && umc.IsEnabled && umc.Attempt == loCurrentAttempt.Attempt && umc.UserSubModuleID == loCurrentAttempt.ID)).FirstOrDefault<UserSubModuleMultipleChoiceAnswer>() == null)
            {
              DAL.MultipleChoiceQuestion multipleChoiceQuestion = trainingEntities.MultipleChoiceQuestions.Where<DAL.MultipleChoiceQuestion>((Expression<Func<DAL.MultipleChoiceQuestion, bool>>) (mc => mc.IsEnabled && mc.ID == paMultipleChoiceID)).FirstOrDefault<DAL.MultipleChoiceQuestion>();
              DAL.UserSubModule userSubModule = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
              {
                usm = usm,
                um = um
              }).Where(data => data.usm.IsEnabled && data.um.IsEnabled && !data.um.IsComplete && data.um.IsStarted && !data.usm.IsComplete && data.usm.IsStarted && data.um.UserID == paUserID && data.um.ModuleID == paModuleID && data.usm.SubModuleID == paSubModuleID).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
              if (multipleChoiceQuestion == null)
              {
                if (userSubModule == null)
                  goto label_15;
              }
              bool flag2 = false;
              if (paMultipleChoiceAnswer == multipleChoiceQuestion.Answer)
                flag2 = true;
              UserSubModuleMultipleChoiceAnswer entity = new UserSubModuleMultipleChoiceAnswer()
              {
                DateTimeCreated = DateTime.Now,
                IsEnabled = true,
                MultipleChoiceQuestionID = paMultipleChoiceID,
                UserAnswerID = paMultipleChoiceAnswer,
                UserSubModuleID = userSubModule.ID,
                IsCorrect = flag2,
                Attempt = loCurrentAttempt.Attempt
              };
              student.LastActivity = DateTime.Now;
              trainingEntities.UserSubModuleMultipleChoiceAnswers.Add(entity);
              trainingEntities.SaveChanges();
              flag1 = true;
            }
            else
              flag1 = false;
          }
          else
            flag1 = false;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag1 = false;
      }
label_15:
      Logging.Log("AddUserMultipleChoiceResult: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag1;
    }

    public static bool AddUserTrueFalseResult(
      int paModuleID,
      int paSubModuleID,
      int paTrueFalseID,
      bool paTrueFalseAnswer,
      string paUserID,
      bool paFinish)
    {
      bool flag1 = false;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            COML.Classes.UserSubModule loCurrentAttempt = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
            {
              um = um,
              usm = usm
            }).Where(data => data.um.IsEnabled && data.um.ModuleID == paModuleID && data.usm.IsEnabled && data.usm.SubModuleID == paSubModuleID && data.um.UserID == paUserID && !data.um.IsComplete && data.um.IsStarted && !data.usm.IsComplete && data.usm.IsStarted).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, COML.Classes.UserSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserSubModule.set_ID)), )))); //unable to render the statement
            if (trainingEntities.UserSubModuleTrueFalseAnswers.Where<UserSubModuleTrueFalseAnswer>((Expression<Func<UserSubModuleTrueFalseAnswer, bool>>) (utf => utf.TrueFalseID == paTrueFalseID && utf.IsEnabled && utf.Attempt == loCurrentAttempt.Attempt && utf.UserSubModuleID == loCurrentAttempt.ID)).FirstOrDefault<UserSubModuleTrueFalseAnswer>() == null)
            {
              DAL.TrueFalseQuestion trueFalseQuestion = trainingEntities.TrueFalseQuestions.Where<DAL.TrueFalseQuestion>((Expression<Func<DAL.TrueFalseQuestion, bool>>) (tf => tf.IsEnabled && tf.ID == paTrueFalseID)).FirstOrDefault<DAL.TrueFalseQuestion>();
              DAL.UserSubModule userSubModule = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
              {
                usm = usm,
                um = um
              }).Where(data => data.usm.IsEnabled && data.um.IsEnabled && !data.um.IsComplete && data.um.IsStarted && !data.usm.IsComplete && data.usm.IsStarted && data.um.UserID == paUserID && data.um.ModuleID == paModuleID && data.usm.SubModuleID == paSubModuleID).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
              if (trueFalseQuestion == null)
              {
                if (userSubModule == null)
                  goto label_15;
              }
              bool flag2 = false;
              if (paTrueFalseAnswer == trueFalseQuestion.Answer)
                flag2 = true;
              UserSubModuleTrueFalseAnswer entity = new UserSubModuleTrueFalseAnswer()
              {
                DateTimeCreated = DateTime.Now,
                IsEnabled = true,
                TrueFalseID = paTrueFalseID,
                UserAnswer = paTrueFalseAnswer,
                UserSubModuleID = userSubModule.ID,
                IsCorrect = flag2,
                Attempt = loCurrentAttempt.Attempt
              };
              student.LastActivity = DateTime.Now;
              trainingEntities.UserSubModuleTrueFalseAnswers.Add(entity);
              trainingEntities.SaveChanges();
              flag1 = true;
            }
            else
              flag1 = false;
          }
          else
            flag1 = false;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag1 = false;
      }
label_15:
      Logging.Log("AddUserTrueFalseResult: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag1;
    }

    public static AssessmentResult MarkUserAssessment(
      int paModuleID,
      int paSubModuleID,
      string paUserID)
    {
      AssessmentResult assessmentResult1 = new AssessmentResult();
      int num1 = 0;
      int num2 = 0;
      bool flag1 = false;
      assessmentResult1.MultipleChoice = new List<IncorrectMultipleChoiceQuestion>();
      assessmentResult1.TrueFalse = new List<TrueFalseAnswer>();
      assessmentResult1.Passed = false;
      try
      {
        bool flag2 = false;
        bool flag3 = false;
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          DAL.UserSubModule loUserAttempt = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (ums => ums.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (ums, um) => new
          {
            ums = ums,
            um = um
          }).Where(data => data.ums.IsEnabled && data.ums.IsStarted && !data.ums.IsComplete && data.um.IsEnabled && !data.um.IsComplete && data.um.ModuleID == paModuleID && data.ums.SubModuleID == paSubModuleID && data.um.UserID == paUserID).Select(data => data.ums).FirstOrDefault<DAL.UserSubModule>();
          DAL.SubModule loSubModule = trainingEntities.SubModules.Where<DAL.SubModule>((Expression<Func<DAL.SubModule, bool>>) (sm => sm.IsEnabled && sm.ID == paSubModuleID && sm.ModuleID == paModuleID)).FirstOrDefault<DAL.SubModule>();
          UserModule userModule = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.IsEnabled && um.IsStarted && um.ModuleID == loSubModule.ModuleID && um.UserID == paUserID)).FirstOrDefault<UserModule>();
          assessmentResult1.Name = loSubModule.Name;
          assessmentResult1.Code = loSubModule.Code;
          if (loUserAttempt != null)
          {
            assessmentResult1.Attempt = loUserAttempt.Attempts;
            if (assessmentResult1.Attempt == 1)
            {
              List<SubModuleMultipleChoiceQuestion> list1 = trainingEntities.SubModuleMultipleChoiceQuestions.Join((IEnumerable<DAL.MultipleChoiceQuestion>) trainingEntities.MultipleChoiceQuestions, (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (smmc, mc) => new
              {
                smmc = smmc,
                mc = mc
              }).Where(data => data.smmc.IsEnabled && data.smmc.SubModuleID == paSubModuleID).Select(data => data.smmc).ToList<SubModuleMultipleChoiceQuestion>();
              List<MultipleChoiceSubQuestion> list2 = trainingEntities.MultipleChoiceSubQuestions.Join((IEnumerable<DAL.MultipleChoiceQuestion>) trainingEntities.MultipleChoiceQuestions, (Expression<Func<MultipleChoiceSubQuestion, int>>) (msm => msm.MultipleChoiceQuestionID), (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (msm, mc) => new
              {
                msm = msm,
                mc = mc
              }).Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, data => data.mc.ID, (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (data, smmc) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                smmc = smmc
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.msm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.mc.IsEnabled && data.smmc.IsEnabled && data.smmc.SubModuleID == paSubModuleID).Select(data => data.\u003C\u003Eh__TransparentIdentifier0.msm).ToList<MultipleChoiceSubQuestion>();
              ParameterExpression parameterExpression;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<IncorrectMultipleChoiceQuestion> list3 = trainingEntities.SubModuleMultipleChoiceQuestions.Join((IEnumerable<UserSubModuleMultipleChoiceAnswer>) trainingEntities.UserSubModuleMultipleChoiceAnswers, (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (smmc, usmmc) => new
              {
                smmc = smmc,
                usmmc = usmmc
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmmc.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Join((IEnumerable<DAL.MultipleChoiceQuestion>) trainingEntities.MultipleChoiceQuestions, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.MultipleChoiceQuestionID, (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (data, mc) => new
              {
                \u003C\u003Eh__TransparentIdentifier2 = data,
                mc = mc
              }).Join((IEnumerable<MultipleChoiceSubQuestion>) trainingEntities.MultipleChoiceSubQuestions, data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.UserAnswerID, (Expression<Func<MultipleChoiceSubQuestion, int>>) (smc => smc.ID), (data, smc) => new
              {
                \u003C\u003Eh__TransparentIdentifier3 = data,
                smc = smc
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.mc.IsEnabled && data.smc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.Attempts == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.Attempt == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.um.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.um.ModuleID == paModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType39<\u003C\u003Ef__AnonymousType38<\u003C\u003Ef__AnonymousType10<\u003C\u003Ef__AnonymousType29<\u003C\u003Ef__AnonymousType37<SubModuleMultipleChoiceQuestion, UserSubModuleMultipleChoiceAnswer>, DAL.UserSubModule>, UserModule>, DAL.MultipleChoiceQuestion>, MultipleChoiceSubQuestion>, IncorrectMultipleChoiceQuestion>>((Expression) Expression.MemberInit(Expression.New(typeof (IncorrectMultipleChoiceQuestion)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (IncorrectMultipleChoiceQuestion.set_MultipleChoiceID)), )))); //unable to render the statement
              if (list3.Count == 0)
                flag2 = true;
              else if (list3.Count != list1.Count<SubModuleMultipleChoiceQuestion>())
              {
                flag2 = true;
              }
              else
              {
                foreach (IncorrectMultipleChoiceQuestion multipleChoiceQuestion in list3)
                {
                  if (multipleChoiceQuestion.IsCorrect && multipleChoiceQuestion.IsEnabled)
                  {
                    ++num2;
                  }
                  else
                  {
                    multipleChoiceQuestion.OtherAnswers = new List<string>();
                    foreach (MultipleChoiceSubQuestion choiceSubQuestion in list2)
                    {
                      if (choiceSubQuestion.MultipleChoiceQuestionID == multipleChoiceQuestion.MultipleChoiceID && choiceSubQuestion.ID != multipleChoiceQuestion.MultipleChoiceSubID)
                        multipleChoiceQuestion.OtherAnswers.Add(choiceSubQuestion.SubQuestion);
                    }
                    assessmentResult1.MultipleChoice.Add(multipleChoiceQuestion);
                  }
                  ++num1;
                }
              }
            }
            else
            {
              List<int> list1 = trainingEntities.UserSubModuleMultipleChoiceAnswers.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt1), (usmmc, smmc) => new
              {
                usmmc = usmmc,
                smmc = smmc
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmmc.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.Attempt == 1 && data.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.um.ModuleID == paModuleID && data.um.UserID == paUserID && !data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsCorrect).Select(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.MultipleChoiceID_Attempt1).ToList<int>();
              List<int> list2 = trainingEntities.UserSubModuleMultipleChoiceAnswers.Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt2), (usmmc, smmc) => new
              {
                usmmc = usmmc,
                smmc = smmc
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmmc.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.Attempt == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.um.ModuleID == paModuleID && data.um.UserID == paUserID && !data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsCorrect).Select(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.MultipleChoiceID_Attempt2).ToList<int>();
              List<MultipleChoiceSubQuestion> list3 = trainingEntities.MultipleChoiceSubQuestions.Join((IEnumerable<DAL.MultipleChoiceQuestion>) trainingEntities.MultipleChoiceQuestions, (Expression<Func<MultipleChoiceSubQuestion, int>>) (msm => msm.MultipleChoiceQuestionID), (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (msm, mc) => new
              {
                msm = msm,
                mc = mc
              }).Join((IEnumerable<SubModuleMultipleChoiceQuestion>) trainingEntities.SubModuleMultipleChoiceQuestions, data => data.mc.ID, (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt2), (data, smmc) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                smmc = smmc
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.msm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.mc.IsEnabled && data.smmc.IsEnabled && data.smmc.SubModuleID == paSubModuleID).Select(data => data.\u003C\u003Eh__TransparentIdentifier0.msm).ToList<MultipleChoiceSubQuestion>();
              ParameterExpression parameterExpression;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<IncorrectMultipleChoiceQuestion> list4 = trainingEntities.SubModuleMultipleChoiceQuestions.Join((IEnumerable<UserSubModuleMultipleChoiceAnswer>) trainingEntities.UserSubModuleMultipleChoiceAnswers, (Expression<Func<SubModuleMultipleChoiceQuestion, int>>) (smmc => smmc.MultipleChoiceID_Attempt2), (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (usmmc => usmmc.MultipleChoiceQuestionID), (smmc, usmmc) => new
              {
                smmc = smmc,
                usmmc = usmmc
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmmc.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Join((IEnumerable<DAL.MultipleChoiceQuestion>) trainingEntities.MultipleChoiceQuestions, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.MultipleChoiceQuestionID, (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mc => mc.ID), (data, mc) => new
              {
                \u003C\u003Eh__TransparentIdentifier2 = data,
                mc = mc
              }).Join((IEnumerable<MultipleChoiceSubQuestion>) trainingEntities.MultipleChoiceSubQuestions, data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.UserAnswerID, (Expression<Func<MultipleChoiceSubQuestion, int>>) (smc => smc.ID), (data, smc) => new
              {
                \u003C\u003Eh__TransparentIdentifier3 = data,
                smc = smc
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.mc.IsEnabled && data.smc.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smmc.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.Attempts == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmmc.Attempt == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.um.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier3.\u003C\u003Eh__TransparentIdentifier2.um.ModuleID == paModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType39<\u003C\u003Ef__AnonymousType38<\u003C\u003Ef__AnonymousType10<\u003C\u003Ef__AnonymousType29<\u003C\u003Ef__AnonymousType37<SubModuleMultipleChoiceQuestion, UserSubModuleMultipleChoiceAnswer>, DAL.UserSubModule>, UserModule>, DAL.MultipleChoiceQuestion>, MultipleChoiceSubQuestion>, IncorrectMultipleChoiceQuestion>>((Expression) Expression.MemberInit(Expression.New(typeof (IncorrectMultipleChoiceQuestion)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (IncorrectMultipleChoiceQuestion.set_MultipleChoiceID)), )))); //unable to render the statement
              if (list2.Count<int>() == 0)
                flag2 = false;
              else if (list4.Count != list1.Count<int>())
              {
                flag2 = true;
              }
              else
              {
                foreach (IncorrectMultipleChoiceQuestion multipleChoiceQuestion in list4)
                {
                  if (multipleChoiceQuestion.IsCorrect && multipleChoiceQuestion.IsEnabled)
                  {
                    ++num2;
                  }
                  else
                  {
                    multipleChoiceQuestion.OtherAnswers = new List<string>();
                    foreach (MultipleChoiceSubQuestion choiceSubQuestion in list3)
                    {
                      if (choiceSubQuestion.MultipleChoiceQuestionID == multipleChoiceQuestion.MultipleChoiceID && choiceSubQuestion.ID != multipleChoiceQuestion.MultipleChoiceSubID)
                        multipleChoiceQuestion.OtherAnswers.Add(choiceSubQuestion.SubQuestion);
                    }
                    assessmentResult1.MultipleChoice.Add(multipleChoiceQuestion);
                  }
                  ++num1;
                }
              }
            }
            if (loUserAttempt.Attempts == 1)
            {
              int num3 = trainingEntities.SubModuleTrueFalseQuestions.Join((IEnumerable<DAL.TrueFalseQuestion>) trainingEntities.TrueFalseQuestions, (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt1), (Expression<Func<DAL.TrueFalseQuestion, int>>) (tf => tf.ID), (smtf, tf) => new
              {
                smtf = smtf,
                tf = tf
              }).Where(data => data.smtf.IsEnabled && data.smtf.SubModuleID == paSubModuleID).Select(data => data.smtf).Count<SubModuleTrueFalseQuestion>();
              ParameterExpression parameterExpression;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<TrueFalseAnswer> list = trainingEntities.SubModuleTrueFalseQuestions.Join((IEnumerable<UserSubModuleTrueFalseAnswer>) trainingEntities.UserSubModuleTrueFalseAnswers, (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt1), (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (smtf, usmtf) => new
              {
                smtf = smtf,
                usmtf = usmtf
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmtf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Join((IEnumerable<DAL.TrueFalseQuestion>) trainingEntities.TrueFalseQuestions, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.TrueFalseID, (Expression<Func<DAL.TrueFalseQuestion, int>>) (tf => tf.ID), (data, tf) => new
              {
                \u003C\u003Eh__TransparentIdentifier2 = data,
                tf = tf
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.tf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.Attempts == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.Attempt == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier2.um.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier2.um.ModuleID == paModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType42<\u003C\u003Ef__AnonymousType10<\u003C\u003Ef__AnonymousType29<\u003C\u003Ef__AnonymousType41<SubModuleTrueFalseQuestion, UserSubModuleTrueFalseAnswer>, DAL.UserSubModule>, UserModule>, DAL.TrueFalseQuestion>, TrueFalseAnswer>>((Expression) Expression.MemberInit(Expression.New(typeof (TrueFalseAnswer)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (TrueFalseAnswer.set_IsCorrect)), )))); //unable to render the statement
              if (list.Count != 0)
              {
                if (num3 != list.Count)
                {
                  flag3 = true;
                }
                else
                {
                  foreach (TrueFalseAnswer trueFalseAnswer in list)
                  {
                    if (trueFalseAnswer.IsCorrect && trueFalseAnswer.IsEnabled)
                      ++num2;
                    else
                      assessmentResult1.TrueFalse.Add(trueFalseAnswer);
                    ++num1;
                  }
                }
              }
              else
                flag3 = num3 != 0 || list.Count != 0;
            }
            else
            {
              List<int> list1 = trainingEntities.UserSubModuleTrueFalseAnswers.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt1), (usmtf, smtf) => new
              {
                usmtf = usmtf,
                smtf = smtf
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmtf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.Attempt == 1 && data.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.um.ModuleID == paModuleID && data.um.UserID == paUserID && !data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsCorrect).Select(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubTrueFalseID_Attempt1).ToList<int>();
              List<int> list2 = trainingEntities.UserSubModuleTrueFalseAnswers.Join((IEnumerable<SubModuleTrueFalseQuestion>) trainingEntities.SubModuleTrueFalseQuestions, (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt2), (usmtf, smtf) => new
              {
                usmtf = usmtf,
                smtf = smtf
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmtf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.Attempt == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.um.ModuleID == paModuleID && data.um.UserID == paUserID && !data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsCorrect).Select(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubTrueFalseID_Attempt2).ToList<int>();
              ParameterExpression parameterExpression;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<TrueFalseAnswer> list3 = trainingEntities.SubModuleTrueFalseQuestions.Join((IEnumerable<UserSubModuleTrueFalseAnswer>) trainingEntities.UserSubModuleTrueFalseAnswers, (Expression<Func<SubModuleTrueFalseQuestion, int>>) (smtf => smtf.SubTrueFalseID_Attempt2), (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (usmtf => usmtf.TrueFalseID), (smtf, usmtf) => new
              {
                smtf = smtf,
                usmtf = usmtf
              }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmtf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                usm = usm
              }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
              {
                \u003C\u003Eh__TransparentIdentifier1 = data,
                um = um
              }).Join((IEnumerable<DAL.TrueFalseQuestion>) trainingEntities.TrueFalseQuestions, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.TrueFalseID, (Expression<Func<DAL.TrueFalseQuestion, int>>) (tf => tf.ID), (data, tf) => new
              {
                \u003C\u003Eh__TransparentIdentifier2 = data,
                tf = tf
              }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.IsEnabled && data.tf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.smtf.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.Attempts == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.usmtf.Attempt == loUserAttempt.Attempts && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.usm.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier2.um.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier2.um.ModuleID == paModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType42<\u003C\u003Ef__AnonymousType10<\u003C\u003Ef__AnonymousType29<\u003C\u003Ef__AnonymousType41<SubModuleTrueFalseQuestion, UserSubModuleTrueFalseAnswer>, DAL.UserSubModule>, UserModule>, DAL.TrueFalseQuestion>, TrueFalseAnswer>>((Expression) Expression.MemberInit(Expression.New(typeof (TrueFalseAnswer)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (TrueFalseAnswer.set_IsCorrect)), )))); //unable to render the statement
              if (list3.Count != 0)
              {
                if (list1.Count != list3.Count)
                {
                  flag3 = true;
                }
                else
                {
                  foreach (TrueFalseAnswer trueFalseAnswer in list3)
                  {
                    if (trueFalseAnswer.IsCorrect && trueFalseAnswer.IsEnabled)
                      ++num2;
                    else
                      assessmentResult1.TrueFalse.Add(trueFalseAnswer);
                    ++num1;
                  }
                }
              }
              else
                flag3 = list2.Count<int>() != 0 || list3.Count<TrueFalseAnswer>() != 0;
            }
          }
          if (!flag3)
          {
            if (!flag2)
            {
              if (assessmentResult1.TrueFalse.Count<TrueFalseAnswer>() == 0 && assessmentResult1.MultipleChoice.Count<IncorrectMultipleChoiceQuestion>() == 0)
              {
                loUserAttempt.DateCompleted = new DateTime?(DateTime.Now);
                loUserAttempt.IsComplete = true;
                loUserAttempt.IsSuccessful = true;
                loUserAttempt.Mark = new int?(num2);
                loUserAttempt.Total = new int?(num1);
                trainingEntities.SaveChanges();
                assessmentResult1.TotalMark = num1;
                assessmentResult1.StudentMark = num2;
                assessmentResult1.Passed = true;
                assessmentResult1.OverallTotal = num1;
                assessmentResult1.OverallMark = num2;
                flag1 = true;
              }
              else if (loUserAttempt.Attempts >= 2)
              {
                loUserAttempt.DateCompleted = new DateTime?(DateTime.Now);
                loUserAttempt.IsComplete = true;
                loUserAttempt.IsSuccessful = true;
                DAL.UserSubModule userSubModule = loUserAttempt;
                int? nullable1 = loUserAttempt.Mark;
                int num3 = num2;
                int? nullable2 = nullable1.HasValue ? new int?(nullable1.GetValueOrDefault() + num3) : new int?();
                userSubModule.Mark = nullable2;
                student.IsLocked = false;
                AssessmentResult assessmentResult2 = assessmentResult1;
                nullable1 = loUserAttempt.Total;
                int num4 = nullable1 ?? 0;
                assessmentResult2.OverallTotal = num4;
                AssessmentResult assessmentResult3 = assessmentResult1;
                nullable1 = loUserAttempt.Mark;
                int num5 = nullable1 ?? 0;
                assessmentResult3.OverallMark = num5;
                trainingEntities.SaveChanges();
                assessmentResult1.TotalMark = num1;
                assessmentResult1.StudentMark = num2;
                assessmentResult1.Passed = false;
                flag1 = true;
              }
              else if (loUserAttempt.Attempts <= 1)
              {
                loUserAttempt.IsComplete = false;
                loUserAttempt.IsSuccessful = false;
                ++loUserAttempt.Attempts;
                loUserAttempt.Mark = new int?(num2);
                loUserAttempt.Total = new int?(num1);
                AssessmentResult assessmentResult2 = assessmentResult1;
                int? nullable = loUserAttempt.Total;
                int num3 = nullable ?? 0;
                assessmentResult2.OverallTotal = num3;
                AssessmentResult assessmentResult3 = assessmentResult1;
                nullable = loUserAttempt.Mark;
                int num4 = nullable ?? 0;
                assessmentResult3.OverallMark = num4;
                student.IsLocked = false;
                trainingEntities.SaveChanges();
                assessmentResult1.TotalMark = num1;
                assessmentResult1.StudentMark = num2;
                assessmentResult1.Passed = false;
              }
              if (flag1)
              {
                List<int> list1 = trainingEntities.SubModules.Where<DAL.SubModule>((Expression<Func<DAL.SubModule, bool>>) (sm => sm.ModuleID == paModuleID && sm.IsEnabled)).OrderBy<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.Order)).Select<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.ID)).ToList<int>();
                if (list1 != null)
                {
                  List<int> loCompletedByUser = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
                  {
                    usm = usm,
                    um = um
                  }).Where(data => data.usm.IsComplete && data.usm.IsEnabled && data.usm.IsSuccessful && data.um.UserID == paUserID).Select(data => data.usm.SubModuleID).ToList<int>();
                  IEnumerable<int> source = list1.Where<int>((Func<int, bool>) (p => !loCompletedByUser.Any<int>((Func<int, bool>) (p2 => p2 == p))));
                  if (source.Count<int>() > 0)
                  {
                    int num3 = source.Select<int, int>((Func<int, int>) (p => p)).FirstOrDefault<int>();
                    DAL.UserSubModule entity = new DAL.UserSubModule()
                    {
                      Attempts = 1,
                      IsComplete = false,
                      IsEnabled = true,
                      IsStarted = true,
                      IsSuccessful = false,
                      SubModuleID = num3,
                      UserModuleID = loUserAttempt.UserModuleID,
                      DateCreated = DateTime.Now,
                      Mark = new int?(0),
                      Total = new int?(0)
                    };
                    trainingEntities.UserSubModules.Add(entity);
                    trainingEntities.SaveChanges();
                  }
                  else if (userModule != null)
                  {
                    userModule.IsComplete = true;
                    userModule.IsSuccessful = true;
                    userModule.DateComplete = new DateTime?(DateTime.Now);
                    trainingEntities.SaveChanges();
                    List<int> loCompletedModules = trainingEntities.Modules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.Module, int>>) (m => m.ID), (Expression<Func<UserModule, int>>) (um => um.ModuleID), (m, um) => new
                    {
                      m = m,
                      um = um
                    }).Where(data => data.m.IsEnabled && data.um.IsEnabled && data.um.UserID == paUserID && data.um.IsComplete).OrderBy(data => data.m.Order).Select(data => data.m.ID).ToList<int>();
                    List<int> list2 = trainingEntities.Modules.Where<DAL.Module>((Expression<Func<DAL.Module, bool>>) (m => m.IsEnabled && !loCompletedModules.Contains(m.ID))).OrderBy<DAL.Module, int>((Expression<Func<DAL.Module, int>>) (m => m.Order)).Select<DAL.Module, int>((Expression<Func<DAL.Module, int>>) (m => m.ID)).ToList<int>();
                    if (list2 != null)
                    {
                      if (list2.Count<int>() != 0)
                      {
                        int loNextModuleID = list2[0];
                        List<int> list3 = trainingEntities.SubModules.Where<DAL.SubModule>((Expression<Func<DAL.SubModule, bool>>) (sm => sm.IsEnabled && sm.ModuleID == loNextModuleID)).OrderBy<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.Order)).Select<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.ID)).ToList<int>();
                        if (list3.Count > 0)
                        {
                          int num3 = list3[0];
                          UserModule entity1 = new UserModule()
                          {
                            DateCreated = DateTime.Now,
                            IsComplete = false,
                            IsEnabled = true,
                            IsStarted = true,
                            IsSuccessful = false,
                            ModuleID = loNextModuleID,
                            UserID = paUserID
                          };
                          trainingEntities.UserModules.Add(entity1);
                          DAL.UserSubModule entity2 = new DAL.UserSubModule()
                          {
                            DateCreated = DateTime.Now,
                            IsComplete = false,
                            IsEnabled = true,
                            IsStarted = true,
                            IsSuccessful = false,
                            Attempts = 1,
                            SubModuleID = list3[0],
                            UserModuleID = entity1.ID,
                            Mark = new int?(0),
                            Total = new int?(0)
                          };
                          trainingEntities.UserSubModules.Add(entity2);
                          trainingEntities.SaveChanges();
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        assessmentResult1.Passed = false;
      }
      return assessmentResult1;
    }

    public static bool EnableNextUnitStandard(int paModuleID, int paSubModuleID, string paUserID)
    {
      return false;
    }
  }
}
