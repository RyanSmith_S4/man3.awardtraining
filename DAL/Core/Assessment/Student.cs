﻿// Decompiled with JetBrains decompiler
// Type: DAL.Core.Assessment.Student
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Core.Assessment
{
  public class Student
  {
    public static StudentStats GetStudentStats(string paUserID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).Select<DAL.Student, COML.Classes.Student>(Expression.Lambda<Func<DAL.Student, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_UserID)), )))); //unable to render the statement
          UserModule loModule = trainingEntities.UserModules.OrderByDescending<UserModule, DateTime>((Expression<Func<UserModule, DateTime>>) (um => um.DateCreated)).Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.IsEnabled && um.IsStarted && um.UserID == loStudent.UserID)).FirstOrDefault<UserModule>();
          DAL.UserSubModule userSubModule = trainingEntities.UserSubModules.OrderByDescending<DAL.UserSubModule, DateTime>((Expression<Func<DAL.UserSubModule, DateTime>>) (usm => usm.DateCreated)).Where<DAL.UserSubModule>((Expression<Func<DAL.UserSubModule, bool>>) (usm => usm.IsEnabled && usm.IsStarted && usm.UserModuleID == loModule.ID)).FirstOrDefault<DAL.UserSubModule>();
          return new StudentStats()
          {
            CurrentModuleID = loModule.ModuleID,
            CurrentSubModuleID = userSubModule.SubModuleID,
            IsGraduate = loModule.ModuleID == 10 && userSubModule.SubModuleID >= 19 && (userSubModule.IsComplete && userSubModule.IsSuccessful),
            IsChatEnabled = loStudent.IsChatEnabled,
            IsLocked = loStudent.IsLocked
          };
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (StudentStats) null;
    }

    public static COML.Classes.Student GetStudent(string paUserID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.Student student1 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).Select<DAL.Student, COML.Classes.Student>(Expression.Lambda<Func<DAL.Student, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_UserID)), )))); //unable to render the statement
          COML.Classes.Student student2 = new COML.Classes.Student();
          return student1;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (COML.Classes.Student) null;
    }

    public static COML.Classes.Student GetStudentByID(int paStudentID)
    {
      COML.Classes.Student student1 = new COML.Classes.Student();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student2 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (student2 != null)
          {
            student1.ID = student2.ID;
            student1.UserID = student2.UserID;
            student1.FirstName = student2.FirstNames;
            student1.LastName = student2.LastNames;
            student1.Nickname = student2.Nickname;
            student1.TelephoneCell = student2.TelephoneCell;
            student1.EmailAddress = student2.EmailAddress;
            student1.IdentityNumber = student2.IdentityNumber;
            student1.HomeLanguage = student2.HomeLanguage;
            student1.SelectGender = student2.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            student1.HighestQualification = student2.HighestQualification;
            student1.HighestQualificationTitle = student2.HighestQualificationTitle;
            student1.Employed = student2.CurrentlyEmployed;
            student1.Age = student2.Age;
            student1.PostalAddress1 = student2.PostalAddress1;
            student1.PostalAddress2 = student2.PostalAddress2;
            student1.PostalAddress3 = student2.PostalAddress3;
            student1.PostalAddressCity = student2.PostalCity;
            student1.PostalAddressCode = student2.PostalCode;
            student1.PostalAddressProvince = student2.PostalProvince;
            student1.FsaCode = student2.FSACode == "SYSTEM" ? "DEFAULT" : student2.FSACode;
            COML.Classes.Student student3 = student1;
            bool? isChatEnabled = student2.IsChatEnabled;
            int num = isChatEnabled.HasValue ? (isChatEnabled.GetValueOrDefault() ? 1 : 0) : 0;
            student3.IsChatEnabled = num != 0;
            student1.IsEnabled = student2.IsEnabled;
            string race = student2.Race;
            student1.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return student1;
    }

    public static COML.Classes.Student GetStudentRecordByID(int paStudentID)
    {
      COML.Classes.Student student1 = new COML.Classes.Student();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student2 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (student2 != null)
          {
            student1.ID = student2.ID;
            student1.UserID = student2.UserID;
            student1.FirstName = student2.FirstNames;
            student1.LastName = student2.LastNames;
            student1.Nickname = student2.Nickname;
            student1.TelephoneCell = student2.TelephoneCell;
            student1.EmailAddress = student2.EmailAddress;
            student1.IdentityNumber = student2.IdentityNumber;
            student1.HomeLanguage = student2.HomeLanguage;
            student1.SelectGender = student2.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            student1.HighestQualification = student2.HighestQualification;
            student1.HighestQualificationTitle = student2.HighestQualificationTitle;
            student1.Employed = student2.CurrentlyEmployed;
            student1.Age = student2.Age;
            student1.PostalAddress1 = student2.PostalAddress1;
            student1.PostalAddress2 = student2.PostalAddress2;
            student1.PostalAddress3 = student2.PostalAddress3;
            student1.PostalAddressCity = student2.PostalCity;
            student1.PostalAddressCode = student2.PostalCode;
            student1.PostalAddressProvince = student2.PostalProvince;
            student1.FsaCode = student2.FSACode == "SYSTEM" ? "DEFAULT" : student2.FSACode;
            COML.Classes.Student student3 = student1;
            bool? isChatEnabled = student2.IsChatEnabled;
            int num = isChatEnabled.HasValue ? (isChatEnabled.GetValueOrDefault() ? 1 : 0) : 0;
            student3.IsChatEnabled = num != 0;
            student1.IsEnabled = student2.IsEnabled;
            string race = student2.Race;
            student1.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return student1;
    }

    public static bool StudentIsFriend(int paStudentID, string paUserID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student1 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          DAL.Student student2 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          return student1 != null && student2 != null && student1.ReferredUserID == student2.UserID;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static COML.Classes.Student GetStudentByIDForSponsor(
      int paStudentID,
      int paSponsorID)
    {
      try
      {
        COML.Classes.Student student1 = new COML.Classes.Student();
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> loFsaCodes = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID)).Select<SponsorFsaCode, string>((Expression<Func<SponsorFsaCode, string>>) (f => f.FsaCode)).ToList<string>();
          if (loFsaCodes.Contains("DEFAULT"))
            loFsaCodes.Add("SYSTEM");
          DAL.Student student2 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID && loFsaCodes.Contains(s.FSACode))).FirstOrDefault<DAL.Student>();
          if (student2 != null)
          {
            student1.ID = student2.ID;
            student1.UserID = student2.UserID;
            student1.FirstName = student2.FirstNames;
            student1.LastName = student2.LastNames;
            student1.Nickname = student2.Nickname;
            student1.TelephoneCell = student2.TelephoneCell;
            student1.EmailAddress = student2.EmailAddress;
            student1.IdentityNumber = student2.IdentityNumber;
            student1.HomeLanguage = student2.HomeLanguage;
            student1.SelectGender = student2.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            student1.HighestQualification = student2.HighestQualification;
            student1.HighestQualificationTitle = student2.HighestQualificationTitle;
            student1.Employed = student2.CurrentlyEmployed;
            student1.Age = student2.Age;
            student1.PostalAddress1 = student2.PostalAddress1;
            student1.PostalAddress2 = student2.PostalAddress2;
            student1.PostalAddress3 = student2.PostalAddress3;
            student1.PostalAddressCity = student2.PostalCity;
            student1.PostalAddressCode = student2.PostalCode;
            student1.PostalAddressProvince = student2.PostalProvince;
            string race = student2.Race;
            student1.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
            return student1;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (COML.Classes.Student) null;
    }

    public static bool SaveStudentFile(
      string paStudentID,
      int paSystemFile,
      int paTypeFile,
      string paFileName,
      string paFileLocation)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paStudentID && !s.IsLocked)).FirstOrDefault<DAL.Student>() != null)
          {
            DAL.UserSubModule userSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
            {
              um = um,
              usm = usm
            }).Where(data => data.um.UserID == paStudentID && data.um.IsEnabled && data.um.IsStarted && !data.um.IsComplete && !data.um.IsSuccessful && data.usm.IsEnabled && data.usm.IsStarted && !data.usm.IsComplete && !data.usm.IsSuccessful).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
            if (userSubModule != null)
            {
              DAL.UserFile entity = new DAL.UserFile()
              {
                DateTimeCreated = DateTime.Now,
                IsEnabled = true,
                Name = paFileName,
                SystemType = paSystemFile,
                Path = paFileLocation,
                UserID = paStudentID,
                FileType = paTypeFile
              };
              trainingEntities.UserFiles.Add(entity);
              trainingEntities.SaveChanges();
              trainingEntities.UserSubModuleFiles.Add(new UserSubModuleFile()
              {
                Attempt = userSubModule.Attempts,
                DateTimeCreated = DateTime.Now,
                IsCorrect = false,
                IsEnabled = true,
                IsAssessed = false,
                UserSubModuleID = userSubModule.ID,
                UserFileID = entity.ID
              });
              trainingEntities.SaveChanges();
              flag = true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static void RemoveStudents()
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [EmailLog]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [MaterialLog]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [PageLog]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [SmsLog]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [UserSubModuleMultipleChoiceAnswer]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [UserSubModuleTrueFalseAnswer]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [UserSubModule]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [UserModule]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [Student]");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [AspNetUserRoles] WHERE RoleId <> 1");
          trainingEntities.Database.ExecuteSqlCommand("DELETE FROM [AspNetUsers] WHERE Email <> 'admin@award.co.za'");
          trainingEntities.SaveChanges();
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static COML.Classes.UserFile GetStudentFile(string paStudentID, int paFileID)
    {
      COML.Classes.UserFile userFile1 = new COML.Classes.UserFile();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.UserFile userFile2 = trainingEntities.UserFiles.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (Expression<Func<DAL.Student, string>>) (s => s.UserID), (uf, s) => new
          {
            uf = uf,
            s = s
          }).Where(data => data.uf.IsEnabled && data.uf.ID == paFileID && data.s.UserID == paStudentID && data.s.IsEnabled && !data.s.IsLocked).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType3<DAL.UserFile, DAL.Student>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
          if (userFile2 != null)
            userFile1 = userFile2;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return userFile1;
    }

    public static List<UserAssessedFile> GetStudentsNonAssessedFiles(
      string paStudentID)
    {
      List<UserAssessedFile> userAssessedFileList1 = new List<UserAssessedFile>();
      List<UserAssessedFile> userAssessedFileList2 = new List<UserAssessedFile>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<UserAssessedFile> list = trainingEntities.Students.Join((IEnumerable<DAL.UserFile>) trainingEntities.UserFiles, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (s, uf) => new
          {
            s = s,
            uf = uf
          }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.s.UserID, (Expression<Func<UserModule, string>>) (um => um.UserID), (data, um) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            um = um
          }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.um.ID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (data, usm) => new
          {
            \u003C\u003Eh__TransparentIdentifier1 = data,
            usm = usm
          }).Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.uf.ID, (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (data, usmf) => new
          {
            \u003C\u003Eh__TransparentIdentifier2 = data,
            usmf = usmf
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.UserID == paStudentID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.ModuleID == 10 && data.\u003C\u003Eh__TransparentIdentifier2.usm.SubModuleID == 19 && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.usm.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.uf.IsEnabled && data.usmf.IsEnabled && data.usmf.UserSubModuleID == data.\u003C\u003Eh__TransparentIdentifier2.usm.ID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType15<\u003C\u003Ef__AnonymousType14<\u003C\u003Ef__AnonymousType13<\u003C\u003Ef__AnonymousType12<DAL.Student, DAL.UserFile>, UserModule>, DAL.UserSubModule>, UserSubModuleFile>, UserAssessedFile>>((Expression) Expression.MemberInit(Expression.New(typeof (UserAssessedFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (UserAssessedFile.set_ID)), )))); //unable to render the statement
          if (list.Count > 0)
          {
            foreach (UserAssessedFile userAssessedFile in list)
            {
              if (userAssessedFile.Attempt == 1)
                userAssessedFileList1.Add(userAssessedFile);
              else if (userAssessedFile.Attempt >= 2)
                userAssessedFileList2.Add(userAssessedFile);
            }
          }
          if (userAssessedFileList2.Count > 0)
            return userAssessedFileList2;
          return userAssessedFileList1;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (List<UserAssessedFile>) null;
    }

    public static List<UserAssessedFile> GetStudentsAssessedFiles(
      string paStudentID)
    {
      List<UserAssessedFile> userAssessedFileList1 = new List<UserAssessedFile>();
      List<UserAssessedFile> userAssessedFileList2 = new List<UserAssessedFile>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<UserAssessedFile> list = trainingEntities.Students.Join((IEnumerable<DAL.UserFile>) trainingEntities.UserFiles, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (s, uf) => new
          {
            s = s,
            uf = uf
          }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.s.UserID, (Expression<Func<UserModule, string>>) (um => um.UserID), (data, um) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            um = um
          }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.um.ID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (data, usm) => new
          {
            \u003C\u003Eh__TransparentIdentifier1 = data,
            usm = usm
          }).Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.uf.ID, (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (data, usmf) => new
          {
            \u003C\u003Eh__TransparentIdentifier2 = data,
            usmf = usmf
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.UserID == paStudentID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.ModuleID == 10 && data.\u003C\u003Eh__TransparentIdentifier2.usm.SubModuleID == 19 && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.usm.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.uf.IsEnabled && data.usmf.IsEnabled && data.usmf.UserSubModuleID == data.\u003C\u003Eh__TransparentIdentifier2.usm.ID && data.usmf.IsAssessed && data.usmf.IsCorrect && data.usmf.Attempt == data.\u003C\u003Eh__TransparentIdentifier2.usm.Attempts).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType15<\u003C\u003Ef__AnonymousType14<\u003C\u003Ef__AnonymousType13<\u003C\u003Ef__AnonymousType12<DAL.Student, DAL.UserFile>, UserModule>, DAL.UserSubModule>, UserSubModuleFile>, UserAssessedFile>>((Expression) Expression.MemberInit(Expression.New(typeof (UserAssessedFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (UserAssessedFile.set_ID)), )))); //unable to render the statement
          if (list.Count > 0)
          {
            foreach (UserAssessedFile userAssessedFile in list)
            {
              if (userAssessedFile.Attempt == 1)
                userAssessedFileList1.Add(userAssessedFile);
              else if (userAssessedFile.Attempt >= 2)
                userAssessedFileList2.Add(userAssessedFile);
            }
          }
          if (userAssessedFileList2.Count > 0)
            return userAssessedFileList2;
          return userAssessedFileList1;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (List<UserAssessedFile>) null;
    }
  }
}
