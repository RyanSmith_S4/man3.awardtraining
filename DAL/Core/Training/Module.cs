﻿// Decompiled with JetBrains decompiler
// Type: DAL.Core.Training.Module
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Core.Training
{
  public class Module
  {
    public static List<COML.Classes.Module> GetModules()
    {
      List<COML.Classes.Module> moduleList = new List<COML.Classes.Module>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          moduleList = trainingEntities.Modules.Where<DAL.Module>((Expression<Func<DAL.Module, bool>>) (m => m.IsEnabled)).OrderBy<DAL.Module, int>((Expression<Func<DAL.Module, int>>) (m => m.Order)).Select<DAL.Module, COML.Classes.Module>(Expression.Lambda<Func<DAL.Module, COML.Classes.Module>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Module)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Module.set_ID)), )))); //unable to render the statement
          foreach (COML.Classes.Module module in moduleList)
          {
            COML.Classes.Module lpModule = module;
            lpModule.SubModules = new List<COML.Classes.SubModule>();
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.SubModule> list1 = trainingEntities.SubModules.Where<DAL.SubModule>((Expression<Func<DAL.SubModule, bool>>) (sm => sm.ModuleID == lpModule.ID && sm.IsEnabled)).OrderBy<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.Order)).Select<DAL.SubModule, COML.Classes.SubModule>(Expression.Lambda<Func<DAL.SubModule, COML.Classes.SubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModule.set_ID)), )))); //unable to render the statement
            foreach (COML.Classes.SubModule subModule in list1)
            {
              COML.Classes.SubModule lpSubModule = subModule;
              lpSubModule.Materials = new List<COML.Classes.SubModuleMaterial>();
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<COML.Classes.SubModuleMaterial> list2 = trainingEntities.SubModuleModuleMaterials.Join((IEnumerable<DAL.SubModuleMaterial>) trainingEntities.SubModuleMaterials, (Expression<Func<SubModuleModuleMaterial, int>>) (smmm => smmm.SubModuleMaterialID), (Expression<Func<DAL.SubModuleMaterial, int>>) (smm => smm.ID), (smmm, smm) => new
              {
                smmm = smmm,
                smm = smm
              }).Where(data => data.smmm.IsEnabled && data.smm.IsEnabled && data.smmm.SubModuleID == lpSubModule.ID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType54<SubModuleModuleMaterial, DAL.SubModuleMaterial>, COML.Classes.SubModuleMaterial>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModuleMaterial)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModuleMaterial.set_ID)), )))); //unable to render the statement
              lpSubModule.Materials = list2;
            }
            lpModule.SubModules = list1;
          }
        }
        return moduleList;
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetModules: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return moduleList;
    }

    public static bool GetUserSubModuleWait(string paUserID, int paSubModuleID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          bool flag = false;
          DAL.UserSubModule loUserSubModule = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
          {
            usm = usm,
            um = um
          }).Where(data => data.um.IsEnabled && data.usm.IsEnabled && data.um.UserID == paUserID && data.usm.SubModuleID == paSubModuleID).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
          List<UserSubModuleFile> list = trainingEntities.UserFiles.Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (uf, usmf) => new
          {
            uf = uf,
            usmf = usmf
          }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            usm = usm
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.uf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.uf.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier0.usmf.IsEnabled && data.usm.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usmf.Attempt == loUserSubModule.Attempts).Select(data => data.\u003C\u003Eh__TransparentIdentifier0.usmf).ToList<UserSubModuleFile>();
          if (list.Count == 0)
            return false;
          foreach (UserSubModuleFile userSubModuleFile in list)
          {
            if (!userSubModuleFile.IsAssessed)
              flag = true;
          }
          return flag;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    public static bool GetUserSubModuleUploadsViewable(string paUserID, int paSubModuleID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          bool flag = false;
          trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
          {
            usm = usm,
            um = um
          }).Where(data => data.um.IsEnabled && data.usm.IsEnabled && data.um.UserID == paUserID && data.usm.SubModuleID == paSubModuleID).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
          if (trainingEntities.UserFiles.Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (uf, usmf) => new
          {
            uf = uf,
            usmf = usmf
          }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.usmf.UserSubModuleID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (data, usm) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            usm = usm
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.uf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.uf.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier0.usmf.IsEnabled && data.usm.SubModuleID == paSubModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usmf.Attempt == data.usm.Attempts).Select(data => data.\u003C\u003Eh__TransparentIdentifier0.usmf).ToList<UserSubModuleFile>().Count > 0)
            flag = true;
          return flag;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    public static List<COML.Classes.Module> GetModulesForUser(string paUserID)
    {
      List<COML.Classes.Module> moduleList = new List<COML.Classes.Module>();
      DateTime now = DateTime.Now;
      try
      {
        moduleList = Module.GetModules();
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<UserModule> list1 = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
          {
            um = um,
            m = m
          }).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.UserID == paUserID).OrderBy(data => data.m.Order).Select(data => data.um).ToList<UserModule>();
          foreach (UserModule userModule in list1)
          {
            UserModule lpModule = userModule;
            lpModule.UserSubModules = (ICollection<DAL.UserSubModule>) new List<DAL.UserSubModule>();
            List<DAL.UserSubModule> list2 = trainingEntities.UserSubModules.Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.SubModuleID), (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (usm, sm) => new
            {
              usm = usm,
              sm = sm
            }).Where(data => data.usm.IsEnabled == true && data.sm.IsEnabled == true && data.usm.UserModuleID == lpModule.ID).OrderBy(data => data.sm.Order).Select(data => data.usm).ToList<DAL.UserSubModule>();
            lpModule.UserSubModules = (ICollection<DAL.UserSubModule>) list2;
          }
          foreach (COML.Classes.Module module in moduleList)
          {
            foreach (UserModule userModule in list1)
            {
              if (module.ID == userModule.ModuleID)
              {
                bool flag = true;
                module.IsAvailable = userModule.IsComplete || userModule.IsStarted;
                foreach (COML.Classes.SubModule subModule in module.SubModules)
                {
                  foreach (DAL.UserSubModule userSubModule in (IEnumerable<DAL.UserSubModule>) userModule.UserSubModules)
                  {
                    if (userSubModule.SubModuleID == subModule.ID)
                    {
                      subModule.IsAvailable = userSubModule.IsStarted;
                      subModule.IsCurrent = !userSubModule.IsComplete;
                      subModule.IsLocked = userSubModule.Attempts >= 2 && userSubModule.IsComplete && !userSubModule.IsSuccessful;
                      subModule.IsWait = false;
                      if (!userModule.IsComplete)
                        flag = false;
                    }
                  }
                }
                if (!module.IsAvailable)
                  flag = false;
                module.IsCompleted = flag;
              }
            }
          }
        }
        return moduleList;
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetModulesForUser: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return moduleList;
    }

    public static List<COML.Classes.Module> GetModulesForUserByID(int paStudentID)
    {
      List<COML.Classes.Module> moduleList = new List<COML.Classes.Module>();
      DateTime now = DateTime.Now;
      try
      {
        moduleList = Module.GetModules();
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (loStudent != null)
          {
            List<UserModule> list1 = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
            {
              um = um,
              m = m
            }).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.UserID == loStudent.UserID).OrderBy(data => data.m.Order).Select(data => data.um).ToList<UserModule>();
            foreach (UserModule userModule in list1)
            {
              UserModule lpModule = userModule;
              lpModule.UserSubModules = (ICollection<DAL.UserSubModule>) new List<DAL.UserSubModule>();
              List<DAL.UserSubModule> list2 = trainingEntities.UserSubModules.Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.SubModuleID), (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (usm, sm) => new
              {
                usm = usm,
                sm = sm
              }).Where(data => data.usm.IsEnabled == true && data.sm.IsEnabled == true && data.usm.UserModuleID == lpModule.ID).OrderBy(data => data.sm.Order).Select(data => data.usm).ToList<DAL.UserSubModule>();
              lpModule.UserSubModules = (ICollection<DAL.UserSubModule>) list2;
            }
            foreach (COML.Classes.Module module in moduleList)
            {
              foreach (UserModule userModule in list1)
              {
                if (module.ID == userModule.ModuleID)
                {
                  bool flag = true;
                  module.IsAvailable = userModule.IsComplete || userModule.IsStarted;
                  foreach (COML.Classes.SubModule subModule in module.SubModules)
                  {
                    foreach (DAL.UserSubModule userSubModule in (IEnumerable<DAL.UserSubModule>) userModule.UserSubModules)
                    {
                      if (userSubModule.SubModuleID == subModule.ID)
                      {
                        subModule.IsAvailable = userSubModule.IsStarted;
                        subModule.IsCurrent = !userSubModule.IsComplete;
                        subModule.IsLocked = userSubModule.Attempts >= 2 && userSubModule.IsComplete && !userSubModule.IsSuccessful;
                        subModule.IsComplete = userSubModule.IsComplete;
                        subModule.IsStarted = userSubModule.IsStarted;
                        subModule.IsNotYetCompetent = userSubModule.Attempts >= 2 && !userSubModule.IsComplete;
                        if (!userModule.IsComplete)
                          flag = false;
                      }
                    }
                  }
                  if (!module.IsAvailable)
                    flag = false;
                  module.IsCompleted = flag;
                }
              }
            }
          }
        }
        return moduleList;
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetModulesForUser: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return moduleList;
    }

    public static List<COML.Classes.Module> GetModulesForUserByIDforSponsor(
      int paStudentID,
      int paSponsorID)
    {
      List<COML.Classes.Module> moduleList = new List<COML.Classes.Module>();
      DateTime now = DateTime.Now;
      try
      {
        moduleList = Module.GetModules();
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (sp => sp.IsEnabled && !sp.IsLocked && sp.ID == paSponsorID)).FirstOrDefault<DAL.Sponsor>() != null)
          {
            List<string> loSponsorFsaCodes = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID)).Select<SponsorFsaCode, string>((Expression<Func<SponsorFsaCode, string>>) (f => f.FsaCode)).ToList<string>();
            if (loSponsorFsaCodes.Contains("DEFAULT"))
              loSponsorFsaCodes.Add("SYSTEM");
            DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID && loSponsorFsaCodes.Contains(s.FSACode))).FirstOrDefault<DAL.Student>();
            if (loStudent != null)
            {
              List<UserModule> list1 = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
              {
                um = um,
                m = m
              }).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.UserID == loStudent.UserID).OrderBy(data => data.m.Order).Select(data => data.um).ToList<UserModule>();
              foreach (UserModule userModule in list1)
              {
                UserModule lpModule = userModule;
                lpModule.UserSubModules = (ICollection<DAL.UserSubModule>) new List<DAL.UserSubModule>();
                List<DAL.UserSubModule> list2 = trainingEntities.UserSubModules.Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.SubModuleID), (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (usm, sm) => new
                {
                  usm = usm,
                  sm = sm
                }).Where(data => data.usm.IsEnabled == true && data.sm.IsEnabled == true && data.usm.UserModuleID == lpModule.ID).OrderBy(data => data.sm.Order).Select(data => data.usm).ToList<DAL.UserSubModule>();
                lpModule.UserSubModules = (ICollection<DAL.UserSubModule>) list2;
              }
              foreach (COML.Classes.Module module in moduleList)
              {
                foreach (UserModule userModule in list1)
                {
                  if (module.ID == userModule.ModuleID)
                  {
                    bool flag = true;
                    module.IsAvailable = userModule.IsComplete || userModule.IsStarted;
                    foreach (COML.Classes.SubModule subModule in module.SubModules)
                    {
                      foreach (DAL.UserSubModule userSubModule in (IEnumerable<DAL.UserSubModule>) userModule.UserSubModules)
                      {
                        if (userSubModule.SubModuleID == subModule.ID)
                        {
                          subModule.IsAvailable = userSubModule.IsStarted;
                          subModule.IsCurrent = !userSubModule.IsComplete;
                          subModule.IsLocked = userSubModule.Attempts >= 2 && userSubModule.IsComplete && !userSubModule.IsSuccessful;
                          subModule.IsComplete = userSubModule.IsComplete;
                          subModule.IsStarted = userSubModule.IsStarted;
                          subModule.IsNotYetCompetent = userSubModule.Attempts >= 2 && !userSubModule.IsComplete;
                          if (!userModule.IsComplete)
                            flag = false;
                        }
                      }
                    }
                    if (!module.IsAvailable)
                      flag = false;
                    module.IsCompleted = flag;
                  }
                }
              }
            }
          }
        }
        return moduleList;
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetModulesForUser: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return moduleList;
    }

    public static COML.Classes.SubModuleMaterial GetMaterial(int paMaterialID)
    {
      COML.Classes.SubModuleMaterial subModuleMaterial = new COML.Classes.SubModuleMaterial();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          subModuleMaterial = trainingEntities.SubModuleMaterials.Where<DAL.SubModuleMaterial>((Expression<Func<DAL.SubModuleMaterial, bool>>) (smm => smm.IsEnabled && smm.ID == paMaterialID)).Select<DAL.SubModuleMaterial, COML.Classes.SubModuleMaterial>(Expression.Lambda<Func<DAL.SubModuleMaterial, COML.Classes.SubModuleMaterial>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModuleMaterial)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModuleMaterial.set_ID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetMaterial: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return subModuleMaterial;
    }

    public static COML.Classes.Module GetSubModuleForUser(
      string paUserID,
      int paModuleID,
      int paSubModuleID)
    {
      COML.Classes.Module module1 = new COML.Classes.Module();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.Module module2 = trainingEntities.Modules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.Module, int>>) (m => m.ID), (Expression<Func<UserModule, int>>) (um => um.ModuleID), (m, um) => new
          {
            m = m,
            um = um
          }).Where(data => data.m.IsEnabled && data.um.IsEnabled && data.m.ID == paModuleID && data.um.UserID == paUserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType43<DAL.Module, UserModule>, COML.Classes.Module>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Module)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Module.set_ID)), )))); //unable to render the statement
          if (module2 != null)
          {
            module1.ID = module2.ID;
            module1.Code = module2.Code;
            module1.Description = module2.Description;
            module1.IsAvailable = module2.IsAvailable;
            module1.IsCompleted = module2.IsCompleted;
            module1.Name = module2.Name;
            module1.SubModules = new List<COML.Classes.SubModule>();
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            COML.Classes.SubModule loSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
            {
              um = um,
              usm = usm
            }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              sm = sm
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == paModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.SubModuleID == paSubModuleID && data.sm.ID == paSubModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, COML.Classes.SubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModule.set_Attempts)), )))); //unable to render the statement
            if (loSubModule != null)
            {
              loSubModule.Materials = new List<COML.Classes.SubModuleMaterial>();
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<COML.Classes.SubModuleMaterial> list = trainingEntities.SubModuleModuleMaterials.Join((IEnumerable<DAL.SubModuleMaterial>) trainingEntities.SubModuleMaterials, (Expression<Func<SubModuleModuleMaterial, int>>) (smmm => smmm.SubModuleMaterialID), (Expression<Func<DAL.SubModuleMaterial, int>>) (smm => smm.ID), (smmm, smm) => new
              {
                smmm = smmm,
                smm = smm
              }).Where(data => data.smmm.IsEnabled && data.smm.IsEnabled && data.smmm.SubModuleID == loSubModule.ID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType54<SubModuleModuleMaterial, DAL.SubModuleMaterial>, COML.Classes.SubModuleMaterial>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModuleMaterial)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModuleMaterial.set_ID)), )))); //unable to render the statement
              loSubModule.Materials = list;
              int num1 = trainingEntities.SubModuleMultipleChoiceQuestions.Where<SubModuleMultipleChoiceQuestion>((Expression<Func<SubModuleMultipleChoiceQuestion, bool>>) (smmc => smmc.IsEnabled && smmc.SubModuleID == loSubModule.ID)).Count<SubModuleMultipleChoiceQuestion>();
              int num2 = trainingEntities.SubModuleTrueFalseQuestions.Where<SubModuleTrueFalseQuestion>((Expression<Func<SubModuleTrueFalseQuestion, bool>>) (smtf => smtf.IsEnabled && smtf.SubModuleID == loSubModule.ID)).Count<SubModuleTrueFalseQuestion>();
              loSubModule.HasAssessment = num1 != 0 || num2 != 0;
              module1.SubModules.Add(loSubModule);
            }
          }
        }
      }
      catch (Exception ex)
      {
        module1 = (COML.Classes.Module) null;
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetModuleForUser: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return module1;
    }

    public static void SetSubModuleComplete(string paUserID, int paModuleID, int paSubModuleID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.UserSubModule userSubModule = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
          {
            usm = usm,
            um = um
          }).Where(data => data.usm.IsEnabled && data.um.IsEnabled && data.um.UserID == paUserID && data.um.ModuleID == paModuleID && data.usm.SubModuleID == paSubModuleID && !data.um.IsComplete && !data.usm.IsComplete && data.usm.Attempts <= 2).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
          if (userSubModule == null)
            return;
          userSubModule.DateCompleted = new DateTime?(DateTime.Now);
          userSubModule.IsComplete = true;
          userSubModule.IsSuccessful = true;
          trainingEntities.SaveChanges();
          if (userSubModule.SubModuleID != 18)
          {
            List<int> list1 = trainingEntities.SubModules.Where<DAL.SubModule>((Expression<Func<DAL.SubModule, bool>>) (sm => sm.ModuleID == paModuleID && sm.IsEnabled)).OrderBy<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.Order)).Select<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.ID)).ToList<int>();
            if (list1 == null)
              return;
            List<int> loCompletedByUser = trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
            {
              usm = usm,
              um = um
            }).Where(data => data.usm.IsComplete && data.usm.IsEnabled && data.usm.IsSuccessful && data.um.UserID == paUserID).Select(data => data.usm.SubModuleID).ToList<int>();
            IEnumerable<int> source = list1.Where<int>((Func<int, bool>) (p => !loCompletedByUser.Any<int>((Func<int, bool>) (p2 => p2 == p))));
            if (source.Count<int>() > 0)
            {
              int num = source.Select<int, int>((Func<int, int>) (p => p)).FirstOrDefault<int>();
              DAL.UserSubModule entity = new DAL.UserSubModule()
              {
                Attempts = 1,
                IsComplete = false,
                IsEnabled = true,
                IsStarted = true,
                IsSuccessful = false,
                SubModuleID = num,
                UserModuleID = userSubModule.UserModuleID,
                DateCreated = DateTime.Now,
                Mark = new int?(0),
                Total = new int?(0)
              };
              trainingEntities.UserSubModules.Add(entity);
              trainingEntities.SaveChanges();
            }
            else
            {
              UserModule userModule = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.IsEnabled && um.IsStarted && um.ModuleID == paModuleID && um.UserID == paUserID)).FirstOrDefault<UserModule>();
              if (userModule == null)
                return;
              userModule.IsComplete = true;
              userModule.IsSuccessful = true;
              userModule.DateComplete = new DateTime?(DateTime.Now);
              trainingEntities.SaveChanges();
              List<int> loCompletedModules = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.IsEnabled && um.IsComplete && um.IsSuccessful && um.UserID == paUserID)).Select<UserModule, int>((Expression<Func<UserModule, int>>) (um => um.ModuleID)).ToList<int>();
              int loNextModulesID = trainingEntities.Modules.Where<DAL.Module>((Expression<Func<DAL.Module, bool>>) (m => m.ID != paModuleID && m.IsEnabled)).OrderBy<DAL.Module, int>((Expression<Func<DAL.Module, int>>) (m => m.Order)).Select<DAL.Module, int>((Expression<Func<DAL.Module, int>>) (m => m.ID)).ToList<int>().Where<int>((Func<int, bool>) (p => !loCompletedModules.Any<int>((Func<int, bool>) (p2 => p2 == p)))).FirstOrDefault<int>();
              if (loNextModulesID <= 0)
                return;
              List<int> list2 = trainingEntities.SubModules.Where<DAL.SubModule>((Expression<Func<DAL.SubModule, bool>>) (sm => sm.IsEnabled && sm.ModuleID == loNextModulesID)).OrderBy<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.Order)).Select<DAL.SubModule, int>((Expression<Func<DAL.SubModule, int>>) (sm => sm.ID)).ToList<int>();
              if (list2.Count <= 0)
                return;
              UserModule entity1 = new UserModule()
              {
                DateCreated = DateTime.Now,
                IsComplete = false,
                IsEnabled = true,
                IsStarted = true,
                IsSuccessful = false,
                ModuleID = loNextModulesID,
                UserID = paUserID
              };
              trainingEntities.UserModules.Add(entity1);
              trainingEntities.SaveChanges();
              DAL.UserSubModule entity2 = new DAL.UserSubModule()
              {
                DateCreated = DateTime.Now,
                IsComplete = false,
                IsEnabled = true,
                IsStarted = true,
                IsSuccessful = false,
                Attempts = 1,
                SubModuleID = list2[0],
                UserModuleID = entity1.ID,
                Mark = new int?(0),
                Total = new int?(0)
              };
              trainingEntities.UserSubModules.Add(entity2);
              trainingEntities.SaveChanges();
            }
          }
          else
          {
            UserModule userModule = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.IsEnabled && um.IsStarted && um.ModuleID == paModuleID && um.UserID == paUserID)).FirstOrDefault<UserModule>();
            if (userModule == null)
              return;
            userModule.IsComplete = true;
            userModule.IsSuccessful = true;
            userModule.DateComplete = new DateTime?(DateTime.Now);
            trainingEntities.SaveChanges();
            if (trainingEntities.UserSubModules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (Expression<Func<UserModule, int>>) (um => um.ID), (usm, um) => new
            {
              usm = usm,
              um = um
            }).Where(data => data.usm.IsEnabled && (data.usm.SubModuleID == 19 || data.usm.SubModuleID == 20) && data.um.ModuleID == 10 && data.um.UserID == paUserID).Select(data => data.usm).ToList<DAL.UserSubModule>().Count != 0)
              return;
            UserModule entity1 = new UserModule()
            {
              DateCreated = DateTime.Now,
              IsComplete = false,
              IsEnabled = true,
              IsStarted = true,
              IsSuccessful = false,
              ModuleID = 10,
              UserID = paUserID
            };
            trainingEntities.UserModules.Add(entity1);
            trainingEntities.SaveChanges();
            DAL.UserSubModule entity2 = new DAL.UserSubModule()
            {
              DateCreated = DateTime.Now,
              IsComplete = false,
              IsEnabled = true,
              IsStarted = true,
              IsSuccessful = false,
              Attempts = 1,
              SubModuleID = 19,
              UserModuleID = entity1.ID
            };
            DAL.UserSubModule entity3 = new DAL.UserSubModule()
            {
              DateCreated = DateTime.Now,
              IsComplete = true,
              IsEnabled = true,
              IsStarted = true,
              IsSuccessful = true,
              Attempts = 1,
              SubModuleID = 20,
              UserModuleID = entity1.ID
            };
            trainingEntities.UserSubModules.Add(entity2);
            trainingEntities.UserSubModules.Add(entity3);
            trainingEntities.SaveChanges();
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static bool IsStudentComplete(COML.Classes.Student paStudent)
    {
      bool flag = false;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
          flag = trainingEntities.UserModules.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<UserModule, string>>) (sm => sm.UserID), (Expression<Func<DAL.Student, string>>) (s => s.UserID), (sm, s) => new
          {
            sm = sm,
            s = s
          }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.s.ID == paStudent.ID && data.sm.ModuleID == 10 && data.sm.IsStarted && data.sm.IsEnabled).Select(data => data.sm).FirstOrDefault<UserModule>() != null;
      }
      catch (Exception ex)
      {
        flag = false;
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("Is Student Complete: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag;
    }

    public static COML.Classes.Module GetStudentCurrentModule(string paStudentID)
    {
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          COML.Classes.Module module = new COML.Classes.Module();
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.Module loModule = trainingEntities.Modules.Join((IEnumerable<UserModule>) trainingEntities.UserModules, (Expression<Func<DAL.Module, int>>) (m => m.ID), (Expression<Func<UserModule, int>>) (um => um.ModuleID), (m, um) => new
          {
            m = m,
            um = um
          }).Where(data => data.m.IsEnabled && data.um.IsEnabled && !data.um.IsComplete && !data.um.IsSuccessful && data.um.IsStarted && data.um.UserID == paStudentID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType43<DAL.Module, UserModule>, COML.Classes.Module>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Module)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Module.set_ID)), )))); //unable to render the statement
          if (loModule != null)
          {
            module.ID = loModule.ID;
            module.Code = loModule.Code;
            module.Description = loModule.Description;
            module.IsAvailable = loModule.IsAvailable;
            module.IsCompleted = loModule.IsCompleted;
            module.Name = loModule.Name;
            module.SubModules = new List<COML.Classes.SubModule>();
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            COML.Classes.SubModule loSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
            {
              um = um,
              usm = usm
            }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              sm = sm
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == paStudentID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loModule.ID && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && !data.\u003C\u003Eh__TransparentIdentifier0.usm.IsComplete && !data.\u003C\u003Eh__TransparentIdentifier0.usm.IsSuccessful).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, COML.Classes.SubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModule.set_Attempts)), )))); //unable to render the statement
            if (loSubModule != null)
            {
              loSubModule.Materials = new List<COML.Classes.SubModuleMaterial>();
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              List<COML.Classes.SubModuleMaterial> list = trainingEntities.SubModuleModuleMaterials.Join((IEnumerable<DAL.SubModuleMaterial>) trainingEntities.SubModuleMaterials, (Expression<Func<SubModuleModuleMaterial, int>>) (smmm => smmm.SubModuleMaterialID), (Expression<Func<DAL.SubModuleMaterial, int>>) (smm => smm.ID), (smmm, smm) => new
              {
                smmm = smmm,
                smm = smm
              }).Where(data => data.smmm.IsEnabled && data.smm.IsEnabled && data.smmm.SubModuleID == loSubModule.ID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType54<SubModuleModuleMaterial, DAL.SubModuleMaterial>, COML.Classes.SubModuleMaterial>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.SubModuleMaterial)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.SubModuleMaterial.set_ID)), )))); //unable to render the statement
              loSubModule.Materials = list;
              int num1 = trainingEntities.SubModuleMultipleChoiceQuestions.Where<SubModuleMultipleChoiceQuestion>((Expression<Func<SubModuleMultipleChoiceQuestion, bool>>) (smmc => smmc.IsEnabled && smmc.SubModuleID == loSubModule.ID)).Count<SubModuleMultipleChoiceQuestion>();
              int num2 = trainingEntities.SubModuleTrueFalseQuestions.Where<SubModuleTrueFalseQuestion>((Expression<Func<SubModuleTrueFalseQuestion, bool>>) (smtf => smtf.IsEnabled && smtf.SubModuleID == loSubModule.ID)).Count<SubModuleTrueFalseQuestion>();
              loSubModule.HasAssessment = num1 != 0 || num2 != 0;
              module.SubModules.Add(loSubModule);
              return module;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (COML.Classes.Module) null;
      }
      Logging.Log("GetModuleForUser: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return (COML.Classes.Module) null;
    }
  }
}
