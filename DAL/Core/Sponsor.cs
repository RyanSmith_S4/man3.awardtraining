﻿// Decompiled with JetBrains decompiler
// Type: DAL.Core.Sponsor
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Core
{
  public class Sponsor
  {
    public static DAL.Sponsor ConvertToEntity(COML.Classes.Sponsor paSponsor)
    {
      return new DAL.Sponsor()
      {
        Company = paSponsor.Company,
        EmailAddress = paSponsor.EmailAddress,
        UserEmailAddress = paSponsor.UserEmailAddress,
        DateTime_Created = paSponsor.DateTimeCreated,
        FirstNames = paSponsor.FirstName,
        LastNames = paSponsor.LastName,
        LastActivity = paSponsor.LastActivity,
        TelephoneCell = paSponsor.TelephoneCell,
        UserID = paSponsor.UserID,
        IsEnabled = paSponsor.IsEnabled,
        IsLocked = paSponsor.IsLocked
      };
    }

    public static bool AddSponsor(COML.Classes.Sponsor paSponsor)
    {
      bool flag = true;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<SponsorFsaCode> sponsorFsaCodeList = new List<SponsorFsaCode>();
          DAL.Sponsor entity = Sponsor.ConvertToEntity(paSponsor);
          trainingEntities.Sponsors.Add(entity);
          trainingEntities.SaveChanges();
          foreach (FsaCode fsaCode in paSponsor.FsaCodes)
            sponsorFsaCodeList.Add(new SponsorFsaCode()
            {
              FsaCode = fsaCode.Code,
              DateTime_Created = DateTime.Now,
              IsEnabled = true,
              SponsorID = entity.ID
            });
          if (sponsorFsaCodeList.Count > 0)
          {
            trainingEntities.SponsorFsaCodes.AddRange((IEnumerable<SponsorFsaCode>) sponsorFsaCodeList);
            trainingEntities.SaveChanges();
          }
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message.ToString(), Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      Logging.Log("Add Sponsor: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag;
    }

    public static bool AddSponsorFsaCode(int paSponsorID, string paCode)
    {
      bool flag = true;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID && f.FsaCode.ToUpper() == paCode)).FirstOrDefault<SponsorFsaCode>() == null)
          {
            SponsorFsaCode entity = new SponsorFsaCode()
            {
              DateTime_Created = DateTime.Now,
              FsaCode = paCode,
              IsEnabled = true,
              SponsorID = paSponsorID
            };
            trainingEntities.SponsorFsaCodes.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
          else
            flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message.ToString(), Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      Logging.Log("Add Sponsor FSA Code: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag;
    }

    public static List<SponsorSummary> GetSponsors()
    {
      List<SponsorSummary> sponsorSummaryList = new List<SponsorSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          sponsorSummaryList = trainingEntities.Sponsors.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Sponsor, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Sponsor, AspNetUser>, SponsorSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (SponsorSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (SponsorSummary.set_SponsortID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetSponsors: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return sponsorSummaryList;
    }

    public static List<SponsorSummary> GetStudentsByFsaCode(string paWord)
    {
      List<SponsorSummary> sponsorSummaryList = new List<SponsorSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          sponsorSummaryList = trainingEntities.Sponsors.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Sponsor, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Join((IEnumerable<SponsorFsaCode>) trainingEntities.SponsorFsaCodes, data => data.s.ID, (Expression<Func<SponsorFsaCode, int>>) (sf => sf.SponsorID), (data, sf) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            sf = sf
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.sf.FsaCode.ToUpper() == paWord && data.sf.IsEnabled).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType52<\u003C\u003Ef__AnonymousType5<DAL.Sponsor, AspNetUser>, SponsorFsaCode>, SponsorSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (SponsorSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (SponsorSummary.set_SponsortID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetSponsors: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return sponsorSummaryList;
    }

    public static COML.Classes.Sponsor GetSponsorByID(int paSponsorID)
    {
      COML.Classes.Sponsor sponsor1 = new COML.Classes.Sponsor();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Sponsor sponsor2 = trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (s => s.IsEnabled && s.ID == paSponsorID)).FirstOrDefault<DAL.Sponsor>();
          if (sponsor2 != null)
          {
            sponsor1.ID = sponsor2.ID;
            sponsor1.UserID = sponsor2.UserID;
            sponsor1.FirstName = sponsor2.FirstNames;
            sponsor1.LastName = sponsor2.LastNames;
            sponsor1.TelephoneCell = sponsor2.TelephoneCell;
            sponsor1.EmailAddress = sponsor2.EmailAddress;
            sponsor1.IsLocked = sponsor2.IsLocked;
            sponsor1.LastActivity = sponsor2.LastActivity;
            sponsor1.UserEmailAddress = sponsor2.UserEmailAddress;
            sponsor1.Company = sponsor2.Company;
            sponsor1.FsaCodes = new List<FsaCode>();
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            List<FsaCode> list = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID)).Select<SponsorFsaCode, FsaCode>(Expression.Lambda<Func<SponsorFsaCode, FsaCode>>((Expression) Expression.MemberInit(Expression.New(typeof (FsaCode)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (FsaCode.set_Code)), )))); //unable to render the statement
            sponsor1.FsaCodes = list;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return sponsor1;
    }

    public static COML.Classes.Sponsor GetSponsorByUserID(string paSponsorID)
    {
      COML.Classes.Sponsor sponsor = new COML.Classes.Sponsor();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Sponsor loSponsor = trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (s => s.IsEnabled && s.UserID == paSponsorID)).FirstOrDefault<DAL.Sponsor>();
          if (loSponsor != null)
          {
            sponsor.ID = loSponsor.ID;
            sponsor.UserID = loSponsor.UserID;
            sponsor.FirstName = loSponsor.FirstNames;
            sponsor.LastName = loSponsor.LastNames;
            sponsor.TelephoneCell = loSponsor.TelephoneCell;
            sponsor.EmailAddress = loSponsor.EmailAddress;
            sponsor.IsLocked = loSponsor.IsLocked;
            sponsor.LastActivity = loSponsor.LastActivity;
            sponsor.UserEmailAddress = loSponsor.UserEmailAddress;
            sponsor.Company = loSponsor.Company;
            sponsor.FsaCodes = new List<FsaCode>();
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            List<FsaCode> list = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == loSponsor.ID)).Select<SponsorFsaCode, FsaCode>(Expression.Lambda<Func<SponsorFsaCode, FsaCode>>((Expression) Expression.MemberInit(Expression.New(typeof (FsaCode)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (FsaCode.set_Code)), )))); //unable to render the statement
            sponsor.FsaCodes = list;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return sponsor;
    }

    public static int GetSponsorIDByUserID(string paSponsorID)
    {
      int num1 = -1;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          int num2 = trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (s => s.IsEnabled && !s.IsLocked && s.UserID == paSponsorID)).Select<DAL.Sponsor, int>((Expression<Func<DAL.Sponsor, int>>) (s => s.ID)).FirstOrDefault<int>();
          if (num2 > 0)
            num1 = num2;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return num1;
    }

    public static bool RemoveSponsorFsaCode(int paSponsorID, string paCode)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          SponsorFsaCode sponsorFsaCode = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID && f.FsaCode == paCode)).FirstOrDefault<SponsorFsaCode>();
          if (sponsorFsaCode != null)
          {
            sponsorFsaCode.IsEnabled = false;
            trainingEntities.SaveChanges();
          }
          return true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    public static bool LockSponsor(int paSponsorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (i => i.IsEnabled && i.ID == paSponsorID)).FirstOrDefault<DAL.Sponsor>().IsLocked = true;
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool UnlockSponsor(int paSponsorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (i => i.IsEnabled && i.ID == paSponsorID)).FirstOrDefault<DAL.Sponsor>().IsLocked = false;
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool IsLocked(string paSponsorID)
    {
      bool flag = true;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Sponsor sponsor = trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (i => i.IsEnabled && i.UserID == paSponsorID)).FirstOrDefault<DAL.Sponsor>();
          if (sponsor != null)
          {
            flag = sponsor.IsLocked;
            if (!sponsor.IsEnabled)
              flag = true;
          }
          else
            flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static List<StudentSummaryDetailed> GetStudentsBySponsor(
      string paSponsorID)
    {
      List<StudentSummaryDetailed> studentSummaryDetailedList = new List<StudentSummaryDetailed>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> loSponsor = trainingEntities.Sponsors.Join((IEnumerable<SponsorFsaCode>) trainingEntities.SponsorFsaCodes, (Expression<Func<DAL.Sponsor, int>>) (s => s.ID), (Expression<Func<SponsorFsaCode, int>>) (sfc => sfc.SponsorID), (s, sfc) => new
          {
            s = s,
            sfc = sfc
          }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.s.UserID == paSponsorID && data.sfc.IsEnabled).Select(data => data.sfc.FsaCode).ToList<string>();
          if (loSponsor.Count > 0)
          {
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<StudentSummaryDetailed> list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
            {
              s = s,
              u = u
            }).Where(data => data.s.IsEnabled && loSponsor.Contains(data.s.FSACode)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummaryDetailed>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailed)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailed.set_StudentID)), )))); //unable to render the statement
            foreach (StudentSummaryDetailed studentSummaryDetailed in list)
            {
              StudentSummaryDetailed lpStudent = studentSummaryDetailed;
              string str1 = "";
              string str2 = "";
              string race = lpStudent.Race;
              if (!(race == "W"))
              {
                if (!(race == "A"))
                {
                  if (!(race == "I"))
                  {
                    if (!(race == "O"))
                    {
                      if (race == "C")
                        str2 = "Coloured";
                    }
                    else
                      str2 = "Other";
                  }
                  else
                    str2 = "Indian";
                }
                else
                  str2 = "African";
              }
              else
                str2 = "White";
              DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Instructor>();
              if (instructor != null)
              {
                str1 = instructor.FirstNames + " " + instructor.LastNames;
              }
              else
              {
                DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (l => l.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Student>();
                if (student != null)
                  str1 = student.FirstNames + " " + student.LastNames;
              }
              lpStudent.ReferredFriend = str1;
              lpStudent.Race = str2;
              ParameterExpression parameterExpression2;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              StudentSummaryDetailedModule loCurrentModule = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
              {
                um = um,
                m = m
              }).OrderByDescending(data => data.um.DateCreated).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.IsStarted && data.um.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType22<UserModule, DAL.Module>, StudentSummaryDetailedModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedModule.set_ModuleID)), )))); //unable to render the statement
              if (loCurrentModule != null)
              {
                lpStudent.CurrentModule = loCurrentModule.Name;
                ParameterExpression parameterExpression3;
                // ISSUE: method reference
                // ISSUE: method reference
                // ISSUE: method reference
                // ISSUE: method reference
                StudentSummaryDetailedSubModule detailedSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
                {
                  um = um,
                  usm = usm
                }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
                {
                  \u003C\u003Eh__TransparentIdentifier0 = data,
                  sm = sm
                }).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.usm.DateCreated).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == lpStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ID == loCurrentModule.UserModuleID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loCurrentModule.ModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.UserModuleID == loCurrentModule.UserModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, StudentSummaryDetailedSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedSubModule.set_SubModuleID)), )))); //unable to render the statement
                lpStudent.CurrentSubModule = detailedSubModule == null ? "" : (detailedSubModule.Code == "" ? detailedSubModule.Name : detailedSubModule.Code + " - " + detailedSubModule.Name);
              }
              else
              {
                lpStudent.CurrentModule = "";
                lpStudent.CurrentSubModule = "";
              }
            }
            studentSummaryDetailedList = list;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentSummaryDetailedList;
    }
  }
}
