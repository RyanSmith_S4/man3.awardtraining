﻿// Decompiled with JetBrains decompiler
// Type: DAL.Core.Instructor
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Core
{
  public class Instructor
  {
    public static List<AssessorSummary> GetAssessors()
    {
      List<AssessorSummary> assessorSummaryList = new List<AssessorSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          assessorSummaryList = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsAssessor)).OrderByDescending<DAL.Instructor, DateTime>((Expression<Func<DAL.Instructor, DateTime>>) (i => i.DateTime_Created)).Select<DAL.Instructor, AssessorSummary>(Expression.Lambda<Func<DAL.Instructor, AssessorSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (AssessorSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (AssessorSummary.set_AssessorID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetAssessors: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return assessorSummaryList;
    }

    public static List<MentorSummary> GetMentors()
    {
      List<MentorSummary> mentorSummaryList = new List<MentorSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          mentorSummaryList = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsMentor)).OrderByDescending<DAL.Instructor, DateTime>((Expression<Func<DAL.Instructor, DateTime>>) (i => i.DateTime_Created)).Select<DAL.Instructor, MentorSummary>(Expression.Lambda<Func<DAL.Instructor, MentorSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (MentorSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (MentorSummary.set_MentorID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetMentors: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return mentorSummaryList;
    }

    public static List<AssessorSummary> GetAssessorByIdNumber(string paWord)
    {
      List<AssessorSummary> assessorSummaryList = new List<AssessorSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          assessorSummaryList = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IdentityNumber.Contains(paWord) && i.IsAssessor)).OrderByDescending<DAL.Instructor, DateTime>((Expression<Func<DAL.Instructor, DateTime>>) (i => i.DateTime_Created)).Select<DAL.Instructor, AssessorSummary>(Expression.Lambda<Func<DAL.Instructor, AssessorSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (AssessorSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (AssessorSummary.set_AssessorID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("Get Assessors By ID Number: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return assessorSummaryList;
    }

    public static List<MentorSummary> GetMentorByIdNumber(string paWord)
    {
      List<MentorSummary> mentorSummaryList = new List<MentorSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          mentorSummaryList = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IdentityNumber.Contains(paWord) && i.IsMentor)).OrderByDescending<DAL.Instructor, DateTime>((Expression<Func<DAL.Instructor, DateTime>>) (i => i.DateTime_Created)).Select<DAL.Instructor, MentorSummary>(Expression.Lambda<Func<DAL.Instructor, MentorSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (MentorSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (MentorSummary.set_MentorID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("Get Mentors By ID Number: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return mentorSummaryList;
    }

    public static DAL.Instructor ConvertToEntity(COML.Classes.Instructor paInstructor)
    {
      string str;
      switch (paInstructor.SelectedRace)
      {
        case Enumerations.enumRace.African:
          str = "A";
          break;
        case Enumerations.enumRace.Coloured:
          str = "C";
          break;
        case Enumerations.enumRace.Indian:
          str = "I";
          break;
        case Enumerations.enumRace.White:
          str = "W";
          break;
        case Enumerations.enumRace.Other:
          str = "O";
          break;
        default:
          str = "O";
          break;
      }
      return new DAL.Instructor()
      {
        Age = paInstructor.Age,
        AssignedEmailAddress = paInstructor.AssignedEmailAddress,
        EmailAddress = paInstructor.EmailAddress,
        CurrentlyEmployed = paInstructor.Employed,
        DateTime_Created = paInstructor.DateTimeCreated,
        FirstNames = paInstructor.FirstName,
        LastNames = paInstructor.LastName,
        Nickname = paInstructor.Nickname,
        Gender = paInstructor.SelectGender == Enumerations.enumGender.Male ? "M" : "F",
        HighestQualification = paInstructor.HighestQualification,
        HighestQualificationTitle = paInstructor.HighestQualificationTitle,
        HomeLanguage = paInstructor.HomeLanguage,
        IdentityNumber = paInstructor.IdentityNumber,
        LastActivity = paInstructor.LastActivity,
        PostalAddress1 = paInstructor.PostalAddress1,
        PostalAddress2 = paInstructor.PostalAddress2,
        PostalAddress3 = paInstructor.PostalAddress3,
        PostalCity = paInstructor.PostalAddressCity,
        PostalCode = paInstructor.PostalAddressCode,
        PostalProvince = paInstructor.PostalAddressProvince,
        TelephoneCell = paInstructor.TelephoneCell,
        UserID = paInstructor.UserID,
        Race = str,
        IsEnabled = paInstructor.IsEnabled,
        IsLocked = paInstructor.IsLocked,
        IsApproved = paInstructor.IsApproved,
        IsAssessor = paInstructor.IsAssessor,
        AssessorCode = paInstructor.AssessorCode,
        IsMentor = paInstructor.IsMentor,
        MentorCode = paInstructor.MentorCode,
        IsChatEnabled = new bool?(paInstructor.IsChatEnabled)
      };
    }

    public static bool IDNumberExists(string paIDNumber)
    {
      bool flag = false;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<DAL.Instructor> list = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (t1 => t1.IdentityNumber == paIDNumber)).ToList<DAL.Instructor>();
          flag = list != null && list.Count != 0;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      Logging.Log("IDNumberExists: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag;
    }

    public static int RegisterInstructor(COML.Classes.Instructor paInstructor)
    {
      int num = -1;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor entity = Instructor.ConvertToEntity(paInstructor);
          trainingEntities.Instructors.Add(entity);
          trainingEntities.SaveChanges();
          num = entity.ID;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message.ToString(), Enumerations.LogLevel.Error, ex);
        num = -1;
      }
      Logging.Log("RegisterInstructor: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return num;
    }

    public static COML.Classes.Instructor GetAssessorByID(int paInstructorID)
    {
      COML.Classes.Instructor loResult = new COML.Classes.Instructor();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.ID == paInstructorID && s.IsAssessor)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            loResult.ID = instructor.ID;
            loResult.UserID = instructor.UserID;
            loResult.FirstName = instructor.FirstNames;
            loResult.LastName = instructor.LastNames;
            loResult.Nickname = instructor.Nickname;
            loResult.TelephoneCell = instructor.TelephoneCell;
            loResult.EmailAddress = instructor.EmailAddress;
            loResult.IdentityNumber = instructor.IdentityNumber;
            loResult.HomeLanguage = instructor.HomeLanguage;
            loResult.SelectGender = instructor.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            loResult.HighestQualification = instructor.HighestQualification;
            loResult.HighestQualificationTitle = instructor.HighestQualificationTitle;
            loResult.Employed = instructor.CurrentlyEmployed;
            loResult.Age = instructor.Age;
            loResult.PostalAddress1 = instructor.PostalAddress1;
            loResult.PostalAddress2 = instructor.PostalAddress2;
            loResult.PostalAddress3 = instructor.PostalAddress3;
            loResult.PostalAddressCity = instructor.PostalCity;
            loResult.PostalAddressCode = instructor.PostalCode;
            loResult.PostalAddressProvince = instructor.PostalProvince;
            loResult.IsApproved = instructor.IsApproved;
            loResult.IsMentor = instructor.IsMentor;
            loResult.IsEnabled = instructor.IsEnabled;
            loResult.IsLocked = instructor.IsLocked;
            loResult.IsAssessor = instructor.IsAssessor;
            loResult.AssessorCode = instructor.AssessorCode;
            loResult.MentorCode = instructor.MentorCode;
            loResult.AssignedEmailAddress = instructor.AssignedEmailAddress;
            loResult.DateTimeApproved = instructor.DateTime_Approved;
            string race = instructor.Race;
            loResult.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.UserFile> list1 = trainingEntities.UserFiles.Join((IEnumerable<InstructorFile>) trainingEntities.InstructorFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<InstructorFile, int>>) (iuf => iuf.UserFileID), (uf, iuf) => new
            {
              uf = uf,
              iuf = iuf
            }).Where(data => data.iuf.InstructorID == loResult.ID && data.uf.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType0<DAL.UserFile, InstructorFile>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
            if (list1.Count > 0)
            {
              loResult.UserFiles = new List<COML.Classes.UserFile>();
              loResult.UserFiles = list1;
            }
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.Student> list2 = trainingEntities.AssignedAssessors.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID), (Expression<Func<DAL.Student, int>>) (s => s.ID), (aus, s) => new
            {
              aus = aus,
              s = s
            }).Where(data => data.aus.IsEnabled && data.aus.InstructorID == paInstructorID && data.s.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType1<AssignedAssessor, DAL.Student>, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_ID)), )))); //unable to render the statement
            loResult.Students = new List<COML.Classes.Student>();
            loResult.Students = list2;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return loResult;
    }

    public static COML.Classes.Instructor GetAssessorByUserID(string paInstructorID)
    {
      COML.Classes.Instructor loResult = new COML.Classes.Instructor();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.UserID == paInstructorID && s.IsAssessor)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            loResult.ID = instructor.ID;
            loResult.UserID = instructor.UserID;
            loResult.FirstName = instructor.FirstNames;
            loResult.LastName = instructor.LastNames;
            loResult.Nickname = instructor.Nickname;
            loResult.TelephoneCell = instructor.TelephoneCell;
            loResult.EmailAddress = instructor.EmailAddress;
            loResult.IdentityNumber = instructor.IdentityNumber;
            loResult.HomeLanguage = instructor.HomeLanguage;
            loResult.SelectGender = instructor.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            loResult.HighestQualification = instructor.HighestQualification;
            loResult.HighestQualificationTitle = instructor.HighestQualificationTitle;
            loResult.Employed = instructor.CurrentlyEmployed;
            loResult.Age = instructor.Age;
            loResult.PostalAddress1 = instructor.PostalAddress1;
            loResult.PostalAddress2 = instructor.PostalAddress2;
            loResult.PostalAddress3 = instructor.PostalAddress3;
            loResult.PostalAddressCity = instructor.PostalCity;
            loResult.PostalAddressCode = instructor.PostalCode;
            loResult.PostalAddressProvince = instructor.PostalProvince;
            loResult.IsApproved = instructor.IsApproved;
            loResult.IsMentor = instructor.IsMentor;
            loResult.IsEnabled = instructor.IsEnabled;
            loResult.IsLocked = instructor.IsLocked;
            loResult.IsAssessor = instructor.IsAssessor;
            loResult.AssessorCode = instructor.AssessorCode;
            loResult.MentorCode = instructor.MentorCode;
            loResult.AssignedEmailAddress = instructor.AssignedEmailAddress;
            loResult.DateTimeApproved = instructor.DateTime_Approved;
            string race = instructor.Race;
            loResult.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.UserFile> list1 = trainingEntities.UserFiles.Join((IEnumerable<InstructorFile>) trainingEntities.InstructorFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<InstructorFile, int>>) (iuf => iuf.UserFileID), (uf, iuf) => new
            {
              uf = uf,
              iuf = iuf
            }).Where(data => data.iuf.InstructorID == loResult.ID && data.uf.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType0<DAL.UserFile, InstructorFile>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
            if (list1.Count > 0)
            {
              loResult.UserFiles = new List<COML.Classes.UserFile>();
              loResult.UserFiles = list1;
            }
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.Student> list2 = trainingEntities.AssignedAssessors.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID), (Expression<Func<DAL.Student, int>>) (s => s.ID), (aus, s) => new
            {
              aus = aus,
              s = s
            }).Where(data => data.aus.IsEnabled && data.aus.InstructorID == loResult.ID && data.s.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType1<AssignedAssessor, DAL.Student>, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_ID)), )))); //unable to render the statement
            loResult.Students = new List<COML.Classes.Student>();
            if (list2 != null)
            {
              foreach (COML.Classes.Student student in list2)
              {
                COML.Classes.Student lpStudent = student;
                ParameterExpression parameterExpression3;
                // ISSUE: method reference
                // ISSUE: method reference
                List<UserAssessedFile> list3 = trainingEntities.UserFiles.Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (uf, usmf) => new
                {
                  uf = uf,
                  usmf = usmf
                }).Where(data => data.uf.IsEnabled && data.usmf.IsEnabled && data.uf.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType2<DAL.UserFile, UserSubModuleFile>, UserAssessedFile>>((Expression) Expression.MemberInit(Expression.New(typeof (UserAssessedFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (UserAssessedFile.set_ID)), )))); //unable to render the statement
                lpStudent.Files = list3;
              }
            }
            loResult.Students = list2;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return loResult;
    }

    public static COML.Classes.Instructor GetMentorByID(int paInstructorID)
    {
      COML.Classes.Instructor loResult = new COML.Classes.Instructor();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.ID == paInstructorID && s.IsMentor)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            loResult.ID = instructor.ID;
            loResult.UserID = instructor.UserID;
            loResult.FirstName = instructor.FirstNames;
            loResult.LastName = instructor.LastNames;
            loResult.Nickname = instructor.Nickname;
            loResult.TelephoneCell = instructor.TelephoneCell;
            loResult.EmailAddress = instructor.EmailAddress;
            loResult.IdentityNumber = instructor.IdentityNumber;
            loResult.HomeLanguage = instructor.HomeLanguage;
            loResult.SelectGender = instructor.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            loResult.HighestQualification = instructor.HighestQualification;
            loResult.HighestQualificationTitle = instructor.HighestQualificationTitle;
            loResult.Employed = instructor.CurrentlyEmployed;
            loResult.Age = instructor.Age;
            loResult.PostalAddress1 = instructor.PostalAddress1;
            loResult.PostalAddress2 = instructor.PostalAddress2;
            loResult.PostalAddress3 = instructor.PostalAddress3;
            loResult.PostalAddressCity = instructor.PostalCity;
            loResult.PostalAddressCode = instructor.PostalCode;
            loResult.PostalAddressProvince = instructor.PostalProvince;
            loResult.IsApproved = instructor.IsApproved;
            loResult.IsMentor = instructor.IsMentor;
            loResult.IsEnabled = instructor.IsEnabled;
            loResult.IsLocked = instructor.IsLocked;
            loResult.IsAssessor = instructor.IsAssessor;
            loResult.AssessorCode = instructor.AssessorCode;
            loResult.MentorCode = instructor.MentorCode;
            loResult.AssignedEmailAddress = instructor.AssignedEmailAddress;
            loResult.DateTimeApproved = instructor.DateTime_Approved;
            string race = instructor.Race;
            loResult.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.UserFile> list = trainingEntities.UserFiles.Join((IEnumerable<InstructorFile>) trainingEntities.InstructorFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<InstructorFile, int>>) (iuf => iuf.UserFileID), (uf, iuf) => new
            {
              uf = uf,
              iuf = iuf
            }).Where(data => data.iuf.InstructorID == loResult.ID && data.uf.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType0<DAL.UserFile, InstructorFile>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
            if (list.Count > 0)
            {
              loResult.UserFiles = new List<COML.Classes.UserFile>();
              loResult.UserFiles = list;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return loResult;
    }

    public static COML.Classes.Instructor GetInstructorByID(int paInstructorID)
    {
      COML.Classes.Instructor loResult = new COML.Classes.Instructor();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.ID == paInstructorID)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            loResult.ID = instructor.ID;
            loResult.UserID = instructor.UserID;
            loResult.FirstName = instructor.FirstNames;
            loResult.LastName = instructor.LastNames;
            loResult.Nickname = instructor.Nickname;
            loResult.TelephoneCell = instructor.TelephoneCell;
            loResult.EmailAddress = instructor.EmailAddress;
            loResult.IdentityNumber = instructor.IdentityNumber;
            loResult.HomeLanguage = instructor.HomeLanguage;
            loResult.SelectGender = instructor.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            loResult.HighestQualification = instructor.HighestQualification;
            loResult.HighestQualificationTitle = instructor.HighestQualificationTitle;
            loResult.Employed = instructor.CurrentlyEmployed;
            loResult.Age = instructor.Age;
            loResult.PostalAddress1 = instructor.PostalAddress1;
            loResult.PostalAddress2 = instructor.PostalAddress2;
            loResult.PostalAddress3 = instructor.PostalAddress3;
            loResult.PostalAddressCity = instructor.PostalCity;
            loResult.PostalAddressCode = instructor.PostalCode;
            loResult.PostalAddressProvince = instructor.PostalProvince;
            loResult.IsApproved = instructor.IsApproved;
            loResult.IsMentor = instructor.IsMentor;
            loResult.IsEnabled = instructor.IsEnabled;
            loResult.IsAssessor = instructor.IsAssessor;
            loResult.AssessorCode = instructor.AssessorCode;
            loResult.MentorCode = instructor.MentorCode;
            loResult.AssignedEmailAddress = instructor.AssignedEmailAddress;
            loResult.DateTimeApproved = instructor.DateTime_Approved;
            string race = instructor.Race;
            loResult.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.UserFile> list = trainingEntities.UserFiles.Join((IEnumerable<InstructorFile>) trainingEntities.InstructorFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<InstructorFile, int>>) (iuf => iuf.UserFileID), (uf, iuf) => new
            {
              uf = uf,
              iuf = iuf
            }).Where(data => data.iuf.UserFileID == loResult.ID && data.uf.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType0<DAL.UserFile, InstructorFile>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
            if (list.Count > 0)
            {
              loResult.UserFiles = new List<COML.Classes.UserFile>();
              loResult.UserFiles = list;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return loResult;
    }

    public static COML.Classes.Instructor GetInstructorByUserID(string paInstructorID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor1 = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.UserID == paInstructorID)).FirstOrDefault<DAL.Instructor>();
          if (instructor1 != null)
          {
            COML.Classes.Instructor loResult = new COML.Classes.Instructor();
            loResult.ID = instructor1.ID;
            loResult.UserID = instructor1.UserID;
            loResult.FirstName = instructor1.FirstNames;
            loResult.LastName = instructor1.LastNames;
            loResult.Nickname = instructor1.Nickname;
            loResult.TelephoneCell = instructor1.TelephoneCell;
            loResult.EmailAddress = instructor1.EmailAddress;
            loResult.IdentityNumber = instructor1.IdentityNumber;
            loResult.HomeLanguage = instructor1.HomeLanguage;
            loResult.SelectGender = instructor1.Gender == "M" ? Enumerations.enumGender.Male : Enumerations.enumGender.Female;
            loResult.HighestQualification = instructor1.HighestQualification;
            loResult.HighestQualificationTitle = instructor1.HighestQualificationTitle;
            loResult.Employed = instructor1.CurrentlyEmployed;
            loResult.Age = instructor1.Age;
            loResult.PostalAddress1 = instructor1.PostalAddress1;
            loResult.PostalAddress2 = instructor1.PostalAddress2;
            loResult.PostalAddress3 = instructor1.PostalAddress3;
            loResult.PostalAddressCity = instructor1.PostalCity;
            loResult.PostalAddressCode = instructor1.PostalCode;
            loResult.PostalAddressProvince = instructor1.PostalProvince;
            loResult.IsApproved = instructor1.IsApproved;
            loResult.IsMentor = instructor1.IsMentor;
            loResult.IsEnabled = instructor1.IsEnabled;
            loResult.IsAssessor = instructor1.IsAssessor;
            loResult.AssessorCode = instructor1.AssessorCode;
            loResult.MentorCode = instructor1.MentorCode;
            loResult.AssignedEmailAddress = instructor1.AssignedEmailAddress;
            loResult.DateTimeApproved = instructor1.DateTime_Approved;
            COML.Classes.Instructor instructor2 = loResult;
            bool? isChatEnabled = instructor1.IsChatEnabled;
            int num = isChatEnabled.HasValue ? (isChatEnabled.GetValueOrDefault() ? 1 : 0) : 0;
            instructor2.IsChatEnabled = num != 0;
            loResult.IsLocked = instructor1.IsLocked;
            string race = instructor1.Race;
            loResult.SelectedRace = race == "W" ? Enumerations.enumRace.White : (race == "I" ? Enumerations.enumRace.Indian : (race == "A" ? Enumerations.enumRace.African : (race == "C" ? Enumerations.enumRace.Coloured : (race == "O" ? Enumerations.enumRace.Other : Enumerations.enumRace.Other))));
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<COML.Classes.UserFile> list = trainingEntities.UserFiles.Join((IEnumerable<InstructorFile>) trainingEntities.InstructorFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<InstructorFile, int>>) (iuf => iuf.UserFileID), (uf, iuf) => new
            {
              uf = uf,
              iuf = iuf
            }).Where(data => data.iuf.UserFileID == loResult.ID && data.uf.IsEnabled).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType0<DAL.UserFile, InstructorFile>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
            if (list.Count > 0)
            {
              loResult.UserFiles = new List<COML.Classes.UserFile>();
              loResult.UserFiles = list;
            }
            return loResult;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (COML.Classes.Instructor) null;
      }
      return (COML.Classes.Instructor) null;
    }

    public static bool DeregisterInstructor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && !i.IsApproved)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            instructor.IsEnabled = false;
            instructor.IsLocked = true;
            trainingEntities.SaveChanges();
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool SaveInstructorFile(
      int paInstructorID,
      int paSystemFile,
      int paTypeFile,
      string paFileName,
      string paFileLocation)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            DAL.UserFile entity = new DAL.UserFile()
            {
              DateTimeCreated = DateTime.Now,
              IsEnabled = true,
              Name = paFileName,
              SystemType = paSystemFile,
              Path = paFileLocation,
              UserID = (string) null,
              FileType = paTypeFile
            };
            trainingEntities.UserFiles.Add(entity);
            trainingEntities.SaveChanges();
            trainingEntities.InstructorFiles.Add(new InstructorFile()
            {
              InstructorID = instructor.ID,
              UserFileID = entity.ID
            });
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static COML.Classes.UserFile GetIntructorFile(int paInstructorID, int paFileID)
    {
      COML.Classes.UserFile userFile1 = new COML.Classes.UserFile();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          COML.Classes.UserFile userFile2 = trainingEntities.UserFiles.Join((IEnumerable<InstructorFile>) trainingEntities.InstructorFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<InstructorFile, int>>) (iuf => iuf.UserFileID), (uf, iuf) => new
          {
            uf = uf,
            iuf = iuf
          }).Where(data => data.uf.IsEnabled && data.uf.ID == paFileID && data.iuf.InstructorID == paInstructorID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType0<DAL.UserFile, InstructorFile>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
          if (userFile2 != null)
            userFile1 = userFile2;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return userFile1;
    }

    public static COML.Classes.UserFile GetAssessorStudentFile(
      string paInstructorID,
      int paStudentID,
      int paFileID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor loAssessor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == paInstructorID && i.IsApproved && i.IsAssessor && i.IsEnabled && !i.IsLocked)).FirstOrDefault<DAL.Instructor>();
          if (loAssessor != null)
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            COML.Classes.UserFile userFile = trainingEntities.UserFiles.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (Expression<Func<DAL.Student, string>>) (s => s.UserID), (uf, s) => new
            {
              uf = uf,
              s = s
            }).Join((IEnumerable<AssignedAssessor>) trainingEntities.AssignedAssessors, data => data.s.ID, (Expression<Func<AssignedAssessor, int>>) (au => au.StudentID), (data, au) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              au = au
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.uf.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.uf.ID == paFileID && data.au.InstructorID == loAssessor.ID && data.\u003C\u003Eh__TransparentIdentifier0.s.ID == paStudentID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType4<\u003C\u003Ef__AnonymousType3<DAL.UserFile, DAL.Student>, AssignedAssessor>, COML.Classes.UserFile>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.UserFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.UserFile.set_ID)), )))); //unable to render the statement
            if (userFile != null)
              return userFile;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (COML.Classes.UserFile) null;
      }
      return (COML.Classes.UserFile) null;
    }

    public static bool LockAssessor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && i.IsAssessor)).FirstOrDefault<DAL.Instructor>().IsLocked = true;
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool UnlockAssessor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && i.IsAssessor)).FirstOrDefault<DAL.Instructor>().IsLocked = false;
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool ApproveAssessor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && i.IsAssessor)).FirstOrDefault<DAL.Instructor>();
          instructor.IsLocked = false;
          instructor.IsApproved = true;
          instructor.DateTime_Approved = new DateTime?(DateTime.Now);
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool LockMentor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && i.IsMentor)).FirstOrDefault<DAL.Instructor>().IsLocked = true;
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool UnlockMentor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && i.IsMentor)).FirstOrDefault<DAL.Instructor>().IsLocked = false;
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool ApproveMentor(int paInstructorID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.ID == paInstructorID && i.IsMentor)).FirstOrDefault<DAL.Instructor>();
          instructor.IsLocked = false;
          instructor.IsApproved = true;
          instructor.DateTime_Approved = new DateTime?(DateTime.Now);
          trainingEntities.SaveChanges();
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool IsLocked(string paInstructorID)
    {
      bool flag = true;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.UserID == paInstructorID && !i.IsLocked && i.IsApproved)).FirstOrDefault<DAL.Instructor>() != null)
            flag = false;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool UnassignAssessorStudent(int paInstructorID, int paStudentID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          AssignedAssessor assignedAssessor = trainingEntities.AssignedAssessors.Where<AssignedAssessor>((Expression<Func<AssignedAssessor, bool>>) (aa => aa.IsEnabled && aa.InstructorID == paInstructorID && aa.StudentID == paStudentID)).FirstOrDefault<AssignedAssessor>();
          if (assignedAssessor != null)
          {
            assignedAssessor.IsEnabled = false;
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static List<COML.Classes.Student> GetStudentsNotAssignedToAssessors()
    {
      List<COML.Classes.Student> studentList = new List<COML.Classes.Student>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<int> loAssignedStudents = trainingEntities.AssignedAssessors.Where<AssignedAssessor>((Expression<Func<AssignedAssessor, bool>>) (aus => aus.IsEnabled && aus.InstructorID > 0 && aus.StudentID > 0)).Select<AssignedAssessor, int>((Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID)).ToList<int>();
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<COML.Classes.Student> list = trainingEntities.Students.OrderByDescending<DAL.Student, DateTime>((Expression<Func<DAL.Student, DateTime>>) (s => s.DateTime_Created)).Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked && !loAssignedStudents.Contains(s.ID))).Select<DAL.Student, COML.Classes.Student>(Expression.Lambda<Func<DAL.Student, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_UserID)), )))); //unable to render the statement
          if (list.Count > 0)
            studentList = list;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      Logging.Log("GetAssessors: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentList;
    }

    public static List<COML.Classes.Student> GetStudentsByIDNumberAndCodeForAssessor(
      string paWord,
      string paCode)
    {
      List<COML.Classes.Student> studentList = new List<COML.Classes.Student>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          bool flag1 = paCode != null && paCode != "";
          bool flag2 = paWord != null && paWord != "";
          List<int> loAssignedStudents = trainingEntities.AssignedAssessors.Where<AssignedAssessor>((Expression<Func<AssignedAssessor, bool>>) (aus => aus.IsEnabled && aus.InstructorID > 0 && aus.StudentID > 0)).Select<AssignedAssessor, int>((Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID)).ToList<int>();
          if (flag1 && !flag2)
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            studentList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
            {
              s = s,
              u = u
            }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.s.FSACode.ToUpper() == paCode.ToUpper() && !loAssignedStudents.Contains(data.s.ID)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_UserID)), )))); //unable to render the statement
          }
          else if (!flag1 & flag2)
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            studentList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
            {
              s = s,
              u = u
            }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.s.IdentityNumber == paWord && !loAssignedStudents.Contains(data.s.ID)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_UserID)), )))); //unable to render the statement
          }
          else if (flag1 & flag2)
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            studentList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
            {
              s = s,
              u = u
            }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.s.FSACode.ToUpper() == paCode.ToUpper() && data.s.IdentityNumber == paWord && !loAssignedStudents.Contains(data.s.ID)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_UserID)), )))); //unable to render the statement
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentList;
    }

    public static List<COML.Classes.Student> GetAssignedStudentsByIDNumberForAssessor(
      string paInstructorID,
      string paWord)
    {
      List<COML.Classes.Student> studentList = new List<COML.Classes.Student>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<COML.Classes.Student> list1 = trainingEntities.AssignedAssessors.Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, (Expression<Func<AssignedAssessor, int>>) (aus => aus.InstructorID), (Expression<Func<DAL.Instructor, int>>) (i => i.ID), (aus, i) => new
          {
            aus = aus,
            i = i
          }).Join((IEnumerable<DAL.Student>) trainingEntities.Students, data => data.aus.StudentID, (Expression<Func<DAL.Student, int>>) (s => s.ID), (data, s) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            s = s
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.aus.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.i.IsEnabled && data.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.i.IsApproved && !data.\u003C\u003Eh__TransparentIdentifier0.i.IsLocked && data.\u003C\u003Eh__TransparentIdentifier0.i.UserID == paInstructorID && data.s.IdentityNumber.Contains(paWord)).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType7<\u003C\u003Ef__AnonymousType6<AssignedAssessor, DAL.Instructor>, DAL.Student>, COML.Classes.Student>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Student)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_ID)), )))); //unable to render the statement
          if (list1.Count > 0)
          {
            foreach (COML.Classes.Student student in list1)
            {
              COML.Classes.Student lpStudent = student;
              ParameterExpression parameterExpression2;
              // ISSUE: method reference
              // ISSUE: method reference
              List<UserAssessedFile> list2 = trainingEntities.UserFiles.Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (uf, usmf) => new
              {
                uf = uf,
                usmf = usmf
              }).Where(data => data.uf.IsEnabled && data.usmf.IsEnabled && data.uf.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType2<DAL.UserFile, UserSubModuleFile>, UserAssessedFile>>((Expression) Expression.MemberInit(Expression.New(typeof (UserAssessedFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (UserAssessedFile.set_ID)), )))); //unable to render the statement
              lpStudent.Files = list2;
            }
          }
          studentList = list1;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentList;
    }

    public static bool AssignLearnerToAssessor(int paInstructorID, int paStudentID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (trainingEntities.AssignedAssessors.Where<AssignedAssessor>((Expression<Func<AssignedAssessor, bool>>) (aus => aus.IsEnabled && aus.InstructorID > 0 && aus.StudentID == paStudentID)).Select<AssignedAssessor, int>((Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID)).ToList<int>().Count == 0)
          {
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (a => a.IsApproved && a.IsEnabled && !a.IsLocked && a.IsAssessor && a.ID == paInstructorID)).FirstOrDefault<DAL.Instructor>();
            DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
            if (instructor != null)
            {
              AssignedAssessor entity = new AssignedAssessor()
              {
                InstructorID = instructor.ID,
                StudentID = student.ID,
                DateTimeCreated = DateTime.Now,
                IsEnabled = true
              };
              trainingEntities.AssignedAssessors.Add(entity);
              trainingEntities.SaveChanges();
              flag = true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static StudentState GetStudentsNonAssessedFiles(
      string paUserID,
      int paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor loAssessor = trainingEntities.AssignedAssessors.Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, (Expression<Func<AssignedAssessor, int>>) (aus => aus.InstructorID), (Expression<Func<DAL.Instructor, int>>) (i => i.ID), (aus, i) => new
          {
            aus = aus,
            i = i
          }).Where(data => data.aus.IsEnabled && data.i.IsEnabled && data.i.IsApproved && data.i.IsAssessor && !data.i.IsLocked && data.i.UserID == paUserID && data.aus.StudentID == paStudentID).Select(data => data.i).FirstOrDefault<DAL.Instructor>();
          if (loAssessor == null)
            return (StudentState) null;
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          StudentState loStudent = trainingEntities.Students.Join((IEnumerable<AssignedAssessor>) trainingEntities.AssignedAssessors, (Expression<Func<DAL.Student, int>>) (s => s.ID), (Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID), (s, aus) => new
          {
            s = s,
            aus = aus
          }).Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, data => data.aus.InstructorID, (Expression<Func<DAL.Instructor, int>>) (i => i.ID), (data, i) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            i = i
          }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.\u003C\u003Eh__TransparentIdentifier0.s.UserID, (Expression<Func<UserModule, string>>) (um => um.UserID), (data, um) => new
          {
            \u003C\u003Eh__TransparentIdentifier1 = data,
            um = um
          }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.um.ID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (data, usm) => new
          {
            \u003C\u003Eh__TransparentIdentifier2 = data,
            usm = usm
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.ID == paStudentID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.aus.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.i.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.i.UserID == loAssessor.UserID && data.\u003C\u003Eh__TransparentIdentifier2.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.um.ModuleID == 10 && data.usm.IsStarted && data.usm.IsEnabled && data.usm.SubModuleID == 19).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType11<\u003C\u003Ef__AnonymousType10<\u003C\u003Ef__AnonymousType9<\u003C\u003Ef__AnonymousType8<DAL.Student, AssignedAssessor>, DAL.Instructor>, UserModule>, DAL.UserSubModule>, StudentState>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentState)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Student.set_ID)), )))); //unable to render the statement
          if (loStudent == null)
            return (StudentState) null;
          ParameterExpression parameterExpression2;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<UserAssessedFile> list = trainingEntities.Students.Join((IEnumerable<DAL.UserFile>) trainingEntities.UserFiles, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (s, uf) => new
          {
            s = s,
            uf = uf
          }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.s.UserID, (Expression<Func<UserModule, string>>) (um => um.UserID), (data, um) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            um = um
          }).Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, data => data.um.ID, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (data, usm) => new
          {
            \u003C\u003Eh__TransparentIdentifier1 = data,
            usm = usm
          }).Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.uf.ID, (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (data, usmf) => new
          {
            \u003C\u003Eh__TransparentIdentifier2 = data,
            usmf = usmf
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.ID == loStudent.ID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.UserID == loStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.s.ID == paStudentID && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.ModuleID == 10 && data.\u003C\u003Eh__TransparentIdentifier2.usm.SubModuleID == 19 && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.usm.IsStarted && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.uf.IsEnabled && data.usmf.IsEnabled && data.usmf.UserSubModuleID == data.\u003C\u003Eh__TransparentIdentifier2.usm.ID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType15<\u003C\u003Ef__AnonymousType14<\u003C\u003Ef__AnonymousType13<\u003C\u003Ef__AnonymousType12<DAL.Student, DAL.UserFile>, UserModule>, DAL.UserSubModule>, UserSubModuleFile>, UserAssessedFile>>((Expression) Expression.MemberInit(Expression.New(typeof (UserAssessedFile)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (UserAssessedFile.set_ID)), )))); //unable to render the statement
          loStudent.Files = new List<UserAssessedFile>();
          loStudent.Files = list;
          return loStudent;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (StudentState) null;
    }

    public static bool IsMyLearner(string paUserID, int paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
          return trainingEntities.Students.Join((IEnumerable<AssignedAssessor>) trainingEntities.AssignedAssessors, (Expression<Func<DAL.Student, int>>) (s => s.ID), (Expression<Func<AssignedAssessor, int>>) (aus => aus.StudentID), (s, aus) => new
          {
            s = s,
            aus = aus
          }).Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, data => data.aus.InstructorID, (Expression<Func<DAL.Instructor, int>>) (i => i.ID), (data, i) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            i = i
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.s.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.s.ID == paStudentID && data.\u003C\u003Eh__TransparentIdentifier0.aus.IsEnabled && data.i.IsEnabled && data.i.IsAssessor && !data.i.IsLocked && data.i.UserID == paUserID && data.\u003C\u003Eh__TransparentIdentifier0.aus.StudentID == paStudentID).Select(data => data.\u003C\u003Eh__TransparentIdentifier0.s).FirstOrDefault<DAL.Student>() != null;
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool MarkFileOK(string paUserID, int paStudentID, int paFileID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          DAL.Instructor loInstructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsAssessor && !i.IsLocked && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
          if (loStudent == null || loInstructor == null)
            return false;
          if (trainingEntities.AssignedAssessors.Join((IEnumerable<DAL.UserFile>) trainingEntities.UserFiles, (Expression<Func<AssignedAssessor, string>>) (aus => loStudent.UserID), (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (aus, uf) => new
          {
            aus = aus,
            uf = uf
          }).Where(data => data.aus.IsEnabled && data.aus.StudentID == paStudentID && data.aus.InstructorID == loInstructor.ID && data.uf.IsEnabled && data.uf.ID == paFileID).Select(data => data.uf).FirstOrDefault<DAL.UserFile>() == null)
            return false;
          UserSubModuleFile userSubModuleFile = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
          {
            um = um,
            usm = usm
          }).Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, data => data.usm.ID, (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserSubModuleID), (data, usmf) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            usmf = usmf
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == loStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == 10 && data.\u003C\u003Eh__TransparentIdentifier0.usm.SubModuleID == 19 && data.usmf.UserFileID == paFileID && data.usmf.IsEnabled).Select(data => data.usmf).FirstOrDefault<UserSubModuleFile>();
          if (userSubModuleFile == null)
            return false;
          userSubModuleFile.IsCorrect = true;
          userSubModuleFile.IsAssessed = true;
          userSubModuleFile.DateTimeAssessed = new DateTime?(DateTime.Now);
          userSubModuleFile.InstructorID = new int?(loInstructor.ID);
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool MarkFileNOK(string paUserID, int paStudentID, int paFileID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          DAL.Instructor loInstructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsAssessor && !i.IsLocked && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
          if (loStudent == null || loInstructor == null)
            return false;
          if (trainingEntities.AssignedAssessors.Join((IEnumerable<DAL.UserFile>) trainingEntities.UserFiles, (Expression<Func<AssignedAssessor, string>>) (aus => loStudent.UserID), (Expression<Func<DAL.UserFile, string>>) (uf => uf.UserID), (aus, uf) => new
          {
            aus = aus,
            uf = uf
          }).Where(data => data.aus.IsEnabled && data.aus.StudentID == paStudentID && data.aus.InstructorID == loInstructor.ID && data.uf.IsEnabled && data.uf.ID == paFileID).Select(data => data.uf).FirstOrDefault<DAL.UserFile>() == null)
            return false;
          UserSubModuleFile userSubModuleFile = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
          {
            um = um,
            usm = usm
          }).Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, data => data.usm.ID, (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserSubModuleID), (data, usmf) => new
          {
            \u003C\u003Eh__TransparentIdentifier0 = data,
            usmf = usmf
          }).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == loStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == 10 && data.\u003C\u003Eh__TransparentIdentifier0.usm.SubModuleID == 19 && data.usmf.UserFileID == paFileID && data.usmf.IsEnabled).Select(data => data.usmf).FirstOrDefault<UserSubModuleFile>();
          if (userSubModuleFile == null)
            return false;
          userSubModuleFile.IsCorrect = false;
          userSubModuleFile.IsAssessed = true;
          userSubModuleFile.DateTimeAssessed = new DateTime?(DateTime.Now);
          userSubModuleFile.InstructorID = new int?(loInstructor.ID);
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool FinalizeStudentOK(string paUserID, int paStudentID)
    {
      try
      {
        bool flag = true;
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsAssessor && !i.IsLocked && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
          if (loStudent == null || instructor == null)
            return false;
          UserModule loUserModule = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.UserID == loStudent.UserID && um.IsEnabled && um.IsStarted && um.ModuleID == 10)).FirstOrDefault<UserModule>();
          DAL.UserSubModule loUserSubModule = trainingEntities.UserSubModules.Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (Expression<Func<UserSubModuleFile, int>>) (usf => usf.UserSubModuleID), (usm, usf) => new
          {
            usm = usm,
            usf = usf
          }).Where(data => data.usm.UserModuleID == loUserModule.ID && data.usm.IsStarted && data.usm.IsEnabled && data.usm.SubModuleID == 19).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
          if (loUserSubModule != null)
          {
            DbSet<UserSubModuleFile> userSubModuleFiles = trainingEntities.UserSubModuleFiles;
            Expression<Func<UserSubModuleFile, bool>> predicate = (Expression<Func<UserSubModuleFile, bool>>) (usf => usf.UserSubModuleID == loUserSubModule.ID && usf.Attempt == loUserSubModule.Attempts && usf.IsEnabled);
            foreach (UserSubModuleFile userSubModuleFile in userSubModuleFiles.Where<UserSubModuleFile>(predicate).ToList<UserSubModuleFile>())
            {
              if (!userSubModuleFile.IsCorrect && !userSubModuleFile.IsAssessed)
                flag = false;
            }
            if (flag)
            {
              loUserSubModule.DateCompleted = new DateTime?(DateTime.Now);
              loUserSubModule.IsComplete = true;
              loUserSubModule.IsSuccessful = true;
              trainingEntities.SaveChanges();
              DAL.UserSubModule entity = new DAL.UserSubModule()
              {
                Attempts = 1,
                DateCreated = DateTime.Now,
                IsComplete = false,
                IsSuccessful = false,
                IsEnabled = true,
                IsStarted = true,
                SubModuleID = 20,
                UserModuleID = loUserModule.ID
              };
              trainingEntities.UserSubModules.Add(entity);
              trainingEntities.SaveChanges();
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool FinalizeStudentNOK(string paUserID, int paStudentID)
    {
      try
      {
        bool flag = true;
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsAssessor && !i.IsLocked && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
          if (loStudent == null || instructor == null)
            return false;
          UserModule loUserModule = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.UserID == loStudent.UserID && um.IsEnabled && um.IsStarted && um.ModuleID == 10)).FirstOrDefault<UserModule>();
          DAL.UserSubModule loUserSubModule = trainingEntities.UserSubModules.Join((IEnumerable<UserSubModuleFile>) trainingEntities.UserSubModuleFiles, (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (Expression<Func<UserSubModuleFile, int>>) (usf => usf.UserSubModuleID), (usm, usf) => new
          {
            usm = usm,
            usf = usf
          }).Where(data => data.usm.UserModuleID == loUserModule.ID && data.usm.IsStarted && data.usm.IsEnabled && data.usm.SubModuleID == 19).Select(data => data.usm).FirstOrDefault<DAL.UserSubModule>();
          if (loUserSubModule != null)
          {
            DbSet<UserSubModuleFile> userSubModuleFiles = trainingEntities.UserSubModuleFiles;
            Expression<Func<UserSubModuleFile, bool>> predicate = (Expression<Func<UserSubModuleFile, bool>>) (usf => usf.UserSubModuleID == loUserSubModule.ID && usf.Attempt == loUserSubModule.Attempts && usf.IsEnabled);
            foreach (UserSubModuleFile userSubModuleFile in userSubModuleFiles.Where<UserSubModuleFile>(predicate).ToList<UserSubModuleFile>())
            {
              if (!userSubModuleFile.IsCorrect && userSubModuleFile.IsAssessed)
                flag = false;
            }
            if (!flag)
            {
              if (loUserSubModule.Attempts >= 2)
              {
                loUserSubModule.IsComplete = true;
                loUserSubModule.DateCompleted = new DateTime?(DateTime.Now);
                loUserSubModule.IsSuccessful = false;
                loStudent.IsLocked = true;
                trainingEntities.SaveChanges();
              }
              else
              {
                loUserSubModule.Attempts = 2;
                trainingEntities.SaveChanges();
              }
              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool StudentIsFriend(int paStudentID, string paUserID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
          return student != null && instructor != null && student.ReferredUserID == instructor.UserID;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static List<StudentSummary> GetStudentReferredStudents(
      string paStudentID)
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Instructors.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<DAL.Instructor, string>>) (i => i.UserID), (Expression<Func<DAL.Student, string>>) (rs => rs.ReferredUserID), (i, rs) => new
          {
            i = i,
            rs = rs
          }).Where(data => data.i.IsEnabled && data.rs.IsEnabled && data.i.UserID == paStudentID).OrderByDescending(data => data.i.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType20<DAL.Instructor, DAL.Student>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentSummaryList;
    }
  }
}
