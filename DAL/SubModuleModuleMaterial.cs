﻿// Decompiled with JetBrains decompiler
// Type: DAL.SubModuleModuleMaterial
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

namespace DAL
{
  public class SubModuleModuleMaterial
  {
    public int SubModuleID { get; set; }

    public int SubModuleMaterialID { get; set; }

    public bool IsEnabled { get; set; }

    public virtual SubModule SubModule { get; set; }

    public virtual SubModuleMaterial SubModuleMaterial { get; set; }
  }
}
