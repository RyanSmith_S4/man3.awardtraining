﻿// Decompiled with JetBrains decompiler
// Type: DAL.UserSubModule
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;
using System.Collections.Generic;

namespace DAL
{
  public class UserSubModule
  {
    public UserSubModule()
    {
      this.UserSubModuleFiles = (ICollection<UserSubModuleFile>) new HashSet<UserSubModuleFile>();
      this.UserSubModuleMultipleChoiceAnswers = (ICollection<UserSubModuleMultipleChoiceAnswer>) new HashSet<UserSubModuleMultipleChoiceAnswer>();
      this.UserSubModuleTrueFalseAnswers = (ICollection<UserSubModuleTrueFalseAnswer>) new HashSet<UserSubModuleTrueFalseAnswer>();
    }

    public int ID { get; set; }

    public int UserModuleID { get; set; }

    public int SubModuleID { get; set; }

    public int Attempts { get; set; }

    public bool IsStarted { get; set; }

    public bool IsComplete { get; set; }

    public bool IsSuccessful { get; set; }

    public DateTime DateCreated { get; set; }

    public bool IsEnabled { get; set; }

    public DateTime? DateCompleted { get; set; }

    public int? Mark { get; set; }

    public int? Total { get; set; }

    public virtual SubModule SubModule { get; set; }

    public virtual UserModule UserModule { get; set; }

    public virtual ICollection<UserSubModuleFile> UserSubModuleFiles { get; set; }

    public virtual ICollection<UserSubModuleMultipleChoiceAnswer> UserSubModuleMultipleChoiceAnswers { get; set; }

    public virtual ICollection<UserSubModuleTrueFalseAnswer> UserSubModuleTrueFalseAnswers { get; set; }
  }
}
