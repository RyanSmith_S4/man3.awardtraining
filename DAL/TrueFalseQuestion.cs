﻿// Decompiled with JetBrains decompiler
// Type: DAL.TrueFalseQuestion
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Collections.Generic;

namespace DAL
{
  public class TrueFalseQuestion
  {
    public TrueFalseQuestion()
    {
      this.SubModuleTrueFalseQuestions = (ICollection<SubModuleTrueFalseQuestion>) new HashSet<SubModuleTrueFalseQuestion>();
      this.SubModuleTrueFalseQuestions1 = (ICollection<SubModuleTrueFalseQuestion>) new HashSet<SubModuleTrueFalseQuestion>();
      this.UserSubModuleTrueFalseAnswers = (ICollection<UserSubModuleTrueFalseAnswer>) new HashSet<UserSubModuleTrueFalseAnswer>();
    }

    public int ID { get; set; }

    public string Question { get; set; }

    public bool Answer { get; set; }

    public bool IsEnabled { get; set; }

    public virtual ICollection<SubModuleTrueFalseQuestion> SubModuleTrueFalseQuestions { get; set; }

    public virtual ICollection<SubModuleTrueFalseQuestion> SubModuleTrueFalseQuestions1 { get; set; }

    public virtual ICollection<UserSubModuleTrueFalseAnswer> UserSubModuleTrueFalseAnswers { get; set; }
  }
}
