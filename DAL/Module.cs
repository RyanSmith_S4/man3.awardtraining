﻿// Decompiled with JetBrains decompiler
// Type: DAL.Module
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Collections.Generic;

namespace DAL
{
  public class Module
  {
    public Module()
    {
      this.SubModules = (ICollection<SubModule>) new HashSet<SubModule>();
      this.UserModules = (ICollection<UserModule>) new HashSet<UserModule>();
    }

    public int ID { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public int Order { get; set; }

    public bool IsEnabled { get; set; }

    public virtual ICollection<SubModule> SubModules { get; set; }

    public virtual ICollection<UserModule> UserModules { get; set; }
  }
}
