﻿// Decompiled with JetBrains decompiler
// Type: DAL.AspNetRole
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Collections.Generic;

namespace DAL
{
  public class AspNetRole
  {
    public AspNetRole()
    {
      this.AspNetUsers = (ICollection<AspNetUser>) new HashSet<AspNetUser>();
    }

    public string Id { get; set; }

    public string Name { get; set; }

    public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
  }
}
