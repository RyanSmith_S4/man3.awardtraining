﻿// Decompiled with JetBrains decompiler
// Type: DAL.PageLog
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;

namespace DAL
{
  public class PageLog
  {
    public int ID { get; set; }

    public string UserID { get; set; }

    public string Page { get; set; }

    public DateTime DateTime_Created { get; set; }

    public virtual AspNetUser AspNetUser { get; set; }
  }
}
