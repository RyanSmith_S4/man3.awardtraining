﻿// Decompiled with JetBrains decompiler
// Type: DAL.BusinessTrainingEntities
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DAL
{
  public class BusinessTrainingEntities : DbContext
  {
    public BusinessTrainingEntities()
      : base("name=BusinessTrainingEntities")
    {
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      throw new UnintentionalCodeFirstException();
    }

    public virtual DbSet<Module> Modules { get; set; }

    public virtual DbSet<MultipleChoiceQuestion> MultipleChoiceQuestions { get; set; }

    public virtual DbSet<MultipleChoiceSubQuestion> MultipleChoiceSubQuestions { get; set; }

    public virtual DbSet<SubModule> SubModules { get; set; }

    public virtual DbSet<SubModuleMaterial> SubModuleMaterials { get; set; }

    public virtual DbSet<TrueFalseQuestion> TrueFalseQuestions { get; set; }

    public virtual DbSet<UserModule> UserModules { get; set; }

    public virtual DbSet<UserSubModuleMultipleChoiceAnswer> UserSubModuleMultipleChoiceAnswers { get; set; }

    public virtual DbSet<UserSubModuleTrueFalseAnswer> UserSubModuleTrueFalseAnswers { get; set; }

    public virtual DbSet<SubModuleModuleMaterial> SubModuleModuleMaterials { get; set; }

    public virtual DbSet<SubModuleMultipleChoiceQuestion> SubModuleMultipleChoiceQuestions { get; set; }

    public virtual DbSet<SubModuleTrueFalseQuestion> SubModuleTrueFalseQuestions { get; set; }

    public virtual DbSet<AspNetUser> AspNetUsers { get; set; }

    public virtual DbSet<EmailLog> EmailLogs { get; set; }

    public virtual DbSet<MaterialLog> MaterialLogs { get; set; }

    public virtual DbSet<SmsLog> SmsLogs { get; set; }

    public virtual DbSet<PageLog> PageLogs { get; set; }

    public virtual DbSet<AspNetRole> AspNetRoles { get; set; }

    public virtual DbSet<SystemLog> SystemLogs { get; set; }

    public virtual DbSet<InstructorFile> InstructorFiles { get; set; }

    public virtual DbSet<StudentFile> StudentFiles { get; set; }

    public virtual DbSet<UserFile> UserFiles { get; set; }

    public virtual DbSet<AssignedAssessor> AssignedAssessors { get; set; }

    public virtual DbSet<AssignedMentor> AssignedMentors { get; set; }

    public virtual DbSet<SponsorFsaCode> SponsorFsaCodes { get; set; }

    public virtual DbSet<Sponsor> Sponsors { get; set; }

    public virtual DbSet<ChatMessageLog> ChatMessageLogs { get; set; }

    public virtual DbSet<LearnerResource> LearnerResources { get; set; }

    public virtual DbSet<UserSubModuleFile> UserSubModuleFiles { get; set; }

    public virtual DbSet<GeneralChatMessageLog> GeneralChatMessageLogs { get; set; }

    public virtual DbSet<UserPasswordReset> UserPasswordResets { get; set; }

    public virtual DbSet<Student> Students { get; set; }

    public virtual DbSet<Instructor> Instructors { get; set; }

    public virtual DbSet<InactivityNotification> InactivityNotifications { get; set; }

    public virtual DbSet<UserSubModule> UserSubModules { get; set; }
  }
}
