﻿// Decompiled with JetBrains decompiler
// Type: DAL.InactivityNotification
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;

namespace DAL
{
  public class InactivityNotification
  {
    public int ID { get; set; }

    public string UserID { get; set; }

    public bool? NotificationOneSent { get; set; }

    public DateTime? NotificationOneDateTimeSent { get; set; }

    public bool? NotificationTwoSent { get; set; }

    public DateTime? NotificationTwoDateTimeSent { get; set; }

    public bool? NotificationThreeSent { get; set; }

    public DateTime? NotificationThreeDateTimeSent { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public bool IsEnabled { get; set; }
  }
}
