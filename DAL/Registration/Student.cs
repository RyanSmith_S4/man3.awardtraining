﻿// Decompiled with JetBrains decompiler
// Type: DAL.Registration.Student
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Registration
{
  public class Student
  {
    public static DAL.Student ConvertToEntity(COML.Classes.Student paStudent)
    {
      string str;
      switch (paStudent.SelectedRace)
      {
        case Enumerations.enumRace.African:
          str = "A";
          break;
        case Enumerations.enumRace.Coloured:
          str = "C";
          break;
        case Enumerations.enumRace.Indian:
          str = "I";
          break;
        case Enumerations.enumRace.White:
          str = "W";
          break;
        case Enumerations.enumRace.Other:
          str = "O";
          break;
        default:
          str = "O";
          break;
      }
      return new DAL.Student()
      {
        Age = paStudent.Age,
        EmailAddress = paStudent.EmailAddress,
        CurrentlyEmployed = paStudent.Employed,
        DateTime_Created = paStudent.DateTimeCreated,
        FirstNames = paStudent.FirstName,
        LastNames = paStudent.LastName,
        Nickname = paStudent.Nickname,
        FSACode = paStudent.FsaCode,
        Gender = paStudent.SelectGender == Enumerations.enumGender.Male ? "M" : "F",
        HighestQualification = paStudent.HighestQualification,
        HighestQualificationTitle = paStudent.HighestQualificationTitle,
        HomeLanguage = paStudent.HomeLanguage,
        IdentityNumber = paStudent.IdentityNumber,
        LastActivity = paStudent.LastActivity,
        PostalAddress1 = paStudent.PostalAddress1,
        PostalAddress2 = paStudent.PostalAddress2,
        PostalAddress3 = paStudent.PostalAddress3,
        PostalCity = paStudent.PostalAddressCity,
        PostalCode = paStudent.PostalAddressCode,
        PostalProvince = paStudent.PostalAddressProvince,
        ReferralCode = paStudent.ReferralCode,
        ReferredUserID = paStudent.ReferralUser,
        ReferralStudentID = new int?(paStudent.ReferralStudent),
        TelephoneCell = paStudent.TelephoneCell,
        UserID = paStudent.UserID,
        Race = str,
        IsEnabled = paStudent.IsEnabled,
        IsLocked = paStudent.IsLocked,
        IsChatEnabled = new bool?(paStudent.IsChatEnabled)
      };
    }

    public static bool RegisterStudent(COML.Classes.Student paStudent)
    {
      bool flag = true;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student entity1 = Student.ConvertToEntity(paStudent);
          trainingEntities.Students.Add(entity1);
          trainingEntities.SaveChanges();
          if (Settings.AutoAssignLearnersToAssessor)
          {
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsApproved && i.IsAssessor && i.FirstNames.Contains("Graham") && i.LastNames.Contains("Ward") && i.EmailAddress.Contains("graham"))).FirstOrDefault<DAL.Instructor>();
            if (instructor != null)
            {
              AssignedAssessor entity2 = new AssignedAssessor()
              {
                InstructorID = instructor.ID,
                StudentID = entity1.ID,
                DateTimeCreated = DateTime.Now,
                IsEnabled = true
              };
              trainingEntities.AssignedAssessors.Add(entity2);
              trainingEntities.SaveChanges();
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log("Error registering student: " + ex.Message.ToString() + " Exception: " + (object) ex, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      Logging.Log("RegisterStudent: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag;
    }

    public static bool IDNumberExists(string paIDNumber)
    {
      bool flag = false;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<DAL.Student> list = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (t1 => t1.IdentityNumber == paIDNumber)).ToList<DAL.Student>();
          flag = list != null && list.Count != 0;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      Logging.Log("IDNumberExists: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag;
    }

    public static bool IsLocked(string paStudentID)
    {
      bool flag = true;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
          flag = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (i => i.IsEnabled && i.UserID == paStudentID)).FirstOrDefault<DAL.Student>() == null;
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool UpdateStudentFsaCode(int paStudentID, string paFsaCode)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (i => i.IsEnabled && i.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            student.FSACode = paFsaCode;
            trainingEntities.SaveChanges();
            flag = true;
          }
          else
            flag = false;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool UpdateStudentByID(COML.Classes.Student paStudent)
    {
      try
      {
        string str;
        switch (paStudent.SelectedRace)
        {
          case Enumerations.enumRace.African:
            str = "A";
            break;
          case Enumerations.enumRace.Coloured:
            str = "C";
            break;
          case Enumerations.enumRace.Indian:
            str = "I";
            break;
          case Enumerations.enumRace.White:
            str = "W";
            break;
          case Enumerations.enumRace.Other:
            str = "O";
            break;
          default:
            str = "O";
            break;
        }
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudent.ID)).FirstOrDefault<DAL.Student>();
          if (loStudent.EmailAddress != paStudent.EmailAddress)
          {
            List<AspNetUser> list = trainingEntities.AspNetUsers.Where<AspNetUser>((Expression<Func<AspNetUser, bool>>) (ut => ut.Email == paStudent.EmailAddress)).ToList<AspNetUser>();
            if (list == null || list.Count == 0)
            {
              AspNetUser aspNetUser = trainingEntities.AspNetUsers.Where<AspNetUser>((Expression<Func<AspNetUser, bool>>) (u => u.Id == loStudent.UserID)).FirstOrDefault<AspNetUser>();
              if (aspNetUser != null)
              {
                aspNetUser.Email = paStudent.EmailAddress;
                aspNetUser.UserName = paStudent.EmailAddress;
                loStudent.EmailAddress = paStudent.EmailAddress;
              }
            }
          }
          loStudent.Age = paStudent.Age;
          loStudent.CurrentlyEmployed = paStudent.Employed;
          loStudent.Gender = paStudent.SelectGender == Enumerations.enumGender.Male ? "M" : "F";
          loStudent.Race = str;
          loStudent.HighestQualification = paStudent.HighestQualification;
          loStudent.HighestQualificationTitle = paStudent.HighestQualificationTitle;
          loStudent.HomeLanguage = paStudent.HomeLanguage;
          loStudent.Nickname = paStudent.Nickname;
          loStudent.PostalAddress1 = paStudent.PostalAddress1;
          loStudent.PostalAddress2 = paStudent.PostalAddress2;
          loStudent.PostalAddress3 = paStudent.PostalAddress3;
          loStudent.PostalCity = paStudent.PostalAddressCity;
          loStudent.PostalCode = paStudent.PostalAddressCode;
          loStudent.PostalProvince = paStudent.PostalAddressProvince;
          loStudent.TelephoneCell = paStudent.TelephoneCell;
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message.ToString(), Enumerations.LogLevel.Error, ex);
        return false;
      }
    }
  }
}
