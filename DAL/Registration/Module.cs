﻿// Decompiled with JetBrains decompiler
// Type: DAL.Registration.Module
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using System;
using System.Linq;

namespace DAL.Registration
{
  public class Module
  {
    public static bool RegisterStudentModule(string paStudentID)
    {
      bool flag1 = true;
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          foreach (COML.Classes.Module module in DAL.Core.Training.Module.GetModules())
          {
            if (module.Order == 1)
            {
              bool flag2 = module.Order == 1;
              UserModule entity1 = new UserModule()
              {
                IsComplete = false,
                IsEnabled = true,
                IsStarted = flag2,
                IsSuccessful = false,
                DateCreated = DateTime.Now,
                ModuleID = module.ID,
                UserID = paStudentID
              };
              module.SubModules.OrderBy<COML.Classes.SubModule, int>((Func<COML.Classes.SubModule, int>) (o => o.Order));
              trainingEntities.UserModules.Add(entity1);
              trainingEntities.SaveChanges();
              if (entity1 != null && entity1.ID > 0)
              {
                DAL.UserSubModule entity2 = new DAL.UserSubModule()
                {
                  SubModuleID = module.SubModules[0].ID,
                  DateCreated = DateTime.Now,
                  IsComplete = false,
                  IsEnabled = true,
                  IsStarted = true,
                  IsSuccessful = false,
                  Attempts = 1,
                  UserModuleID = entity1.ID
                };
                trainingEntities.UserSubModules.Add(entity2);
                trainingEntities.SaveChanges();
                if (entity2 != null && entity2.ID > 0)
                  flag1 = true;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log("Error setting learner modules. Error: " + ex.Message + " Exception: " + (object) ex, Enumerations.LogLevel.Error, ex);
        flag1 = false;
      }
      Logging.Log("RegisterStudentModule: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return flag1;
    }
  }
}
