﻿// Decompiled with JetBrains decompiler
// Type: DAL.MultipleChoiceQuestion
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Collections.Generic;

namespace DAL
{
  public class MultipleChoiceQuestion
  {
    public MultipleChoiceQuestion()
    {
      this.SubModuleMultipleChoiceQuestions = (ICollection<SubModuleMultipleChoiceQuestion>) new HashSet<SubModuleMultipleChoiceQuestion>();
      this.SubModuleMultipleChoiceQuestions1 = (ICollection<SubModuleMultipleChoiceQuestion>) new HashSet<SubModuleMultipleChoiceQuestion>();
    }

    public int ID { get; set; }

    public string Question { get; set; }

    public int Answer { get; set; }

    public bool IsEnabled { get; set; }

    public virtual ICollection<SubModuleMultipleChoiceQuestion> SubModuleMultipleChoiceQuestions { get; set; }

    public virtual ICollection<SubModuleMultipleChoiceQuestion> SubModuleMultipleChoiceQuestions1 { get; set; }
  }
}
