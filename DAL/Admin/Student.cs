﻿// Decompiled with JetBrains decompiler
// Type: DAL.Admin.Student
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;

namespace DAL.Admin
{
  public class Student
  {
    public static List<StudentSummary> GetStudentReferredStudents(
      string paStudentID)
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<DAL.Student, string>>) (rs => rs.ReferredUserID), (s, rs) => new
          {
            s = s,
            rs = rs
          }).Where(data => data.s.IsEnabled && data.rs.IsEnabled && data.s.UserID == paStudentID).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType21<DAL.Student, DAL.Student>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentSummaryList;
    }

    public static List<StudentSummary> GetStudents()
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList;
    }

    public static List<StudentSummary> GetAllStudents()
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList;
    }

    public static List<StudentSummary> GetStudentsOrderedByName()
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled).OrderBy(data => data.s.FirstNames).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList;
    }

    public static List<StudentSummaryDetailed> GetAllStudentsDetailed()
    {
      List<StudentSummaryDetailed> studentSummaryDetailedList = new List<StudentSummaryDetailed>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<StudentSummaryDetailed> list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummaryDetailed>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailed)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailed.set_StudentID)), )))); //unable to render the statement
          foreach (StudentSummaryDetailed studentSummaryDetailed in list)
          {
            StudentSummaryDetailed lpStudent = studentSummaryDetailed;
            string str1 = "";
            string str2 = "";
            string race = lpStudent.Race;
            if (!(race == "W"))
            {
              if (!(race == "A"))
              {
                if (!(race == "I"))
                {
                  if (!(race == "O"))
                  {
                    if (race == "C")
                      str2 = "Coloured";
                  }
                  else
                    str2 = "Other";
                }
                else
                  str2 = "Indian";
              }
              else
                str2 = "African";
            }
            else
              str2 = "White";
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Instructor>();
            if (instructor != null)
            {
              str1 = instructor.FirstNames + " " + instructor.LastNames;
            }
            else
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (l => l.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
                str1 = student.FirstNames + " " + student.LastNames;
            }
            lpStudent.ReferredFriend = str1;
            lpStudent.Race = str2;
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            StudentSummaryDetailedModule loCurrentModule = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
            {
              um = um,
              m = m
            }).OrderByDescending(data => data.um.DateCreated).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.IsStarted && data.um.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType22<UserModule, DAL.Module>, StudentSummaryDetailedModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedModule.set_ModuleID)), )))); //unable to render the statement
            if (loCurrentModule != null)
            {
              lpStudent.CurrentModule = loCurrentModule.Name;
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              StudentSummaryDetailedSubModule detailedSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
              {
                um = um,
                usm = usm
              }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                sm = sm
              }).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.usm.DateCreated).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == lpStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ID == loCurrentModule.UserModuleID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loCurrentModule.ModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.UserModuleID == loCurrentModule.UserModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, StudentSummaryDetailedSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedSubModule.set_SubModuleID)), )))); //unable to render the statement
              lpStudent.CurrentSubModule = detailedSubModule == null ? "" : (detailedSubModule.Code == "" ? detailedSubModule.Name : detailedSubModule.Code + " - " + detailedSubModule.Name);
            }
            else
            {
              lpStudent.CurrentModule = "";
              lpStudent.CurrentSubModule = "";
            }
          }
          studentSummaryDetailedList = list;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentSummaryDetailedList;
    }

    public static List<StudentSummaryDetailed> GetStudentsDetailed()
    {
      List<StudentSummaryDetailed> studentSummaryDetailedList = new List<StudentSummaryDetailed>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<StudentSummaryDetailed> list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummaryDetailed>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailed)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailed.set_StudentID)), )))); //unable to render the statement
          foreach (StudentSummaryDetailed studentSummaryDetailed in list)
          {
            StudentSummaryDetailed lpStudent = studentSummaryDetailed;
            string str1 = "";
            string str2 = "";
            string race = lpStudent.Race;
            if (!(race == "W"))
            {
              if (!(race == "A"))
              {
                if (!(race == "I"))
                {
                  if (!(race == "O"))
                  {
                    if (race == "C")
                      str2 = "Coloured";
                  }
                  else
                    str2 = "Other";
                }
                else
                  str2 = "Indian";
              }
              else
                str2 = "African";
            }
            else
              str2 = "White";
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Instructor>();
            if (instructor != null)
            {
              str1 = instructor.FirstNames + " " + instructor.LastNames;
            }
            else
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (l => l.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
                str1 = student.FirstNames + " " + student.LastNames;
            }
            lpStudent.ReferredFriend = str1;
            lpStudent.Race = str2;
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            StudentSummaryDetailedModule loCurrentModule = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
            {
              um = um,
              m = m
            }).OrderByDescending(data => data.um.DateCreated).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.IsStarted && data.um.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType22<UserModule, DAL.Module>, StudentSummaryDetailedModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedModule.set_ModuleID)), )))); //unable to render the statement
            if (loCurrentModule != null)
            {
              lpStudent.CurrentModule = loCurrentModule.Name;
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              StudentSummaryDetailedSubModule detailedSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
              {
                um = um,
                usm = usm
              }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                sm = sm
              }).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.usm.DateCreated).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == lpStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ID == loCurrentModule.UserModuleID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loCurrentModule.ModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.UserModuleID == loCurrentModule.UserModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, StudentSummaryDetailedSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedSubModule.set_SubModuleID)), )))); //unable to render the statement
              lpStudent.CurrentSubModule = detailedSubModule == null ? "" : (detailedSubModule.Code == "" ? detailedSubModule.Name : detailedSubModule.Code + " - " + detailedSubModule.Name);
            }
            else
            {
              lpStudent.CurrentModule = "";
              lpStudent.CurrentSubModule = "";
            }
          }
          studentSummaryDetailedList = list;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return studentSummaryDetailedList;
    }

    public static List<StudentSummaryDetailed> GetStudentsActiveInPeriod(
      DateTime paDateTimeStart,
      DateTime paDateTimeStop)
    {
      List<StudentSummaryDetailed> studentSummaryDetailedList = new List<StudentSummaryDetailed>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<StudentSummaryDetailed> list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled && data.s.LastActivity >= paDateTimeStart && data.s.LastActivity <= paDateTimeStop).OrderByDescending(data => data.s.LastActivity).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummaryDetailed>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailed)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailed.set_StudentID)), )))); //unable to render the statement
          foreach (StudentSummaryDetailed studentSummaryDetailed in list)
          {
            StudentSummaryDetailed lpStudent = studentSummaryDetailed;
            string str1 = "";
            string str2 = "";
            string race = lpStudent.Race;
            if (!(race == "W"))
            {
              if (!(race == "A"))
              {
                if (!(race == "I"))
                {
                  if (!(race == "O"))
                  {
                    if (race == "C")
                      str2 = "Coloured";
                  }
                  else
                    str2 = "Other";
                }
                else
                  str2 = "Indian";
              }
              else
                str2 = "African";
            }
            else
              str2 = "White";
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Instructor>();
            if (instructor != null)
            {
              str1 = instructor.FirstNames + " " + instructor.LastNames;
            }
            else
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (l => l.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
                str1 = student.FirstNames + " " + student.LastNames;
            }
            lpStudent.ReferredFriend = str1;
            lpStudent.Race = str2;
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            StudentSummaryDetailedModule loCurrentModule = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
            {
              um = um,
              m = m
            }).OrderByDescending(data => data.um.DateCreated).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.IsStarted && data.um.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType22<UserModule, DAL.Module>, StudentSummaryDetailedModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedModule.set_ModuleID)), )))); //unable to render the statement
            if (loCurrentModule != null)
            {
              lpStudent.CurrentModule = loCurrentModule.Name;
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              StudentSummaryDetailedSubModule detailedSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
              {
                um = um,
                usm = usm
              }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                sm = sm
              }).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.usm.DateCreated).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == lpStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ID == loCurrentModule.UserModuleID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loCurrentModule.ModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.UserModuleID == loCurrentModule.UserModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, StudentSummaryDetailedSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedSubModule.set_SubModuleID)), )))); //unable to render the statement
              lpStudent.CurrentSubModule = detailedSubModule == null ? "" : (detailedSubModule.Code == "" ? detailedSubModule.Name : detailedSubModule.Code + " - " + detailedSubModule.Name);
            }
            else
            {
              lpStudent.CurrentModule = "";
              lpStudent.CurrentSubModule = "";
            }
          }
          studentSummaryDetailedList = list;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("Get Students Detailed: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryDetailedList;
    }

    public static List<StudentSummaryDetailed> GetInactiveStudentsDetailed()
    {
      List<StudentSummaryDetailed> studentSummaryDetailedList = new List<StudentSummaryDetailed>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<StudentSummaryDetailed> list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => !data.s.IsEnabled).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummaryDetailed>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailed)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailed.set_StudentID)), )))); //unable to render the statement
          foreach (StudentSummaryDetailed studentSummaryDetailed in list)
          {
            StudentSummaryDetailed lpStudent = studentSummaryDetailed;
            string str1 = "";
            string str2 = "";
            string race = lpStudent.Race;
            if (!(race == "W"))
            {
              if (!(race == "A"))
              {
                if (!(race == "I"))
                {
                  if (!(race == "O"))
                  {
                    if (race == "C")
                      str2 = "Coloured";
                  }
                  else
                    str2 = "Other";
                }
                else
                  str2 = "Indian";
              }
              else
                str2 = "African";
            }
            else
              str2 = "White";
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Instructor>();
            if (instructor != null)
            {
              str1 = instructor.FirstNames + " " + instructor.LastNames;
            }
            else
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (l => l.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
                str1 = student.FirstNames + " " + student.LastNames;
            }
            lpStudent.ReferredFriend = str1;
            lpStudent.Race = str2;
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            StudentSummaryDetailedModule loCurrentModule = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
            {
              um = um,
              m = m
            }).OrderByDescending(data => data.um.DateCreated).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.IsStarted && data.um.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType22<UserModule, DAL.Module>, StudentSummaryDetailedModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedModule.set_ModuleID)), )))); //unable to render the statement
            if (loCurrentModule != null)
            {
              lpStudent.CurrentModule = loCurrentModule.Name;
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              StudentSummaryDetailedSubModule detailedSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
              {
                um = um,
                usm = usm
              }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                sm = sm
              }).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.usm.DateCreated).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == lpStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ID == loCurrentModule.UserModuleID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loCurrentModule.ModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.UserModuleID == loCurrentModule.UserModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, StudentSummaryDetailedSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedSubModule.set_SubModuleID)), )))); //unable to render the statement
              lpStudent.CurrentSubModule = detailedSubModule == null ? "" : (detailedSubModule.Code == "" ? detailedSubModule.Name : detailedSubModule.Code + " - " + detailedSubModule.Name);
            }
            else
            {
              lpStudent.CurrentModule = "";
              lpStudent.CurrentSubModule = "";
            }
          }
          studentSummaryDetailedList = list;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("Get Students Detailed: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryDetailedList;
    }

    public static List<StudentSummaryDetailed> GetLockedStudentsDetailed()
    {
      List<StudentSummaryDetailed> studentSummaryDetailedList = new List<StudentSummaryDetailed>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<StudentSummaryDetailed> list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled && data.s.IsLocked).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummaryDetailed>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailed)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailed.set_StudentID)), )))); //unable to render the statement
          foreach (StudentSummaryDetailed studentSummaryDetailed in list)
          {
            StudentSummaryDetailed lpStudent = studentSummaryDetailed;
            string str1 = "";
            string str2 = "";
            string race = lpStudent.Race;
            if (!(race == "W"))
            {
              if (!(race == "A"))
              {
                if (!(race == "I"))
                {
                  if (!(race == "O"))
                  {
                    if (race == "C")
                      str2 = "Coloured";
                  }
                  else
                    str2 = "Other";
                }
                else
                  str2 = "Indian";
              }
              else
                str2 = "African";
            }
            else
              str2 = "White";
            DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Instructor>();
            if (instructor != null)
            {
              str1 = instructor.FirstNames + " " + instructor.LastNames;
            }
            else
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (l => l.UserID == lpStudent.ReferredFriendUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
                str1 = student.FirstNames + " " + student.LastNames;
            }
            lpStudent.ReferredFriend = str1;
            lpStudent.Race = str2;
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            StudentSummaryDetailedModule loCurrentModule = trainingEntities.UserModules.Join((IEnumerable<DAL.Module>) trainingEntities.Modules, (Expression<Func<UserModule, int>>) (um => um.ModuleID), (Expression<Func<DAL.Module, int>>) (m => m.ID), (um, m) => new
            {
              um = um,
              m = m
            }).OrderByDescending(data => data.um.DateCreated).Where(data => data.um.IsEnabled && data.m.IsEnabled && data.um.IsStarted && data.um.UserID == lpStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType22<UserModule, DAL.Module>, StudentSummaryDetailedModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedModule.set_ModuleID)), )))); //unable to render the statement
            if (loCurrentModule != null)
            {
              lpStudent.CurrentModule = loCurrentModule.Name;
              ParameterExpression parameterExpression3;
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              // ISSUE: method reference
              StudentSummaryDetailedSubModule detailedSubModule = trainingEntities.UserModules.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserModule, int>>) (um => um.ID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.UserModuleID), (um, usm) => new
              {
                um = um,
                usm = usm
              }).Join((IEnumerable<DAL.SubModule>) trainingEntities.SubModules, data => data.usm.SubModuleID, (Expression<Func<DAL.SubModule, int>>) (sm => sm.ID), (data, sm) => new
              {
                \u003C\u003Eh__TransparentIdentifier0 = data,
                sm = sm
              }).OrderByDescending(data => data.\u003C\u003Eh__TransparentIdentifier0.usm.DateCreated).Where(data => data.\u003C\u003Eh__TransparentIdentifier0.um.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.usm.IsStarted && data.sm.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier0.um.IsStarted && data.\u003C\u003Eh__TransparentIdentifier0.um.UserID == lpStudent.UserID && data.\u003C\u003Eh__TransparentIdentifier0.um.ID == loCurrentModule.UserModuleID && data.\u003C\u003Eh__TransparentIdentifier0.um.ModuleID == loCurrentModule.ModuleID && data.\u003C\u003Eh__TransparentIdentifier0.usm.UserModuleID == loCurrentModule.UserModuleID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType23<\u003C\u003Ef__AnonymousType17<UserModule, DAL.UserSubModule>, DAL.SubModule>, StudentSummaryDetailedSubModule>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummaryDetailedSubModule)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummaryDetailedSubModule.set_SubModuleID)), )))); //unable to render the statement
              lpStudent.CurrentSubModule = detailedSubModule == null ? "" : (detailedSubModule.Code == "" ? detailedSubModule.Name : detailedSubModule.Code + " - " + detailedSubModule.Name);
            }
            else
            {
              lpStudent.CurrentModule = "";
              lpStudent.CurrentSubModule = "";
            }
          }
          studentSummaryDetailedList = list;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("Get Students Detailed: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryDetailedList;
    }

    public static List<StudentSummary> GetStudentsForSponsor(int paSponsorID)
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> loSponsorFsaCodes = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID)).Select<SponsorFsaCode, string>((Expression<Func<SponsorFsaCode, string>>) (f => f.FsaCode)).ToList<string>();
          if (loSponsorFsaCodes.Contains("DEFAULT"))
            loSponsorFsaCodes.Add("SYSTEM");
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled && loSponsorFsaCodes.Contains(data.s.FSACode)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList;
    }

    public static List<StudentSummary> GetStudentsByIDNumber(string paWord)
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IdentityNumber.Contains(paWord)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList;
    }

    public static List<StudentSummary> GetStudentsByIDNumberForSponsor(
      int paSponsorID,
      string paWord)
    {
      List<StudentSummary> studentSummaryList = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> loSponsorFsaCodes = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID)).Select<SponsorFsaCode, string>((Expression<Func<SponsorFsaCode, string>>) (f => f.FsaCode)).ToList<string>();
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          studentSummaryList = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
          {
            s = s,
            u = u
          }).Where(data => data.s.IsEnabled && data.s.IdentityNumber.Contains(paWord) && loSponsorFsaCodes.Contains(data.s.FSACode)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList;
    }

    public static List<StudentSummary> GetStudentsByIDNumberAndCodeForSponsor(
      int paSponsorID,
      string paWord,
      string paCode)
    {
      List<StudentSummary> studentSummaryList1 = new List<StudentSummary>();
      DateTime now = DateTime.Now;
      List<StudentSummary> studentSummaryList2 = new List<StudentSummary>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> loSponsorFsaCodes = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (f => f.IsEnabled && f.SponsorID == paSponsorID)).Select<SponsorFsaCode, string>((Expression<Func<SponsorFsaCode, string>>) (f => f.FsaCode.ToUpper())).ToList<string>();
          if (loSponsorFsaCodes.Contains("DEFAULT"))
            loSponsorFsaCodes.Add("SYSTEM");
          if (paCode != null && paCode != "")
            loSponsorFsaCodes.RemoveAll((Predicate<string>) (x => x != paCode));
          List<StudentSummary> list;
          if (paWord != null && paWord != "")
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
            {
              s = s,
              u = u
            }).Where(data => data.s.IsEnabled && data.s.IdentityNumber.Contains(paWord) && loSponsorFsaCodes.Contains(data.s.FSACode.ToUpper())).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
          }
          else
          {
            ParameterExpression parameterExpression;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            list = trainingEntities.Students.Join((IEnumerable<AspNetUser>) trainingEntities.AspNetUsers, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<AspNetUser, string>>) (u => u.Id), (s, u) => new
            {
              s = s,
              u = u
            }).Where(data => data.s.IsEnabled && loSponsorFsaCodes.Contains(data.s.FSACode)).OrderByDescending(data => data.s.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType5<DAL.Student, AspNetUser>, StudentSummary>>((Expression) Expression.MemberInit(Expression.New(typeof (StudentSummary)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (StudentSummary.set_StudentID)), )))); //unable to render the statement
          }
          studentSummaryList1 = list;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      COML.Logging.Log("GetStudents: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
      return studentSummaryList1;
    }

    public static bool UnlockStudent(int paStudentID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (loStudent != null)
          {
            UserModule loUserModule = trainingEntities.UserModules.Where<UserModule>((Expression<Func<UserModule, bool>>) (um => um.IsEnabled && um.UserID == loStudent.UserID && um.IsStarted && !um.IsSuccessful)).FirstOrDefault<UserModule>();
            if (loUserModule != null)
            {
              DAL.UserSubModule loUserSubModule = trainingEntities.UserSubModules.Where<DAL.UserSubModule>((Expression<Func<DAL.UserSubModule, bool>>) (usm => usm.IsEnabled && usm.UserModuleID == loUserModule.ID && !usm.IsSuccessful && usm.Attempts >= 2 && usm.IsComplete)).FirstOrDefault<DAL.UserSubModule>();
              if (loUserSubModule != null)
              {
                List<UserSubModuleMultipleChoiceAnswer> list1 = trainingEntities.UserSubModuleMultipleChoiceAnswers.Where<UserSubModuleMultipleChoiceAnswer>((Expression<Func<UserSubModuleMultipleChoiceAnswer, bool>>) (umc => umc.IsEnabled && umc.UserSubModuleID == loUserSubModule.ID)).ToList<UserSubModuleMultipleChoiceAnswer>();
                List<UserSubModuleTrueFalseAnswer> list2 = trainingEntities.UserSubModuleTrueFalseAnswers.Where<UserSubModuleTrueFalseAnswer>((Expression<Func<UserSubModuleTrueFalseAnswer, bool>>) (utf => utf.IsEnabled && utf.UserSubModuleID == loUserSubModule.ID)).ToList<UserSubModuleTrueFalseAnswer>();
                foreach (UserSubModuleMultipleChoiceAnswer multipleChoiceAnswer in list1)
                  multipleChoiceAnswer.IsEnabled = false;
                foreach (UserSubModuleTrueFalseAnswer moduleTrueFalseAnswer in list2)
                  moduleTrueFalseAnswer.IsEnabled = false;
                loUserSubModule.IsComplete = false;
                loUserSubModule.IsStarted = true;
                loUserSubModule.IsSuccessful = false;
                loUserSubModule.Attempts = 1;
                loUserSubModule.DateCompleted = new DateTime?();
                loStudent.IsLocked = false;
                trainingEntities.SaveChanges();
                flag = true;
                if (trainingEntities.UserSubModuleFiles.Join((IEnumerable<DAL.UserFile>) trainingEntities.UserFiles, (Expression<Func<UserSubModuleFile, int>>) (usmf => usmf.UserFileID), (Expression<Func<DAL.UserFile, int>>) (uf => uf.ID), (usmf, uf) => new
                {
                  usmf = usmf,
                  uf = uf
                }).Where(data => data.uf.UserID == loStudent.UserID).Select(data => data.usmf).ToList<UserSubModuleFile>().Count <= 0)
                  ;
              }
              else if (loStudent.IsLocked)
              {
                loStudent.IsLocked = false;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else if (loStudent.IsLocked)
            {
              loStudent.IsLocked = false;
              trainingEntities.SaveChanges();
              flag = true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool DisableChat(int paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (student == null)
            return false;
          student.IsChatEnabled = new bool?(false);
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool EnableChat(int paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (student == null)
            return false;
          student.IsChatEnabled = new bool?(true);
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool DisableAccount(int paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (loStudent == null)
            return false;
          DbSet<InactivityNotification> inactivityNotifications = trainingEntities.InactivityNotifications;
          Expression<Func<InactivityNotification, bool>> predicate = (Expression<Func<InactivityNotification, bool>>) (un => un.UserID == loStudent.UserID);
          foreach (InactivityNotification inactivityNotification in inactivityNotifications.Where<InactivityNotification>(predicate).ToList<InactivityNotification>())
          {
            inactivityNotification.IsEnabled = false;
            inactivityNotification.NotificationOneDateTimeSent = new DateTime?();
            inactivityNotification.NotificationTwoDateTimeSent = new DateTime?();
            inactivityNotification.NotificationThreeDateTimeSent = new DateTime?();
            inactivityNotification.NotificationOneSent = new bool?();
            inactivityNotification.NotificationTwoSent = new bool?();
            inactivityNotification.NotificationThreeSent = new bool?();
          }
          loStudent.IsEnabled = false;
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool ClearNotifications(string paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<InactivityNotification> list = trainingEntities.InactivityNotifications.Where<InactivityNotification>((Expression<Func<InactivityNotification, bool>>) (n => n.IsEnabled && n.UserID == paStudentID)).ToList<InactivityNotification>();
          if (list != null && list.Count > 0)
          {
            foreach (InactivityNotification inactivityNotification in list)
              inactivityNotification.IsEnabled = false;
            trainingEntities.SaveChanges();
          }
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool DisableInactiveAccounts()
    {
      DateTime loLastDate = DateTime.Now.AddDays(-75.0);
      DateTime loMiddleDate = DateTime.Now.AddDays(-60.0);
      DateTime loFirstDate = DateTime.Now.AddDays(-30.0);
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> loNotified = trainingEntities.InactivityNotifications.Where<InactivityNotification>((Expression<Func<InactivityNotification, bool>>) (un => un.IsEnabled)).Select<InactivityNotification, string>((Expression<Func<InactivityNotification, string>>) (un => un.UserID)).ToList<string>();
          DbSet<DAL.Student> students = trainingEntities.Students;
          Expression<Func<DAL.Student, bool>> predicate = (Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked && !loNotified.Contains(s.UserID));
          foreach (DAL.Student student in students.Where<DAL.Student>(predicate).ToList<DAL.Student>())
          {
            InactivityNotification entity = new InactivityNotification()
            {
              UserID = student.UserID,
              IsEnabled = true,
              DateTimeCreated = DateTime.Now
            };
            trainingEntities.InactivityNotifications.Add(entity);
          }
          trainingEntities.SaveChanges();
          List<InactivityNotification> list1 = trainingEntities.InactivityNotifications.Where<InactivityNotification>((Expression<Func<InactivityNotification, bool>>) (an => an.IsEnabled)).ToList<InactivityNotification>();
          List<DAL.Student> list2 = trainingEntities.Students.Join((IEnumerable<InactivityNotification>) trainingEntities.InactivityNotifications, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<InactivityNotification, string>>) (un => un.UserID), (s, un) => new
          {
            s = s,
            un = un
          }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.un.IsEnabled && data.s.LastActivity <= loFirstDate && data.s.LastActivity > loMiddleDate).Select(data => data.s).ToList<DAL.Student>();
          List<DAL.Student> list3 = trainingEntities.Students.Join((IEnumerable<InactivityNotification>) trainingEntities.InactivityNotifications, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<InactivityNotification, string>>) (un => un.UserID), (s, un) => new
          {
            s = s,
            un = un
          }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.un.IsEnabled && data.s.LastActivity <= loMiddleDate && data.s.LastActivity > loLastDate).Select(data => data.s).ToList<DAL.Student>();
          List<DAL.Student> list4 = trainingEntities.Students.Join((IEnumerable<InactivityNotification>) trainingEntities.InactivityNotifications, (Expression<Func<DAL.Student, string>>) (s => s.UserID), (Expression<Func<InactivityNotification, string>>) (un => un.UserID), (s, un) => new
          {
            s = s,
            un = un
          }).Where(data => data.s.IsEnabled && !data.s.IsLocked && data.un.IsEnabled && data.s.LastActivity <= loLastDate).Select(data => data.s).ToList<DAL.Student>();
          foreach (DAL.Student student in list2)
          {
            foreach (InactivityNotification inactivityNotification in list1)
            {
              if (inactivityNotification.UserID == student.UserID && !inactivityNotification.NotificationOneDateTimeSent.HasValue)
              {
                if (inactivityNotification.NotificationOneSent.HasValue)
                {
                  bool? notificationOneSent = inactivityNotification.NotificationOneSent;
                  bool flag = false;
                  if ((notificationOneSent.GetValueOrDefault() == flag ? (notificationOneSent.HasValue ? 1 : 0) : 0) == 0)
                    continue;
                }
                Student.SendFirstInactivityEmail(student.EmailAddress, student.FirstNames, student.IdentityNumber, student.UserID);
                inactivityNotification.NotificationOneDateTimeSent = new DateTime?(DateTime.Now);
                inactivityNotification.NotificationOneSent = new bool?(true);
                trainingEntities.SaveChanges();
              }
            }
          }
          foreach (DAL.Student student in list3)
          {
            foreach (InactivityNotification inactivityNotification in list1)
            {
              if (inactivityNotification.UserID == student.UserID && !inactivityNotification.NotificationTwoDateTimeSent.HasValue)
              {
                if (inactivityNotification.NotificationTwoSent.HasValue)
                {
                  bool? notificationTwoSent = inactivityNotification.NotificationTwoSent;
                  bool flag = false;
                  if ((notificationTwoSent.GetValueOrDefault() == flag ? (notificationTwoSent.HasValue ? 1 : 0) : 0) == 0)
                    continue;
                }
                Student.SendSecondInactivityEmail(student.EmailAddress, student.FirstNames, student.IdentityNumber, student.UserID);
                inactivityNotification.NotificationTwoDateTimeSent = new DateTime?(DateTime.Now);
                inactivityNotification.NotificationTwoSent = new bool?(true);
                trainingEntities.SaveChanges();
              }
            }
          }
          foreach (DAL.Student student in list4)
          {
            foreach (InactivityNotification inactivityNotification in list1)
            {
              if (inactivityNotification.UserID == student.UserID && !inactivityNotification.NotificationThreeDateTimeSent.HasValue)
              {
                if (inactivityNotification.NotificationThreeSent.HasValue)
                {
                  bool? notificationThreeSent = inactivityNotification.NotificationThreeSent;
                  bool flag = false;
                  if ((notificationThreeSent.GetValueOrDefault() == flag ? (notificationThreeSent.HasValue ? 1 : 0) : 0) == 0)
                    continue;
                }
                Student.SendFinalInactivityEmail(student.EmailAddress, student.FirstNames, student.IdentityNumber, student.UserID);
                inactivityNotification.NotificationThreeDateTimeSent = new DateTime?(DateTime.Now);
                inactivityNotification.NotificationThreeSent = new bool?(true);
                student.IsEnabled = false;
                trainingEntities.SaveChanges();
              }
            }
          }
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool EnableAccount(int paStudentID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (loStudent == null)
            return false;
          List<InactivityNotification> list = trainingEntities.InactivityNotifications.Where<InactivityNotification>((Expression<Func<InactivityNotification, bool>>) (n => n.UserID == loStudent.UserID)).ToList<InactivityNotification>();
          if (list != null && list.Count > 0)
          {
            foreach (InactivityNotification inactivityNotification in list)
              inactivityNotification.IsEnabled = false;
          }
          loStudent.IsEnabled = true;
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static void SendFirstInactivityEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      if (!Settings.InactivityMailerEnabled)
        return;
      try
      {
        string str = "Start A Business - Inactivity";
        string input = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>" + "<p>We notice your account has been inactive for over 30 days.</p>" + "<p>We encourage you to finish what you started and complete your business plan.</p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>We are committed to entrepreneurial success.</p>" + "<br>" + "<p>Kind Regards,</p>" + "<p>The Start a Business™ Team!</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Inactivity"),
          To = {
            paToAddress
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveServiceEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendSecondInactivityEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      if (!Settings.InactivityMailerEnabled)
        return;
      try
      {
        string str = "Start A Business - Inactivity";
        string input = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>" + "<p>We notice your account has been inactive for over 60 days.</p>" + "<p>It appears as if you will not be making use of the free training at Start a Business™</p>" + "<p>Your account will be disabled should it remain inactive after 90 days.</p>" + "<p>If you need support along the way, please feel free to contact us at admin@award.co.za.</p>" + "<p>We are committed to entrepreneurial success.</p>" + "<br>" + "<p>Kind Regards,</p>" + "<p>The Start a Business™ Team!</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Inactivity"),
          To = {
            paToAddress
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveServiceEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }

    public static void SendFinalInactivityEmail(
      string paToAddress,
      string paStudentName,
      string paIDNumber,
      string paUserID)
    {
      if (!Settings.MailerEnabled)
        return;
      if (!Settings.InactivityMailerEnabled)
        return;
      try
      {
        string str = "Start A Business - Inactivity";
        string input = "<p>Hi " + paStudentName.ToUpper() + " - I.D. No: " + paIDNumber + "</p>" + "<p>Your account has been inactive for over 90 days and has been disabled.</p>" + "<p>Should you wish to have your account reactivated, please contact us at admin@award.co.za.</p>" + "<p>We are committed to entrepreneurial success.</p>" + "<br>" + "<p>Kind Regards,</p>" + "<p>The Start a Business™ Team!</p>";
        new SmtpClient(Settings.MailerHost, Settings.MailerPort)
        {
          UseDefaultCredentials = false,
          Credentials = ((ICredentialsByHost) new NetworkCredential(Settings.MailerFromAddress, Settings.MailerFromPassword))
        }.Send(new MailMessage()
        {
          From = new MailAddress(Settings.MailerFromAddress, "Start A Business - Inactivity"),
          To = {
            paToAddress
          },
          Subject = str,
          IsBodyHtml = true,
          Body = "<body style=\"font-family:Verdana;font-size:10pt;\">" + input + "<body>"
        });
        string paMessage = Regex.Replace(input, "<.*?>", string.Empty);
        DAL.Logging.Logging.SaveServiceEmailLog(paUserID, paMessage);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
    }
  }
}
