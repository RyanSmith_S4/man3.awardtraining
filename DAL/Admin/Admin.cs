﻿// Decompiled with JetBrains decompiler
// Type: DAL.Admin.Admin
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Admin
{
  public class Admin
  {
    public static Stats GetStats()
    {
      Stats stats = new Stats()
      {
        LearnersLately = 0,
        TotalLearners = 0
      };
      DateTime loMonthAgo = DateTime.Now.AddDays(-30.0);
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          int num1 = trainingEntities.Students.Select<DAL.Student, DAL.Student>((Expression<Func<DAL.Student, DAL.Student>>) (s => s)).Count<DAL.Student>();
          int num2 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsLocked && s.IsEnabled)).Count<DAL.Student>();
          int num3 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => !s.IsEnabled)).Count<DAL.Student>();
          int num4 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => !s.IsLocked && s.IsEnabled)).Count<DAL.Student>();
          int num5 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked && s.DateTime_Created >= loMonthAgo)).Count<DAL.Student>();
          stats.LearnersLately = num5;
          stats.TotalLearners = num1;
          stats.TotalLearnersActive = num4;
          stats.TotalLearnersInactive = num3;
          stats.TotalLearnersLocked = num2;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return stats;
    }

    public static List<User> GetUsers()
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> list1 = trainingEntities.AspNetUsers.Where<AspNetUser>((Expression<Func<AspNetUser, bool>>) (u => u.Email.ToLower() != "admin@award.co.za")).Select<AspNetUser, string>((Expression<Func<AspNetUser, string>>) (u => u.Id)).ToList<string>();
          List<DAL.Instructor> list2 = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.IsApproved && !i.IsLocked)).ToList<DAL.Instructor>();
          List<DAL.Student> list3 = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked)).ToList<DAL.Student>();
          if (list1 != null)
          {
            List<User> source = new List<User>();
            if (list2 != null)
            {
              if (list3 != null)
              {
                foreach (DAL.Instructor instructor in list2)
                {
                  if (list1.Contains(instructor.UserID))
                  {
                    source.Add(new User()
                    {
                      FirstName = instructor.FirstNames,
                      LastName = instructor.LastNames,
                      UserID = instructor.UserID,
                      FSACode = ""
                    });
                    list1.Remove(instructor.UserID);
                  }
                }
                foreach (DAL.Student student in list3)
                {
                  if (list1.Contains(student.UserID))
                  {
                    source.Add(new User()
                    {
                      FirstName = student.FirstNames,
                      LastName = student.LastNames,
                      UserID = student.UserID,
                      FSACode = student.FSACode == "SYSTEM" ? "" : student.FSACode
                    });
                    list1.Remove(student.UserID);
                  }
                }
                return source.OrderBy<User, string>((Func<User, string>) (x => x.FirstName)).ToList<User>();
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (List<User>) null;
    }

    public static List<string> GetFSACodes()
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<string> list = trainingEntities.SponsorFsaCodes.Where<SponsorFsaCode>((Expression<Func<SponsorFsaCode, bool>>) (sc => sc.IsEnabled)).Select<SponsorFsaCode, string>((Expression<Func<SponsorFsaCode, string>>) (sc => sc.FsaCode.ToUpper())).Distinct<string>().OrderBy<string, string>((Expression<Func<string, string>>) (x => x)).ToList<string>();
          if (!list.Contains("SYSTEM"))
            list.Add("DEFAULT");
          return list;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (List<string>) null;
    }
  }
}
