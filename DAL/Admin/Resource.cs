﻿// Decompiled with JetBrains decompiler
// Type: DAL.Admin.Resource
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Admin
{
  public class Resource
  {
    public static COML.Classes.Resource GetResource(int paID)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          LearnerResource loResource = trainingEntities.LearnerResources.Where<LearnerResource>((Expression<Func<LearnerResource, bool>>) (r => r.IsEnabled && r.ID == paID)).FirstOrDefault<LearnerResource>();
          if (loResource != null)
          {
            DAL.UserFile userFile = trainingEntities.UserFiles.Where<DAL.UserFile>((Expression<Func<DAL.UserFile, bool>>) (uf => uf.IsEnabled && uf.ID == loResource.UserFileID)).FirstOrDefault<DAL.UserFile>();
            if (userFile != null)
              return new COML.Classes.Resource()
              {
                ID = loResource.ID,
                Description = loResource.Description,
                ContactPerson = loResource.ContactPersonName,
                ContactPersonCell = loResource.ContactPersonCell,
                ContactPersonEmail = loResource.ContactPersonEmail,
                DateTimeCreated = loResource.DateTimeCreated,
                Link = loResource.Link,
                Logo = new COML.Classes.UserFile()
                {
                  ID = userFile.ID,
                  Filepath = userFile.Path,
                  DateCreated = userFile.DateTimeCreated,
                  Name = userFile.Name,
                  SystemFile = (Enumerations.enumSystemFile) userFile.SystemType,
                  TypeFile = (Enumerations.enumTypeFile) userFile.FileType
                }
              };
          }
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (COML.Classes.Resource) null;
    }

    public static List<COML.Classes.Resource> GetResources()
    {
      List<COML.Classes.Resource> resourceList = new List<COML.Classes.Resource>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<COML.Classes.Resource> list = trainingEntities.LearnerResources.OrderByDescending<LearnerResource, DateTime>((Expression<Func<LearnerResource, DateTime>>) (r => r.DateTimeCreated)).Where<LearnerResource>((Expression<Func<LearnerResource, bool>>) (r => r.IsEnabled)).Select<LearnerResource, COML.Classes.Resource>(Expression.Lambda<Func<LearnerResource, COML.Classes.Resource>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Resource)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Resource.set_ID)), )))); //unable to render the statement
          if (list.Count > 0)
            resourceList = list;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return resourceList;
    }

    public static List<COML.Classes.Resource> GetResourcesBySearch(string paWord)
    {
      List<COML.Classes.Resource> resourceList = new List<COML.Classes.Resource>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<COML.Classes.Resource> list = trainingEntities.LearnerResources.OrderByDescending<LearnerResource, DateTime>((Expression<Func<LearnerResource, DateTime>>) (r => r.DateTimeCreated)).Where<LearnerResource>((Expression<Func<LearnerResource, bool>>) (r => r.IsEnabled && r.Description.Contains(paWord))).Select<LearnerResource, COML.Classes.Resource>(Expression.Lambda<Func<LearnerResource, COML.Classes.Resource>>((Expression) Expression.MemberInit(Expression.New(typeof (COML.Classes.Resource)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (COML.Classes.Resource.set_ID)), )))); //unable to render the statement
          if (list.Count > 0)
            resourceList = list;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return resourceList;
    }

    public static bool RemoveResource(int paID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          LearnerResource learnerResource = trainingEntities.LearnerResources.Where<LearnerResource>((Expression<Func<LearnerResource, bool>>) (r => r.IsEnabled && r.ID == paID)).FirstOrDefault<LearnerResource>();
          if (learnerResource != null)
          {
            learnerResource.IsEnabled = false;
            trainingEntities.SaveChanges();
          }
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }

    public static bool SaveResource(COML.Classes.Resource paResource, string paUserID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.UserFile entity1 = new DAL.UserFile()
          {
            DateTimeCreated = DateTime.Now,
            FileType = 3,
            SystemType = 9,
            IsEnabled = true,
            Name = paResource.Logo.Name,
            Path = paResource.Logo.Filepath,
            UserID = paUserID
          };
          trainingEntities.UserFiles.Add(entity1);
          trainingEntities.SaveChanges();
          if (entity1.ID > 0)
          {
            LearnerResource entity2 = new LearnerResource()
            {
              DateTimeCreated = paResource.DateTimeCreated,
              IsEnabled = true,
              ContactPersonCell = paResource.ContactPersonCell,
              ContactPersonEmail = paResource.ContactPersonEmail,
              ContactPersonName = paResource.ContactPerson,
              Description = paResource.Description,
              Link = paResource.Link,
              UserFileID = entity1.ID
            };
            trainingEntities.LearnerResources.Add(entity2);
            trainingEntities.SaveChanges();
          }
          flag = true;
        }
      }
      catch (Exception ex)
      {
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return flag;
    }
  }
}
