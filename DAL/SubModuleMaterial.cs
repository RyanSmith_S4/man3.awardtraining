﻿// Decompiled with JetBrains decompiler
// Type: DAL.SubModuleMaterial
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System.Collections.Generic;

namespace DAL
{
  public class SubModuleMaterial
  {
    public SubModuleMaterial()
    {
      this.SubModuleModuleMaterials = (ICollection<SubModuleModuleMaterial>) new HashSet<SubModuleModuleMaterial>();
    }

    public int ID { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public int Type { get; set; }

    public string Path { get; set; }

    public bool IsEnabled { get; set; }

    public virtual ICollection<SubModuleModuleMaterial> SubModuleModuleMaterials { get; set; }
  }
}
