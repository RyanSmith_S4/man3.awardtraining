﻿// Decompiled with JetBrains decompiler
// Type: DAL.UserFile
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;
using System.Collections.Generic;

namespace DAL
{
  public class UserFile
  {
    public UserFile()
    {
      this.InstructorFiles = (ICollection<InstructorFile>) new HashSet<InstructorFile>();
      this.StudentFiles = (ICollection<StudentFile>) new HashSet<StudentFile>();
      this.LearnerResources = (ICollection<LearnerResource>) new HashSet<LearnerResource>();
      this.UserSubModuleFiles = (ICollection<UserSubModuleFile>) new HashSet<UserSubModuleFile>();
    }

    public int ID { get; set; }

    public int SystemType { get; set; }

    public int FileType { get; set; }

    public string Name { get; set; }

    public string Path { get; set; }

    public string UserID { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public bool IsEnabled { get; set; }

    public virtual ICollection<InstructorFile> InstructorFiles { get; set; }

    public virtual ICollection<StudentFile> StudentFiles { get; set; }

    public virtual ICollection<LearnerResource> LearnerResources { get; set; }

    public virtual ICollection<UserSubModuleFile> UserSubModuleFiles { get; set; }
  }
}
