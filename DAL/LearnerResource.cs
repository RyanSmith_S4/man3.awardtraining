﻿// Decompiled with JetBrains decompiler
// Type: DAL.LearnerResource
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;

namespace DAL
{
  public class LearnerResource
  {
    public int ID { get; set; }

    public int UserFileID { get; set; }

    public string Description { get; set; }

    public string Link { get; set; }

    public string ContactPersonEmail { get; set; }

    public string ContactPersonName { get; set; }

    public string ContactPersonCell { get; set; }

    public DateTime DateTimeCreated { get; set; }

    public bool IsEnabled { get; set; }

    public virtual UserFile UserFile { get; set; }
  }
}
