﻿// Decompiled with JetBrains decompiler
// Type: DAL.AspNetUser
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;
using System.Collections.Generic;

namespace DAL
{
  public class AspNetUser
  {
    public AspNetUser()
    {
      this.EmailLogs = (ICollection<EmailLog>) new HashSet<EmailLog>();
      this.MaterialLogs = (ICollection<MaterialLog>) new HashSet<MaterialLog>();
      this.SmsLogs = (ICollection<SmsLog>) new HashSet<SmsLog>();
      this.PageLogs = (ICollection<PageLog>) new HashSet<PageLog>();
      this.AspNetRoles = (ICollection<AspNetRole>) new HashSet<AspNetRole>();
      this.SystemLogs = (ICollection<SystemLog>) new HashSet<SystemLog>();
      this.Sponsors = (ICollection<Sponsor>) new HashSet<Sponsor>();
      this.ChatMessageLogs = (ICollection<ChatMessageLog>) new HashSet<ChatMessageLog>();
      this.GeneralChatMessageLogs = (ICollection<GeneralChatMessageLog>) new HashSet<GeneralChatMessageLog>();
      this.UserPasswordResets = (ICollection<UserPasswordReset>) new HashSet<UserPasswordReset>();
      this.Students = (ICollection<Student>) new HashSet<Student>();
      this.Instructors = (ICollection<Instructor>) new HashSet<Instructor>();
    }

    public string Id { get; set; }

    public string Email { get; set; }

    public bool EmailConfirmed { get; set; }

    public string PasswordHash { get; set; }

    public string SecurityStamp { get; set; }

    public string PhoneNumber { get; set; }

    public bool PhoneNumberConfirmed { get; set; }

    public bool TwoFactorEnabled { get; set; }

    public DateTime? LockoutEndDateUtc { get; set; }

    public bool LockoutEnabled { get; set; }

    public int AccessFailedCount { get; set; }

    public string UserName { get; set; }

    public virtual ICollection<EmailLog> EmailLogs { get; set; }

    public virtual ICollection<MaterialLog> MaterialLogs { get; set; }

    public virtual ICollection<SmsLog> SmsLogs { get; set; }

    public virtual ICollection<PageLog> PageLogs { get; set; }

    public virtual ICollection<AspNetRole> AspNetRoles { get; set; }

    public virtual ICollection<SystemLog> SystemLogs { get; set; }

    public virtual ICollection<Sponsor> Sponsors { get; set; }

    public virtual ICollection<ChatMessageLog> ChatMessageLogs { get; set; }

    public virtual ICollection<GeneralChatMessageLog> GeneralChatMessageLogs { get; set; }

    public virtual ICollection<UserPasswordReset> UserPasswordResets { get; set; }

    public virtual ICollection<Student> Students { get; set; }

    public virtual ICollection<Instructor> Instructors { get; set; }
  }
}
