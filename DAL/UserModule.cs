﻿// Decompiled with JetBrains decompiler
// Type: DAL.UserModule
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using System;
using System.Collections.Generic;

namespace DAL
{
  public class UserModule
  {
    public UserModule()
    {
      this.UserSubModules = (ICollection<UserSubModule>) new HashSet<UserSubModule>();
    }

    public int ID { get; set; }

    public string UserID { get; set; }

    public int ModuleID { get; set; }

    public bool IsStarted { get; set; }

    public bool IsComplete { get; set; }

    public bool IsSuccessful { get; set; }

    public DateTime DateCreated { get; set; }

    public bool IsEnabled { get; set; }

    public DateTime? DateComplete { get; set; }

    public virtual Module Module { get; set; }

    public virtual ICollection<UserSubModule> UserSubModules { get; set; }
  }
}
