﻿// Decompiled with JetBrains decompiler
// Type: DAL.Logging.Logging
// Assembly: DAL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2CD59348-7B1B-4A5E-9CF8-7A0BB92CF4AE
// Assembly location: C:\Users\S4\Desktop\man\bin\DAL.dll

using COML;
using COML.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DAL.Logging
{
  public class Logging
  {
    public static List<GenericLog> GetStudentLogs(int paStudentUD)
    {
      List<GenericLog> genericLogList = new List<GenericLog>();
      List<GenericLog> source = new List<GenericLog>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student loStudent = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.ID == paStudentUD)).FirstOrDefault<DAL.Student>();
          if (loStudent != null)
          {
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list1 = trainingEntities.SmsLogs.Where<SmsLog>((Expression<Func<SmsLog, bool>>) (sl => sl.UserID == loStudent.UserID)).Select<SmsLog, GenericLog>(Expression.Lambda<Func<SmsLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list2 = trainingEntities.EmailLogs.Where<EmailLog>((Expression<Func<EmailLog, bool>>) (el => el.UserID == loStudent.UserID)).Select<EmailLog, GenericLog>(Expression.Lambda<Func<EmailLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list3 = trainingEntities.PageLogs.Where<PageLog>((Expression<Func<PageLog, bool>>) (pl => pl.UserID == loStudent.UserID)).Select<PageLog, GenericLog>(Expression.Lambda<Func<PageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression4;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list4 = trainingEntities.MaterialLogs.Join((IEnumerable<DAL.SubModuleMaterial>) trainingEntities.SubModuleMaterials, (Expression<Func<MaterialLog, int>>) (ml => ml.SubModuleMaterialID), (Expression<Func<DAL.SubModuleMaterial, int>>) (smm => smm.ID), (ml, smm) => new
            {
              ml = ml,
              smm = smm
            }).Where(data => data.ml.UserID == loStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType44<MaterialLog, DAL.SubModuleMaterial>, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression5;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list5 = trainingEntities.UserSubModuleMultipleChoiceAnswers.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserSubModuleMultipleChoiceAnswer, int>>) (ql => ql.UserSubModuleID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (ql, usm) => new
            {
              ql = ql,
              usm = usm
            }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              um = um
            }).Join((IEnumerable<DAL.MultipleChoiceQuestion>) trainingEntities.MultipleChoiceQuestions, data => data.\u003C\u003Eh__TransparentIdentifier0.ql.MultipleChoiceQuestionID, (Expression<Func<DAL.MultipleChoiceQuestion, int>>) (mcq => mcq.ID), (data, mcq) => new
            {
              \u003C\u003Eh__TransparentIdentifier1 = data,
              mcq = mcq
            }).Join((IEnumerable<MultipleChoiceSubQuestion>) trainingEntities.MultipleChoiceSubQuestions, data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.ql.UserAnswerID, (Expression<Func<MultipleChoiceSubQuestion, int>>) (mca => mca.ID), (data, mca) => new
            {
              \u003C\u003Eh__TransparentIdentifier2 = data,
              mca = mca
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier2.mcq.IsEnabled && data.mca.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier2.\u003C\u003Eh__TransparentIdentifier1.um.UserID == loStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType47<\u003C\u003Ef__AnonymousType46<\u003C\u003Ef__AnonymousType13<\u003C\u003Ef__AnonymousType45<UserSubModuleMultipleChoiceAnswer, DAL.UserSubModule>, UserModule>, DAL.MultipleChoiceQuestion>, MultipleChoiceSubQuestion>, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression6;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list6 = trainingEntities.UserSubModuleTrueFalseAnswers.Join((IEnumerable<DAL.UserSubModule>) trainingEntities.UserSubModules, (Expression<Func<UserSubModuleTrueFalseAnswer, int>>) (ql => ql.UserSubModuleID), (Expression<Func<DAL.UserSubModule, int>>) (usm => usm.ID), (ql, usm) => new
            {
              ql = ql,
              usm = usm
            }).Join((IEnumerable<UserModule>) trainingEntities.UserModules, data => data.usm.UserModuleID, (Expression<Func<UserModule, int>>) (um => um.ID), (data, um) => new
            {
              \u003C\u003Eh__TransparentIdentifier0 = data,
              um = um
            }).Join((IEnumerable<DAL.TrueFalseQuestion>) trainingEntities.TrueFalseQuestions, data => data.\u003C\u003Eh__TransparentIdentifier0.ql.TrueFalseID, (Expression<Func<DAL.TrueFalseQuestion, int>>) (tfq => tfq.ID), (data, tfq) => new
            {
              \u003C\u003Eh__TransparentIdentifier1 = data,
              tfq = tfq
            }).Where(data => data.\u003C\u003Eh__TransparentIdentifier1.\u003C\u003Eh__TransparentIdentifier0.ql.IsEnabled && data.tfq.IsEnabled && data.\u003C\u003Eh__TransparentIdentifier1.um.UserID == loStudent.UserID).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType48<\u003C\u003Ef__AnonymousType13<\u003C\u003Ef__AnonymousType45<UserSubModuleTrueFalseAnswer, DAL.UserSubModule>, UserModule>, DAL.TrueFalseQuestion>, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression7;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list7 = trainingEntities.SystemLogs.Where<SystemLog>((Expression<Func<SystemLog, bool>>) (pl => pl.UserID == loStudent.UserID)).Select<SystemLog, GenericLog>(Expression.Lambda<Func<SystemLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression8;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list8 = trainingEntities.ChatMessageLogs.Where<ChatMessageLog>((Expression<Func<ChatMessageLog, bool>>) (cl => cl.UserID == loStudent.UserID)).Select<ChatMessageLog, GenericLog>(Expression.Lambda<Func<ChatMessageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression9;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list9 = trainingEntities.GeneralChatMessageLogs.Where<GeneralChatMessageLog>((Expression<Func<GeneralChatMessageLog, bool>>) (cl => cl.UserID == loStudent.UserID)).Select<GeneralChatMessageLog, GenericLog>(Expression.Lambda<Func<GeneralChatMessageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            if (list1.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list1);
            if (list2.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list2);
            if (list3.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list3);
            if (list4.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list4);
            if (list5.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list5);
            if (list6.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list6);
            if (list7.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list7);
            if (list8.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list8);
            if (list9.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list9);
            source = source.OrderByDescending<GenericLog, DateTime>((Func<GenericLog, DateTime>) (x => x.TimeStamp)).ToList<GenericLog>();
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return source;
    }

    public static List<GenericLog> GetSponsorLogs(int paSponsorID)
    {
      List<GenericLog> genericLogList = new List<GenericLog>();
      List<GenericLog> source = new List<GenericLog>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Sponsor loSponsor = trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (s => s.IsEnabled && s.ID == paSponsorID)).FirstOrDefault<DAL.Sponsor>();
          if (loSponsor != null)
          {
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list1 = trainingEntities.SmsLogs.Where<SmsLog>((Expression<Func<SmsLog, bool>>) (sl => sl.UserID == loSponsor.UserID)).Select<SmsLog, GenericLog>(Expression.Lambda<Func<SmsLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list2 = trainingEntities.EmailLogs.Where<EmailLog>((Expression<Func<EmailLog, bool>>) (el => el.UserID == loSponsor.UserID)).Select<EmailLog, GenericLog>(Expression.Lambda<Func<EmailLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list3 = trainingEntities.PageLogs.Where<PageLog>((Expression<Func<PageLog, bool>>) (pl => pl.UserID == loSponsor.UserID)).Select<PageLog, GenericLog>(Expression.Lambda<Func<PageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression4;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list4 = trainingEntities.SystemLogs.Where<SystemLog>((Expression<Func<SystemLog, bool>>) (pl => pl.UserID == loSponsor.UserID)).Select<SystemLog, GenericLog>(Expression.Lambda<Func<SystemLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            if (list1.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list1);
            if (list2.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list2);
            if (list3.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list3);
            if (list4.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list4);
            source = source.OrderByDescending<GenericLog, DateTime>((Func<GenericLog, DateTime>) (x => x.TimeStamp)).ToList<GenericLog>();
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return source;
    }

    public static List<GenericLog> GetMentorLogs(int paInstructorID)
    {
      List<GenericLog> genericLogList = new List<GenericLog>();
      List<GenericLog> source = new List<GenericLog>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor loInstructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.IsMentor && s.ID == paInstructorID)).FirstOrDefault<DAL.Instructor>();
          if (loInstructor != null)
          {
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list1 = trainingEntities.SmsLogs.Where<SmsLog>((Expression<Func<SmsLog, bool>>) (sl => sl.UserID == loInstructor.UserID)).Select<SmsLog, GenericLog>(Expression.Lambda<Func<SmsLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list2 = trainingEntities.EmailLogs.Where<EmailLog>((Expression<Func<EmailLog, bool>>) (el => el.UserID == loInstructor.UserID)).Select<EmailLog, GenericLog>(Expression.Lambda<Func<EmailLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list3 = trainingEntities.PageLogs.Where<PageLog>((Expression<Func<PageLog, bool>>) (pl => pl.UserID == loInstructor.UserID)).Select<PageLog, GenericLog>(Expression.Lambda<Func<PageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression4;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list4 = trainingEntities.SystemLogs.Where<SystemLog>((Expression<Func<SystemLog, bool>>) (pl => pl.UserID == loInstructor.UserID)).Select<SystemLog, GenericLog>(Expression.Lambda<Func<SystemLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression5;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list5 = trainingEntities.ChatMessageLogs.Where<ChatMessageLog>((Expression<Func<ChatMessageLog, bool>>) (cl => cl.UserID == loInstructor.UserID)).Select<ChatMessageLog, GenericLog>(Expression.Lambda<Func<ChatMessageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression6;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list6 = trainingEntities.GeneralChatMessageLogs.Where<GeneralChatMessageLog>((Expression<Func<GeneralChatMessageLog, bool>>) (cl => cl.UserID == loInstructor.UserID)).Select<GeneralChatMessageLog, GenericLog>(Expression.Lambda<Func<GeneralChatMessageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            if (list1.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list1);
            if (list2.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list2);
            if (list3.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list3);
            if (list4.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list4);
            if (list5.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list5);
            if (list6.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list6);
            source = source.OrderByDescending<GenericLog, DateTime>((Func<GenericLog, DateTime>) (x => x.TimeStamp)).ToList<GenericLog>();
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return source;
    }

    public static List<GenericLog> GetAssessorLogs(int paInstructorID)
    {
      List<GenericLog> genericLogList = new List<GenericLog>();
      List<GenericLog> source = new List<GenericLog>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor loInstructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (s => s.IsEnabled && s.IsAssessor && s.ID == paInstructorID)).FirstOrDefault<DAL.Instructor>();
          if (loInstructor != null)
          {
            ParameterExpression parameterExpression1;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list1 = trainingEntities.SmsLogs.Where<SmsLog>((Expression<Func<SmsLog, bool>>) (sl => sl.UserID == loInstructor.UserID)).Select<SmsLog, GenericLog>(Expression.Lambda<Func<SmsLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression2;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list2 = trainingEntities.EmailLogs.Where<EmailLog>((Expression<Func<EmailLog, bool>>) (el => el.UserID == loInstructor.UserID)).Select<EmailLog, GenericLog>(Expression.Lambda<Func<EmailLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression3;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list3 = trainingEntities.PageLogs.Where<PageLog>((Expression<Func<PageLog, bool>>) (pl => pl.UserID == loInstructor.UserID)).Select<PageLog, GenericLog>(Expression.Lambda<Func<PageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression4;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list4 = trainingEntities.SystemLogs.Where<SystemLog>((Expression<Func<SystemLog, bool>>) (pl => pl.UserID == loInstructor.UserID)).Select<SystemLog, GenericLog>(Expression.Lambda<Func<SystemLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression5;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list5 = trainingEntities.ChatMessageLogs.Where<ChatMessageLog>((Expression<Func<ChatMessageLog, bool>>) (cl => cl.UserID == loInstructor.UserID)).Select<ChatMessageLog, GenericLog>(Expression.Lambda<Func<ChatMessageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            ParameterExpression parameterExpression6;
            // ISSUE: method reference
            // ISSUE: method reference
            // ISSUE: method reference
            List<GenericLog> list6 = trainingEntities.GeneralChatMessageLogs.Where<GeneralChatMessageLog>((Expression<Func<GeneralChatMessageLog, bool>>) (cl => cl.UserID == loInstructor.UserID)).Select<GeneralChatMessageLog, GenericLog>(Expression.Lambda<Func<GeneralChatMessageLog, GenericLog>>((Expression) Expression.MemberInit(Expression.New(typeof (GenericLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (GenericLog.set_Description)), )))); //unable to render the statement
            if (list1.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list1);
            if (list2.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list2);
            if (list3.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list3);
            if (list4.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list4);
            if (list5.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list5);
            if (list6.Count > 0)
              source.AddRange((IEnumerable<GenericLog>) list6);
            source = source.OrderByDescending<GenericLog, DateTime>((Func<GenericLog, DateTime>) (x => x.TimeStamp)).ToList<GenericLog>();
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return source;
    }

    public static bool SaveSmsLog(string paUserID, string paMessage)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            SmsLog entity = new SmsLog()
            {
              UserID = paUserID,
              SmsMessage = paMessage,
              DateTime_Created = DateTime.Now
            };
            student.LastActivity = DateTime.Now;
            trainingEntities.SmsLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveServiceEmailLog(string paUserID, string paMessage)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>() != null)
          {
            EmailLog entity = new EmailLog()
            {
              UserID = paUserID,
              EmailMessage = paMessage,
              DateTime_Created = DateTime.Now
            };
            trainingEntities.EmailLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveEmailLog(string paUserID, string paMessage)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            EmailLog entity = new EmailLog()
            {
              UserID = paUserID,
              EmailMessage = paMessage,
              DateTime_Created = DateTime.Now
            };
            student.LastActivity = DateTime.Now;
            trainingEntities.EmailLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveMaterialLog(string paUserID, int paSubModuleMaterialID)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            MaterialLog entity = new MaterialLog()
            {
              UserID = paUserID,
              SubModuleMaterialID = paSubModuleMaterialID,
              DateTime_Created = DateTime.Now
            };
            student.LastActivity = DateTime.Now;
            trainingEntities.MaterialLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SavePageLog(string paUserID, string paPage)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            PageLog entity = new PageLog()
            {
              UserID = paUserID,
              Page = paPage,
              DateTime_Created = DateTime.Now
            };
            student.LastActivity = DateTime.Now;
            trainingEntities.PageLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveInstructorPageLog(string paUserID, string paPage)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
          if (instructor != null)
          {
            PageLog entity = new PageLog()
            {
              UserID = paUserID,
              Page = paPage,
              DateTime_Created = DateTime.Now
            };
            instructor.LastActivity = DateTime.Now;
            trainingEntities.PageLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveSponsorPageLog(string paUserID, string paPage)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Sponsor sponsor = trainingEntities.Sponsors.Where<DAL.Sponsor>((Expression<Func<DAL.Sponsor, bool>>) (s => s.IsEnabled && s.UserID == paUserID)).FirstOrDefault<DAL.Sponsor>();
          if (sponsor != null)
          {
            PageLog entity = new PageLog()
            {
              UserID = paUserID,
              Page = paPage,
              DateTime_Created = DateTime.Now
            };
            sponsor.LastActivity = DateTime.Now;
            trainingEntities.PageLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveSystemLog(int paStudentID, string paAction)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.ID == paStudentID)).FirstOrDefault<DAL.Student>();
          if (student != null)
          {
            SystemLog entity = new SystemLog()
            {
              UserID = student.UserID,
              Action = paAction,
              DateTime_Created = DateTime.Now
            };
            student.LastActivity = DateTime.Now;
            trainingEntities.SystemLogs.Add(entity);
            trainingEntities.SaveChanges();
            flag = true;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static bool SaveChatLog(string paUserID, string paMessage, List<string> paRoles)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (paRoles.Count > 0)
          {
            if (paRoles.Contains("Admin"))
            {
              ChatMessageLog entity = new ChatMessageLog()
              {
                DateTime_Created = DateTime.Now,
                ChatMessage = paMessage,
                UserID = paUserID,
                UserType = 0
              };
              trainingEntities.ChatMessageLogs.Add(entity);
              trainingEntities.SaveChanges();
              flag = true;
            }
            else if (paRoles.Contains("Student"))
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
              {
                ChatMessageLog entity = new ChatMessageLog()
                {
                  DateTime_Created = DateTime.Now,
                  ChatMessage = paMessage,
                  UserID = paUserID,
                  UserType = 4
                };
                trainingEntities.ChatMessageLogs.Add(entity);
                student.LastActivity = DateTime.Now;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else if (paRoles.Contains("Mentor"))
            {
              DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && !i.IsLocked && i.IsApproved && i.IsMentor && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
              if (instructor != null)
              {
                ChatMessageLog entity = new ChatMessageLog()
                {
                  DateTime_Created = DateTime.Now,
                  ChatMessage = paMessage,
                  UserID = paUserID,
                  UserType = 2
                };
                trainingEntities.ChatMessageLogs.Add(entity);
                instructor.LastActivity = DateTime.Now;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else if (paRoles.Contains("Assessor"))
            {
              DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && !i.IsLocked && i.IsApproved && i.IsAssessor && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
              if (instructor != null)
              {
                ChatMessageLog entity = new ChatMessageLog()
                {
                  DateTime_Created = DateTime.Now,
                  ChatMessage = paMessage,
                  UserID = paUserID,
                  UserType = 1
                };
                trainingEntities.ChatMessageLogs.Add(entity);
                instructor.LastActivity = DateTime.Now;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else
              flag = false;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static List<ChatLog> GetChatLogs(DateTime paDateTime)
    {
      List<ChatLog> source = new List<ChatLog>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list1 = trainingEntities.ChatMessageLogs.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<ChatMessageLog, string>>) (cl => cl.UserID), (Expression<Func<DAL.Student, string>>) (s => s.UserID), (cl, s) => new
          {
            cl = cl,
            s = s
          }).Where(data => data.cl.DateTime_Created > paDateTime && data.s.IsEnabled && !data.s.IsLocked).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType49<ChatMessageLog, DAL.Student>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          ParameterExpression parameterExpression2;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list2 = trainingEntities.ChatMessageLogs.Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, (Expression<Func<ChatMessageLog, string>>) (cl => cl.UserID), (Expression<Func<DAL.Instructor, string>>) (i => i.UserID), (cl, i) => new
          {
            cl = cl,
            i = i
          }).Where(data => data.cl.DateTime_Created > paDateTime && data.i.IsApproved && data.i.IsEnabled && !data.i.IsLocked && data.i.IsMentor).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType50<ChatMessageLog, DAL.Instructor>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          ParameterExpression parameterExpression3;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list3 = trainingEntities.ChatMessageLogs.Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, (Expression<Func<ChatMessageLog, string>>) (cl => cl.UserID), (Expression<Func<DAL.Instructor, string>>) (i => i.UserID), (cl, i) => new
          {
            cl = cl,
            i = i
          }).Where(data => data.cl.DateTime_Created > paDateTime && data.i.IsApproved && data.i.IsEnabled && !data.i.IsLocked && data.i.IsAssessor).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType50<ChatMessageLog, DAL.Instructor>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          ParameterExpression parameterExpression4;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list4 = trainingEntities.AspNetUsers.Join((IEnumerable<ChatMessageLog>) trainingEntities.ChatMessageLogs, (Expression<Func<AspNetUser, string>>) (a => a.Id), (Expression<Func<ChatMessageLog, string>>) (cl => cl.UserID), (a, cl) => new
          {
            a = a,
            cl = cl
          }).Where(data => data.a.Email == "admin@award.co.za" && data.cl.UserType == 0).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType51<AspNetUser, ChatMessageLog>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          if (list1.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list1);
          if (list2.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list2);
          if (list3.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list3);
          if (list4.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list4);
          source = source.OrderByDescending<ChatLog, DateTime>((Func<ChatLog, DateTime>) (x => x.DateTimeCreated)).Take<ChatLog>(20).ToList<ChatLog>();
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return source;
    }

    public static bool SaveGeneralChatLog(string paUserID, string paMessage, List<string> paRoles)
    {
      bool flag = false;
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          if (paRoles.Count > 0)
          {
            string paRole = paRoles[0];
            if (paRoles.Contains("Admin"))
            {
              GeneralChatMessageLog entity = new GeneralChatMessageLog()
              {
                DateTime_Created = DateTime.Now,
                ChatMessage = paMessage,
                UserID = paUserID,
                UserType = 0
              };
              trainingEntities.GeneralChatMessageLogs.Add(entity);
              trainingEntities.SaveChanges();
              flag = true;
            }
            else if (paRoles.Contains("Student"))
            {
              DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && !s.IsLocked && s.UserID == paUserID)).FirstOrDefault<DAL.Student>();
              if (student != null)
              {
                GeneralChatMessageLog entity = new GeneralChatMessageLog()
                {
                  DateTime_Created = DateTime.Now,
                  ChatMessage = paMessage,
                  UserID = paUserID,
                  UserType = 4
                };
                trainingEntities.GeneralChatMessageLogs.Add(entity);
                student.LastActivity = DateTime.Now;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else if (paRoles.Contains("Mentor"))
            {
              DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && !i.IsLocked && i.IsApproved && i.IsMentor && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
              if (instructor != null)
              {
                GeneralChatMessageLog entity = new GeneralChatMessageLog()
                {
                  DateTime_Created = DateTime.Now,
                  ChatMessage = paMessage,
                  UserID = paUserID,
                  UserType = 2
                };
                trainingEntities.GeneralChatMessageLogs.Add(entity);
                instructor.LastActivity = DateTime.Now;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else if (paRoles.Contains("Assessor"))
            {
              DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && !i.IsLocked && i.IsApproved && i.IsAssessor && i.UserID == paUserID)).FirstOrDefault<DAL.Instructor>();
              if (instructor != null)
              {
                GeneralChatMessageLog entity = new GeneralChatMessageLog()
                {
                  DateTime_Created = DateTime.Now,
                  ChatMessage = paMessage,
                  UserID = paUserID,
                  UserType = 1
                };
                trainingEntities.GeneralChatMessageLogs.Add(entity);
                instructor.LastActivity = DateTime.Now;
                trainingEntities.SaveChanges();
                flag = true;
              }
            }
            else
              flag = false;
          }
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        flag = false;
      }
      return flag;
    }

    public static List<ChatLog> GetGeneralChatLogs(DateTime paDateTime)
    {
      List<ChatLog> source = new List<ChatLog>();
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          ParameterExpression parameterExpression1;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list1 = trainingEntities.GeneralChatMessageLogs.Join((IEnumerable<DAL.Student>) trainingEntities.Students, (Expression<Func<GeneralChatMessageLog, string>>) (cl => cl.UserID), (Expression<Func<DAL.Student, string>>) (s => s.UserID), (cl, s) => new
          {
            cl = cl,
            s = s
          }).Where(data => data.cl.DateTime_Created > paDateTime && data.s.IsEnabled && !data.s.IsLocked).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType49<GeneralChatMessageLog, DAL.Student>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          ParameterExpression parameterExpression2;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list2 = trainingEntities.GeneralChatMessageLogs.Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, (Expression<Func<GeneralChatMessageLog, string>>) (cl => cl.UserID), (Expression<Func<DAL.Instructor, string>>) (i => i.UserID), (cl, i) => new
          {
            cl = cl,
            i = i
          }).Where(data => data.cl.DateTime_Created > paDateTime && data.i.IsApproved && data.i.IsEnabled && !data.i.IsLocked && data.i.IsMentor).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType50<GeneralChatMessageLog, DAL.Instructor>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          ParameterExpression parameterExpression3;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list3 = trainingEntities.GeneralChatMessageLogs.Join((IEnumerable<DAL.Instructor>) trainingEntities.Instructors, (Expression<Func<GeneralChatMessageLog, string>>) (cl => cl.UserID), (Expression<Func<DAL.Instructor, string>>) (i => i.UserID), (cl, i) => new
          {
            cl = cl,
            i = i
          }).Where(data => data.cl.DateTime_Created > paDateTime && data.i.IsApproved && data.i.IsEnabled && !data.i.IsLocked && data.i.IsAssessor).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType50<GeneralChatMessageLog, DAL.Instructor>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          ParameterExpression parameterExpression4;
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          // ISSUE: method reference
          List<ChatLog> list4 = trainingEntities.AspNetUsers.Join((IEnumerable<GeneralChatMessageLog>) trainingEntities.GeneralChatMessageLogs, (Expression<Func<AspNetUser, string>>) (a => a.Id), (Expression<Func<GeneralChatMessageLog, string>>) (cl => cl.UserID), (a, cl) => new
          {
            a = a,
            cl = cl
          }).Where(data => data.a.Email == "admin@award.co.za" && data.cl.UserType == 0).OrderByDescending(data => data.cl.DateTime_Created).Select(Expression.Lambda<Func<\u003C\u003Ef__AnonymousType51<AspNetUser, GeneralChatMessageLog>, ChatLog>>((Expression) Expression.MemberInit(Expression.New(typeof (ChatLog)), (MemberBinding) Expression.Bind((MethodInfo) MethodBase.GetMethodFromHandle(__methodref (ChatLog.set_UserName)), )))); //unable to render the statement
          if (list1.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list1);
          if (list2.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list2);
          if (list3.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list3);
          if (list4.Count > 0)
            source.AddRange((IEnumerable<ChatLog>) list4);
          source = source.OrderByDescending<ChatLog, DateTime>((Func<ChatLog, DateTime>) (x => x.DateTimeCreated)).Take<ChatLog>(20).ToList<ChatLog>();
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return source;
    }

    public static bool CanSendVerificationCode(string paUserID, DateTime paDateTime)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<SmsLog> list = trainingEntities.SmsLogs.OrderByDescending<SmsLog, DateTime>((Expression<Func<SmsLog, DateTime>>) (s => s.DateTime_Created)).Where<SmsLog>((Expression<Func<SmsLog, bool>>) (s => s.SmsMessage.Contains("Verification SMS sent") && s.UserID == paUserID)).ToList<SmsLog>();
          if (list == null || list.Count <= 0)
            return true;
          SmsLog smsLog = list[0];
          return (paDateTime - smsLog.DateTime_Created).TotalSeconds > 3.0;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool SaveResetPasswordCode(string paUserID, string paCode)
    {
      try
      {
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          UserPasswordReset entity = new UserPasswordReset()
          {
            Code = paCode,
            DateTimeCreated = DateTime.Now,
            IsEnabled = true,
            UserID = paUserID
          };
          trainingEntities.UserPasswordResets.Add(entity);
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool GetResetPasswordCode(string paEmail, string paUrlEmail, string paCode)
    {
      try
      {
        DateTime loDateWindow = DateTime.Now.AddDays(-30.0);
        string loUserID = "";
        if (paEmail != paUrlEmail)
          return false;
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          AspNetUser aspNetUser = trainingEntities.AspNetUsers.Where<AspNetUser>((Expression<Func<AspNetUser, bool>>) (u => u.Email.ToLower() == paEmail.ToLower())).FirstOrDefault<AspNetUser>();
          if (aspNetUser == null)
            return false;
          DAL.Student student = trainingEntities.Students.Where<DAL.Student>((Expression<Func<DAL.Student, bool>>) (s => s.IsEnabled && s.EmailAddress == paEmail)).FirstOrDefault<DAL.Student>();
          DAL.Instructor instructor = trainingEntities.Instructors.Where<DAL.Instructor>((Expression<Func<DAL.Instructor, bool>>) (i => i.IsEnabled && !i.IsLocked && i.IsApproved && i.EmailAddress == paEmail)).FirstOrDefault<DAL.Instructor>();
          if (student != null)
            loUserID = student.UserID;
          else if (instructor != null)
          {
            loUserID = instructor.UserID;
          }
          else
          {
            if (!(paEmail.ToLower() == "admin@award.co.za"))
              return false;
            loUserID = aspNetUser.Id;
          }
          UserPasswordReset userPasswordReset = trainingEntities.UserPasswordResets.OrderByDescending<UserPasswordReset, DateTime>((Expression<Func<UserPasswordReset, DateTime>>) (ur => ur.DateTimeCreated)).Where<UserPasswordReset>((Expression<Func<UserPasswordReset, bool>>) (ur => ur.IsEnabled && ur.DateTimeCreated > loDateWindow && ur.UserID == loUserID && ur.Code == paCode)).FirstOrDefault<UserPasswordReset>();
          if (userPasswordReset == null)
            return false;
          userPasswordReset.IsEnabled = false;
          trainingEntities.SaveChanges();
          return true;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return false;
    }

    public static bool IsGeneralChatActive(string paUserID)
    {
      try
      {
        DateTime loDateWindow = DateTime.Now.AddMinutes(-30.0);
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<GeneralChatMessageLog> list = trainingEntities.GeneralChatMessageLogs.Where<GeneralChatMessageLog>((Expression<Func<GeneralChatMessageLog, bool>>) (cl => cl.UserID != paUserID && cl.DateTime_Created >= loDateWindow)).ToList<GeneralChatMessageLog>();
          return list != null && list.Count > 0;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    public static bool IsGraduateChatActive(string paUserID)
    {
      try
      {
        DateTime loDateWindow = DateTime.Now.AddMinutes(-30.0);
        using (BusinessTrainingEntities trainingEntities = new BusinessTrainingEntities())
        {
          List<ChatMessageLog> list = trainingEntities.ChatMessageLogs.Where<ChatMessageLog>((Expression<Func<ChatMessageLog, bool>>) (cl => cl.UserID != paUserID && cl.DateTime_Created >= loDateWindow)).ToList<ChatMessageLog>();
          return list != null && list.Count > 0;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }
  }
}
