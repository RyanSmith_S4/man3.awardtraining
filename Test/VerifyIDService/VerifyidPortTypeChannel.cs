﻿// Decompiled with JetBrains decompiler
// Type: Test.VerifyIDService.VerifyidPortTypeChannel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Test.VerifyIDService
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface VerifyidPortTypeChannel : VerifyidPortType, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
  {
  }
}
