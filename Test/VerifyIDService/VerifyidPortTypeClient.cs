﻿// Decompiled with JetBrains decompiler
// Type: Test.VerifyIDService.VerifyidPortTypeClient
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace Test.VerifyIDService
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class VerifyidPortTypeClient : ClientBase<VerifyidPortType>, VerifyidPortType
  {
    public VerifyidPortTypeClient()
    {
    }

    public VerifyidPortTypeClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public VerifyidPortTypeClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public VerifyidPortTypeClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public VerifyidPortTypeClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    doLoginResponse VerifyidPortType.doLogin(doLoginRequest request)
    {
      return this.Channel.doLogin(request);
    }

    public string doLogin(string username, string apikey)
    {
      return ((VerifyidPortType) this).doLogin(new doLoginRequest()
      {
        username = username,
        apikey = apikey
      }).Result;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<doLoginResponse> VerifyidPortType.doLoginAsync(
      doLoginRequest request)
    {
      return this.Channel.doLoginAsync(request);
    }

    public Task<doLoginResponse> doLoginAsync(string username, string apikey)
    {
      return ((VerifyidPortType) this).doLoginAsync(new doLoginRequest()
      {
        username = username,
        apikey = apikey
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    idphotoResponse VerifyidPortType.idphoto(idphotoRequest request)
    {
      return this.Channel.idphoto(request);
    }

    public string idphoto(string session_id, string apikey, string id_number)
    {
      return ((VerifyidPortType) this).idphoto(new idphotoRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      }).Result;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<idphotoResponse> VerifyidPortType.idphotoAsync(
      idphotoRequest request)
    {
      return this.Channel.idphotoAsync(request);
    }

    public Task<idphotoResponse> idphotoAsync(
      string session_id,
      string apikey,
      string id_number)
    {
      return ((VerifyidPortType) this).idphotoAsync(new idphotoRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    verifyResponse VerifyidPortType.verify(verifyRequest request)
    {
      return this.Channel.verify(request);
    }

    public string verify(string session_id, string apikey, string id_number)
    {
      return ((VerifyidPortType) this).verify(new verifyRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      }).Result;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<verifyResponse> VerifyidPortType.verifyAsync(
      verifyRequest request)
    {
      return this.Channel.verifyAsync(request);
    }

    public Task<verifyResponse> verifyAsync(
      string session_id,
      string apikey,
      string id_number)
    {
      return ((VerifyidPortType) this).verifyAsync(new verifyRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    profileResponse VerifyidPortType.profile(profileRequest request)
    {
      return this.Channel.profile(request);
    }

    public string profile(string session_id, string apikey, string id_number)
    {
      return ((VerifyidPortType) this).profile(new profileRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      }).Result;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<profileResponse> VerifyidPortType.profileAsync(
      profileRequest request)
    {
      return this.Channel.profileAsync(request);
    }

    public Task<profileResponse> profileAsync(
      string session_id,
      string apikey,
      string id_number)
    {
      return ((VerifyidPortType) this).profileAsync(new profileRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    tiny_traceResponse VerifyidPortType.tiny_trace(
      tiny_traceRequest request)
    {
      return this.Channel.tiny_trace(request);
    }

    public string tiny_trace(string session_id, string apikey, string id_number)
    {
      return ((VerifyidPortType) this).tiny_trace(new tiny_traceRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      }).Result;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<tiny_traceResponse> VerifyidPortType.tiny_traceAsync(
      tiny_traceRequest request)
    {
      return this.Channel.tiny_traceAsync(request);
    }

    public Task<tiny_traceResponse> tiny_traceAsync(
      string session_id,
      string apikey,
      string id_number)
    {
      return ((VerifyidPortType) this).tiny_traceAsync(new tiny_traceRequest()
      {
        session_id = session_id,
        apikey = apikey,
        id_number = id_number
      });
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    my_creditsResponse VerifyidPortType.my_credits(
      my_creditsRequest request)
    {
      return this.Channel.my_credits(request);
    }

    public string my_credits(string session_id, string apikey)
    {
      return ((VerifyidPortType) this).my_credits(new my_creditsRequest()
      {
        session_id = session_id,
        apikey = apikey
      }).Result;
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    Task<my_creditsResponse> VerifyidPortType.my_creditsAsync(
      my_creditsRequest request)
    {
      return this.Channel.my_creditsAsync(request);
    }

    public Task<my_creditsResponse> my_creditsAsync(
      string session_id,
      string apikey)
    {
      return ((VerifyidPortType) this).my_creditsAsync(new my_creditsRequest()
      {
        session_id = session_id,
        apikey = apikey
      });
    }
  }
}
