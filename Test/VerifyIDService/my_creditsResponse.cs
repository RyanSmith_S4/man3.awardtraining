﻿// Decompiled with JetBrains decompiler
// Type: Test.VerifyIDService.my_creditsResponse
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace Test.VerifyIDService
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [EditorBrowsable(EditorBrowsableState.Advanced)]
  [MessageContract(IsWrapped = true, WrapperName = "my_creditsResponse", WrapperNamespace = "urn:xmethods-delayed-login")]
  public class my_creditsResponse
  {
    [MessageBodyMember(Namespace = "", Order = 0)]
    public string Result;

    public my_creditsResponse()
    {
    }

    public my_creditsResponse(string Result)
    {
      this.Result = Result;
    }
  }
}
