﻿// Decompiled with JetBrains decompiler
// Type: Test.VerifyIDService.VerifyidPortType
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Test.VerifyIDService
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "VerifyIDService.VerifyidPortType", Namespace = "Verifyid")]
  public interface VerifyidPortType
  {
    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true, Use = OperationFormatUse.Encoded)]
    doLoginResponse doLogin(doLoginRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    Task<doLoginResponse> doLoginAsync(doLoginRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true, Use = OperationFormatUse.Encoded)]
    idphotoResponse idphoto(idphotoRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    Task<idphotoResponse> idphotoAsync(idphotoRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true, Use = OperationFormatUse.Encoded)]
    verifyResponse verify(verifyRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    Task<verifyResponse> verifyAsync(verifyRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true, Use = OperationFormatUse.Encoded)]
    profileResponse profile(profileRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    Task<profileResponse> profileAsync(profileRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true, Use = OperationFormatUse.Encoded)]
    tiny_traceResponse tiny_trace(tiny_traceRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    Task<tiny_traceResponse> tiny_traceAsync(tiny_traceRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    [XmlSerializerFormat(Style = OperationFormatStyle.Rpc, SupportFaults = true, Use = OperationFormatUse.Encoded)]
    my_creditsResponse my_credits(my_creditsRequest request);

    [OperationContract(Action = "urn:xmethods-delayed-login#doLogin", ReplyAction = "*")]
    Task<my_creditsResponse> my_creditsAsync(my_creditsRequest request);
  }
}
