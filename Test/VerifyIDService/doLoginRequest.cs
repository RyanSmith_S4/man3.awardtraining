﻿// Decompiled with JetBrains decompiler
// Type: Test.VerifyIDService.doLoginRequest
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace Test.VerifyIDService
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [EditorBrowsable(EditorBrowsableState.Advanced)]
  [MessageContract(IsWrapped = true, WrapperName = "doLogin", WrapperNamespace = "urn:xmethods-delayed-login")]
  public class doLoginRequest
  {
    [MessageBodyMember(Namespace = "", Order = 0)]
    public string username;
    [MessageBodyMember(Namespace = "", Order = 1)]
    public string apikey;

    public doLoginRequest()
    {
    }

    public doLoginRequest(string username, string apikey)
    {
      this.username = username;
      this.apikey = apikey;
    }
  }
}
