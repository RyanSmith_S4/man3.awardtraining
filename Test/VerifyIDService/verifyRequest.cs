﻿// Decompiled with JetBrains decompiler
// Type: Test.VerifyIDService.verifyRequest
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace Test.VerifyIDService
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [EditorBrowsable(EditorBrowsableState.Advanced)]
  [MessageContract(IsWrapped = true, WrapperName = "verify", WrapperNamespace = "urn:xmethods-delayed-login")]
  public class verifyRequest
  {
    [MessageBodyMember(Namespace = "", Order = 0)]
    public string session_id;
    [MessageBodyMember(Namespace = "", Order = 1)]
    public string apikey;
    [MessageBodyMember(Namespace = "", Order = 2)]
    public string id_number;

    public verifyRequest()
    {
    }

    public verifyRequest(string session_id, string apikey, string id_number)
    {
      this.session_id = session_id;
      this.apikey = apikey;
      this.id_number = id_number;
    }
  }
}
