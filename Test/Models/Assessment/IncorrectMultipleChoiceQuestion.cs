﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Assessment.IncorrectMultipleChoiceQuestion
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.Collections.Generic;

namespace Test.Models.Assessment
{
  public class IncorrectMultipleChoiceQuestion
  {
    public string Question { get; set; }

    public bool IsCorrect { get; set; }

    public string Answer { get; set; }

    public List<string> OtherAnswers { get; set; }
  }
}
