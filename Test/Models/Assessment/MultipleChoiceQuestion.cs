﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Assessment.MultipleChoiceQuestion
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.Collections.Generic;

namespace Test.Models.Assessment
{
  public class MultipleChoiceQuestion
  {
    public int ID { get; set; }

    public string Question { get; set; }

    public List<MultipleChoiceAnswer> Answers { get; set; }
  }
}
