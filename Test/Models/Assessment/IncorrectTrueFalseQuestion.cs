﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Assessment.IncorrectTrueFalseQuestion
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Assessment
{
  public class IncorrectTrueFalseQuestion
  {
    public string Question { get; set; }

    public string Answer { get; set; }
  }
}
