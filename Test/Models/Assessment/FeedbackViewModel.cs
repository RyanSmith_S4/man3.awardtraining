﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Assessment.FeedbackViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.Collections.Generic;

namespace Test.Models.Assessment
{
  public class FeedbackViewModel
  {
    public bool Passed { get; set; }

    public int ModuleID { get; set; }

    public int SubModuleID { get; set; }

    public string Code { get; set; }

    public int Attempt { get; set; }

    public string Name { get; set; }

    public int StudentMark { get; set; }

    public int TotalMark { get; set; }

    public string UserID { get; set; }

    public bool Appeal { get; set; }

    public int OverallMark { get; set; }

    public int OverallTotal { get; set; }

    public string Precentage { get; set; }

    public List<IncorrectMultipleChoiceQuestion> MultipleChoice { get; set; }

    public List<IncorrectTrueFalseQuestion> TrueFalse { get; set; }
  }
}
