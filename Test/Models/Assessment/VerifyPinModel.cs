﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Assessment.VerifyPinModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Assessment
{
  public class VerifyPinModel
  {
    public string Pin { get; set; }

    public string ReferenceNumber { get; set; }
  }
}
