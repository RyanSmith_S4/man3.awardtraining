﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Assessment.AssessmentViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.Collections.Generic;

namespace Test.Models.Assessment
{
  public class AssessmentViewModel
  {
    public int ID { get; set; }

    public int Attempts { get; set; }

    public bool IsVerified { get; set; }

    public string UserID { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string CellphoneNumber { get; set; }

    public int SubModuleID { get; set; }

    public string SubModuleCode { get; set; }

    public string SubModuleName { get; set; }

    public List<MultipleChoiceQuestion> MultipleChoice { get; set; }

    public List<TrueFalseQuestion> TrueOrFalse { get; set; }

    public List<LongQuestion> Worded { get; set; }

    public bool IsAssessed { get; set; }
  }
}
