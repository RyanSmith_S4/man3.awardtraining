﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Home.ContactMailModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Home
{
  public class ContactMailModel
  {
    public string Fullname { get; set; }

    public string Telephone { get; set; }

    public string Email { get; set; }

    public string Message { get; set; }
  }
}
