﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Report.ReportViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Report
{
  public class ReportViewModel
  {
    public string ReportType { get; set; }

    public string DateStart { get; set; }

    public string DateStop { get; set; }
  }
}
