﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.IndexViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Test.Models
{
  public class IndexViewModel
  {
    public bool HasPassword { get; set; }

    public IList<UserLoginInfo> Logins { get; set; }

    public string PhoneNumber { get; set; }

    public bool TwoFactor { get; set; }

    public bool BrowserRemembered { get; set; }
  }
}
