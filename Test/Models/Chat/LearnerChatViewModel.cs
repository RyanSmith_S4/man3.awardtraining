﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Chat.LearnerChatViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;

namespace Test.Models.Chat
{
  public class LearnerChatViewModel
  {
    public int ID { get; set; }

    public string UserID { get; set; }

    public string ChatName { get; set; }

    public Enumerations.enumUserType UserType { get; set; }
  }
}
