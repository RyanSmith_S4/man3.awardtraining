﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Instructor.UserAssessedFile
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using System;

namespace Test.Models.Instructor
{
  public class UserAssessedFile
  {
    public int ID { get; set; }

    public string Name { get; set; }

    public bool IsCorrect { get; set; }

    public bool IsAssessed { get; set; }

    public int SubModuleFileID { get; set; }

    public int Attempt { get; set; }

    public Enumerations.enumSystemFile SystemFile { get; set; }

    public Enumerations.enumTypeFile TypeFile { get; set; }

    public string Filepath { get; set; }

    public DateTime DateCreated { get; set; }
  }
}
