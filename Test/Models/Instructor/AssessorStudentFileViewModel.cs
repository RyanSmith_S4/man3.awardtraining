﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Instructor.AssessorStudentFileViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.Collections.Generic;

namespace Test.Models.Instructor
{
  public class AssessorStudentFileViewModel
  {
    public int StudentID { get; set; }

    public int Attempts { get; set; }

    public string UserID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public string TelephoneCell { get; set; }

    public string IDNumber { get; set; }

    public string FsaCode { get; set; }

    public bool AllChecked { get; set; }

    public bool AllRight { get; set; }

    public bool IsComplete { get; set; }

    public bool IsSuccessful { get; set; }

    public DateTime DateEnroled { get; set; }

    public DateTime LatestActivity { get; set; }

    public List<UserAssessedFile> Attempt1 { get; set; }

    public List<UserAssessedFile> Attempt2 { get; set; }
  }
}
