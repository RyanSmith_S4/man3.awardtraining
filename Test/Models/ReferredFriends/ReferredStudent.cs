﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.ReferredFriends.ReferredStudent
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;

namespace Test.Models.ReferredFriends
{
  public class ReferredStudent
  {
    public int StudentID { get; set; }

    public string UserID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public DateTime DateEnroled { get; set; }
  }
}
