﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.UserFile
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class UserFile
  {
    public int ID { get; set; }

    public string Name { get; set; }

    [Display(Name = "File Type")]
    public Enumerations.enumSystemFile SystemFile { get; set; }

    public Enumerations.enumTypeFile TypeFile { get; set; }

    public string Filepath { get; set; }

    [Display(Name = "Date Uploaded")]
    public DateTime DateCreated { get; set; }
  }
}
