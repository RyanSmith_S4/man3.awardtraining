﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AddSponsorViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class AddSponsorViewModel
  {
    [Required]
    [Display(Name = "First Name")]
    [StringLength(100)]
    public string FirstName { get; set; }

    [Required]
    [Display(Name = "Last Name")]
    [StringLength(100)]
    public string LastName { get; set; }

    [Required]
    [Display(Name = "Company")]
    [StringLength(100)]
    public string Company { get; set; }

    [Required]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    [Display(Name = "Email")]
    public string Email { get; set; }

    [Required]
    [Display(Name = "ContactEmail")]
    public string UserEmail { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Confirm password")]
    [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    public string ConfirmPassword { get; set; }

    [Required(ErrorMessage = "Your Cell Number is required.")]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Your Cell Number is invalid.")]
    [Display(Name = "Cell Number")]
    [StringLength(20)]
    public string TelephoneCell { get; set; }

    [Required(ErrorMessage = "You are required to enter at least 1 FSA Code.")]
    public string FsaCode1 { get; set; }

    public string FsaCode2 { get; set; }

    public string FsaCode3 { get; set; }

    public string FsaCode4 { get; set; }

    public string FsaCode5 { get; set; }
  }
}
