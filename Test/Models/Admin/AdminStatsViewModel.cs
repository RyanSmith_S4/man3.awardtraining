﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AdminStatsViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Admin
{
  public class AdminStatsViewModel
  {
    public int LearnerCount { get; set; }

    public int LearnerActivityCount { get; set; }

    public int LearnerActiveCount { get; set; }

    public int LearnerInactiveCount { get; set; }

    public int LearnerLockedCount { get; set; }
  }
}
