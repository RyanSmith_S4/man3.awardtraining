﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AdminSponsorViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class AdminSponsorViewModel
  {
    [Display(Name = "Sponsor ID")]
    public int SponsorID { get; set; }

    [Display(Name = "User ID")]
    public string UserID { get; set; }

    [Display(Name = "Company")]
    [StringLength(100)]
    public string Company { get; set; }

    [Display(Name = "First Name")]
    [StringLength(100)]
    public string FirstName { get; set; }

    [Display(Name = "Last Name")]
    [StringLength(100)]
    public string LastName { get; set; }

    [Display(Name = "Email")]
    [StringLength(250)]
    public string Email { get; set; }

    [Display(Name = "Date Created")]
    public DateTime DateCreated { get; set; }

    [Display(Name = "Latest Activity")]
    public DateTime LatestActivity { get; set; }

    public bool IsLocked { get; set; }
  }
}
