﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AdminMentorViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class AdminMentorViewModel
  {
    [Display(Name = "Mentor ID")]
    public int MentorID { get; set; }

    [Display(Name = "User ID")]
    public string UserID { get; set; }

    [Display(Name = "First Name")]
    [StringLength(100)]
    public string FirstName { get; set; }

    [Display(Name = "Last Name")]
    [StringLength(100)]
    public string LastName { get; set; }

    [Display(Name = "ID Number")]
    public string IDNumber { get; set; }

    [Display(Name = "Date Enroled")]
    public DateTime DateEnroled { get; set; }

    [Display(Name = "Latest Activity")]
    public DateTime LatestActivity { get; set; }

    public bool IsLocked { get; set; }

    public bool IsApproved { get; set; }
  }
}
