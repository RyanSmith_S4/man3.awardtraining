﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.ResourceDetailsViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class ResourceDetailsViewModel
  {
    [Display(Name = "Resource ID")]
    public int ResourceID { get; set; }

    [Display(Name = "Description")]
    public string Description { get; set; }

    [Display(Name = "Link")]
    public string Link { get; set; }

    [Display(Name = "Contact Person")]
    [StringLength(100)]
    public string ContactPerson { get; set; }

    [Display(Name = "Contact Person Email")]
    [StringLength(100)]
    public string ContactPersonEmail { get; set; }

    [Display(Name = "Contact Person Cell")]
    public string ContactPersonCell { get; set; }

    [Display(Name = "Date Created")]
    public DateTime DateCreated { get; set; }

    [Display(Name = "Logo")]
    public string Logo { get; set; }
  }
}
