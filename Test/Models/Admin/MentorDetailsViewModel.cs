﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.MentorDetailsViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class MentorDetailsViewModel
  {
    [Display(Name = "Mentor ID")]
    public int MentorID { get; set; }

    [Display(Name = "User ID")]
    public string UserID { get; set; }

    [Display(Name = "First Name")]
    [StringLength(100)]
    public string FirstName { get; set; }

    [Display(Name = "Last Name")]
    [StringLength(100)]
    public string LastName { get; set; }

    [Display(Name = "Nickname")]
    [StringLength(100)]
    public string Nickname { get; set; }

    [Display(Name = "ID Number")]
    public string IDNumber { get; set; }

    [Display(Name = "Cell Number")]
    public string CellNumber { get; set; }

    [Display(Name = "Age")]
    public string Age { get; set; }

    [Display(Name = "Gender")]
    public string Gender { get; set; }

    [Display(Name = "Race")]
    public string Race { get; set; }

    [Display(Name = "Home Language")]
    public string HomeLanguage { get; set; }

    [Display(Name = "Postal Address - Street 1")]
    public string PostalAddress1 { get; set; }

    [Display(Name = "Postal Address - Street 2")]
    public string PostalAddress2 { get; set; }

    [Display(Name = "Postal Address - Street 3")]
    public string PostalAddress3 { get; set; }

    [Display(Name = "Postal Address - City")]
    public string PostalAddressCity { get; set; }

    [Display(Name = "Postal Address - Province")]
    public string PostalAddressProvince { get; set; }

    [Display(Name = "Postal Address - Code")]
    public string PostalAddressCode { get; set; }

    [Display(Name = "Highest Qualification")]
    public string HighestQualification { get; set; }

    [Display(Name = "Highest Qualification Title")]
    public string HighestQualificationTitle { get; set; }

    [Display(Name = "Employed")]
    public string Employed { get; set; }

    [Display(Name = "Email")]
    public string Email { get; set; }

    [Display(Name = "Mentor Code")]
    public string MentorCode { get; set; }

    [Display(Name = "Date Applied")]
    public DateTime DateEnroled { get; set; }

    [Display(Name = "Date Approved")]
    public bool IsApproved { get; set; }

    public bool IsLocked { get; set; }

    public DateTime DateApproved { get; set; }

    public List<UserFile> UserFiles { get; set; }
  }
}
