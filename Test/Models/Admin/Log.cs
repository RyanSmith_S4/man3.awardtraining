﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.Log
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class Log
  {
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
    public DateTime Timestamp { get; set; }

    public string Description { get; set; }

    public Enumerations.enumLogType Type { get; set; }
  }
}
