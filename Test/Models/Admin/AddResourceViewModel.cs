﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AddResourceViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Test.Models.Admin
{
  public class AddResourceViewModel
  {
    [Required]
    [Display(Name = "Description")]
    [StringLength(200)]
    public string Description { get; set; }

    [Required]
    [Display(Name = "Link")]
    [StringLength(500)]
    public string Link { get; set; }

    [Required]
    [Display(Name = "Contact Person")]
    [StringLength(100)]
    public string ContactPerson { get; set; }

    [Display(Name = "Contact Person Cell Number")]
    [Required(ErrorMessage = "Your Cell Number is required.")]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Your Cell Number is invalid.")]
    [StringLength(20)]
    public string ContactCell { get; set; }

    [Required]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    [Display(Name = "Contact Person Email")]
    [StringLength(100)]
    public string ContactEmail { get; set; }

    [Required(ErrorMessage = "Please select a Logo.")]
    public HttpPostedFileBase Logo { get; set; }
  }
}
