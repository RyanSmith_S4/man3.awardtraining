﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.StudentLogsViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.Collections.Generic;

namespace Test.Models.Admin
{
  public class StudentLogsViewModel
  {
    public string UserID { get; set; }

    public int StudentID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string IDNumber { get; set; }

    public List<Log> Logs { get; set; }
  }
}
