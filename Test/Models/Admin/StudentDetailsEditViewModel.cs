﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.StudentDetailsEditViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class StudentDetailsEditViewModel
  {
    public int StudentID { get; set; }

    [Required]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    [Display(Name = "Email")]
    public string Email { get; set; }

    [DisplayName("First Name")]
    public string Firstname { get; set; }

    [DisplayName("Last Name")]
    public string Lastname { get; set; }

    public string IDNumber { get; set; }

    [Required(ErrorMessage = "Your nickname is required.")]
    [StringLength(100)]
    [DisplayName("Nickname")]
    public string Nickname { get; set; }

    [Required(ErrorMessage = "Your Age is required.")]
    [DisplayName("Age")]
    public int Age { get; set; }

    [DisplayName("Gender")]
    public Enumerations.enumGender SelectGender { get; set; }

    [Required(ErrorMessage = "Your home language is required.")]
    [StringLength(100)]
    [DisplayName("Home Language")]
    public string HomeLanguage { get; set; }

    [Required]
    public Enumerations.enumRace SelectedRace { get; set; }

    [Required(ErrorMessage = "Postal Address 1 is required.")]
    [StringLength(150)]
    public string PostalAddress1 { get; set; }

    [Required(ErrorMessage = "Postal Address 2 is required.")]
    [StringLength(150)]
    public string PostalAddress2 { get; set; }

    [Required(ErrorMessage = "Postal Address 3 is required.")]
    [StringLength(150)]
    public string PostalAddress3 { get; set; }

    [Required(ErrorMessage = "Postal Address City is required.")]
    [StringLength(150)]
    public string PostalAddressCity { get; set; }

    [Required(ErrorMessage = "Postal Address Code is required.")]
    [StringLength(4)]
    public string PostalAddressCode { get; set; }

    [Required(ErrorMessage = "Your Province is required.")]
    [StringLength(150)]
    public string PostalAddressProvince { get; set; }

    [Required(ErrorMessage = "Your Cell Number is required.")]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Your Cell Number is invalid.")]
    [DisplayName("Cell Number")]
    [StringLength(10)]
    public string TelephoneCell { get; set; }

    [Required(ErrorMessage = "Highest Qualification is required.")]
    [StringLength(150)]
    public string HighestQualification { get; set; }

    [Required(ErrorMessage = "Highest Qualification Title is required.")]
    [StringLength(150)]
    public string HighestQualificationTitle { get; set; }

    [Required(ErrorMessage = "Employed status is required.")]
    public Enumerations.enumTrueFalse CurrentlyEmployed { get; set; }
  }
}
