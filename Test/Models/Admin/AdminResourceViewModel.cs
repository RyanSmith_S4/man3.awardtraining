﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AdminResourceViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class AdminResourceViewModel
  {
    [Display(Name = "Resource ID")]
    public int ResourceID { get; set; }

    [Display(Name = "Description")]
    [StringLength(100)]
    public string Description { get; set; }

    [Display(Name = "Contact Person")]
    [StringLength(100)]
    public string ContactPerson { get; set; }

    [Display(Name = "Contact Person Email")]
    [StringLength(250)]
    public string Email { get; set; }

    [Display(Name = "Date Created")]
    public DateTime DateCreated { get; set; }
  }
}
