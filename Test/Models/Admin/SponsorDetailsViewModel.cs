﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.SponsorDetailsViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class SponsorDetailsViewModel
  {
    [Display(Name = "Sponsor ID")]
    public int SponsorID { get; set; }

    [Display(Name = "User ID")]
    public string UserID { get; set; }

    [Display(Name = "First Name")]
    [StringLength(100)]
    public string FirstName { get; set; }

    [Display(Name = "Last Name")]
    [StringLength(100)]
    public string LastName { get; set; }

    [Display(Name = "Company")]
    public string Company { get; set; }

    [Display(Name = "Cell Number")]
    public string CellNumber { get; set; }

    [Display(Name = "Email Address")]
    public string EmailAddress { get; set; }

    [Display(Name = "Date Created")]
    public DateTime DateCreated { get; set; }

    public List<FdaCodeModel> FsaCodes { get; set; }

    public bool IsLocked { get; set; }

    public string AddFsaCode { get; set; }
  }
}
