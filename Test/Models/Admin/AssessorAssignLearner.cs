﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Admin.AssessorAssignLearner
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System;
using System.ComponentModel.DataAnnotations;

namespace Test.Models.Admin
{
  public class AssessorAssignLearner
  {
    public int UnassignedAssessorID { get; set; }

    public int StudentID { get; set; }

    public string UserID { get; set; }

    [Display(Name = "First Name")]
    public string FirstName { get; set; }

    [Display(Name = "Last Name")]
    public string LastName { get; set; }

    [Display(Name = "Email")]
    public string Email { get; set; }

    [Display(Name = "Cell")]
    public string TelephoneCell { get; set; }

    [Display(Name = "ID Number")]
    public string IDNumber { get; set; }

    [Display(Name = "FSA Code")]
    public string FsaCode { get; set; }

    [Display(Name = "Date Enroled")]
    public DateTime DateEnroled { get; set; }
  }
}
