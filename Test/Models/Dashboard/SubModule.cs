﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Dashboard.SubModule
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Dashboard
{
  public class SubModule
  {
    public int ID { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public bool IsAvailable { get; set; }

    public bool IsCurrent { get; set; }

    public bool IsLocked { get; set; }

    public bool IsWait { get; set; }

    public bool IsUploadedFiles { get; set; }
  }
}
