﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Training.Module
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Training
{
  public class Module
  {
    public int ID { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public SubModule SubModule { get; set; }

    public int Order { get; set; }

    public bool IsAvailable { get; set; }

    public bool IsComplete { get; set; }
  }
}
