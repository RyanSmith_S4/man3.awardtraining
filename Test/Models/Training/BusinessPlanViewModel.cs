﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Training.BusinessPlanViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML.Classes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Test.Models.Training
{
  public class BusinessPlanViewModel
  {
    public Module Module { get; set; }

    public string UserID { get; set; }

    public bool IsLocked { get; set; }

    public bool IsGraduate { get; set; }

    public bool IsChatEnabled { get; set; }

    [Required(ErrorMessage = "Please select a CV.")]
    public HttpPostedFileBase CV1 { get; set; }

    public HttpPostedFileBase CV2 { get; set; }

    [Required(ErrorMessage = "Please select a Business Plan.")]
    public HttpPostedFileBase BusinessPlan { get; set; }

    public HttpPostedFileBase BusinessPlanDoc1 { get; set; }

    public HttpPostedFileBase BusinessPlanDoc2 { get; set; }

    public HttpPostedFileBase BusinessPlanDoc3 { get; set; }

    public List<UserFile> UserFiles { get; set; }
  }
}
