﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.Training.TrainingViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

namespace Test.Models.Training
{
  public class TrainingViewModel
  {
    public Module Module { get; set; }

    public bool IsLocked { get; set; }

    public bool IsGraduate { get; set; }

    public bool IsChatEnabled { get; set; }

    public string UserID { get; set; }
  }
}
