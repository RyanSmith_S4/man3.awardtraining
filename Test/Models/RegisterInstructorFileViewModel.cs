﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.RegisterInstructorFileViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Test.Models
{
  public class RegisterInstructorFileViewModel
  {
    public int ID { get; set; }

    [Required(ErrorMessage = "Please select a CV.")]
    public HttpPostedFileBase CV { get; set; }

    [Required(ErrorMessage = "Please select a qualification.")]
    public HttpPostedFileBase Qualification1 { get; set; }

    public HttpPostedFileBase Qualification2 { get; set; }
  }
}
