﻿// Decompiled with JetBrains decompiler
// Type: Test.Models.RegisterInstructorViewModel
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
  public class RegisterInstructorViewModel
  {
    [Required]
    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    [Display(Name = "Email")]
    public string Email { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Confirm password")]
    [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    public string ConfirmPassword { get; set; }

    [Required(ErrorMessage = "Your first name is required.")]
    [StringLength(100)]
    [DisplayName("First Name")]
    public string Firstname { get; set; }

    [Required(ErrorMessage = "Your last name is required.")]
    [StringLength(100)]
    [DisplayName("Last Name")]
    public string Lastname { get; set; }

    [Required(ErrorMessage = "Your nickname is required.")]
    [StringLength(100)]
    [DisplayName("Nickname")]
    public string Nickname { get; set; }

    [Required(ErrorMessage = "Your ID Number is required.")]
    [StringLength(20)]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid ID Number.")]
    [DisplayName("ID Number")]
    public string IdentityNumber { get; set; }

    [Required(ErrorMessage = "Your Age is required.")]
    [DisplayName("Age")]
    public int Age { get; set; }

    [Required(ErrorMessage = "Your Gender is required.")]
    [DisplayName("Gender")]
    public Enumerations.enumGender SelectGender { get; set; }

    [Required(ErrorMessage = "Your Home Language is required.")]
    [StringLength(100)]
    [DisplayName("Home Language")]
    public string HomeLanguage { get; set; }

    [Required(ErrorMessage = "Your Race is required.")]
    public Enumerations.enumRace SelectedRace { get; set; }

    [Required(ErrorMessage = "Postal Address 1 is required.")]
    [StringLength(150)]
    public string PostalAddress1 { get; set; }

    [Required(ErrorMessage = "Postal Address 2 is required.")]
    [StringLength(150)]
    public string PostalAddress2 { get; set; }

    [Required(ErrorMessage = "Postal Address 3 is required.")]
    [StringLength(150)]
    public string PostalAddress3 { get; set; }

    [Required(ErrorMessage = "Postal Address City is required.")]
    [StringLength(150)]
    public string PostalAddressCity { get; set; }

    [Required(ErrorMessage = "Postal Address Code is required.")]
    [StringLength(4)]
    public string PostalAddressCode { get; set; }

    [Required(ErrorMessage = "Postal Province is required.")]
    [StringLength(150)]
    public string PostalAddressProvince { get; set; }

    [Required(ErrorMessage = "Your Cell Number is required.")]
    [RegularExpression("^[0-9]*$", ErrorMessage = "Your Cell Number is invalid.")]
    [DisplayName("Cell Number")]
    [StringLength(20)]
    public string TelephoneCell { get; set; }

    [Required(ErrorMessage = "Highest Qualification is required.")]
    [StringLength(150)]
    public string HighestQualification { get; set; }

    [Required(ErrorMessage = "Highest Qualification Title is required.")]
    [StringLength(150)]
    public string HighestQualificationTitle { get; set; }

    [Required(ErrorMessage = "Employed status is required.")]
    public Enumerations.enumTrueFalse CurrentlyEmployed { get; set; }

    [DisplayName("Mentor Code")]
    [StringLength(100)]
    public string MentorCode { get; set; }

    public Enumerations.enumTrueFalse IsMentor { get; set; }

    [DisplayName("Assessor Code")]
    [StringLength(100)]
    public string AssessorCode { get; set; }

    public Enumerations.enumTrueFalse IsAssessor { get; set; }
  }
}
