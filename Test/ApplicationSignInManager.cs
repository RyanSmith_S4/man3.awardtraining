﻿// Decompiled with JetBrains decompiler
// Type: Test.ApplicationSignInManager
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using Test.Models;

namespace Test
{
  public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
  {
    public ApplicationSignInManager(
      ApplicationUserManager userManager,
      IAuthenticationManager authenticationManager)
      : base((UserManager<ApplicationUser, string>) userManager, authenticationManager)
    {
    }

    public override Task<ClaimsIdentity> CreateUserIdentityAsync(
      ApplicationUser user)
    {
      return user.GenerateUserIdentityAsync((UserManager<ApplicationUser>) this.UserManager);
    }

    public static ApplicationSignInManager Create(
      IdentityFactoryOptions<ApplicationSignInManager> options,
      IOwinContext context)
    {
      return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
    }
  }
}
