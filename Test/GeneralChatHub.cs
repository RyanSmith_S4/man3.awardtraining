﻿// Decompiled with JetBrains decompiler
// Type: Test.GeneralChatHub
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Test
{
  [HubName("generalChatHub")]
  public class GeneralChatHub : Hub
  {
    public void Send(string name, string message, string random)
    {
      // ISSUE: reference to a compiler-generated field
      if (GeneralChatHub.\u003C\u003Eo__0.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        GeneralChatHub.\u003C\u003Eo__0.\u003C\u003Ep__0 = CallSite<Action<CallSite, object, string, string, string>>.Create(Binder.InvokeMember(CSharpBinderFlags.ResultDiscarded, "addNewMessageToPage", (IEnumerable<Type>) null, typeof (GeneralChatHub), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[4]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      GeneralChatHub.\u003C\u003Eo__0.\u003C\u003Ep__0.Target((CallSite) GeneralChatHub.\u003C\u003Eo__0.\u003C\u003Ep__0, this.Clients.All, name, message, random);
    }
  }
}
