﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AssessmentController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using BLL;
using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Test.Models.Assessment;

namespace Test.Controllers
{
  [Authorize]
  public class AssessmentController : Controller
  {
    [HttpPost]
    public ActionResult SendVerificationCode()
    {
      string str = "";
      bool flag = false;
      try
      {
        string userId = this.User.Identity.GetUserId();
        str = Smser.SendVerificationSms(BLL.Student.GetStudent(userId).TelephoneCell, userId);
        if (str != "")
          flag = true;
      }
      catch (Exception ex)
      {
        COML.Logging.Log("Error sending verification SMS: " + ex.Message.ToString(), Enumerations.LogLevel.Error, (Exception) null);
      }
      return (ActionResult) this.Json((object) new
      {
        success = flag,
        ref_no = str
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult VerifyCode(VerifyPinModel paPin)
    {
      string userId = this.User.Identity.GetUserId();
      return (ActionResult) this.Json((object) new
      {
        success = Smser.VerifySms(paPin.ReferenceNumber, paPin.Pin, userId)
      }, JsonRequestBehavior.AllowGet);
    }

    public ActionResult Index(int paModuleID, int paSubModuleID)
    {
      if (this.Request.IsAuthenticated)
      {
        string userId = this.User.Identity.GetUserId();
        COML.Classes.Student student = BLL.Student.GetStudent(userId);
        if (student != null)
        {
          COML.Classes.Assessment moduleAssessment = BLL.Core.Assessment.GetModuleAssessment(paModuleID, paSubModuleID, userId);
          if (moduleAssessment.Code == null && moduleAssessment.Name == null && (moduleAssessment.MultipleChoice == null && moduleAssessment.TrueOrFalse == null))
          {
            COML.Logging.Log("No assessment details found.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction(nameof (Index), "Dashboard");
          }
          AssessmentViewModel assessmentViewModel = new AssessmentViewModel()
          {
            Attempts = moduleAssessment.Attempts,
            ID = moduleAssessment.ID,
            Code = moduleAssessment.Code,
            Name = moduleAssessment.Name,
            SubModuleID = moduleAssessment.SubModuleID,
            SubModuleCode = moduleAssessment.SubModuleCode,
            SubModuleName = moduleAssessment.SubModuleName,
            UserID = userId,
            IsAssessed = moduleAssessment.IsAssessed,
            CellphoneNumber = "0" + student.TelephoneCell.Substring(2, student.TelephoneCell.Length - 2)
          };
          assessmentViewModel.MultipleChoice = new List<Test.Models.Assessment.MultipleChoiceQuestion>();
          foreach (COML.Classes.Assessment.MultipleChoiceQuestion multipleChoiceQuestion1 in moduleAssessment.MultipleChoice)
          {
            Test.Models.Assessment.MultipleChoiceQuestion multipleChoiceQuestion2 = new Test.Models.Assessment.MultipleChoiceQuestion()
            {
              ID = multipleChoiceQuestion1.ID,
              Question = multipleChoiceQuestion1.Question
            };
            multipleChoiceQuestion2.Answers = new List<Test.Models.Assessment.MultipleChoiceAnswer>();
            for (int index = 0; index < multipleChoiceQuestion1.Answers.Count<COML.Classes.Assessment.MultipleChoiceAnswer>(); ++index)
            {
              COML.Classes.Assessment.MultipleChoiceAnswer answer = multipleChoiceQuestion1.Answers[index];
              string str = "";
              switch (index)
              {
                case 0:
                  str = "a. ";
                  break;
                case 1:
                  str = "b. ";
                  break;
                case 2:
                  str = "c. ";
                  break;
                case 3:
                  str = "d. ";
                  break;
                case 4:
                  str = "e. ";
                  break;
              }
              multipleChoiceQuestion2.Answers.Add(new Test.Models.Assessment.MultipleChoiceAnswer()
              {
                ID = answer.ID,
                Answer = str + answer.Answer
              });
            }
            assessmentViewModel.MultipleChoice.Add(multipleChoiceQuestion2);
          }
          assessmentViewModel.TrueOrFalse = new List<Test.Models.Assessment.TrueFalseQuestion>();
          for (int index = 0; index < moduleAssessment.TrueOrFalse.Count<COML.Classes.Assessment.TrueFalseQuestion>(); ++index)
          {
            COML.Classes.Assessment.TrueFalseQuestion trueFalseQuestion = moduleAssessment.TrueOrFalse[index];
            assessmentViewModel.TrueOrFalse.Add(new Test.Models.Assessment.TrueFalseQuestion()
            {
              ID = trueFalseQuestion.ID,
              Question = trueFalseQuestion.Question
            });
          }
          assessmentViewModel.IsVerified = false;
          BLL.Logging.SavePageLog(userId, "Assessment Page - " + assessmentViewModel.SubModuleName);
          return (ActionResult) this.View((object) assessmentViewModel);
        }
        COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction("LogOffWithoutPost", "Account");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }

    [HttpPost]
    public void MultipleChoiceAnswer(
      int paModuleID,
      int paSubModuleID,
      int paMultipleChoiceID,
      int paMultipleChoiceAnswerID,
      string paUserID,
      bool paFinish)
    {
      BLL.Core.Assessment.AddUserMultipleChoiceResult(paModuleID, paSubModuleID, paMultipleChoiceID, paMultipleChoiceAnswerID, paUserID, paFinish);
    }

    [HttpPost]
    public void TrueFalseAnswer(
      int paModuleID,
      int paSubModuleID,
      int paTrueFalseID,
      int paTrueFalseAnswer,
      string paUserID,
      bool paFinish)
    {
      bool paTrueFalseAnswer1 = paTrueFalseAnswer != 0;
      BLL.Core.Assessment.AddUserTrueFalseResult(paModuleID, paSubModuleID, paTrueFalseID, paTrueFalseAnswer1, paUserID, paFinish);
    }

    public ActionResult Finalise(AssessmentViewModel paModel)
    {
      return (ActionResult) this.RedirectToAction("Feedback", "Assessment", (object) new
      {
        paModuleID = paModel.ID,
        paSubModuleID = paModel.SubModuleID
      });
    }

    public ActionResult Feedback(int paModuleID, int paSubModuleID)
    {
      if (this.Request.IsAuthenticated)
      {
        try
        {
          string userId = this.User.Identity.GetUserId();
          COML.Classes.Student student = BLL.Student.GetStudent(userId);
          FeedbackViewModel feedbackViewModel1 = new FeedbackViewModel();
          DateTime dateTime = new DateTime();
          AssessmentResult assessmentResult = BLL.Core.Assessment.MarkUserAssessment(paModuleID, paSubModuleID, userId);
          COML.Logging.Log("Assessment marking time in seconds: " + (object) (new DateTime() - dateTime).Seconds, Enumerations.LogLevel.Info, (Exception) null);
          string paPercentage = string.Format("{0:P2}", (object) ((double) assessmentResult.OverallMark / (double) assessmentResult.OverallTotal));
          feedbackViewModel1.Attempt = assessmentResult.Attempt;
          feedbackViewModel1.Passed = assessmentResult.Passed;
          feedbackViewModel1.Code = assessmentResult.Code;
          feedbackViewModel1.Name = assessmentResult.Name;
          feedbackViewModel1.TotalMark = assessmentResult.TotalMark;
          feedbackViewModel1.StudentMark = assessmentResult.StudentMark;
          feedbackViewModel1.ModuleID = paModuleID;
          feedbackViewModel1.SubModuleID = paSubModuleID;
          feedbackViewModel1.UserID = userId;
          feedbackViewModel1.OverallMark = assessmentResult.OverallMark;
          feedbackViewModel1.OverallTotal = assessmentResult.OverallTotal;
          feedbackViewModel1.Precentage = paPercentage;
          FeedbackViewModel feedbackViewModel2 = feedbackViewModel1;
          feedbackViewModel2.Appeal = !feedbackViewModel2.Passed && feedbackViewModel1.Attempt >= 2;
          feedbackViewModel1.MultipleChoice = new List<Test.Models.Assessment.IncorrectMultipleChoiceQuestion>();
          feedbackViewModel1.TrueFalse = new List<Test.Models.Assessment.IncorrectTrueFalseQuestion>();
          foreach (COML.Classes.IncorrectMultipleChoiceQuestion multipleChoiceQuestion1 in assessmentResult.MultipleChoice)
          {
            Test.Models.Assessment.IncorrectMultipleChoiceQuestion multipleChoiceQuestion2 = new Test.Models.Assessment.IncorrectMultipleChoiceQuestion()
            {
              Question = multipleChoiceQuestion1.Question,
              Answer = multipleChoiceQuestion1.Answer
            };
            multipleChoiceQuestion2.OtherAnswers = new List<string>();
            foreach (string otherAnswer in multipleChoiceQuestion1.OtherAnswers)
              multipleChoiceQuestion2.OtherAnswers.Add(otherAnswer);
            feedbackViewModel1.MultipleChoice.Add(multipleChoiceQuestion2);
          }
          foreach (TrueFalseAnswer trueFalseAnswer in assessmentResult.TrueFalse)
          {
            Test.Models.Assessment.IncorrectTrueFalseQuestion trueFalseQuestion = new Test.Models.Assessment.IncorrectTrueFalseQuestion()
            {
              Question = trueFalseAnswer.Question,
              Answer = trueFalseAnswer.Answer.ToString()
            };
            feedbackViewModel1.TrueFalse.Add(trueFalseQuestion);
          }
          if (feedbackViewModel1.Passed)
          {
            Mailer.SendResultEmail(student.EmailAddress, feedbackViewModel1.Code, feedbackViewModel1.Name, student.IdentityNumber, student.FirstName + " " + student.LastName, true, feedbackViewModel1.StudentMark, feedbackViewModel1.TotalMark, feedbackViewModel1.Attempt, userId, assessmentResult.OverallMark, assessmentResult.OverallTotal, paPercentage);
            Smser.SendResultSMS(student.Nickname, student.TelephoneCell, feedbackViewModel1.Code, feedbackViewModel1.Name, true, 0, 0, feedbackViewModel1.Attempt, userId, assessmentResult.OverallMark, assessmentResult.OverallTotal);
          }
          else if (feedbackViewModel1.MultipleChoice.Count<Test.Models.Assessment.IncorrectMultipleChoiceQuestion>() > 0 || feedbackViewModel1.TrueFalse.Count<Test.Models.Assessment.IncorrectTrueFalseQuestion>() > 0)
          {
            Mailer.SendResultEmail(student.EmailAddress, feedbackViewModel1.Code, feedbackViewModel1.Name, student.IdentityNumber, student.FirstName + " " + student.LastName, false, feedbackViewModel1.StudentMark, feedbackViewModel1.TotalMark, feedbackViewModel1.Attempt, userId, assessmentResult.OverallMark, assessmentResult.OverallTotal, paPercentage);
            Smser.SendResultSMS(student.Nickname, student.TelephoneCell, feedbackViewModel1.Code, feedbackViewModel1.Name, false, feedbackViewModel1.StudentMark, feedbackViewModel1.TotalMark, feedbackViewModel1.Attempt, userId, assessmentResult.OverallMark, assessmentResult.OverallTotal);
          }
          BLL.Logging.SavePageLog(userId, "Feedback Page - " + feedbackViewModel1.Name);
          return (ActionResult) this.View((object) feedbackViewModel1);
        }
        catch (Exception ex)
        {
          COML.Logging.Log(ex.Message.ToString(), Enumerations.LogLevel.Error, (Exception) null);
          return (ActionResult) this.RedirectToAction("Index", "Dashboard");
        }
      }
      else
      {
        COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
    }

    [HttpPost]
    public ActionResult SendAppealMail(FeedbackMailModel paModel)
    {
      bool flag = false;
      try
      {
        COML.Classes.Student student = BLL.Student.GetStudent(paModel.UserID);
        Mailer.SendAppealEmail(student.IdentityNumber, student.EmailAddress, paModel.Message, paModel.UserID);
        flag = true;
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.Json((object) new
      {
        success = flag
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult SendFeedbackMail(FeedbackMailModel paModel)
    {
      bool flag = false;
      try
      {
        COML.Classes.Student student = BLL.Student.GetStudent(paModel.UserID);
        Mailer.SendFeedbackEmail(student.IdentityNumber, student.EmailAddress, paModel.Message, paModel.UserID);
        flag = true;
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.Json((object) new
      {
        success = flag
      }, JsonRequestBehavior.AllowGet);
    }
  }
}
