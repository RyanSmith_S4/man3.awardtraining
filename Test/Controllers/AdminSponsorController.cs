﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminSponsorController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using BLL;
using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.Mvc;
using Test.Models.Admin;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  public class AdminSponsorController : Controller
  {
    public ActionResult Index(int paId = 1)
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          List<SponsorSummary> sponsors = BLL.Sponsor.GetSponsors();
          List<AdminSponsorViewModel> allItems = new List<AdminSponsorViewModel>();
          foreach (SponsorSummary sponsorSummary in sponsors)
          {
            AdminSponsorViewModel sponsorViewModel = new AdminSponsorViewModel()
            {
              SponsorID = sponsorSummary.SponsortID,
              UserID = sponsorSummary.UserID,
              DateCreated = sponsorSummary.DateTimeCreated,
              Company = sponsorSummary.Company,
              FirstName = sponsorSummary.FirstName,
              LastName = sponsorSummary.LastName,
              LatestActivity = sponsorSummary.LastActivity,
              IsLocked = sponsorSummary.IsLocked
            };
            allItems.Add(sponsorViewModel);
          }
          return (ActionResult) this.View((object) allItems.ToPagedList<AdminSponsorViewModel>(paId, 10));
        }
        COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }

    public ActionResult Search(int paId = 1, string paWord = null)
    {
      List<SponsorSummary> sponsorSummaryList1 = new List<SponsorSummary>();
      List<SponsorSummary> sponsorSummaryList2 = string.IsNullOrWhiteSpace(paWord) ? BLL.Sponsor.GetSponsors() : BLL.Sponsor.GetStudentsByFsaCode(paWord.ToUpper());
      List<AdminSponsorViewModel> allItems = new List<AdminSponsorViewModel>();
      foreach (SponsorSummary sponsorSummary in sponsorSummaryList2)
      {
        AdminSponsorViewModel sponsorViewModel = new AdminSponsorViewModel()
        {
          SponsorID = sponsorSummary.SponsortID,
          UserID = sponsorSummary.UserID,
          DateCreated = sponsorSummary.DateTimeCreated,
          Company = sponsorSummary.Company,
          FirstName = sponsorSummary.FirstName,
          LastName = sponsorSummary.LastName,
          LatestActivity = sponsorSummary.LastActivity,
          IsLocked = sponsorSummary.IsLocked
        };
        allItems.Add(sponsorViewModel);
      }
      return (ActionResult) this.View("Index", (object) allItems.ToPagedList<AdminSponsorViewModel>(paId, 10));
    }

    public ActionResult AddSponsor()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> AddSponsor(AddSponsorViewModel model)
    {
      try
      {
        if (this.User.Identity.IsAuthenticated)
        {
          if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
          {
            if (this.ModelState.IsValid)
            {
              AccountController accountController = new AccountController();
              if (!accountController.UserExists(model.Email))
              {
                string str = accountController.RegisterSponsor(model.Email, model.Password);
                if (str.ToString() != "")
                {
                  COML.Classes.Sponsor paSponsor = new COML.Classes.Sponsor()
                  {
                    Company = model.Company,
                    DateTimeCreated = DateTime.Now,
                    FirstName = model.FirstName,
                    IsLocked = false,
                    IsEnabled = true,
                    EmailAddress = model.Email,
                    UserEmailAddress = model.UserEmail,
                    LastActivity = DateTime.Now,
                    LastName = model.LastName,
                    TelephoneCell = model.TelephoneCell,
                    UserID = str.ToString()
                  };
                  paSponsor.FsaCodes = new List<FsaCode>();
                  if (model.FsaCode1 != null && model.FsaCode1 != "")
                    paSponsor.FsaCodes.Add(new FsaCode()
                    {
                      Code = model.FsaCode1.ToUpper()
                    });
                  if (model.FsaCode2 != null && model.FsaCode2 != "")
                    paSponsor.FsaCodes.Add(new FsaCode()
                    {
                      Code = model.FsaCode2.ToUpper()
                    });
                  if (model.FsaCode3 != null && model.FsaCode3 != "")
                    paSponsor.FsaCodes.Add(new FsaCode()
                    {
                      Code = model.FsaCode3.ToUpper()
                    });
                  if (model.FsaCode4 != null && model.FsaCode4 != "")
                    paSponsor.FsaCodes.Add(new FsaCode()
                    {
                      Code = model.FsaCode4.ToUpper()
                    });
                  if (model.FsaCode5 != null && model.FsaCode5 != "")
                    paSponsor.FsaCodes.Add(new FsaCode()
                    {
                      Code = model.FsaCode5.ToUpper()
                    });
                  if (!BLL.Sponsor.AddSponsor(paSponsor))
                  {
                    this.ModelState.AddModelError("", "Error processing your request.");
                  }
                  else
                  {
                    Mailer.SendSponsorRegistrationEmail(model.UserEmail, model.FirstName, model.LastName, paSponsor.UserID, paSponsor.UserEmailAddress, model.Password, model.Company);
                    return (ActionResult) this.View("AddSponsorConfirmation");
                  }
                }
                else
                  this.ModelState.AddModelError("", "Error processing your request.");
              }
              else
                this.ModelState.AddModelError("Email", "The email address used as a username is already in use.");
            }
          }
          else
          {
            COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction("Index", "Home");
          }
        }
        else
        {
          COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) model);
    }

    public ActionResult SponsorLogs(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Sponsor sponsorById = BLL.Sponsor.GetSponsorByID(paId);
      List<GenericLog> sponsorLogs = BLL.Logging.GetSponsorLogs(paId);
      SponsorLogsViewModel sponsorLogsViewModel = new SponsorLogsViewModel();
      if (sponsorById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminSponsor");
      sponsorLogsViewModel.FirstName = sponsorById.FirstName;
      sponsorLogsViewModel.LastName = sponsorById.LastName;
      sponsorLogsViewModel.SponsorID = sponsorById.ID;
      sponsorLogsViewModel.Company = sponsorById.Company;
      sponsorLogsViewModel.Logs = new List<Log>();
      foreach (GenericLog genericLog in sponsorLogs)
        sponsorLogsViewModel.Logs.Add(new Log()
        {
          Timestamp = genericLog.TimeStamp,
          Description = genericLog.Description,
          Type = genericLog.Type
        });
      return (ActionResult) this.View((object) sponsorLogsViewModel);
    }

    public ActionResult DownloadSponsorLogReport(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        COML.Classes.Sponsor sponsorById = BLL.Sponsor.GetSponsorByID(paId);
        List<GenericLog> sponsorLogs = BLL.Logging.GetSponsorLogs(paId);
        if (sponsorLogs == null || sponsorLogs.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("SPONSOR LOG EXPORT - " + sponsorById.FirstName + " " + sponsorById.LastName + " - " + sponsorById.Company + " - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "Timestamp");
        excelWorksheet.SetValue(3, 2, (object) "Type");
        excelWorksheet.SetValue(3, 3, (object) "Description");
        using (ExcelRange cell = excelWorksheet.Cells["A3:C3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < sponsorLogs.Count; ++index)
        {
          GenericLog genericLog = sponsorLogs[index];
          excelWorksheet.SetValue(index + 4, 1, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) genericLog.TimeStamp));
          excelWorksheet.SetValue(index + 4, 2, (object) genericLog.Type);
          excelWorksheet.SetValue(index + 4, 3, (object) genericLog.Description);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LogExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminSponsor");
      }
    }

    public ActionResult DownloadSponsorsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<SponsorSummary> sponsors = BLL.Sponsor.GetSponsors();
        if (sponsors == null || sponsors.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("SPONSOR EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "First Name");
        excelWorksheet.SetValue(3, 2, (object) "Last Name");
        excelWorksheet.SetValue(3, 3, (object) "Company");
        excelWorksheet.SetValue(3, 4, (object) "Email");
        excelWorksheet.SetValue(3, 5, (object) "Date Enroled");
        using (ExcelRange cell = excelWorksheet.Cells["A3:E3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < sponsors.Count; ++index)
        {
          SponsorSummary sponsorSummary = sponsors[index];
          excelWorksheet.SetValue(index + 4, 1, (object) sponsorSummary.FirstName);
          excelWorksheet.SetValue(index + 4, 2, (object) sponsorSummary.LastName);
          excelWorksheet.SetValue(index + 4, 3, (object) sponsorSummary.Company);
          excelWorksheet.SetValue(index + 4, 4, (object) sponsorSummary.EmailAddress);
          excelWorksheet.SetValue(index + 4, 5, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) sponsorSummary.DateTimeCreated));
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("SponsorExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      }
    }

    public ActionResult SponsorDetails(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Sponsor sponsorById = BLL.Sponsor.GetSponsorByID(paId);
      SponsorDetailsViewModel detailsViewModel = new SponsorDetailsViewModel();
      if (sponsorById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminSponsor");
      detailsViewModel.CellNumber = sponsorById.TelephoneCell;
      detailsViewModel.DateCreated = sponsorById.DateTimeCreated;
      detailsViewModel.FirstName = sponsorById.FirstName;
      detailsViewModel.LastName = sponsorById.LastName;
      detailsViewModel.Company = sponsorById.Company;
      detailsViewModel.SponsorID = sponsorById.ID;
      detailsViewModel.EmailAddress = sponsorById.EmailAddress;
      detailsViewModel.UserID = sponsorById.UserID;
      detailsViewModel.IsLocked = sponsorById.IsLocked;
      detailsViewModel.FsaCodes = new List<FdaCodeModel>();
      if (sponsorById.FsaCodes.Count > 0)
      {
        foreach (FsaCode fsaCode in sponsorById.FsaCodes)
          detailsViewModel.FsaCodes.Add(new FdaCodeModel()
          {
            Code = fsaCode.Code
          });
      }
      return (ActionResult) this.View((object) detailsViewModel);
    }

    public ActionResult RemoveFsaCode(int paSponsorID, string paCode)
    {
      try
      {
        if (this.User.Identity.IsAuthenticated)
        {
          if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
          {
            if (BLL.Sponsor.RemoveSponsorFsaCode(paSponsorID, paCode))
              return (ActionResult) this.RedirectToAction("SponsorDetails", "AdminSponsor", (object) new
              {
                paId = paSponsorID
              });
          }
          else
          {
            COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction("Index", "Home");
          }
        }
        else
        {
          COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.RedirectToAction("SponsorDetails", "AdminSponsor", (object) new
      {
        paId = paSponsorID
      });
    }

    public ActionResult AddSponsorFsaCode(SponsorDetailsViewModel model)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (model.AddFsaCode != null)
      {
        if (!(model.AddFsaCode == ""))
        {
          try
          {
            if (BLL.Sponsor.AddSponsorFsaCode(model.SponsorID, model.AddFsaCode.ToUpper()))
            {
              this.TempData["Status"] = (object) true;
              this.TempData["Message"] = (object) "FSA Code successfully added.";
            }
            else
            {
              this.TempData["Status"] = (object) false;
              this.TempData["Message"] = (object) "An error occurred";
            }
          }
          catch (Exception ex)
          {
            COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
            this.TempData["Status"] = (object) false;
            this.TempData["Message"] = (object) "An error occurred";
          }
          return (ActionResult) this.RedirectToAction("SponsorDetails", "AdminSponsor", (object) new
          {
            paId = model.SponsorID
          });
        }
      }
      this.TempData["Status"] = (object) false;
      this.TempData["Message"] = (object) "Please enter an FSA Code.";
      return (ActionResult) this.RedirectToAction("SponsorDetails", "AdminSponsor", (object) new
      {
        paId = model.SponsorID
      });
    }

    public ActionResult SponsorUnlock(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Sponsor.UnlockSponsor(paId);
      return (ActionResult) this.RedirectToAction("SponsorDetails", "AdminSponsor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult SponsorLock(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Sponsor.LockSponsor(paId);
      return (ActionResult) this.RedirectToAction("SponsorDetails", "AdminSponsor", (object) new
      {
        paId = paId
      });
    }
  }
}
