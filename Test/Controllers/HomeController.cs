﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.HomeController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using BLL;
using COML;
using Microsoft.AspNet.Identity;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using Test.Models.Home;

namespace Test.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction(nameof (Index), "AdminStudent");
      if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Sponsor"))
        return (ActionResult) this.RedirectToAction(nameof (Index), "Sponsor");
      if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Student"))
        return (ActionResult) this.RedirectToAction(nameof (Index), "Dashboard");
      return (ActionResult) this.View();
    }

    public ActionResult About()
    {
      // ISSUE: reference to a compiler-generated field
      if (HomeController.\u003C\u003Eo__1.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HomeController.\u003C\u003Eo__1.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "Message", typeof (HomeController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType | CSharpArgumentInfoFlags.Constant, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = HomeController.\u003C\u003Eo__1.\u003C\u003Ep__0.Target((CallSite) HomeController.\u003C\u003Eo__1.\u003C\u003Ep__0, this.ViewBag, "Your application description page.");
      return (ActionResult) this.View();
    }

    public ActionResult Contact()
    {
      // ISSUE: reference to a compiler-generated field
      if (HomeController.\u003C\u003Eo__2.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        HomeController.\u003C\u003Eo__2.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "Message", typeof (HomeController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType | CSharpArgumentInfoFlags.Constant, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = HomeController.\u003C\u003Eo__2.\u003C\u003Ep__0.Target((CallSite) HomeController.\u003C\u003Eo__2.\u003C\u003Ep__0, this.ViewBag, "Your contact page.");
      return (ActionResult) this.View();
    }

    [HttpPost]
    public ActionResult SendContactMail(ContactMailModel paModel)
    {
      bool flag = false;
      try
      {
        flag = Mailer.SendContactEmail(paModel.Fullname, paModel.Email, paModel.Message, paModel.Telephone);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.Json((object) new
      {
        success = flag
      }, JsonRequestBehavior.AllowGet);
    }

    public ActionResult Dashboard()
    {
      return (ActionResult) this.View();
    }

    public ActionResult Training()
    {
      return (ActionResult) this.View();
    }
  }
}
