﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.DashboardController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Test.Models.Dashboard;

namespace Test.Controllers
{
  public class DashboardController : Controller
  {
    public ActionResult Index()
    {
      DateTime now = DateTime.Now;
      try
      {
        if (this.Request.IsAuthenticated)
        {
          DashboardViewModel dashboardViewModel = new DashboardViewModel();
          string userId = this.User.Identity.GetUserId();
          List<COML.Classes.Module> modulesForUser = BLL.Module.GetModulesForUser(userId);
          COML.Classes.Student student = BLL.Student.GetStudent(userId);
          if (student != null)
          {
            if (student != null && student.Nickname != "")
              this.Session["UserNickname"] = (object) student.Nickname;
            dashboardViewModel.UserID = userId;
            dashboardViewModel.IsLocked = student.IsLocked;
            dashboardViewModel.Modules = new List<Test.Models.Dashboard.Module>();
            dashboardViewModel.IsGraduate = false;
            dashboardViewModel.IsChatEnabled = student.IsChatEnabled;
            foreach (COML.Classes.Module module1 in modulesForUser)
            {
              Test.Models.Dashboard.Module module2 = new Test.Models.Dashboard.Module()
              {
                ID = module1.ID,
                Code = module1.Code,
                Description = module1.Description,
                Name = module1.Name,
                Order = module1.Order,
                IsAvailable = module1.IsAvailable,
                IsComplete = module1.IsCompleted,
                IsLocked = false
              };
              module2.SubModules = new List<Test.Models.Dashboard.SubModule>();
              foreach (COML.Classes.SubModule subModule1 in module1.SubModules)
              {
                bool flag1 = false;
                bool flag2 = false;
                Test.Models.Dashboard.SubModule subModule2 = new Test.Models.Dashboard.SubModule()
                {
                  ID = subModule1.ID,
                  Code = subModule1.Code,
                  Description = subModule1.Description,
                  Name = subModule1.Name,
                  IsAvailable = subModule1.IsAvailable,
                  IsCurrent = subModule1.IsCurrent,
                  IsLocked = subModule1.IsLocked,
                  IsWait = flag1,
                  IsUploadedFiles = flag2
                };
                if (subModule2.IsLocked)
                {
                  module2.IsLocked = true;
                  dashboardViewModel.IsLocked = true;
                }
                subModule2.Code = subModule2.Code == "" ? subModule2.Code : " - " + subModule2.Code;
                module2.SubModules.Add(subModule2);
              }
              dashboardViewModel.Modules.Add(module2);
            }
            BLL.Logging.SavePageLog(userId, "Skills Programme");
            COML.Logging.Log("Dashboard Index: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.View((object) dashboardViewModel);
          }
          COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
          COML.Logging.Log("Dashboard Index: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
          return (ActionResult) this.RedirectToAction("LogOffWithoutPost", "Account");
        }
        COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
        COML.Logging.Log("Dashboard Index: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction("Login", "Account");
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        COML.Logging.Log("Dashboard Index: " + DateTime.Now.Subtract(now).TotalSeconds.ToString(), Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
    }
  }
}
