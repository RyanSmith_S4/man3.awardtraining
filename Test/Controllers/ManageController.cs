﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.ManageController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Test.Models;

namespace Test.Controllers
{
  [Authorize]
  public class ManageController : Controller
  {
    private ApplicationSignInManager _signInManager;
    private ApplicationUserManager _userManager;
    private const string XsrfKey = "XsrfId";

    public ManageController()
    {
    }

    public ManageController(
      ApplicationUserManager userManager,
      ApplicationSignInManager signInManager)
    {
      this.UserManager = userManager;
      this.SignInManager = signInManager;
    }

    public ApplicationSignInManager SignInManager
    {
      get
      {
        return this._signInManager ?? this.HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
      }
      private set
      {
        this._signInManager = value;
      }
    }

    public ApplicationUserManager UserManager
    {
      get
      {
        return this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
      }
      private set
      {
        this._userManager = value;
      }
    }

    public async Task<ActionResult> Index(ManageController.ManageMessageId? message)
    {
      // ISSUE: reference to a compiler-generated field
      if (ManageController.\u003C\u003Eo__10.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ManageController.\u003C\u003Eo__10.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "StatusMessage", typeof (ManageController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      Func<CallSite, object, string, object> target = ManageController.\u003C\u003Eo__10.\u003C\u003Ep__0.Target;
      // ISSUE: reference to a compiler-generated field
      CallSite<Func<CallSite, object, string, object>> p0 = ManageController.\u003C\u003Eo__10.\u003C\u003Ep__0;
      object viewBag = this.ViewBag;
      ManageController.ManageMessageId? nullable = message;
      ManageController.ManageMessageId manageMessageId1 = ManageController.ManageMessageId.ChangePasswordSuccess;
      string str;
      if ((nullable.GetValueOrDefault() == manageMessageId1 ? (nullable.HasValue ? 1 : 0) : 0) == 0)
      {
        nullable = message;
        ManageController.ManageMessageId manageMessageId2 = ManageController.ManageMessageId.SetPasswordSuccess;
        if ((nullable.GetValueOrDefault() == manageMessageId2 ? (nullable.HasValue ? 1 : 0) : 0) == 0)
        {
          nullable = message;
          ManageController.ManageMessageId manageMessageId3 = ManageController.ManageMessageId.SetTwoFactorSuccess;
          if ((nullable.GetValueOrDefault() == manageMessageId3 ? (nullable.HasValue ? 1 : 0) : 0) == 0)
          {
            nullable = message;
            ManageController.ManageMessageId manageMessageId4 = ManageController.ManageMessageId.Error;
            if ((nullable.GetValueOrDefault() == manageMessageId4 ? (nullable.HasValue ? 1 : 0) : 0) == 0)
            {
              nullable = message;
              ManageController.ManageMessageId manageMessageId5 = ManageController.ManageMessageId.AddPhoneSuccess;
              if ((nullable.GetValueOrDefault() == manageMessageId5 ? (nullable.HasValue ? 1 : 0) : 0) == 0)
              {
                nullable = message;
                ManageController.ManageMessageId manageMessageId6 = ManageController.ManageMessageId.RemovePhoneSuccess;
                str = (nullable.GetValueOrDefault() == manageMessageId6 ? (nullable.HasValue ? 1 : 0) : 0) != 0 ? "Your phone number was removed." : "";
              }
              else
                str = "Your phone number was added.";
            }
            else
              str = "An error has occurred.";
          }
          else
            str = "Your two-factor authentication provider has been set.";
        }
        else
          str = "Your password has been set.";
      }
      else
        str = "Your password has been changed.";
      object obj = target((CallSite) p0, viewBag, str);
      string userId = this.User.Identity.GetUserId();
      IndexViewModel indexViewModel1 = new IndexViewModel();
      indexViewModel1.HasPassword = this.HasPassword();
      IndexViewModel indexViewModel2 = indexViewModel1;
      string phoneNumberAsync = await this.UserManager.GetPhoneNumberAsync(userId);
      indexViewModel2.PhoneNumber = phoneNumberAsync;
      IndexViewModel indexViewModel3 = indexViewModel1;
      int num1 = await this.UserManager.GetTwoFactorEnabledAsync(userId) ? 1 : 0;
      indexViewModel3.TwoFactor = num1 != 0;
      IndexViewModel indexViewModel4 = indexViewModel1;
      IList<UserLoginInfo> loginsAsync = await this.UserManager.GetLoginsAsync(userId);
      indexViewModel4.Logins = loginsAsync;
      IndexViewModel indexViewModel5 = indexViewModel1;
      int num2 = await this.AuthenticationManager.TwoFactorBrowserRememberedAsync(userId) ? 1 : 0;
      indexViewModel5.BrowserRemembered = num2 != 0;
      IndexViewModel indexViewModel = indexViewModel1;
      indexViewModel2 = (IndexViewModel) null;
      indexViewModel3 = (IndexViewModel) null;
      indexViewModel4 = (IndexViewModel) null;
      indexViewModel5 = (IndexViewModel) null;
      indexViewModel1 = (IndexViewModel) null;
      return (ActionResult) this.View((object) indexViewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> RemoveLogin(
      string loginProvider,
      string providerKey)
    {
      ManageController.ManageMessageId? nullable;
      if ((await this.UserManager.RemoveLoginAsync(this.User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey))).Succeeded)
      {
        ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
        if (byIdAsync != null)
          await this.SignInManager.SignInAsync(byIdAsync, false, false);
        nullable = new ManageController.ManageMessageId?(ManageController.ManageMessageId.RemoveLoginSuccess);
      }
      else
        nullable = new ManageController.ManageMessageId?(ManageController.ManageMessageId.Error);
      return (ActionResult) this.RedirectToAction("ManageLogins", (object) new
      {
        Message = nullable
      });
    }

    public ActionResult AddPhoneNumber()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View((object) model);
      string numberTokenAsync = await this.UserManager.GenerateChangePhoneNumberTokenAsync(this.User.Identity.GetUserId(), model.Number);
      if (this.UserManager.SmsService != null)
        await this.UserManager.SmsService.SendAsync(new IdentityMessage()
        {
          Destination = model.Number,
          Body = "Your security code is: " + numberTokenAsync
        });
      return (ActionResult) this.RedirectToAction("VerifyPhoneNumber", (object) new
      {
        PhoneNumber = model.Number
      });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> EnableTwoFactorAuthentication()
    {
      IdentityResult identityResult = await this.UserManager.SetTwoFactorEnabledAsync(this.User.Identity.GetUserId(), true);
      ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
      if (byIdAsync != null)
        await this.SignInManager.SignInAsync(byIdAsync, false, false);
      return (ActionResult) this.RedirectToAction("Index", "Manage");
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> DisableTwoFactorAuthentication()
    {
      IdentityResult identityResult = await this.UserManager.SetTwoFactorEnabledAsync(this.User.Identity.GetUserId(), false);
      ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
      if (byIdAsync != null)
        await this.SignInManager.SignInAsync(byIdAsync, false, false);
      return (ActionResult) this.RedirectToAction("Index", "Manage");
    }

    public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
    {
      string numberTokenAsync = await this.UserManager.GenerateChangePhoneNumberTokenAsync(this.User.Identity.GetUserId(), phoneNumber);
      ViewResult viewResult;
      if (phoneNumber != null)
        viewResult = this.View((object) new VerifyPhoneNumberViewModel()
        {
          PhoneNumber = phoneNumber
        });
      else
        viewResult = this.View("Error");
      return (ActionResult) viewResult;
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> VerifyPhoneNumber(
      VerifyPhoneNumberViewModel model)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View((object) model);
      if ((await this.UserManager.ChangePhoneNumberAsync(this.User.Identity.GetUserId(), model.PhoneNumber, model.Code)).Succeeded)
      {
        ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
        if (byIdAsync != null)
          await this.SignInManager.SignInAsync(byIdAsync, false, false);
        return (ActionResult) this.RedirectToAction("Index", (object) new
        {
          Message = ManageController.ManageMessageId.AddPhoneSuccess
        });
      }
      this.ModelState.AddModelError("", "Failed to verify phone");
      return (ActionResult) this.View((object) model);
    }

    public async Task<ActionResult> RemovePhoneNumber()
    {
      if (!(await this.UserManager.SetPhoneNumberAsync(this.User.Identity.GetUserId(), (string) null)).Succeeded)
        return (ActionResult) this.RedirectToAction("Index", (object) new
        {
          Message = ManageController.ManageMessageId.Error
        });
      ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
      if (byIdAsync != null)
        await this.SignInManager.SignInAsync(byIdAsync, false, false);
      return (ActionResult) this.RedirectToAction("Index", (object) new
      {
        Message = ManageController.ManageMessageId.RemovePhoneSuccess
      });
    }

    public ActionResult ChangePassword()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View((object) model);
      IdentityResult result = await this.UserManager.ChangePasswordAsync(this.User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
      if (result.Succeeded)
      {
        ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
        if (byIdAsync != null)
          await this.SignInManager.SignInAsync(byIdAsync, false, false);
        return (ActionResult) this.RedirectToAction("Index", (object) new
        {
          Message = ManageController.ManageMessageId.ChangePasswordSuccess
        });
      }
      this.AddErrors(result);
      return (ActionResult) this.View((object) model);
    }

    public ActionResult SetPassword()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
    {
      if (this.ModelState.IsValid)
      {
        IdentityResult result = await this.UserManager.AddPasswordAsync(this.User.Identity.GetUserId(), model.NewPassword);
        if (result.Succeeded)
        {
          ApplicationUser byIdAsync = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
          if (byIdAsync != null)
            await this.SignInManager.SignInAsync(byIdAsync, false, false);
          return (ActionResult) this.RedirectToAction("Index", (object) new
          {
            Message = ManageController.ManageMessageId.SetPasswordSuccess
          });
        }
        this.AddErrors(result);
        result = (IdentityResult) null;
      }
      return (ActionResult) this.View((object) model);
    }

    public async Task<ActionResult> ManageLogins(
      ManageController.ManageMessageId? message)
    {
      // ISSUE: reference to a compiler-generated field
      if (ManageController.\u003C\u003Eo__23.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ManageController.\u003C\u003Eo__23.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "StatusMessage", typeof (ManageController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      Func<CallSite, object, string, object> target = ManageController.\u003C\u003Eo__23.\u003C\u003Ep__0.Target;
      // ISSUE: reference to a compiler-generated field
      CallSite<Func<CallSite, object, string, object>> p0 = ManageController.\u003C\u003Eo__23.\u003C\u003Ep__0;
      object viewBag = this.ViewBag;
      ManageController.ManageMessageId? nullable = message;
      ManageController.ManageMessageId manageMessageId1 = ManageController.ManageMessageId.RemoveLoginSuccess;
      string str;
      if ((nullable.GetValueOrDefault() == manageMessageId1 ? (nullable.HasValue ? 1 : 0) : 0) == 0)
      {
        nullable = message;
        ManageController.ManageMessageId manageMessageId2 = ManageController.ManageMessageId.Error;
        str = (nullable.GetValueOrDefault() == manageMessageId2 ? (nullable.HasValue ? 1 : 0) : 0) != 0 ? "An error has occurred." : "";
      }
      else
        str = "The external login was removed.";
      object obj1 = target((CallSite) p0, viewBag, str);
      ApplicationUser user = await this.UserManager.FindByIdAsync(this.User.Identity.GetUserId());
      if (user == null)
        return (ActionResult) this.View("Error");
      IList<UserLoginInfo> userLogins;
      IList<UserLoginInfo> userLoginInfoList = userLogins;
      userLogins = await this.UserManager.GetLoginsAsync(this.User.Identity.GetUserId());
      List<AuthenticationDescription> list = this.AuthenticationManager.GetExternalAuthenticationTypes().Where<AuthenticationDescription>((Func<AuthenticationDescription, bool>) (auth => userLogins.All<UserLoginInfo>((Func<UserLoginInfo, bool>) (ul => auth.AuthenticationType != ul.LoginProvider)))).ToList<AuthenticationDescription>();
      // ISSUE: reference to a compiler-generated field
      if (ManageController.\u003C\u003Eo__23.\u003C\u003Ep__1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ManageController.\u003C\u003Eo__23.\u003C\u003Ep__1 = CallSite<Func<CallSite, object, bool, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "ShowRemoveButton", typeof (ManageController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj2 = ManageController.\u003C\u003Eo__23.\u003C\u003Ep__1.Target((CallSite) ManageController.\u003C\u003Eo__23.\u003C\u003Ep__1, this.ViewBag, user.PasswordHash != null || userLogins.Count > 1);
      return (ActionResult) this.View((object) new ManageLoginsViewModel()
      {
        CurrentLogins = userLogins,
        OtherLogins = (IList<AuthenticationDescription>) list
      });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult LinkLogin(string provider)
    {
      return (ActionResult) new AccountController.ChallengeResult(provider, this.Url.Action("LinkLoginCallback", "Manage"), this.User.Identity.GetUserId());
    }

    public async Task<ActionResult> LinkLoginCallback()
    {
      ExternalLoginInfo externalLoginInfoAsync = await this.AuthenticationManager.GetExternalLoginInfoAsync("XsrfId", this.User.Identity.GetUserId());
      if (externalLoginInfoAsync == null)
        return (ActionResult) this.RedirectToAction("ManageLogins", (object) new
        {
          Message = ManageController.ManageMessageId.Error
        });
      return (await this.UserManager.AddLoginAsync(this.User.Identity.GetUserId(), externalLoginInfoAsync.Login)).Succeeded ? (ActionResult) this.RedirectToAction("ManageLogins") : (ActionResult) this.RedirectToAction("ManageLogins", (object) new
      {
        Message = ManageController.ManageMessageId.Error
      });
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this._userManager != null)
      {
        this._userManager.Dispose();
        this._userManager = (ApplicationUserManager) null;
      }
      base.Dispose(disposing);
    }

    private IAuthenticationManager AuthenticationManager
    {
      get
      {
        return this.HttpContext.GetOwinContext().Authentication;
      }
    }

    private void AddErrors(IdentityResult result)
    {
      foreach (string error in result.Errors)
        this.ModelState.AddModelError("", error);
    }

    private bool HasPassword()
    {
      ApplicationUser byId = this.UserManager.FindById<ApplicationUser, string>(this.User.Identity.GetUserId());
      if (byId != null)
        return byId.PasswordHash != null;
      return false;
    }

    private bool HasPhoneNumber()
    {
      ApplicationUser byId = this.UserManager.FindById<ApplicationUser, string>(this.User.Identity.GetUserId());
      if (byId != null)
        return byId.PhoneNumber != null;
      return false;
    }

    public enum ManageMessageId
    {
      AddPhoneSuccess,
      ChangePasswordSuccess,
      SetTwoFactorSuccess,
      SetPasswordSuccess,
      RemoveLoginSuccess,
      RemovePhoneSuccess,
      Error,
    }
  }
}
