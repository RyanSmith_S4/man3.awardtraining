﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.ReferredFriendsController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Test.Models.ReferredFriends;

namespace Test.Controllers
{
  public class ReferredFriendsController : Controller
  {
    public ActionResult Index()
    {
      try
      {
        if (!this.Request.IsAuthenticated)
          return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
        string userId = this.User.Identity.GetUserId();
        StudentStats studentStats = BLL.Student.GetStudentStats(userId);
        if (studentStats == null)
          return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
        List<StudentSummary> referredStudents = BLL.Student.GetStudentReferredStudents(userId);
        ReferredFriendViewModel referredFriendViewModel = new ReferredFriendViewModel()
        {
          Students = new List<ReferredStudent>(),
          IsGraduate = studentStats.IsGraduate && studentStats.CurrentModuleID >= 10 && studentStats.CurrentSubModuleID >= 19,
          IsChatEnabled = studentStats.IsChatEnabled,
          IsLocked = studentStats.IsLocked
        };
        foreach (StudentSummary studentSummary in referredStudents)
          referredFriendViewModel.Students.Add(new ReferredStudent()
          {
            StudentID = studentSummary.StudentID,
            UserID = studentSummary.UserID,
            FirstName = studentSummary.FirstName,
            LastName = studentSummary.LastName,
            DateEnroled = studentSummary.DateTimeCreated
          });
        return (ActionResult) this.View((object) referredFriendViewModel);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
    }

    public ActionResult FriendProgress(int paId)
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(userId, "Student"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      FriendProgressViewModel progressViewModel = new FriendProgressViewModel();
      List<COML.Classes.Module> modulesForUserById = BLL.Module.GetModulesForUserByID(paId);
      if (!BLL.Student.StudentIsFriend(paId, userId))
        return (ActionResult) this.RedirectToAction("Index", "ReferredFriends");
      COML.Classes.Student studentById = BLL.Student.GetStudentByID(paId);
      StudentStats studentStats = BLL.Student.GetStudentStats(userId);
      if (studentById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      progressViewModel.IsGraduate = studentStats.IsGraduate && studentStats.CurrentModuleID >= 10 && studentStats.CurrentSubModuleID >= 19;
      progressViewModel.IsChatEnabled = studentStats.IsChatEnabled;
      progressViewModel.IsLocked = studentStats.IsLocked;
      if (modulesForUserById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      progressViewModel.FirstName = studentById.FirstName;
      progressViewModel.LastName = studentById.LastName;
      progressViewModel.Modules = new List<Test.Models.ReferredFriends.Module>();
      progressViewModel.IDNumber = studentById.IdentityNumber;
      foreach (COML.Classes.Module module1 in modulesForUserById)
      {
        Test.Models.ReferredFriends.Module module2 = new Test.Models.ReferredFriends.Module()
        {
          ID = module1.ID,
          Code = module1.Code,
          Description = module1.Description,
          Name = module1.Name,
          Order = module1.Order,
          IsAvailable = module1.IsAvailable,
          IsComplete = module1.IsCompleted,
          IsLocked = false
        };
        module2.SubModules = new List<Test.Models.ReferredFriends.SubModule>();
        foreach (COML.Classes.SubModule subModule1 in module1.SubModules)
        {
          Test.Models.ReferredFriends.SubModule subModule2 = new Test.Models.ReferredFriends.SubModule()
          {
            ID = subModule1.ID,
            Code = subModule1.Code,
            Description = subModule1.Description,
            Name = subModule1.Name,
            IsComplete = subModule1.IsComplete,
            IsStarted = subModule1.IsStarted,
            IsLocked = subModule1.IsLocked,
            IsCurrent = subModule1.IsCurrent,
            IsAvailable = subModule1.IsAvailable,
            IsNotYetCompetent = subModule1.IsNotYetCompetent
          };
          if (subModule2.IsLocked)
          {
            module2.IsLocked = true;
            progressViewModel.IsLocked = true;
          }
          subModule2.Code = subModule2.Code == "" ? subModule2.Code : " - " + subModule2.Code;
          module2.SubModules.Add(subModule2);
        }
        progressViewModel.Modules.Add(module2);
      }
      return (ActionResult) this.View((object) progressViewModel);
    }
  }
}
