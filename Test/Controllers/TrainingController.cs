﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.TrainingController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using BLL;
using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using Test.Models.Training;

namespace Test.Controllers
{
  public class TrainingController : Controller
  {
    public ActionResult Index(int paModuleID, int paSubModuleID)
    {
      if (this.Request.IsAuthenticated)
      {
        if (paModuleID <= 0 || paSubModuleID <= 0)
          return (ActionResult) this.RedirectToAction(nameof (Index), "Dashboard");
        string userId = this.User.Identity.GetUserId();
        COML.Classes.Module subModuleForUser = BLL.Module.GetSubModuleForUser(userId, paModuleID, paSubModuleID);
        COML.Classes.Student student = BLL.Student.GetStudent(userId);
        if (student != null)
        {
          StudentStats studentStats = BLL.Student.GetStudentStats(userId);
          if (subModuleForUser == null)
            return (ActionResult) this.RedirectToAction(nameof (Index), "Dashboard");
          TrainingViewModel trainingViewModel = new TrainingViewModel()
          {
            Module = new Test.Models.Training.Module()
            {
              ID = subModuleForUser.ID,
              Code = subModuleForUser.Code,
              Description = subModuleForUser.Description,
              Name = subModuleForUser.Name,
              IsAvailable = subModuleForUser.IsAvailable,
              IsComplete = subModuleForUser.IsCompleted,
              Order = subModuleForUser.Order
            },
            UserID = userId
          };
          trainingViewModel.Module.SubModule = new Test.Models.Training.SubModule();
          trainingViewModel.IsLocked = student.IsLocked;
          trainingViewModel.IsGraduate = studentStats.IsGraduate;
          trainingViewModel.IsChatEnabled = studentStats.IsChatEnabled;
          COML.Classes.SubModule subModule1 = subModuleForUser.SubModules[0];
          if (subModule1 != null)
          {
            Test.Models.Training.SubModule subModule2 = new Test.Models.Training.SubModule()
            {
              Attempts = subModule1.Attempts,
              ID = subModule1.ID,
              Code = subModule1.Code,
              Description = subModule1.Description,
              Name = subModule1.Name,
              HasAssessment = subModule1.HasAssessment,
              IsAvailable = subModule1.IsAvailable,
              IsCurrent = subModule1.IsCurrent,
              IsLocked = subModule1.IsLocked
            };
            subModule2.Materials = new List<Material>();
            foreach (SubModuleMaterial material1 in subModule1.Materials)
            {
              Material material2 = new Material()
              {
                ID = material1.ID,
                Description = material1.Description,
                Name = material1.Name,
                Type = material1.Type,
                Path = material1.Path
              };
              subModule2.Materials.Add(material2);
            }
            trainingViewModel.Module.SubModule = subModule2;
            if (!subModule1.HasAssessment && subModule1.IsCurrent && (!subModule1.IsLocked && subModule1.IsAvailable))
              BLL.Module.SetSubModuleComplete(userId, trainingViewModel.Module.ID, trainingViewModel.Module.SubModule.ID);
          }
          BLL.Logging.SavePageLog(userId, "Training Page - " + trainingViewModel.Module.SubModule.Name);
          return (ActionResult) this.View((object) trainingViewModel);
        }
        COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction("LogOffWithoutPost", "Account");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Dashboard");
    }

    public FileResult Download(int paFileID)
    {
      string userId = this.User.Identity.GetUserId();
      SubModuleMaterial material = BLL.Module.GetMaterial(paFileID);
      if (material != null)
      {
        string path = "";
        switch (material.Type)
        {
          case Enumerations.enumMaterialType.Doc:
          case Enumerations.enumMaterialType.Pdf:
          case Enumerations.enumMaterialType.PowerPoint:
          case Enumerations.enumMaterialType.Excel:
            path = this.HttpContext.Server.MapPath("~\\Files\\Modules" + material.Path);
            break;
          case Enumerations.enumMaterialType.Url:
          case Enumerations.enumMaterialType.UrlVideo:
            path = "";
            break;
        }
        if (path != "")
        {
          FileContentResult fileContentResult = new FileContentResult(File.ReadAllBytes(path), "application/octet-stream");
          string str1 = material.Path.Substring(material.Path.Length - 4, 4);
          string str2 = DateTime.Now.ToString().Replace("/", "").Replace(" ", "_").Replace(":", "");
          BLL.Logging.SaveMaterialLog(userId, paFileID);
          fileContentResult.FileDownloadName = "Download_" + str2 + str1;
          return (FileResult) fileContentResult;
        }
      }
      return (FileResult) null;
    }

    [HttpPost]
    [AllowAnonymous]
    public void VideoViewed(string paUserID, int paSubModuleMaterialID)
    {
      BLL.Logging.SaveMaterialLog(paUserID, paSubModuleMaterialID);
    }

    public ActionResult SubmitBusinessPlan()
    {
      if (!this.Request.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Dashboard");
      string userId = this.User.Identity.GetUserId();
      COML.Classes.Module studentCurrentModule = BLL.Module.GetStudentCurrentModule(userId);
      StudentStats studentStats = BLL.Student.GetStudentStats(userId);
      if (studentCurrentModule != null)
      {
        BusinessPlanViewModel businessPlanViewModel = new BusinessPlanViewModel();
        BLL.Logging.SavePageLog(userId, "Submit Business Plan");
        businessPlanViewModel.UserID = userId;
        businessPlanViewModel.IsChatEnabled = studentStats.IsChatEnabled;
        businessPlanViewModel.IsLocked = studentStats.IsLocked;
        businessPlanViewModel.IsGraduate = studentStats.IsGraduate && studentStats.CurrentModuleID >= 10 && studentStats.CurrentSubModuleID >= 19;
        businessPlanViewModel.Module = new Test.Models.Training.Module()
        {
          ID = studentCurrentModule.ID,
          Code = studentCurrentModule.Code,
          Description = studentCurrentModule.Description,
          IsAvailable = studentCurrentModule.IsAvailable,
          IsComplete = studentCurrentModule.IsCompleted,
          Name = studentCurrentModule.Name,
          SubModule = new Test.Models.Training.SubModule()
        };
        Test.Models.Training.SubModule subModule1 = new Test.Models.Training.SubModule();
        if (studentCurrentModule.SubModules != null && studentCurrentModule.SubModules.Count > 0)
        {
          COML.Classes.SubModule subModule2 = studentCurrentModule.SubModules[0];
          subModule1.Attempts = subModule2.Attempts;
          subModule1.ID = subModule2.ID;
          subModule1.Code = subModule2.Code;
          subModule1.Description = subModule2.Description;
          subModule1.Name = subModule2.Name;
          subModule1.IsAvailable = subModule2.IsAvailable;
          subModule1.IsCurrent = subModule2.IsCurrent;
          subModule1.IsLocked = subModule2.IsLocked;
        }
        businessPlanViewModel.Module.SubModule = subModule1;
        if (subModule1.ID == 19 && studentCurrentModule.ID == 10 && (subModule1.Attempts < 3 && subModule1.IsCurrent))
          return (ActionResult) this.View((object) businessPlanViewModel);
        return (ActionResult) this.RedirectToAction("Index", "Dashboard");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction("LogOffWithoutPost", "Account");
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> SubmitBusinessPlan(BusinessPlanViewModel model)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return (ActionResult) this.View((object) model);
        string userId = this.User.Identity.GetUserId();
        if (!(model.UserID == userId))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        COML.Classes.Student student = BLL.Student.GetStudent(userId);
        StudentStats studentStats = BLL.Student.GetStudentStats(userId);
        if (student == null)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        model.IsChatEnabled = studentStats.IsChatEnabled;
        model.IsLocked = studentStats.IsLocked;
        model.IsGraduate = studentStats.IsGraduate && studentStats.CurrentModuleID >= 10 && studentStats.CurrentSubModuleID >= 19;
        if (model.CV1 != null)
        {
          Guid guid1 = Guid.NewGuid();
          string contentType1 = model.CV1.ContentType;
          Guid guid2 = Guid.NewGuid();
          string contentType2 = model.BusinessPlan.ContentType;
          bool flag1 = true;
          bool flag2 = true;
          bool flag3 = true;
          bool flag4 = true;
          bool flag5 = true;
          string str1 = "";
          string paFileLocation1 = "";
          int paSystemFile1 = 0;
          int paTypeFile1 = 0;
          if (!(contentType1 == "application/pdf"))
          {
            if (contentType1 == "application/msword" || contentType1 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
              str1 = guid1.ToString() + ".doc";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str1);
              paFileLocation1 = Path.Combine("Files", "Users", "Others", str1);
              paSystemFile1 = 1;
              paTypeFile1 = 2;
              model.CV1.SaveAs(filename);
            }
          }
          else
          {
            str1 = guid1.ToString() + ".pdf";
            string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str1);
            paFileLocation1 = Path.Combine("Files", "Users", "Others", str1);
            paSystemFile1 = 1;
            paTypeFile1 = 1;
            model.CV1.SaveAs(filename);
          }
          bool flag6 = BLL.Student.SaveStudentFile(model.UserID, paSystemFile1, paTypeFile1, str1, paFileLocation1);
          if (model.CV2 != null)
          {
            Guid guid3 = Guid.NewGuid();
            string contentType3 = model.CV2.ContentType;
            string str2 = "";
            string paFileLocation2 = "";
            int paSystemFile2 = 0;
            int paTypeFile2 = 0;
            if (!(contentType1 == "application/pdf"))
            {
              if (contentType1 == "application/msword" || contentType1 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
              {
                str2 = guid3.ToString() + ".doc";
                string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
                paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
                paSystemFile2 = 5;
                paTypeFile2 = 2;
                model.CV2.SaveAs(filename);
              }
            }
            else
            {
              str2 = guid3.ToString() + ".pdf";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
              paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
              paSystemFile2 = 5;
              paTypeFile2 = 1;
              model.CV2.SaveAs(filename);
            }
            flag1 = BLL.Student.SaveStudentFile(model.UserID, paSystemFile2, paTypeFile2, str2, paFileLocation2);
          }
          if (model.BusinessPlan != null)
          {
            string str2 = "";
            string paFileLocation2 = "";
            int paSystemFile2 = 0;
            int paTypeFile2 = 0;
            if (!(contentType2 == "application/pdf"))
            {
              if (contentType2 == "application/msword" || contentType2 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
              {
                str2 = guid2.ToString() + ".doc";
                string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
                paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
                paSystemFile2 = 2;
                paTypeFile2 = 2;
                model.BusinessPlan.SaveAs(filename);
              }
            }
            else
            {
              str2 = guid2.ToString() + ".pdf";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
              paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
              paSystemFile2 = 2;
              paTypeFile2 = 1;
              model.BusinessPlan.SaveAs(filename);
            }
            flag2 = BLL.Student.SaveStudentFile(model.UserID, paSystemFile2, paTypeFile2, str2, paFileLocation2);
          }
          else
            this.ModelState.AddModelError("BusinessPlan", "Please select a Business Plan.");
          if (model.BusinessPlanDoc1 != null)
          {
            Guid guid3 = Guid.NewGuid();
            string contentType3 = model.BusinessPlanDoc1.ContentType;
            string str2 = "";
            string paFileLocation2 = "";
            int paSystemFile2 = 0;
            int paTypeFile2 = 0;
            if (!(contentType3 == "application/pdf"))
            {
              if (contentType3 == "application/msword" || contentType3 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
              {
                str2 = guid3.ToString() + ".doc";
                string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
                paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
                paSystemFile2 = 6;
                paTypeFile2 = 2;
                model.BusinessPlanDoc1.SaveAs(filename);
              }
            }
            else
            {
              str2 = guid3.ToString() + ".pdf";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
              paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
              paSystemFile2 = 6;
              paTypeFile2 = 1;
              model.BusinessPlanDoc1.SaveAs(filename);
            }
            flag3 = BLL.Student.SaveStudentFile(model.UserID, paSystemFile2, paTypeFile2, str2, paFileLocation2);
          }
          if (model.BusinessPlanDoc2 != null)
          {
            Guid guid3 = Guid.NewGuid();
            string contentType3 = model.BusinessPlanDoc2.ContentType;
            string str2 = "";
            string paFileLocation2 = "";
            int paSystemFile2 = 0;
            int paTypeFile2 = 0;
            if (!(contentType3 == "application/pdf"))
            {
              if (contentType3 == "application/msword" || contentType3 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
              {
                str2 = guid3.ToString() + ".doc";
                string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
                paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
                paSystemFile2 = 7;
                paTypeFile2 = 2;
                model.BusinessPlanDoc2.SaveAs(filename);
              }
            }
            else
            {
              str2 = guid3.ToString() + ".pdf";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
              paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
              paSystemFile2 = 7;
              paTypeFile2 = 1;
              model.BusinessPlanDoc2.SaveAs(filename);
            }
            flag4 = BLL.Student.SaveStudentFile(model.UserID, paSystemFile2, paTypeFile2, str2, paFileLocation2);
          }
          if (model.BusinessPlanDoc3 != null)
          {
            Guid guid3 = Guid.NewGuid();
            string contentType3 = model.BusinessPlanDoc3.ContentType;
            string str2 = "";
            string paFileLocation2 = "";
            int paSystemFile2 = 0;
            int paTypeFile2 = 0;
            if (!(contentType3 == "application/pdf"))
            {
              if (contentType3 == "application/msword" || contentType3 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
              {
                str2 = guid3.ToString() + ".doc";
                string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
                paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
                paSystemFile2 = 8;
                paTypeFile2 = 2;
                model.BusinessPlanDoc3.SaveAs(filename);
              }
            }
            else
            {
              str2 = guid3.ToString() + ".pdf";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
              paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
              paSystemFile2 = 8;
              paTypeFile2 = 1;
              model.BusinessPlanDoc3.SaveAs(filename);
            }
            flag5 = BLL.Student.SaveStudentFile(model.UserID, paSystemFile2, paTypeFile2, str2, paFileLocation2);
          }
          if (flag6 & flag1 & flag2 & flag3 & flag4 & flag5)
          {
            Mailer.SendSubmissionEmail(student.FirstName + " " + student.LastName, student.IdentityNumber, student.UserID);
            return (ActionResult) this.View("SubmitBusinessPlanConfirmation");
          }
          this.ModelState.AddModelError("", "Error processing your request.");
        }
        else
          this.ModelState.AddModelError("", "Error processing your request.");
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) model);
    }

    public ActionResult SubmitBusinessPlanConfirmation()
    {
      return (ActionResult) this.View();
    }

    public ActionResult SubmitBusinessPlanWait()
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Student"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Student.GetStudent(userId);
      List<UserAssessedFile> nonAssessedFiles = BLL.Student.GetStudentsNonAssessedFiles(userId);
      BusinessPlanWaitModel businessPlanWaitModel = new BusinessPlanWaitModel();
      StudentStats studentStats = BLL.Student.GetStudentStats(userId);
      businessPlanWaitModel.UserFiles = new List<UserFile>();
      businessPlanWaitModel.IsChatEnabled = studentStats.IsChatEnabled;
      businessPlanWaitModel.IsLocked = studentStats.IsLocked;
      businessPlanWaitModel.IsGraduate = studentStats.IsGraduate && studentStats.CurrentModuleID >= 10 && studentStats.CurrentSubModuleID >= 19;
      foreach (UserAssessedFile userAssessedFile in nonAssessedFiles)
        businessPlanWaitModel.UserFiles.Add(new UserFile()
        {
          ID = userAssessedFile.ID,
          Filepath = userAssessedFile.Filepath,
          Name = userAssessedFile.Name,
          SystemFile = userAssessedFile.SystemFile,
          TypeFile = userAssessedFile.TypeFile
        });
      return (ActionResult) this.View((object) businessPlanWaitModel);
    }

    public FileResult StudentFileDownload(int paFileId)
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (FileResult) null;
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Student"))
        return (FileResult) null;
      try
      {
        UserFile studentFile = BLL.Student.GetStudentFile(userId, paFileId);
        if (studentFile == null)
          return (FileResult) null;
        FileContentResult fileContentResult = new FileContentResult(File.ReadAllBytes(this.HttpContext.Server.MapPath("~\\" + studentFile.Filepath)), "application/octet-stream");
        fileContentResult.FileDownloadName = "Download_" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "_").Replace(":", "") + studentFile.Filepath.Substring(studentFile.Filepath.Length - 4, 4);
        return (FileResult) fileContentResult;
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (FileResult) null;
      }
    }

    public ActionResult ViewBusinessPlan()
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Student"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      List<UserAssessedFile> studentsAssessedFiles = BLL.Student.GetStudentsAssessedFiles(userId);
      BusinessPlanWaitModel businessPlanWaitModel = new BusinessPlanWaitModel();
      StudentStats studentStats = BLL.Student.GetStudentStats(userId);
      businessPlanWaitModel.UserFiles = new List<UserFile>();
      businessPlanWaitModel.IsChatEnabled = studentStats.IsChatEnabled;
      businessPlanWaitModel.IsLocked = studentStats.IsLocked;
      businessPlanWaitModel.IsGraduate = studentStats.IsGraduate && studentStats.CurrentModuleID >= 10 && studentStats.CurrentSubModuleID >= 19;
      foreach (UserAssessedFile userAssessedFile in studentsAssessedFiles)
        businessPlanWaitModel.UserFiles.Add(new UserFile()
        {
          ID = userAssessedFile.ID,
          Filepath = userAssessedFile.Filepath,
          Name = userAssessedFile.Name,
          SystemFile = userAssessedFile.SystemFile,
          TypeFile = userAssessedFile.TypeFile
        });
      return (ActionResult) this.View((object) businessPlanWaitModel);
    }
  }
}
