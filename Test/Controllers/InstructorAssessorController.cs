﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.InstructorAssessorController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using BLL;
using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Test.Models.Instructor;
using Test.Models.ReferredFriends;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  public class InstructorAssessorController : Controller
  {
    public ActionResult Index()
    {
      BLL.Logging.SaveInstructorPageLog(this.User.Identity.GetUserId(), "Assessor Landing Page");
      return (ActionResult) this.View();
    }

    public ActionResult AssessorStudents(int paId = 1)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        COML.Classes.Instructor assessorByUserId = BLL.Instructor.GetAssessorByUserID(userId);
        List<AssessorStudentViewModel> allItems = new List<AssessorStudentViewModel>();
        if (assessorByUserId == null)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (assessorByUserId.Students != null && assessorByUserId.Students.Count > 0)
        {
          foreach (COML.Classes.Student student in assessorByUserId.Students)
          {
            bool flag = student.Files.Count > 0;
            allItems.Add(new AssessorStudentViewModel()
            {
              StudentID = student.ID,
              Email = student.EmailAddress,
              FirstName = student.FirstName,
              IDNumber = student.IdentityNumber,
              LastName = student.LastName,
              TelephoneCell = student.TelephoneCell,
              UserID = student.UserID,
              DateEnroled = student.DateTimeCreated,
              LatestActivity = student.LastActivity,
              HasFiles = flag
            });
          }
        }
        PagedList<AssessorStudentViewModel> pagedList = allItems.ToPagedList<AssessorStudentViewModel>(paId, 10);
        BLL.Logging.SaveInstructorPageLog(userId, "Viewed Learners");
        return (ActionResult) this.View((object) pagedList);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public ActionResult AssessorStudentsSearch(int paId = 1, string paWord = null)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        List<COML.Classes.Student> studentList = new List<COML.Classes.Student>();
        List<AssessorStudentViewModel> allItems = new List<AssessorStudentViewModel>();
        if (!string.IsNullOrWhiteSpace(paWord))
        {
          studentList = BLL.Instructor.GetAssignedStudentsByIDNumberForAssessor(userId, paWord);
        }
        else
        {
          COML.Classes.Instructor assessorByUserId = BLL.Instructor.GetAssessorByUserID(userId);
          if (assessorByUserId != null && assessorByUserId.Students != null)
            studentList = assessorByUserId.Students;
        }
        if (studentList == null)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (studentList != null && studentList.Count > 0)
        {
          foreach (COML.Classes.Student student in studentList)
          {
            bool flag = student.Files.Count > 0;
            allItems.Add(new AssessorStudentViewModel()
            {
              StudentID = student.ID,
              Email = student.EmailAddress,
              FirstName = student.FirstName,
              IDNumber = student.IdentityNumber,
              LastName = student.LastName,
              TelephoneCell = student.TelephoneCell,
              UserID = student.UserID,
              DateEnroled = student.DateTimeCreated,
              LatestActivity = student.LastActivity,
              HasFiles = flag
            });
          }
        }
        PagedList<AssessorStudentViewModel> pagedList = allItems.ToPagedList<AssessorStudentViewModel>(paId, 10);
        BLL.Logging.SaveInstructorPageLog(userId, "Searched for Learners");
        return (ActionResult) this.View("AssessorStudents", (object) pagedList);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public ActionResult AssessorStudentFiles(int paStudentId)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!BLL.Instructor.IsMyLearner(userId, paStudentId))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        StudentState nonAssessedFiles = BLL.Instructor.GetStudentsNonAssessedFiles(userId, paStudentId);
        AssessorStudentFileViewModel studentFileViewModel = new AssessorStudentFileViewModel();
        studentFileViewModel.Attempt1 = new List<Test.Models.Instructor.UserAssessedFile>();
        studentFileViewModel.Attempt2 = new List<Test.Models.Instructor.UserAssessedFile>();
        studentFileViewModel.AllChecked = true;
        studentFileViewModel.AllRight = true;
        studentFileViewModel.IsSuccessful = nonAssessedFiles.IsSuccessful;
        studentFileViewModel.IsComplete = nonAssessedFiles.IsComplete;
        studentFileViewModel.Attempts = nonAssessedFiles.Attempts;
        if (nonAssessedFiles == null)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        studentFileViewModel.StudentID = nonAssessedFiles.ID;
        studentFileViewModel.Email = nonAssessedFiles.EmailAddress;
        studentFileViewModel.FirstName = nonAssessedFiles.FirstName;
        studentFileViewModel.IDNumber = nonAssessedFiles.IdentityNumber;
        studentFileViewModel.LastName = nonAssessedFiles.LastName;
        studentFileViewModel.TelephoneCell = nonAssessedFiles.TelephoneCell;
        studentFileViewModel.UserID = nonAssessedFiles.UserID;
        studentFileViewModel.DateEnroled = nonAssessedFiles.DateTimeCreated;
        studentFileViewModel.LatestActivity = nonAssessedFiles.LastActivity;
        foreach (COML.Classes.UserAssessedFile file in nonAssessedFiles.Files)
        {
          if (file.Attempt == 1)
            studentFileViewModel.Attempt1.Add(new Test.Models.Instructor.UserAssessedFile()
            {
              ID = file.ID,
              Attempt = 1,
              DateCreated = file.DateCreated,
              Filepath = file.Filepath,
              SubModuleFileID = file.SubModuleFileID,
              SystemFile = file.SystemFile,
              TypeFile = file.TypeFile,
              IsAssessed = file.IsAssessed,
              IsCorrect = file.IsCorrect,
              Name = file.Name
            });
          else if (file.Attempt == 2)
            studentFileViewModel.Attempt2.Add(new Test.Models.Instructor.UserAssessedFile()
            {
              ID = file.ID,
              Attempt = 2,
              DateCreated = file.DateCreated,
              Filepath = file.Filepath,
              SubModuleFileID = file.SubModuleFileID,
              SystemFile = file.SystemFile,
              TypeFile = file.TypeFile,
              IsAssessed = file.IsAssessed,
              IsCorrect = file.IsCorrect,
              Name = file.Name
            });
        }
        if (studentFileViewModel.Attempt1.Count > 0 && studentFileViewModel.Attempt2.Count == 0)
        {
          foreach (Test.Models.Instructor.UserAssessedFile userAssessedFile in studentFileViewModel.Attempt1)
          {
            if (!userAssessedFile.IsCorrect)
              studentFileViewModel.AllRight = false;
            if (!userAssessedFile.IsAssessed)
              studentFileViewModel.AllChecked = false;
          }
        }
        else if (studentFileViewModel.Attempt1.Count > 0 && studentFileViewModel.Attempt2.Count > 0)
        {
          foreach (Test.Models.Instructor.UserAssessedFile userAssessedFile in studentFileViewModel.Attempt2)
          {
            if (!userAssessedFile.IsCorrect)
              studentFileViewModel.AllRight = false;
            if (!userAssessedFile.IsAssessed)
              studentFileViewModel.AllChecked = false;
          }
        }
        if (studentFileViewModel.Attempts == 1 && studentFileViewModel.Attempt1.Count == 0)
        {
          studentFileViewModel.AllRight = false;
          studentFileViewModel.AllChecked = false;
        }
        if (studentFileViewModel.Attempts == 2 && studentFileViewModel.Attempt2.Count == 0)
        {
          studentFileViewModel.AllRight = false;
          studentFileViewModel.AllChecked = false;
        }
        BLL.Logging.SaveInstructorPageLog(userId, "Viewed " + studentFileViewModel.FirstName + " " + studentFileViewModel.LastName + " file.");
        return (ActionResult) this.View(nameof (AssessorStudentFiles), (object) studentFileViewModel);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public FileResult StudentFileDownload(int paStudentId, int paFileId)
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (FileResult) null;
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Assessor"))
        return (FileResult) null;
      try
      {
        UserFile assessorStudentFile = BLL.Instructor.GetAssessorStudentFile(userId, paStudentId, paFileId);
        if (assessorStudentFile == null)
          return (FileResult) null;
        FileContentResult fileContentResult = new FileContentResult(File.ReadAllBytes(this.HttpContext.Server.MapPath("~\\" + assessorStudentFile.Filepath)), "application/octet-stream");
        fileContentResult.FileDownloadName = "Download_" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "_").Replace(":", "") + assessorStudentFile.Filepath.Substring(assessorStudentFile.Filepath.Length - 4, 4);
        return (FileResult) fileContentResult;
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (FileResult) null;
      }
    }

    public ActionResult ApproveFile(int paStudentId, int paFileId)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!BLL.Instructor.IsMyLearner(userId, paStudentId))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (BLL.Instructor.MarkFileOK(userId, paStudentId, paFileId))
          BLL.Logging.SaveInstructorPageLog(userId, "Approved learner file");
        return (ActionResult) this.RedirectToAction("AssessorStudentFiles", (object) new
        {
          paStudentId = paStudentId
        });
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public ActionResult RejectFile(int paStudentId, int paFileId)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!BLL.Instructor.IsMyLearner(userId, paStudentId))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (BLL.Instructor.MarkFileNOK(userId, paStudentId, paFileId))
          BLL.Logging.SaveInstructorPageLog(userId, "Rejected learner file");
        return (ActionResult) this.RedirectToAction("AssessorStudentFiles", (object) new
        {
          paStudentId = paStudentId
        });
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public ActionResult FinalizeStudentPass(int paStudentId)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!BLL.Instructor.IsMyLearner(userId, paStudentId))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        COML.Classes.Student studentById = BLL.Student.GetStudentByID(paStudentId);
        int num = BLL.Instructor.FinalizeStudentOK(userId, paStudentId) ? 1 : 0;
        Mailer.SendCertificateEmail(studentById.UserID, studentById.FirstName, studentById.LastName, studentById.IdentityNumber, studentById.EmailAddress);
        if (num != 0)
          BLL.Logging.SaveInstructorPageLog(userId, "Passed: " + studentById.FirstName + " " + studentById.LastName);
        return (ActionResult) this.RedirectToAction("AssessorStudentFiles", (object) new
        {
          paStudentId = paStudentId
        });
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public ActionResult FinalizeStudentFail(int paStudentId)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!BLL.Instructor.IsMyLearner(userId, paStudentId))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        COML.Classes.Student studentById = BLL.Student.GetStudentByID(paStudentId);
        if (BLL.Instructor.FinalizeStudentNOK(userId, paStudentId))
          BLL.Logging.SaveInstructorPageLog(userId, "Failed: " + studentById.FirstName + " " + studentById.LastName);
        return (ActionResult) this.RedirectToAction("AssessorStudentFiles", (object) new
        {
          paStudentId = paStudentId
        });
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      }
    }

    public ActionResult AssessorReferredFriends()
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.Request.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        List<StudentSummary> referredStudents = BLL.Instructor.GetStudentReferredStudents(userId);
        ReferredFriendViewModel referredFriendViewModel = new ReferredFriendViewModel()
        {
          Students = new List<ReferredStudent>()
        };
        foreach (StudentSummary studentSummary in referredStudents)
          referredFriendViewModel.Students.Add(new ReferredStudent()
          {
            StudentID = studentSummary.StudentID,
            UserID = studentSummary.UserID,
            FirstName = studentSummary.FirstName,
            LastName = studentSummary.LastName,
            DateEnroled = studentSummary.DateTimeCreated
          });
        BLL.Logging.SaveInstructorPageLog(userId, "Referred Friends");
        return (ActionResult) this.View((object) referredFriendViewModel);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
    }

    public ActionResult AssessorReferredFriendProgress(int paId)
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(userId, "Assessor"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      FriendProgressViewModel progressViewModel = new FriendProgressViewModel();
      List<COML.Classes.Module> modulesForUserById = BLL.Module.GetModulesForUserByID(paId);
      if (!BLL.Instructor.StudentIsFriend(paId, userId))
        return (ActionResult) this.RedirectToAction("AssessorReferredFriends", "InstructorAssessor");
      COML.Classes.Student studentById = BLL.Student.GetStudentByID(paId);
      if (studentById == null)
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      if (modulesForUserById == null)
        return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
      progressViewModel.FirstName = studentById.FirstName;
      progressViewModel.LastName = studentById.LastName;
      progressViewModel.IsLocked = false;
      progressViewModel.Modules = new List<Test.Models.ReferredFriends.Module>();
      progressViewModel.IDNumber = studentById.IdentityNumber;
      foreach (COML.Classes.Module module1 in modulesForUserById)
      {
        Test.Models.ReferredFriends.Module module2 = new Test.Models.ReferredFriends.Module()
        {
          ID = module1.ID,
          Code = module1.Code,
          Description = module1.Description,
          Name = module1.Name,
          Order = module1.Order,
          IsAvailable = module1.IsAvailable,
          IsComplete = module1.IsCompleted,
          IsLocked = false
        };
        module2.SubModules = new List<Test.Models.ReferredFriends.SubModule>();
        foreach (COML.Classes.SubModule subModule1 in module1.SubModules)
        {
          Test.Models.ReferredFriends.SubModule subModule2 = new Test.Models.ReferredFriends.SubModule()
          {
            ID = subModule1.ID,
            Code = subModule1.Code,
            Description = subModule1.Description,
            Name = subModule1.Name,
            IsComplete = subModule1.IsComplete,
            IsStarted = subModule1.IsStarted,
            IsLocked = subModule1.IsLocked,
            IsCurrent = subModule1.IsCurrent,
            IsAvailable = subModule1.IsAvailable,
            IsNotYetCompetent = subModule1.IsNotYetCompetent
          };
          if (subModule2.IsLocked)
          {
            module2.IsLocked = true;
            progressViewModel.IsLocked = true;
          }
          subModule2.Code = subModule2.Code == "" ? subModule2.Code : " - " + subModule2.Code;
          module2.SubModules.Add(subModule2);
        }
        progressViewModel.Modules.Add(module2);
      }
      BLL.Logging.SaveInstructorPageLog(userId, "Referred Friend Progress");
      return (ActionResult) this.View((object) progressViewModel);
    }
  }
}
