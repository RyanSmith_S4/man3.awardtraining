﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.InstructorMentorController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Test.Models.ReferredFriends;

namespace Test.Controllers
{
  public class InstructorMentorController : Controller
  {
    public ActionResult Index()
    {
      BLL.Logging.SaveInstructorPageLog(this.User.Identity.GetUserId(), "Mentor Landing Page");
      return (ActionResult) this.View();
    }

    public ActionResult MentorReferredFriends()
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.Request.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Mentor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        List<StudentSummary> referredStudents = BLL.Instructor.GetStudentReferredStudents(userId);
        ReferredFriendViewModel referredFriendViewModel = new ReferredFriendViewModel()
        {
          Students = new List<ReferredStudent>()
        };
        foreach (StudentSummary studentSummary in referredStudents)
          referredFriendViewModel.Students.Add(new ReferredStudent()
          {
            StudentID = studentSummary.StudentID,
            UserID = studentSummary.UserID,
            FirstName = studentSummary.FirstName,
            LastName = studentSummary.LastName,
            DateEnroled = studentSummary.DateTimeCreated
          });
        return (ActionResult) this.View((object) referredFriendViewModel);
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
    }

    public ActionResult MentorReferredFriendProgress(int paId)
    {
      string userId = this.User.Identity.GetUserId();
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(userId, "Mentor"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      FriendProgressViewModel progressViewModel = new FriendProgressViewModel();
      List<COML.Classes.Module> modulesForUserById = BLL.Module.GetModulesForUserByID(paId);
      if (!BLL.Instructor.StudentIsFriend(paId, userId))
        return (ActionResult) this.RedirectToAction("MentorReferredFriends", "InstructorMentor");
      COML.Classes.Student studentById = BLL.Student.GetStudentByID(paId);
      if (studentById == null)
        return (ActionResult) this.RedirectToAction("Index", "InstructorMentor");
      if (modulesForUserById == null)
        return (ActionResult) this.RedirectToAction("Index", "InstructorMentor");
      progressViewModel.FirstName = studentById.FirstName;
      progressViewModel.LastName = studentById.LastName;
      progressViewModel.IsLocked = false;
      progressViewModel.Modules = new List<Test.Models.ReferredFriends.Module>();
      progressViewModel.IDNumber = studentById.IdentityNumber;
      foreach (COML.Classes.Module module1 in modulesForUserById)
      {
        Test.Models.ReferredFriends.Module module2 = new Test.Models.ReferredFriends.Module()
        {
          ID = module1.ID,
          Code = module1.Code,
          Description = module1.Description,
          Name = module1.Name,
          Order = module1.Order,
          IsAvailable = module1.IsAvailable,
          IsComplete = module1.IsCompleted,
          IsLocked = false
        };
        module2.SubModules = new List<Test.Models.ReferredFriends.SubModule>();
        foreach (COML.Classes.SubModule subModule1 in module1.SubModules)
        {
          Test.Models.ReferredFriends.SubModule subModule2 = new Test.Models.ReferredFriends.SubModule()
          {
            ID = subModule1.ID,
            Code = subModule1.Code,
            Description = subModule1.Description,
            Name = subModule1.Name,
            IsComplete = subModule1.IsComplete,
            IsStarted = subModule1.IsStarted,
            IsLocked = subModule1.IsLocked,
            IsCurrent = subModule1.IsCurrent,
            IsAvailable = subModule1.IsAvailable,
            IsNotYetCompetent = subModule1.IsNotYetCompetent
          };
          if (subModule2.IsLocked)
          {
            module2.IsLocked = true;
            progressViewModel.IsLocked = true;
          }
          subModule2.Code = subModule2.Code == "" ? subModule2.Code : " - " + subModule2.Code;
          module2.SubModules.Add(subModule2);
        }
        progressViewModel.Modules.Add(module2);
      }
      BLL.Logging.SaveInstructorPageLog(userId, "Referred Friend Progress");
      return (ActionResult) this.View((object) progressViewModel);
    }
  }
}
