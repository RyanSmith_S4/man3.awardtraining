﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminStudentController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using Microsoft.CSharp.RuntimeBinder;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using Test.Models;
using Test.Models.Admin;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  [Authorize]
  public class AdminStudentController : Controller
  {
    public ActionResult Index(int paId = 1)
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          List<StudentSummary> allStudents = BLL.Student.GetAllStudents();
          List<AdminStudentViewModel> allItems = new List<AdminStudentViewModel>();
          foreach (StudentSummary studentSummary in allStudents)
          {
            AdminStudentViewModel studentViewModel = new AdminStudentViewModel()
            {
              UserID = studentSummary.UserID,
              DateEnroled = studentSummary.DateTimeCreated,
              IDNumber = studentSummary.IdentityNumber,
              FirstName = studentSummary.FirstName,
              LastName = studentSummary.LastName,
              LatestActivity = studentSummary.LastActivity,
              StudentID = studentSummary.StudentID,
              IsLocked = studentSummary.IsLocked,
              IsEnabled = studentSummary.IsEnabled
            };
            allItems.Add(studentViewModel);
          }
          return (ActionResult) this.View((object) allItems.ToPagedList<AdminStudentViewModel>(paId, 10));
        }
        COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }

    public ActionResult Search(int paId = 1, string paWord = null)
    {
      List<StudentSummary> studentSummaryList1 = new List<StudentSummary>();
      List<StudentSummary> studentSummaryList2 = string.IsNullOrWhiteSpace(paWord) ? BLL.Student.GetAllStudents() : BLL.Student.GetStudentsByIDNumber(paWord);
      List<AdminStudentViewModel> allItems = new List<AdminStudentViewModel>();
      foreach (StudentSummary studentSummary in studentSummaryList2)
      {
        AdminStudentViewModel studentViewModel = new AdminStudentViewModel()
        {
          StudentID = studentSummary.StudentID,
          UserID = studentSummary.UserID,
          DateEnroled = studentSummary.DateTimeCreated,
          IDNumber = studentSummary.IdentityNumber,
          FirstName = studentSummary.FirstName,
          LastName = studentSummary.LastName,
          IsEnabled = studentSummary.IsEnabled,
          IsLocked = studentSummary.IsLocked,
          LatestActivity = studentSummary.LastActivity
        };
        allItems.Add(studentViewModel);
      }
      return (ActionResult) this.View("Index", (object) allItems.ToPagedList<AdminStudentViewModel>(paId, 10));
    }

    public ActionResult StudentDetails(int paId, string paMessage)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Student studentRecordById = BLL.Student.GetStudentRecordByID(paId);
      StudentDetailsViewModel detailsViewModel = new StudentDetailsViewModel();
      List<string> fsaCodes = BLL.Core.Admin.GetFSACodes();
      List<FSACode> fsaCodeList = new List<FSACode>();
      foreach (string str in fsaCodes)
        fsaCodeList.Add(new FSACode()
        {
          ID = str,
          Code = str
        });
      // ISSUE: reference to a compiler-generated field
      if (AdminStudentController.\u003C\u003Eo__2.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AdminStudentController.\u003C\u003Eo__2.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, SelectList, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "FsaCodes", typeof (AdminStudentController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = AdminStudentController.\u003C\u003Eo__2.\u003C\u003Ep__0.Target((CallSite) AdminStudentController.\u003C\u003Eo__2.\u003C\u003Ep__0, this.ViewBag, new SelectList((IEnumerable) fsaCodeList, "ID", "Code"));
      if (studentRecordById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      detailsViewModel.Age = studentRecordById.Age.ToString();
      detailsViewModel.CellNumber = studentRecordById.TelephoneCell;
      detailsViewModel.DateEnroled = studentRecordById.DateTimeCreated;
      detailsViewModel.Employed = studentRecordById.Employed ? "Yes" : "No";
      detailsViewModel.FirstName = studentRecordById.FirstName;
      detailsViewModel.LastName = studentRecordById.LastName;
      detailsViewModel.Nickname = studentRecordById.Nickname;
      detailsViewModel.Gender = studentRecordById.SelectGender.ToString();
      detailsViewModel.EmailAddress = studentRecordById.EmailAddress;
      detailsViewModel.Race = studentRecordById.SelectedRace.ToString();
      detailsViewModel.HighestQualification = studentRecordById.HighestQualification;
      detailsViewModel.HighestQualificationTitle = studentRecordById.HighestQualificationTitle;
      detailsViewModel.HomeLanguage = studentRecordById.HomeLanguage;
      detailsViewModel.IDNumber = studentRecordById.IdentityNumber;
      detailsViewModel.PostalAddress1 = studentRecordById.PostalAddress1;
      detailsViewModel.PostalAddress2 = studentRecordById.PostalAddress2;
      detailsViewModel.PostalAddress3 = studentRecordById.PostalAddress3;
      detailsViewModel.PostalAddressCity = studentRecordById.PostalAddressCity;
      detailsViewModel.PostalAddressCode = studentRecordById.PostalAddressCode;
      detailsViewModel.PostalAddressProvince = studentRecordById.PostalAddressProvince;
      detailsViewModel.StudentID = studentRecordById.ID;
      detailsViewModel.UserID = studentRecordById.UserID;
      detailsViewModel.FsaCode = studentRecordById.FsaCode;
      detailsViewModel.IsChatEnabled = studentRecordById.IsChatEnabled;
      detailsViewModel.IsEnabled = studentRecordById.IsEnabled;
      return (ActionResult) this.View((object) detailsViewModel);
    }

    public ActionResult StudentProgress(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      StudentProgressViewModel progressViewModel = new StudentProgressViewModel();
      List<COML.Classes.Module> modulesForUserById = BLL.Module.GetModulesForUserByID(paId);
      COML.Classes.Student studentRecordById = BLL.Student.GetStudentRecordByID(paId);
      if (studentRecordById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      if (modulesForUserById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      progressViewModel.FirstName = studentRecordById.FirstName;
      progressViewModel.LastName = studentRecordById.LastName;
      progressViewModel.IsLocked = false;
      progressViewModel.Modules = new List<Test.Models.Admin.Module>();
      progressViewModel.IDNumber = studentRecordById.IdentityNumber;
      foreach (COML.Classes.Module module1 in modulesForUserById)
      {
        Test.Models.Admin.Module module2 = new Test.Models.Admin.Module()
        {
          ID = module1.ID,
          Code = module1.Code,
          Description = module1.Description,
          Name = module1.Name,
          Order = module1.Order,
          IsAvailable = module1.IsAvailable,
          IsComplete = module1.IsCompleted,
          IsLocked = false
        };
        module2.SubModules = new List<Test.Models.Admin.SubModule>();
        foreach (COML.Classes.SubModule subModule1 in module1.SubModules)
        {
          Test.Models.Admin.SubModule subModule2 = new Test.Models.Admin.SubModule()
          {
            ID = subModule1.ID,
            Code = subModule1.Code,
            Description = subModule1.Description,
            Name = subModule1.Name,
            IsComplete = subModule1.IsComplete,
            IsStarted = subModule1.IsStarted,
            IsLocked = subModule1.IsLocked,
            IsCurrent = subModule1.IsCurrent,
            IsAvailable = subModule1.IsAvailable,
            IsNotYetCompetent = subModule1.IsNotYetCompetent
          };
          if (subModule2.IsLocked)
          {
            module2.IsLocked = true;
            progressViewModel.IsLocked = true;
          }
          subModule2.Code = subModule2.Code == "" ? subModule2.Code : " - " + subModule2.Code;
          module2.SubModules.Add(subModule2);
        }
        progressViewModel.Modules.Add(module2);
      }
      return (ActionResult) this.View((object) progressViewModel);
    }

    public ActionResult StudentLogs(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Student studentRecordById = BLL.Student.GetStudentRecordByID(paId);
      List<GenericLog> studentLogs = BLL.Logging.GetStudentLogs(paId);
      StudentLogsViewModel studentLogsViewModel = new StudentLogsViewModel();
      if (studentRecordById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      studentLogsViewModel.FirstName = studentRecordById.FirstName;
      studentLogsViewModel.LastName = studentRecordById.LastName;
      studentLogsViewModel.StudentID = studentRecordById.ID;
      studentLogsViewModel.IDNumber = studentRecordById.IdentityNumber;
      studentLogsViewModel.Logs = new List<Log>();
      foreach (GenericLog genericLog in studentLogs)
        studentLogsViewModel.Logs.Add(new Log()
        {
          Timestamp = genericLog.TimeStamp,
          Description = genericLog.Description,
          Type = genericLog.Type
        });
      return (ActionResult) this.View((object) studentLogsViewModel);
    }

    public ActionResult DownloadStudentsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetStudentsDetailed();
        if (studentsDetailed == null || studentsDetailed.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("ACTIVE LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 30.0;
        excelWorksheet.Column(2).Width = 40.0;
        excelWorksheet.Column(3).Width = 30.0;
        excelWorksheet.Column(4).Width = 30.0;
        excelWorksheet.Column(5).Width = 30.0;
        excelWorksheet.Column(6).Width = 30.0;
        excelWorksheet.Column(7).Width = 30.0;
        excelWorksheet.Column(8).Width = 30.0;
        excelWorksheet.Column(9).Width = 30.0;
        excelWorksheet.Column(10).Width = 30.0;
        excelWorksheet.Column(11).Width = 30.0;
        excelWorksheet.Column(12).Width = 30.0;
        excelWorksheet.Column(13).Width = 30.0;
        excelWorksheet.Column(14).Width = 30.0;
        excelWorksheet.Column(15).Width = 30.0;
        excelWorksheet.Column(16).Width = 30.0;
        excelWorksheet.Column(17).Width = 30.0;
        excelWorksheet.Column(18).Width = 30.0;
        excelWorksheet.Column(19).Width = 30.0;
        excelWorksheet.Column(20).Width = 30.0;
        excelWorksheet.Column(21).Width = 30.0;
        excelWorksheet.Column(22).Width = 30.0;
        excelWorksheet.Column(23).Width = 30.0;
        excelWorksheet.Column(24).Width = 30.0;
        excelWorksheet.Column(25).Width = 30.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "ID");
        excelWorksheet.SetValue(3, 2, (object) "First Name");
        excelWorksheet.SetValue(3, 3, (object) "Last Name");
        excelWorksheet.SetValue(3, 4, (object) "Age");
        excelWorksheet.SetValue(3, 5, (object) "Gender");
        excelWorksheet.SetValue(3, 6, (object) "Race");
        excelWorksheet.SetValue(3, 7, (object) "FSA Code");
        excelWorksheet.SetValue(3, 8, (object) "ID Number");
        excelWorksheet.SetValue(3, 9, (object) "Email");
        excelWorksheet.SetValue(3, 10, (object) "Cell Number");
        excelWorksheet.SetValue(3, 11, (object) "Postal Address 1");
        excelWorksheet.SetValue(3, 12, (object) "Postal Address 2");
        excelWorksheet.SetValue(3, 13, (object) "Postal Address 3");
        excelWorksheet.SetValue(3, 14, (object) "Postal Address City");
        excelWorksheet.SetValue(3, 15, (object) "Postal Address Code");
        excelWorksheet.SetValue(3, 16, (object) "Province");
        excelWorksheet.SetValue(3, 17, (object) "Home Language");
        excelWorksheet.SetValue(3, 18, (object) "Highest Qualification");
        excelWorksheet.SetValue(3, 19, (object) "Highest Qualification Title");
        excelWorksheet.SetValue(3, 20, (object) "Currently Employed");
        excelWorksheet.SetValue(3, 21, (object) "Date Enroled");
        excelWorksheet.SetValue(3, 22, (object) "Last Activity");
        excelWorksheet.SetValue(3, 23, (object) "Current Module");
        excelWorksheet.SetValue(3, 24, (object) "Current Submodule");
        excelWorksheet.SetValue(3, 25, (object) "Referred Friend");
        using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < studentsDetailed.Count; ++index)
        {
          StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
          excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.StudentID);
          excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.FirstName);
          excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.LastName);
          excelWorksheet.SetValue(index + 4, 4, (object) studentSummaryDetailed.Age);
          excelWorksheet.SetValue(index + 4, 5, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
          excelWorksheet.SetValue(index + 4, 6, (object) studentSummaryDetailed.Race);
          excelWorksheet.SetValue(index + 4, 7, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
          excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.IdentityNumber);
          excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.EmailAddress);
          excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.TelephoneNumber);
          excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress1);
          excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress2);
          excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalAddress3);
          excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCity);
          excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.PostalCode);
          excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.Province);
          excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HomeLanguage);
          excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualification);
          excelWorksheet.SetValue(index + 4, 19, (object) studentSummaryDetailed.HighestQualificationTitle);
          excelWorksheet.SetValue(index + 4, 20, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
          excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
          excelWorksheet.SetValue(index + 4, 22, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
          excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentModule);
          excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.CurrentSubModule);
          excelWorksheet.SetValue(index + 4, 25, (object) studentSummaryDetailed.ReferredFriend);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("ActiveLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      }
    }

    public ActionResult DownloadInactiveStudentsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetInactiveStudentsDetailed();
        if (studentsDetailed == null || studentsDetailed.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("INACTIVE LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 30.0;
        excelWorksheet.Column(2).Width = 40.0;
        excelWorksheet.Column(3).Width = 30.0;
        excelWorksheet.Column(4).Width = 30.0;
        excelWorksheet.Column(5).Width = 30.0;
        excelWorksheet.Column(6).Width = 30.0;
        excelWorksheet.Column(7).Width = 30.0;
        excelWorksheet.Column(8).Width = 30.0;
        excelWorksheet.Column(9).Width = 30.0;
        excelWorksheet.Column(10).Width = 30.0;
        excelWorksheet.Column(11).Width = 30.0;
        excelWorksheet.Column(12).Width = 30.0;
        excelWorksheet.Column(13).Width = 30.0;
        excelWorksheet.Column(14).Width = 30.0;
        excelWorksheet.Column(15).Width = 30.0;
        excelWorksheet.Column(16).Width = 30.0;
        excelWorksheet.Column(17).Width = 30.0;
        excelWorksheet.Column(18).Width = 30.0;
        excelWorksheet.Column(19).Width = 30.0;
        excelWorksheet.Column(20).Width = 30.0;
        excelWorksheet.Column(21).Width = 30.0;
        excelWorksheet.Column(22).Width = 30.0;
        excelWorksheet.Column(23).Width = 30.0;
        excelWorksheet.Column(24).Width = 30.0;
        excelWorksheet.Column(25).Width = 30.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "ID");
        excelWorksheet.SetValue(3, 2, (object) "First Name");
        excelWorksheet.SetValue(3, 3, (object) "Last Name");
        excelWorksheet.SetValue(3, 4, (object) "Age");
        excelWorksheet.SetValue(3, 5, (object) "Gender");
        excelWorksheet.SetValue(3, 6, (object) "Race");
        excelWorksheet.SetValue(3, 7, (object) "FSA Code");
        excelWorksheet.SetValue(3, 8, (object) "ID Number");
        excelWorksheet.SetValue(3, 9, (object) "Email");
        excelWorksheet.SetValue(3, 10, (object) "Cell Number");
        excelWorksheet.SetValue(3, 11, (object) "Postal Address 1");
        excelWorksheet.SetValue(3, 12, (object) "Postal Address 2");
        excelWorksheet.SetValue(3, 13, (object) "Postal Address 3");
        excelWorksheet.SetValue(3, 14, (object) "Postal Address City");
        excelWorksheet.SetValue(3, 15, (object) "Postal Address Code");
        excelWorksheet.SetValue(3, 16, (object) "Province");
        excelWorksheet.SetValue(3, 17, (object) "Home Language");
        excelWorksheet.SetValue(3, 18, (object) "Highest Qualification");
        excelWorksheet.SetValue(3, 19, (object) "Highest Qualification Title");
        excelWorksheet.SetValue(3, 20, (object) "Currently Employed");
        excelWorksheet.SetValue(3, 21, (object) "Date Enroled");
        excelWorksheet.SetValue(3, 22, (object) "Last Activity");
        excelWorksheet.SetValue(3, 23, (object) "Current Module");
        excelWorksheet.SetValue(3, 24, (object) "Current Submodule");
        excelWorksheet.SetValue(3, 25, (object) "Referred Friend");
        using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < studentsDetailed.Count; ++index)
        {
          StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
          excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.StudentID);
          excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.FirstName);
          excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.LastName);
          excelWorksheet.SetValue(index + 4, 4, (object) studentSummaryDetailed.Age);
          excelWorksheet.SetValue(index + 4, 5, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
          excelWorksheet.SetValue(index + 4, 6, (object) studentSummaryDetailed.Race);
          excelWorksheet.SetValue(index + 4, 7, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
          excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.IdentityNumber);
          excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.EmailAddress);
          excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.TelephoneNumber);
          excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress1);
          excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress2);
          excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalAddress3);
          excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCity);
          excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.PostalCode);
          excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.Province);
          excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HomeLanguage);
          excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualification);
          excelWorksheet.SetValue(index + 4, 19, (object) studentSummaryDetailed.HighestQualificationTitle);
          excelWorksheet.SetValue(index + 4, 20, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
          excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
          excelWorksheet.SetValue(index + 4, 22, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
          excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentModule);
          excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.CurrentSubModule);
          excelWorksheet.SetValue(index + 4, 25, (object) studentSummaryDetailed.ReferredFriend);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("InactiveLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      }
    }

    public ActionResult DownloadLockedStudentsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetLockedStudentsDetailed();
        if (studentsDetailed == null || studentsDetailed.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("LOCKED OUT LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 30.0;
        excelWorksheet.Column(2).Width = 40.0;
        excelWorksheet.Column(3).Width = 30.0;
        excelWorksheet.Column(4).Width = 30.0;
        excelWorksheet.Column(5).Width = 30.0;
        excelWorksheet.Column(6).Width = 30.0;
        excelWorksheet.Column(7).Width = 30.0;
        excelWorksheet.Column(8).Width = 30.0;
        excelWorksheet.Column(9).Width = 30.0;
        excelWorksheet.Column(10).Width = 30.0;
        excelWorksheet.Column(11).Width = 30.0;
        excelWorksheet.Column(12).Width = 30.0;
        excelWorksheet.Column(13).Width = 30.0;
        excelWorksheet.Column(14).Width = 30.0;
        excelWorksheet.Column(15).Width = 30.0;
        excelWorksheet.Column(16).Width = 30.0;
        excelWorksheet.Column(17).Width = 30.0;
        excelWorksheet.Column(18).Width = 30.0;
        excelWorksheet.Column(19).Width = 30.0;
        excelWorksheet.Column(20).Width = 30.0;
        excelWorksheet.Column(21).Width = 30.0;
        excelWorksheet.Column(22).Width = 30.0;
        excelWorksheet.Column(23).Width = 30.0;
        excelWorksheet.Column(24).Width = 30.0;
        excelWorksheet.Column(25).Width = 30.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "ID");
        excelWorksheet.SetValue(3, 2, (object) "First Name");
        excelWorksheet.SetValue(3, 3, (object) "Last Name");
        excelWorksheet.SetValue(3, 4, (object) "Age");
        excelWorksheet.SetValue(3, 5, (object) "Gender");
        excelWorksheet.SetValue(3, 6, (object) "Race");
        excelWorksheet.SetValue(3, 7, (object) "FSA Code");
        excelWorksheet.SetValue(3, 8, (object) "ID Number");
        excelWorksheet.SetValue(3, 9, (object) "Email");
        excelWorksheet.SetValue(3, 10, (object) "Cell Number");
        excelWorksheet.SetValue(3, 11, (object) "Postal Address 1");
        excelWorksheet.SetValue(3, 12, (object) "Postal Address 2");
        excelWorksheet.SetValue(3, 13, (object) "Postal Address 3");
        excelWorksheet.SetValue(3, 14, (object) "Postal Address City");
        excelWorksheet.SetValue(3, 15, (object) "Postal Address Code");
        excelWorksheet.SetValue(3, 16, (object) "Province");
        excelWorksheet.SetValue(3, 17, (object) "Home Language");
        excelWorksheet.SetValue(3, 18, (object) "Highest Qualification");
        excelWorksheet.SetValue(3, 19, (object) "Highest Qualification Title");
        excelWorksheet.SetValue(3, 20, (object) "Currently Employed");
        excelWorksheet.SetValue(3, 21, (object) "Date Enroled");
        excelWorksheet.SetValue(3, 22, (object) "Last Activity");
        excelWorksheet.SetValue(3, 23, (object) "Current Module");
        excelWorksheet.SetValue(3, 24, (object) "Current Submodule");
        excelWorksheet.SetValue(3, 25, (object) "Referred Friend");
        using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < studentsDetailed.Count; ++index)
        {
          StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
          excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.StudentID);
          excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.FirstName);
          excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.LastName);
          excelWorksheet.SetValue(index + 4, 4, (object) studentSummaryDetailed.Age);
          excelWorksheet.SetValue(index + 4, 5, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
          excelWorksheet.SetValue(index + 4, 6, (object) studentSummaryDetailed.Race);
          excelWorksheet.SetValue(index + 4, 7, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
          excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.IdentityNumber);
          excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.EmailAddress);
          excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.TelephoneNumber);
          excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress1);
          excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress2);
          excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalAddress3);
          excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCity);
          excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.PostalCode);
          excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.Province);
          excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HomeLanguage);
          excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualification);
          excelWorksheet.SetValue(index + 4, 19, (object) studentSummaryDetailed.HighestQualificationTitle);
          excelWorksheet.SetValue(index + 4, 20, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
          excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
          excelWorksheet.SetValue(index + 4, 22, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
          excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentModule);
          excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.CurrentSubModule);
          excelWorksheet.SetValue(index + 4, 25, (object) studentSummaryDetailed.ReferredFriend);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LockedOutLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      }
    }

    public ActionResult DownloadStudentLogReport(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        COML.Classes.Student studentRecordById = BLL.Student.GetStudentRecordByID(paId);
        List<GenericLog> studentLogs = BLL.Logging.GetStudentLogs(paId);
        if (studentLogs == null || studentLogs.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("LEARNER LOG EXPORT - " + studentRecordById.FirstName + " " + studentRecordById.LastName + " - " + studentRecordById.IdentityNumber + " - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "Timestamp");
        excelWorksheet.SetValue(3, 2, (object) "Type");
        excelWorksheet.SetValue(3, 3, (object) "Description");
        using (ExcelRange cell = excelWorksheet.Cells["A3:C3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < studentLogs.Count; ++index)
        {
          GenericLog genericLog = studentLogs[index];
          string str;
          switch (genericLog.Type)
          {
            case Enumerations.enumLogType.GeneralChat:
              str = "General Chat";
              break;
            case Enumerations.enumLogType.GraduateChat:
              str = "Graduate Chat";
              break;
            default:
              str = genericLog.Type.ToString();
              break;
          }
          excelWorksheet.SetValue(index + 4, 1, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) genericLog.TimeStamp));
          excelWorksheet.SetValue(index + 4, 2, (object) str);
          excelWorksheet.SetValue(index + 4, 3, (object) genericLog.Description);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LogExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      }
    }

    public ActionResult UnlockStudent(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!BLL.Student.UnlockStudent(paId))
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      BLL.Logging.SaveSystemLog(paId, "Activated user account.", "Admin");
      return (ActionResult) this.RedirectToAction("StudentProgress", "AdminStudent", (object) new
      {
        paId = paId
      });
    }

    public ActionResult ChangeFsaCode(StudentDetailsViewModel loModel)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          if (this.User.Identity.IsAuthenticated)
          {
            if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
            {
              if (loModel.NewFsaCode == null || loModel.NewFsaCode == "")
              {
                COML.Classes.Student studentRecordById = BLL.Student.GetStudentRecordByID(loModel.StudentID);
                StudentDetailsViewModel detailsViewModel = new StudentDetailsViewModel();
                if (studentRecordById == null)
                  return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
                detailsViewModel.Age = studentRecordById.Age.ToString();
                detailsViewModel.CellNumber = studentRecordById.TelephoneCell;
                detailsViewModel.DateEnroled = studentRecordById.DateTimeCreated;
                detailsViewModel.Employed = studentRecordById.Employed ? "Yes" : "No";
                detailsViewModel.FirstName = studentRecordById.FirstName;
                detailsViewModel.LastName = studentRecordById.LastName;
                detailsViewModel.Nickname = studentRecordById.Nickname;
                detailsViewModel.Gender = studentRecordById.SelectGender.ToString();
                detailsViewModel.EmailAddress = studentRecordById.EmailAddress;
                detailsViewModel.Race = studentRecordById.SelectedRace.ToString();
                detailsViewModel.HighestQualification = studentRecordById.HighestQualification;
                detailsViewModel.HighestQualificationTitle = studentRecordById.HighestQualificationTitle;
                detailsViewModel.HomeLanguage = studentRecordById.HomeLanguage;
                detailsViewModel.IDNumber = studentRecordById.IdentityNumber;
                detailsViewModel.PostalAddress1 = studentRecordById.PostalAddress1;
                detailsViewModel.PostalAddress2 = studentRecordById.PostalAddress2;
                detailsViewModel.PostalAddress3 = studentRecordById.PostalAddress3;
                detailsViewModel.PostalAddressCity = studentRecordById.PostalAddressCity;
                detailsViewModel.PostalAddressCode = studentRecordById.PostalAddressCode;
                detailsViewModel.PostalAddressProvince = studentRecordById.PostalAddressProvince;
                detailsViewModel.StudentID = studentRecordById.ID;
                detailsViewModel.UserID = studentRecordById.UserID;
                detailsViewModel.FsaCode = studentRecordById.FsaCode;
                this.ModelState.AddModelError("NewFsaCode", "Please enter a FSA Code");
                return (ActionResult) this.View("StudentDetails", (object) detailsViewModel);
              }
              if (BLL.Student.UpdateStudentFsaCode(loModel.StudentID, loModel.NewFsaCode))
                return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
                {
                  paId = loModel.StudentID
                });
              this.ModelState.AddModelError("", "Error processing your request.");
            }
            else
            {
              COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
              return (ActionResult) this.RedirectToAction("Index", "Home");
            }
          }
          else
          {
            COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction("Index", "Home");
          }
        }
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View();
    }

    public ActionResult DisableChat(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Student.DisableChat(paId);
      return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
      {
        paId = paId,
        paMessage = ""
      });
    }

    public ActionResult EnableChat(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Student.EnableChat(paId);
      return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
      {
        paId = paId,
        paMessage = ""
      });
    }

    public ActionResult EnableAccount(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Student.EnableAccount(paId);
      return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
      {
        paId = paId,
        paMessage = ""
      });
    }

    public ActionResult DisableAccount(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Student.DisableAccount(paId);
      return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
      {
        paId = paId,
        paMessage = ""
      });
    }

    public ActionResult StudentAddDetails()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.View((object) new StudentDetailsEditViewModel());
      return (ActionResult) this.RedirectToAction("Index", "Home");
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult StudentAddDetails(StudentDetailsEditViewModel loModel)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          if (BLL.Student.RegisterStudent(new COML.Classes.Student()
          {
            Age = loModel.Age,
            EmailAddress = loModel.Email.ToLower(),
            Employed = loModel.CurrentlyEmployed == Enumerations.enumTrueFalse.True,
            HighestQualification = loModel.HighestQualification,
            HighestQualificationTitle = loModel.HighestQualificationTitle,
            HomeLanguage = loModel.HomeLanguage,
            Nickname = loModel.Nickname,
            PostalAddress1 = loModel.PostalAddress1,
            PostalAddress2 = loModel.PostalAddress2,
            PostalAddress3 = loModel.PostalAddress3,
            PostalAddressCity = loModel.PostalAddressCity,
            PostalAddressCode = loModel.PostalAddressCode,
            PostalAddressProvince = loModel.PostalAddressProvince,
            SelectedRace = loModel.SelectedRace,
            SelectGender = loModel.SelectGender,
            TelephoneCell = "27" + loModel.TelephoneCell.Substring(1)
          }))
            return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
            {
              paId = loModel.StudentID,
              paMessage = "Learner successfully added"
            });
        }
        else
          this.ModelState.AddModelError("", "Error processing your request.");
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) loModel);
    }

    public ActionResult StudentEditDetails(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Student studentById = BLL.Student.GetStudentByID(paId);
      if (studentById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
      return (ActionResult) this.View((object) new StudentDetailsEditViewModel()
      {
        StudentID = studentById.ID,
        Age = studentById.Age,
        Email = studentById.EmailAddress,
        CurrentlyEmployed = (studentById.Employed ? Enumerations.enumTrueFalse.True : Enumerations.enumTrueFalse.False),
        Firstname = studentById.FirstName,
        Lastname = studentById.LastName,
        IDNumber = studentById.IdentityNumber,
        HighestQualification = studentById.HighestQualification,
        HighestQualificationTitle = studentById.HighestQualificationTitle,
        HomeLanguage = studentById.HomeLanguage,
        SelectGender = studentById.SelectGender,
        SelectedRace = studentById.SelectedRace,
        Nickname = studentById.Nickname,
        PostalAddress1 = studentById.PostalAddress1,
        PostalAddress2 = studentById.PostalAddress2,
        PostalAddress3 = studentById.PostalAddress3,
        PostalAddressCity = studentById.PostalAddressCity,
        PostalAddressCode = studentById.PostalAddressCode,
        PostalAddressProvince = studentById.PostalAddressProvince,
        TelephoneCell = ("0" + studentById.TelephoneCell.Substring(2))
      });
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult StudentEditDetails(StudentDetailsEditViewModel loModel)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          if (BLL.Student.UpdateStudentByID(new COML.Classes.Student()
          {
            ID = loModel.StudentID,
            Age = loModel.Age,
            EmailAddress = loModel.Email.ToLower(),
            Employed = loModel.CurrentlyEmployed == Enumerations.enumTrueFalse.True,
            HighestQualification = loModel.HighestQualification,
            HighestQualificationTitle = loModel.HighestQualificationTitle,
            HomeLanguage = loModel.HomeLanguage,
            Nickname = loModel.Nickname,
            PostalAddress1 = loModel.PostalAddress1,
            PostalAddress2 = loModel.PostalAddress2,
            PostalAddress3 = loModel.PostalAddress3,
            PostalAddressCity = loModel.PostalAddressCity,
            PostalAddressCode = loModel.PostalAddressCode,
            PostalAddressProvince = loModel.PostalAddressProvince,
            SelectedRace = loModel.SelectedRace,
            SelectGender = loModel.SelectGender,
            TelephoneCell = "27" + loModel.TelephoneCell.Substring(1)
          }))
            return (ActionResult) this.RedirectToAction("StudentDetails", "AdminStudent", (object) new
            {
              paId = loModel.StudentID,
              paMessage = "Leaner successfully updated"
            });
        }
        else
          this.ModelState.AddModelError("", "Error processing your request.");
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) loModel);
    }
  }
}
