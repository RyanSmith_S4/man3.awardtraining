﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AccountController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using BLL;
using BLL.Core;
using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Owin.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Test.Models;
using Test.VerifyIDService;

namespace Test.Controllers
{
  [Authorize]
  public class AccountController : Controller
  {
    private ApplicationSignInManager _signInManager;
    private ApplicationUserManager _userManager;
    private const string XsrfKey = "XsrfId";

    public AccountController()
    {
    }

    public AccountController(
      ApplicationUserManager userManager,
      ApplicationSignInManager signInManager)
    {
      this.UserManager = userManager;
      this.SignInManager = signInManager;
    }

    public ApplicationSignInManager SignInManager
    {
      get
      {
        return this._signInManager ?? this.HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
      }
      private set
      {
        this._signInManager = value;
      }
    }

    public ApplicationUserManager UserManager
    {
      get
      {
        return this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
      }
      private set
      {
        this._userManager = value;
      }
    }

    [AllowAnonymous]
    public ActionResult Login(string returnUrl)
    {
      // ISSUE: reference to a compiler-generated field
      if (AccountController.\u003C\u003Eo__10.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountController.\u003C\u003Eo__10.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "ReturnUrl", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = AccountController.\u003C\u003Eo__10.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__10.\u003C\u003Ep__0, this.ViewBag, returnUrl);
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View((object) model);
      switch (await this.SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false))
      {
        case SignInStatus.Success:
          ApplicationUser async = await this.UserManager.FindAsync(model.Email, model.Password);
          if (this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Assessor") && this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Mentor"))
          {
            if (!BLL.Instructor.IsLocked(async.Id))
              return (ActionResult) this.RedirectToAction("Index", "InstructorBoth");
            this.AuthenticationManager.SignOut("ApplicationCookie");
            return (ActionResult) this.RedirectToAction("Contact", "Home");
          }
          if (this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Assessor"))
          {
            if (!BLL.Instructor.IsLocked(async.Id))
              return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
            this.AuthenticationManager.SignOut("ApplicationCookie");
            return (ActionResult) this.RedirectToAction("Contact", "Home");
          }
          if (this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Mentor"))
          {
            if (!BLL.Instructor.IsLocked(async.Id))
              return (ActionResult) this.RedirectToAction("Index", "InstructorMentor");
            this.AuthenticationManager.SignOut("ApplicationCookie");
            return (ActionResult) this.RedirectToAction("Contact", "Home");
          }
          if (this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Admin"))
            return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
          if (this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Sponsor"))
          {
            if (!BLL.Sponsor.IsLocked(async.Id))
              return (ActionResult) this.RedirectToAction("Index", "Sponsor");
            this.AuthenticationManager.SignOut("ApplicationCookie");
            return (ActionResult) this.RedirectToAction("Contact", "Home");
          }
          if (this.UserManager.IsInRole<ApplicationUser, string>(async.Id, "Student"))
          {
            if (BLL.Student.IsLocked(async.Id))
            {
              this.AuthenticationManager.SignOut("ApplicationCookie");
              return (ActionResult) this.RedirectToAction("Contact", "Home");
            }
            BLL.Student.ClearNotifications(async.Id);
            return (ActionResult) this.RedirectToAction("Index", "Dashboard");
          }
          this.AuthenticationManager.SignOut("ApplicationCookie");
          return (ActionResult) this.RedirectToAction("Index", "Home");
        case SignInStatus.LockedOut:
          return (ActionResult) this.View("Lockout");
        case SignInStatus.RequiresVerification:
          return (ActionResult) this.RedirectToAction("SendCode", (object) new
          {
            ReturnUrl = returnUrl,
            RememberMe = model.RememberMe
          });
        default:
          this.ModelState.AddModelError("", "Invalid login attempt.");
          return (ActionResult) this.View((object) model);
      }
    }

    [AllowAnonymous]
    public async Task<ActionResult> VerifyCode(
      string provider,
      string returnUrl,
      bool rememberMe)
    {
      if (!await this.SignInManager.HasBeenVerifiedAsync())
        return (ActionResult) this.View("Error");
      return (ActionResult) this.View((object) new VerifyCodeViewModel()
      {
        Provider = provider,
        ReturnUrl = returnUrl,
        RememberMe = rememberMe
      });
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View((object) model);
      switch (await this.SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, model.RememberBrowser))
      {
        case SignInStatus.Success:
          return this.RedirectToLocal(model.ReturnUrl);
        case SignInStatus.LockedOut:
          return (ActionResult) this.View("Lockout");
        default:
          this.ModelState.AddModelError("", "Invalid code.");
          return (ActionResult) this.View((object) model);
      }
    }

    [AllowAnonymous]
    public ActionResult RegisterMentorAssessor()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> RegisterMentorAssessor(
      RegisterInstructorViewModel model)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return (ActionResult) this.View((object) model);
        string verifyIdUsername = Settings.VerifyIDUsername;
        string verifyIdapiKey = Settings.VerifyIDAPIKey;
        bool verifyIdNumbers = Settings.VerifyIDNumbers;
        IDErrorMessage loVerifyIDResult = new IDErrorMessage();
        bool flag1 = BLL.Instructor.IDNumberExists(model.IdentityNumber);
        List<ApplicationUser> applicationUserList = new List<ApplicationUser>();
        if (model.IsAssessor == Enumerations.enumTrueFalse.False && model.IsMentor == Enumerations.enumTrueFalse.False)
          this.ModelState.AddModelError("IsAssessor", "You have to register as either an Assessor or a Mentor, or both.");
        else if (model.IsAssessor == Enumerations.enumTrueFalse.True && (model.AssessorCode == "" || model.AssessorCode == null))
          this.ModelState.AddModelError("AssessorCode", "Please enter an Assessor Code");
        else if (model.IsMentor == Enumerations.enumTrueFalse.True && (model.MentorCode == "" || model.MentorCode == null))
          this.ModelState.AddModelError("MentorCode", "Please enter an Mentor Code");
        else if (!flag1)
        {
          bool flag2;
          if (verifyIdNumbers && verifyIdapiKey != "" && verifyIdUsername != "")
          {
            loVerifyIDResult = this.VerifyID(model.IdentityNumber, model.Firstname, model.Lastname);
            flag2 = loVerifyIDResult.Valid;
          }
          else
            flag2 = true;
          if (flag2)
          {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
              applicationUserList = applicationDbContext.Users.Where<ApplicationUser>((Expression<Func<ApplicationUser, bool>>) (x => x.Email.ToLower() == model.Email.ToLower())).ToList<ApplicationUser>();
            if (applicationUserList == null || applicationUserList.Count == 0)
            {
              ApplicationUser applicationUser = new ApplicationUser();
              applicationUser.UserName = model.Email.ToLower();
              applicationUser.Email = model.Email.ToLower();
              ApplicationUser loUser = applicationUser;
              IdentityResult async = await this.UserManager.CreateAsync(loUser, model.Password);
              using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
              {
                RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>((IRoleStore<IdentityRole, string>) new RoleStore<IdentityRole>((DbContext) applicationDbContext));
                Microsoft.AspNet.Identity.UserManager<ApplicationUser> manager = new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext));
                if (model.IsAssessor == Enumerations.enumTrueFalse.True)
                  manager.AddToRole<ApplicationUser, string>(loUser.Id, "Assessor");
                if (model.IsMentor == Enumerations.enumTrueFalse.True)
                  manager.AddToRole<ApplicationUser, string>(loUser.Id, "Mentor");
                applicationDbContext.SaveChanges();
              }
              int num = BLL.Instructor.RegisterInstructor(new COML.Classes.Instructor()
              {
                Age = model.Age,
                FirstName = model.Firstname,
                LastName = model.Lastname,
                Nickname = model.Nickname,
                HomeLanguage = model.HomeLanguage == null ? "" : model.HomeLanguage,
                IdentityNumber = model.IdentityNumber,
                EmailAddress = model.Email,
                AssignedEmailAddress = "",
                SelectGender = model.SelectGender,
                HighestQualification = model.HighestQualification == null ? "" : model.HighestQualification,
                HighestQualificationTitle = model.HighestQualificationTitle == null ? "" : model.HighestQualificationTitle,
                PostalAddress1 = model.PostalAddress1 == null ? "" : model.PostalAddress1,
                PostalAddress2 = model.PostalAddress2 == null ? "" : model.PostalAddress2,
                PostalAddress3 = model.PostalAddress3 == null ? "" : model.PostalAddress3,
                PostalAddressCity = model.PostalAddressCity == null ? "" : model.PostalAddressCity,
                PostalAddressCode = model.PostalAddressCode == null ? "" : model.PostalAddressCode,
                PostalAddressProvince = model.PostalAddressProvince == null ? "" : model.PostalAddressProvince,
                SelectedRace = model.SelectedRace,
                TelephoneCell = "27" + model.TelephoneCell.Substring(1, model.TelephoneCell.Length - 1),
                Employed = model.CurrentlyEmployed == Enumerations.enumTrueFalse.True,
                IsAssessor = model.IsAssessor == Enumerations.enumTrueFalse.True,
                AssessorCode = model.AssessorCode == null ? "" : model.AssessorCode,
                MentorCode = model.MentorCode == null ? "" : model.MentorCode,
                IsMentor = model.IsMentor == Enumerations.enumTrueFalse.True,
                IsEnabled = true,
                IsLocked = false,
                IsApproved = false,
                UserID = loUser.Id,
                DateTimeCreated = DateTime.Now,
                LastActivity = DateTime.Now,
                IsChatEnabled = true
              });
              if (num > 0)
              {
                this.TempData["tid"] = (object) num;
                return (ActionResult) this.RedirectToAction("RegisterMentorAssessorFiles");
              }
              this.ModelState.AddModelError("Registration", "Error creating your profile.");
              loUser = (ApplicationUser) null;
            }
            else
              this.ModelState.AddModelError("Email", "Email address already exists. Please enter a different email address.");
          }
          else
            this.ModelState.AddModelError("IdentityNumber", loVerifyIDResult.ErrorMessage);
        }
        else
          this.ModelState.AddModelError("IdentityNumber", "Identity Number already exists. Please enter a different Identity Number.");
        loVerifyIDResult = (IDErrorMessage) null;
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) model);
    }

    [AllowAnonymous]
    public ActionResult RegisterMentorAssessorFiles()
    {
      try
      {
        if (this.TempData["tid"] == null)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        int paInstructorID = (int) this.TempData["tid"];
        if (paInstructorID < 1)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        COML.Classes.Instructor instructorById = BLL.Instructor.GetInstructorByID(paInstructorID);
        if (instructorById.IsApproved || !instructorById.IsEnabled || instructorById.IsLocked)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        return (ActionResult) this.View((object) new RegisterInstructorFileViewModel()
        {
          ID = paInstructorID
        });
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> RegisterMentorAssessorFiles(
      RegisterInstructorFileViewModel model)
    {
      try
      {
        if (!this.ModelState.IsValid)
          return (ActionResult) this.View((object) model);
        if (model.ID <= 0)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        COML.Classes.Instructor instructorById = BLL.Instructor.GetInstructorByID(model.ID);
        if (instructorById == null)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        Guid guid1 = Guid.NewGuid();
        Guid guid2 = Guid.NewGuid();
        Guid guid3 = Guid.NewGuid();
        string contentType1 = model.CV.ContentType;
        string contentType2 = model.Qualification1.ContentType;
        bool flag1 = true;
        string str1 = "";
        string paFileLocation1 = "";
        int paSystemFile1 = 0;
        int paTypeFile1 = 0;
        if (!(contentType1 == "application/pdf"))
        {
          if (contentType1 == "application/msword" || contentType1 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
          {
            str1 = guid1.ToString() + ".doc";
            string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str1);
            paFileLocation1 = Path.Combine("Files", "Users", "Others", str1);
            paSystemFile1 = 3;
            paTypeFile1 = 2;
            model.CV.SaveAs(filename);
          }
        }
        else
        {
          str1 = guid1.ToString() + ".pdf";
          string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str1);
          paFileLocation1 = Path.Combine("Files", "Users", "Others", str1);
          paSystemFile1 = 3;
          paTypeFile1 = 1;
          model.CV.SaveAs(filename);
        }
        bool flag2 = BLL.Instructor.SaveInstructorFile(model.ID, paSystemFile1, paTypeFile1, str1, paFileLocation1);
        string str2 = "";
        string paFileLocation2 = "";
        int paSystemFile2 = 0;
        int paTypeFile2 = 0;
        if (!(contentType2 == "application/pdf"))
        {
          if (contentType2 == "application/msword" || contentType2 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
          {
            str2 = guid2.ToString() + ".doc";
            string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
            paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
            paSystemFile2 = 4;
            paTypeFile2 = 2;
            model.Qualification1.SaveAs(filename);
          }
        }
        else
        {
          str2 = guid2.ToString() + ".pdf";
          string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str2);
          paFileLocation2 = Path.Combine("Files", "Users", "Others", str2);
          paSystemFile2 = 4;
          paTypeFile2 = 1;
          model.Qualification1.SaveAs(filename);
        }
        bool flag3 = BLL.Instructor.SaveInstructorFile(model.ID, paSystemFile2, paTypeFile2, str2, paFileLocation2);
        string str3 = "";
        string paFileLocation3 = "";
        int paSystemFile3 = 0;
        int paTypeFile3 = 0;
        if (model.Qualification2 != null)
        {
          string contentType3 = model.Qualification2.ContentType;
          if (!(contentType1 == "application/pdf"))
          {
            if (contentType1 == "application/msword" || contentType1 == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
              str3 = guid3.ToString() + ".doc";
              string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str3);
              paFileLocation3 = Path.Combine("Files", "Users", "Others", str3);
              paSystemFile3 = 4;
              paTypeFile3 = 2;
              model.Qualification2.SaveAs(filename);
            }
          }
          else
          {
            str3 = guid3.ToString() + ".pdf";
            string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Users", "Others", str3);
            paFileLocation3 = Path.Combine("Files", "Users", "Others", str3);
            paSystemFile3 = 4;
            paTypeFile3 = 1;
            model.Qualification2.SaveAs(filename);
          }
          flag1 = BLL.Instructor.SaveInstructorFile(model.ID, paSystemFile3, paTypeFile3, str3, paFileLocation3);
        }
        if (flag2 & flag3 & flag1)
        {
          Mailer.SendInstructorRegistrationEmail(instructorById.EmailAddress, instructorById.FirstName, instructorById.LastName, instructorById.UserID);
          return (ActionResult) this.View("RegisterMentorAssessorConfirmation");
        }
        this.ModelState.AddModelError("", "Error processing your request.");
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) model);
    }

    [AllowAnonymous]
    public ActionResult Register()
    {
      List<User> users = Admin.GetUsers();
      List<string> fsaCodes = Admin.GetFSACodes();
      List<UserItem> source = new List<UserItem>();
      List<FSACode> fsaCodeList = new List<FSACode>();
      source.Add(new UserItem() { ID = "", Name = "" });
      foreach (User user in users)
      {
        string str = user.FirstName + " " + user.LastName;
        source.Add(new UserItem()
        {
          ID = user.UserID,
          Name = str
        });
      }
      foreach (string str in fsaCodes)
        source.Add(new UserItem() { ID = str, Name = str });
      List<UserItem> list = source.OrderBy<UserItem, string>((Func<UserItem, string>) (x => x.Name)).ToList<UserItem>();
      // ISSUE: reference to a compiler-generated field
      if (AccountController.\u003C\u003Eo__18.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountController.\u003C\u003Eo__18.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, SelectList, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "ID", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = AccountController.\u003C\u003Eo__18.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__18.\u003C\u003Ep__0, this.ViewBag, new SelectList((IEnumerable) list, "ID", "Name"));
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Register(RegisterViewModel model)
    {
      try
      {
        if (this.ModelState.IsValid)
        {
          string verifyIdUsername = Settings.VerifyIDUsername;
          string verifyIdapiKey = Settings.VerifyIDAPIKey;
          List<ApplicationUser> applicationUserList = new List<ApplicationUser>();
          bool verifyIdNumbers = Settings.VerifyIDNumbers;
          IDErrorMessage loVerifyIDResult = new IDErrorMessage();
          List<User> users = Admin.GetUsers();
          List<UserItem> source = new List<UserItem>();
          List<string> fsaCodes = Admin.GetFSACodes();
          string loFsaCode = "";
          string loReferralUser = "";
          source.Add(new UserItem() { ID = "", Name = "" });
          foreach (User user in users)
          {
            string str = user.FirstName + " " + user.LastName;
            source.Add(new UserItem()
            {
              ID = user.UserID,
              Name = str.ToUpper()
            });
          }
          foreach (string str in fsaCodes)
            source.Add(new UserItem()
            {
              ID = str,
              Name = str
            });
          List<UserItem> list = source.OrderBy<UserItem, string>((Func<UserItem, string>) (x => x.Name)).ToList<UserItem>();
          // ISSUE: reference to a compiler-generated field
          if (AccountController.\u003C\u003Eo__19.\u003C\u003Ep__0 == null)
          {
            // ISSUE: reference to a compiler-generated field
            AccountController.\u003C\u003Eo__19.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, SelectList, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "ID", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
            {
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
            }));
          }
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          object obj = AccountController.\u003C\u003Eo__19.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__19.\u003C\u003Ep__0, this.ViewBag, new SelectList((IEnumerable) list, "ID", "Name"));
          using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            applicationUserList = applicationDbContext.Users.Where<ApplicationUser>((Expression<Func<ApplicationUser, bool>>) (x => x.Email.ToLower() == model.Email.ToLower())).ToList<ApplicationUser>();
          bool flag1 = BLL.Student.IDNumberExists(model.IdentityNumber);
          if (applicationUserList == null || applicationUserList.Count == 0)
          {
            if (!flag1)
            {
              bool flag2;
              if (verifyIdNumbers && verifyIdapiKey != "" && verifyIdUsername != "")
              {
                loVerifyIDResult = this.VerifyID(model.IdentityNumber, model.Firstname, model.Lastname);
                flag2 = loVerifyIDResult.Valid;
              }
              else
                flag2 = true;
              if (flag2)
              {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser.UserName = model.Email.ToLower();
                applicationUser.Email = model.Email.ToLower();
                ApplicationUser user = applicationUser;
                IdentityResult result = await this.UserManager.CreateAsync(user, model.Password);
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                  RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>((IRoleStore<IdentityRole, string>) new RoleStore<IdentityRole>((DbContext) applicationDbContext));
                  new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext)).AddToRole<ApplicationUser, string>(user.Id, "Student");
                  applicationDbContext.SaveChanges();
                }
                if (model.ReferralUser == null || model.ReferralUser == "")
                {
                  loFsaCode = "SYSTEM";
                  loReferralUser = "";
                }
                else
                {
                  if (model.ReferralUser.Contains("-") && model.ReferralUser.Length == 36)
                  {
                    if (model.ReferralUser.Split('-').Length == 5)
                    {
                      loFsaCode = "SYSTEM";
                      loReferralUser = model.ReferralUser;
                      goto label_39;
                    }
                  }
                  if (model.ReferralUser.Length > 0)
                  {
                    if (model.ReferralUser.Split('-').Length < 5)
                    {
                      loFsaCode = model.ReferralUser.ToUpper() == "DEFAULT" ? "SYSTEM" : model.ReferralUser.ToUpper();
                      loReferralUser = "";
                    }
                  }
                }
label_39:
                COML.Classes.Student student = new COML.Classes.Student();
                student.Age = model.Age;
                student.FirstName = model.Firstname;
                student.LastName = model.Lastname;
                student.Nickname = model.Nickname;
                student.FsaCode = loFsaCode;
                student.HomeLanguage = model.HomeLanguage == null ? "" : model.HomeLanguage;
                student.IdentityNumber = model.IdentityNumber;
                student.EmailAddress = user.Email;
                student.SelectGender = model.SelectGender;
                student.HighestQualification = model.HighestQualification == null ? "" : model.HighestQualification;
                student.HighestQualificationTitle = model.HighestQualificationTitle == null ? "" : model.HighestQualificationTitle;
                student.PostalAddress1 = model.PostalAddress1 == null ? "" : model.PostalAddress1;
                student.PostalAddress2 = model.PostalAddress2 == null ? "" : model.PostalAddress2;
                student.PostalAddress3 = model.PostalAddress3 == null ? "" : model.PostalAddress3;
                student.PostalAddressCity = model.PostalAddressCity == null ? "" : model.PostalAddressCity;
                student.PostalAddressCode = model.PostalAddressCode == null ? "" : model.PostalAddressCode;
                student.PostalAddressProvince = model.PostalAddressProvince == null ? "" : model.PostalAddressProvince;
                student.ReferralCode = model.ReferralCode == null ? "" : model.ReferralCode;
                int referralStudent = model.ReferralStudent;
                student.ReferralStudent = model.ReferralStudent == 0 ? -1 : model.ReferralStudent;
                student.ReferralUser = loReferralUser;
                student.SelectedRace = model.SelectedRace;
                student.TelephoneCell = "27" + model.TelephoneCell.Substring(1, model.TelephoneCell.Length - 1);
                student.Employed = model.CurrentlyEmployed == Enumerations.enumTrueFalse.True;
                student.UserID = user.Id;
                student.IsEnabled = true;
                student.IsLocked = false;
                student.DateTimeCreated = DateTime.Now;
                student.LastActivity = DateTime.Now;
                student.IsChatEnabled = true;
                COML.Classes.Student paStudent = student;
                bool flag3 = BLL.Student.RegisterStudent(paStudent);
                COML.Logging.Log("User added to DB: " + flag3.ToString(), Enumerations.LogLevel.Info, (Exception) null);
                if (!flag3)
                {
                  this.ModelState.AddModelError("Registration", "Error creating your profile.");
                }
                else
                {
                  using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                  {
                    RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>((IRoleStore<IdentityRole, string>) new RoleStore<IdentityRole>((DbContext) applicationDbContext));
                    new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext)).AddToRole<ApplicationUser, string>(user.Id, "Student");
                    Mailer.SendRegistrationEmail(paStudent.EmailAddress, paStudent.FirstName + " " + paStudent.LastName, paStudent.IdentityNumber, paStudent.UserID, model.Password);
                    COML.Logging.Log("User added to role.", Enumerations.LogLevel.Info, (Exception) null);
                  }
                }
                if (result.Succeeded & flag3)
                {
                  COML.Logging.Log("Attempting user sign in.", Enumerations.LogLevel.Info, (Exception) null);
                  await this.SignInManager.SignInAsync(user, false, false);
                  COML.Logging.Log("Sign in successful. Redirecting.", Enumerations.LogLevel.Info, (Exception) null);
                  return (ActionResult) this.RedirectToAction("Index", "Dashboard");
                }
                this.AddErrors(result);
                user = (ApplicationUser) null;
                result = (IdentityResult) null;
              }
              else
                this.ModelState.AddModelError("IdentityNumber", loVerifyIDResult.ErrorMessage);
            }
            else
              this.ModelState.AddModelError("IdentityNumber", "Identity Number already exists. Please enter a different Identity Number.");
          }
          else
            this.ModelState.AddModelError("Email", "Email address already exists. Please enter a different email address.");
          loVerifyIDResult = (IDErrorMessage) null;
          loFsaCode = (string) null;
          loReferralUser = (string) null;
        }
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log("Error creating learner. Exception: " + (object) ex + " Error: " + ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) model);
    }

    [Authorize]
    public ActionResult RegisterStudent()
    {
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      List<string> fsaCodes = Admin.GetFSACodes();
      List<FSACode> fsaCodeList = new List<FSACode>();
      foreach (string str in fsaCodes)
        fsaCodeList.Add(new FSACode()
        {
          ID = str,
          Code = str
        });
      // ISSUE: reference to a compiler-generated field
      if (AccountController.\u003C\u003Eo__20.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountController.\u003C\u003Eo__20.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, SelectList, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "FsaCodes", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = AccountController.\u003C\u003Eo__20.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__20.\u003C\u003Ep__0, this.ViewBag, new SelectList((IEnumerable) fsaCodeList, "ID", "Code"));
      return (ActionResult) this.View();
    }

    [HttpPost]
    [Authorize]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> RegisterStudent(RegisterViewModel model)
    {
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      try
      {
        if (this.ModelState.IsValid)
        {
          string verifyIdUsername = Settings.VerifyIDUsername;
          string verifyIdapiKey = Settings.VerifyIDAPIKey;
          List<ApplicationUser> applicationUserList = new List<ApplicationUser>();
          bool verifyIdNumbers = Settings.VerifyIDNumbers;
          IDErrorMessage loVerifyIDResult = new IDErrorMessage();
          string loReferralUser = "";
          List<string> fsaCodes = Admin.GetFSACodes();
          List<FSACode> fsaCodeList = new List<FSACode>();
          foreach (string str in fsaCodes)
            fsaCodeList.Add(new FSACode()
            {
              ID = str,
              Code = str
            });
          // ISSUE: reference to a compiler-generated field
          if (AccountController.\u003C\u003Eo__21.\u003C\u003Ep__0 == null)
          {
            // ISSUE: reference to a compiler-generated field
            AccountController.\u003C\u003Eo__21.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, SelectList, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "FsaCodes", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
            {
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
            }));
          }
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          object obj = AccountController.\u003C\u003Eo__21.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__21.\u003C\u003Ep__0, this.ViewBag, new SelectList((IEnumerable) fsaCodeList, "ID", "Code"));
          string loUserPassword = model.Password;
          using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            applicationUserList = applicationDbContext.Users.Where<ApplicationUser>((Expression<Func<ApplicationUser, bool>>) (x => x.Email.ToLower() == model.Email.ToLower())).ToList<ApplicationUser>();
          bool flag1 = BLL.Student.IDNumberExists(model.IdentityNumber);
          if (applicationUserList == null || applicationUserList.Count == 0)
          {
            if (!flag1)
            {
              bool flag2;
              if (verifyIdNumbers && verifyIdapiKey != "" && verifyIdUsername != "")
              {
                loVerifyIDResult = this.VerifyID(model.IdentityNumber, model.Firstname, model.Lastname);
                flag2 = loVerifyIDResult.Valid;
              }
              else
                flag2 = true;
              if (flag2)
              {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser.UserName = model.Email.ToLower();
                applicationUser.Email = model.Email.ToLower();
                ApplicationUser user = applicationUser;
                IdentityResult async = await this.UserManager.CreateAsync(user, model.Password);
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                  RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>((IRoleStore<IdentityRole, string>) new RoleStore<IdentityRole>((DbContext) applicationDbContext));
                  new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext)).AddToRole<ApplicationUser, string>(user.Id, "Student");
                  applicationDbContext.SaveChanges();
                }
                string str;
                if (model.FsaCode == null || model.FsaCode == "" || model.FsaCode == "DEFAULT")
                {
                  str = "SYSTEM";
                  loReferralUser = "";
                }
                else
                  str = model.FsaCode;
                if (model.ReferralUser == null || model.ReferralUser == "")
                {
                  loReferralUser = "";
                }
                else
                {
                  if (model.ReferralUser.Contains("-") && model.ReferralUser.Length == 36)
                  {
                    if (model.ReferralUser.Split('-').Length == 5)
                    {
                      loReferralUser = model.ReferralUser;
                      goto label_37;
                    }
                  }
                  if (model.ReferralUser.Length > 0)
                  {
                    if (model.ReferralUser.Split('-').Length < 5)
                      loReferralUser = "";
                  }
                }
label_37:
                COML.Classes.Student student = new COML.Classes.Student();
                student.Age = model.Age;
                student.FirstName = model.Firstname;
                student.LastName = model.Lastname;
                student.Nickname = model.Nickname;
                student.FsaCode = str;
                student.HomeLanguage = model.HomeLanguage == null ? "" : model.HomeLanguage;
                student.IdentityNumber = model.IdentityNumber;
                student.EmailAddress = user.Email;
                student.SelectGender = model.SelectGender;
                student.HighestQualification = model.HighestQualification == null ? "" : model.HighestQualification;
                student.HighestQualificationTitle = model.HighestQualificationTitle == null ? "" : model.HighestQualificationTitle;
                student.PostalAddress1 = model.PostalAddress1 == null ? "" : model.PostalAddress1;
                student.PostalAddress2 = model.PostalAddress2 == null ? "" : model.PostalAddress2;
                student.PostalAddress3 = model.PostalAddress3 == null ? "" : model.PostalAddress3;
                student.PostalAddressCity = model.PostalAddressCity == null ? "" : model.PostalAddressCity;
                student.PostalAddressCode = model.PostalAddressCode == null ? "" : model.PostalAddressCode;
                student.PostalAddressProvince = model.PostalAddressProvince == null ? "" : model.PostalAddressProvince;
                student.ReferralCode = model.ReferralCode == null ? "" : model.ReferralCode;
                int referralStudent = model.ReferralStudent;
                student.ReferralStudent = model.ReferralStudent == 0 ? -1 : model.ReferralStudent;
                student.ReferralUser = loReferralUser;
                student.SelectedRace = model.SelectedRace;
                student.TelephoneCell = "27" + model.TelephoneCell.Substring(1, model.TelephoneCell.Length - 1);
                student.Employed = model.CurrentlyEmployed == Enumerations.enumTrueFalse.True;
                student.UserID = user.Id;
                student.IsEnabled = true;
                student.IsLocked = false;
                student.DateTimeCreated = DateTime.Now;
                student.LastActivity = DateTime.Now;
                student.IsChatEnabled = true;
                COML.Classes.Student paStudent = student;
                bool flag3 = BLL.Student.RegisterStudent(paStudent);
                COML.Logging.Log("User added to DB: " + flag3.ToString(), Enumerations.LogLevel.Info, (Exception) null);
                if (!flag3)
                {
                  this.ModelState.AddModelError("Registration", "Error creating your profile.");
                }
                else
                {
                  using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                  {
                    RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>((IRoleStore<IdentityRole, string>) new RoleStore<IdentityRole>((DbContext) applicationDbContext));
                    new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext)).AddToRole<ApplicationUser, string>(user.Id, "Student");
                    Mailer.SendRegistrationEmail(paStudent.EmailAddress, paStudent.FirstName + " " + paStudent.LastName, paStudent.IdentityNumber, paStudent.UserID, loUserPassword);
                    COML.Logging.Log("User added to role.", Enumerations.LogLevel.Info, (Exception) null);
                  }
                }
                if (async.Succeeded & flag3)
                {
                  try
                  {
                    return (ActionResult) this.RedirectToAction("Index", "AdminStudent");
                  }
                  catch (Exception ex)
                  {
                    COML.Logging.Log("Error saving learner. " + ex.Message, Enumerations.LogLevel.Info, (Exception) null);
                  }
                }
                this.AddErrors(async);
                user = (ApplicationUser) null;
              }
              else
                this.ModelState.AddModelError("IdentityNumber", loVerifyIDResult.ErrorMessage);
            }
            else
              this.ModelState.AddModelError("IdentityNumber", "Identity Number already exists. Please enter a different Identity Number.");
          }
          else
            this.ModelState.AddModelError("Email", "Email address already exists. Please enter a different email address.");
          loVerifyIDResult = (IDErrorMessage) null;
          loReferralUser = (string) null;
          loUserPassword = (string) null;
        }
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log("Error creating learner. Exception: " + (object) ex + " Error: " + ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) model);
    }

    [AllowAnonymous]
    public ActionResult DeregisterMentorAssessorFiles(int paId)
    {
      try
      {
        BLL.Instructor.DeregisterInstructor(paId);
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.RedirectToAction("Index", "Home");
    }

    [AllowAnonymous]
    public async Task<ActionResult> ConfirmEmail(string userId, string code)
    {
      if (userId == null || code == null)
        return (ActionResult) this.View("Error");
      return (ActionResult) this.View((await this.UserManager.ConfirmEmailAsync(userId, code)).Succeeded ? nameof (ConfirmEmail) : "Error");
    }

    [AllowAnonymous]
    public ActionResult ForgotPassword()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View((object) model);
      ApplicationUser byNameAsync = await this.UserManager.FindByNameAsync(model.Email);
      if (byNameAsync == null)
        return (ActionResult) this.View(nameof (ForgotPassword));
      try
      {
        Guid guid = Guid.NewGuid();
        string paMessage = "<p>Reset password link: " + ("<a href='" + this.Url.Action("ResetPassword", "Account", (object) new
        {
          email = model.Email,
          code = guid
        }, "http") + "'>Reset Password</a>") + "</p>";
        BLL.Logging.SaveResetPasswordCode(byNameAsync.Id, guid.ToString());
        Mailer.SendResetPasswordLink(model.Email, byNameAsync.Id, paMessage);
        return (ActionResult) this.View("ForgotPasswordConfirmation");
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View();
    }

    [AllowAnonymous]
    public ActionResult ForgotPasswordConfirmation()
    {
      return (ActionResult) this.View();
    }

    [AllowAnonymous]
    public ActionResult ResetPassword(string email, string code)
    {
      if (code != null && email != null)
        return (ActionResult) this.View();
      return (ActionResult) this.View("Error");
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ResetPassword(
      ResetPasswordViewModel model,
      string email,
      string code)
    {
      if (this.ModelState.IsValid)
      {
        try
        {
          using (ApplicationDbContext context = new ApplicationDbContext())
          {
            UserStore<ApplicationUser> loStore = new UserStore<ApplicationUser>((DbContext) context);
            Microsoft.AspNet.Identity.UserManager<ApplicationUser> loUserManager = new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) loStore);
            ApplicationUser loUser = await loUserManager.FindByNameAsync(email);
            if (loUser != null & BLL.Logging.GetResetPasswordCode(model.Email, email, code))
            {
              await loStore.SetPasswordHashAsync(loUser, loUserManager.PasswordHasher.HashPassword(model.Password));
              await loStore.UpdateAsync(loUser);
              COML.Classes.Student student = BLL.Student.GetStudent(loUser.Id);
              if (student != null)
                Smser.SendResetPasswordSms(student.Nickname, student.TelephoneCell, model.Password, student.UserID);
              return (ActionResult) this.RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            this.ModelState.AddModelError("", "An error occurred. Please contact <a href=\"admin@award.co.za\">admin@award.co.za</a> ");
            loStore = (UserStore<ApplicationUser>) null;
            loUserManager = (Microsoft.AspNet.Identity.UserManager<ApplicationUser>) null;
            loUser = (ApplicationUser) null;
          }
          return (ActionResult) this.RedirectToAction("Login");
        }
        catch (Exception ex)
        {
          COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        }
      }
      return (ActionResult) this.View();
    }

    [AllowAnonymous]
    public ActionResult ResetPasswordConfirmation()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public ActionResult ExternalLogin(string provider, string returnUrl)
    {
      return (ActionResult) new AccountController.ChallengeResult(provider, this.Url.Action("ExternalLoginCallback", "Account", (object) new
      {
        ReturnUrl = returnUrl
      }));
    }

    [AllowAnonymous]
    public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
    {
      string verifiedUserIdAsync = await this.SignInManager.GetVerifiedUserIdAsync();
      if (verifiedUserIdAsync == null)
        return (ActionResult) this.View("Error");
      List<SelectListItem> list = (await this.UserManager.GetValidTwoFactorProvidersAsync(verifiedUserIdAsync)).Select<string, SelectListItem>((Func<string, SelectListItem>) (purpose => new SelectListItem()
      {
        Text = purpose,
        Value = purpose
      })).ToList<SelectListItem>();
      return (ActionResult) this.View((object) new SendCodeViewModel()
      {
        Providers = (ICollection<SelectListItem>) list,
        ReturnUrl = returnUrl,
        RememberMe = rememberMe
      });
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> SendCode(SendCodeViewModel model)
    {
      if (!this.ModelState.IsValid)
        return (ActionResult) this.View();
      return await this.SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider) ? (ActionResult) this.RedirectToAction("VerifyCode", (object) new
      {
        Provider = model.SelectedProvider,
        ReturnUrl = model.ReturnUrl,
        RememberMe = model.RememberMe
      }) : (ActionResult) this.View("Error");
    }

    [AllowAnonymous]
    public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
    {
      ExternalLoginInfo loginInfo = await this.AuthenticationManager.GetExternalLoginInfoAsync();
      if (loginInfo == null)
        return (ActionResult) this.RedirectToAction("Login");
      switch (await this.SignInManager.ExternalSignInAsync(loginInfo, false))
      {
        case SignInStatus.Success:
          return this.RedirectToLocal(returnUrl);
        case SignInStatus.LockedOut:
          return (ActionResult) this.View("Lockout");
        case SignInStatus.RequiresVerification:
          return (ActionResult) this.RedirectToAction("SendCode", (object) new
          {
            ReturnUrl = returnUrl,
            RememberMe = false
          });
        default:
          // ISSUE: reference to a compiler-generated field
          if (AccountController.\u003C\u003Eo__33.\u003C\u003Ep__0 == null)
          {
            // ISSUE: reference to a compiler-generated field
            AccountController.\u003C\u003Eo__33.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "ReturnUrl", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
            {
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
            }));
          }
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          object obj1 = AccountController.\u003C\u003Eo__33.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__33.\u003C\u003Ep__0, this.ViewBag, returnUrl);
          // ISSUE: reference to a compiler-generated field
          if (AccountController.\u003C\u003Eo__33.\u003C\u003Ep__1 == null)
          {
            // ISSUE: reference to a compiler-generated field
            AccountController.\u003C\u003Eo__33.\u003C\u003Ep__1 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "LoginProvider", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
            {
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
              CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
            }));
          }
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          object obj2 = AccountController.\u003C\u003Eo__33.\u003C\u003Ep__1.Target((CallSite) AccountController.\u003C\u003Eo__33.\u003C\u003Ep__1, this.ViewBag, loginInfo.Login.LoginProvider);
          return (ActionResult) this.View("ExternalLoginConfirmation", (object) new ExternalLoginConfirmationViewModel()
          {
            Email = loginInfo.Email
          });
      }
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ExternalLoginConfirmation(
      ExternalLoginConfirmationViewModel model,
      string returnUrl)
    {
      if (this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Manage");
      if (this.ModelState.IsValid)
      {
        ExternalLoginInfo info = await this.AuthenticationManager.GetExternalLoginInfoAsync();
        if (info == null)
          return (ActionResult) this.View("ExternalLoginFailure");
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.UserName = model.Email;
        applicationUser.Email = model.Email;
        ApplicationUser user = applicationUser;
        IdentityResult result = await this.UserManager.CreateAsync(user);
        if (result.Succeeded)
        {
          result = await this.UserManager.AddLoginAsync(user.Id, info.Login);
          if (result.Succeeded)
          {
            await this.SignInManager.SignInAsync(user, false, false);
            return this.RedirectToLocal(returnUrl);
          }
        }
        this.AddErrors(result);
        info = (ExternalLoginInfo) null;
        user = (ApplicationUser) null;
        result = (IdentityResult) null;
      }
      // ISSUE: reference to a compiler-generated field
      if (AccountController.\u003C\u003Eo__34.\u003C\u003Ep__0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AccountController.\u003C\u003Eo__34.\u003C\u003Ep__0 = CallSite<Func<CallSite, object, string, object>>.Create(Binder.SetMember(CSharpBinderFlags.None, "ReturnUrl", typeof (AccountController), (IEnumerable<CSharpArgumentInfo>) new CSharpArgumentInfo[2]
        {
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, (string) null),
          CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, (string) null)
        }));
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      object obj = AccountController.\u003C\u003Eo__34.\u003C\u003Ep__0.Target((CallSite) AccountController.\u003C\u003Eo__34.\u003C\u003Ep__0, this.ViewBag, returnUrl);
      return (ActionResult) this.View((object) model);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult LogOff()
    {
      this.Session.Remove("UserNickname");
      this.AuthenticationManager.SignOut("ApplicationCookie");
      return (ActionResult) this.RedirectToAction("Index", "Home");
    }

    public ActionResult LogOffWithoutPost()
    {
      this.AuthenticationManager.SignOut("ApplicationCookie");
      return (ActionResult) this.RedirectToAction("Index", "Home");
    }

    public bool LogOffWithoutRedirectAndPost()
    {
      try
      {
        this.AuthenticationManager.SignOut("ApplicationCookie");
        return true;
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    [AllowAnonymous]
    public ActionResult ExternalLoginFailure()
    {
      return (ActionResult) this.View();
    }

    public bool DoesUserHaveRole(string paUserID, string paRole)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      AccountController.\u003C\u003Ec__DisplayClass39_0 cDisplayClass390 = new AccountController.\u003C\u003Ec__DisplayClass39_0();
      // ISSUE: reference to a compiler-generated field
      cDisplayClass390.paRole = paRole;
      // ISSUE: reference to a compiler-generated field
      cDisplayClass390.paUserID = paUserID;
      try
      {
        using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          AccountController.\u003C\u003Ec__DisplayClass39_1 cDisplayClass391 = new AccountController.\u003C\u003Ec__DisplayClass39_1();
          // ISSUE: reference to a compiler-generated field
          IdentityRole identityRole = applicationDbContext.Roles.Where<IdentityRole>((Expression<Func<IdentityRole, bool>>) (r => r.Name.Contains(cDisplayClass390.paRole))).FirstOrDefault<IdentityRole>();
          // ISSUE: reference to a compiler-generated field
          cDisplayClass391.loRole = identityRole;
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          return applicationDbContext.Users.Where<ApplicationUser>((Expression<Func<ApplicationUser, bool>>) (x => x.Roles.Select<IdentityUserRole, string>((Func<IdentityUserRole, string>) (y => y.RoleId)).Contains<string>(cDisplayClass391.loRole.Id))).ToList<ApplicationUser>().Find(new Predicate<ApplicationUser>(cDisplayClass390.\u003CDoesUserHaveRole\u003Eb__0)) != null;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    public List<string> GetUserRoles(string paUserID)
    {
      List<string> stringList = new List<string>();
      try
      {
        using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        {
          foreach (string role in (IEnumerable<string>) new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext)).GetRoles<ApplicationUser, string>(paUserID))
            stringList.Add(role);
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return stringList;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (this._userManager != null)
        {
          this._userManager.Dispose();
          this._userManager = (ApplicationUserManager) null;
        }
        if (this._signInManager != null)
        {
          this._signInManager.Dispose();
          this._signInManager = (ApplicationSignInManager) null;
        }
      }
      base.Dispose(disposing);
    }

    private IAuthenticationManager AuthenticationManager
    {
      get
      {
        return this.HttpContext.GetOwinContext().Authentication;
      }
    }

    private void AddErrors(IdentityResult result)
    {
      foreach (string error in result.Errors)
        this.ModelState.AddModelError("", error);
    }

    private ActionResult RedirectToLocal(string returnUrl)
    {
      if (this.Url.IsLocalUrl(returnUrl))
        return (ActionResult) this.Redirect(returnUrl);
      return (ActionResult) this.RedirectToAction("Index", "Home");
    }

    public IDErrorMessage VerifyID(
      string paID,
      string paFirstName,
      string paLastName)
    {
      IDErrorMessage idErrorMessage = new IDErrorMessage();
      if (Settings.VerifyIDNumbers)
      {
        try
        {
          string verifyIdUsername = Settings.VerifyIDUsername;
          string verifyIdapiKey = Settings.VerifyIDAPIKey;
          VerifyidPortTypeClient verifyidPortTypeClient = new VerifyidPortTypeClient();
          string xml1 = verifyidPortTypeClient.doLogin(verifyIdUsername, verifyIdapiKey);
          XmlDocument xmlDocument1 = new XmlDocument();
          paFirstName = paFirstName.ToLower();
          paLastName = paLastName.ToLower();
          xmlDocument1.LoadXml(xml1);
          XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDocument1.NameTable);
          nsmgr.AddNamespace("schemaLocation", "loc");
          nsmgr.AddNamespace("payload", "loc2");
          nsmgr.AddNamespace("a", "http://www.w3.org/2001/XMLSchema-instance");
          nsmgr.AddNamespace("x", xmlDocument1.DocumentElement.NamespaceURI);
          string innerText1 = xmlDocument1.SelectNodes("//x:SESSIONID", nsmgr)[0].InnerText;
          string innerText2 = xmlDocument1.SelectNodes("//x:LOGIN-MESSAGE", nsmgr)[0].InnerText;
          if (innerText1 == "" || innerText2 != "S00")
          {
            idErrorMessage.Valid = false;
            idErrorMessage.ErrorMessage = "Error validating ID Number";
          }
          else
          {
            string xml2 = verifyidPortTypeClient.verify(innerText1, verifyIdapiKey, paID);
            XmlDocument xmlDocument2 = new XmlDocument();
            bool flag1 = false;
            bool flag2 = false;
            xmlDocument2.LoadXml(xml2);
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(xmlDocument2.NameTable);
            namespaceManager.AddNamespace("schemaLocation", "loc");
            namespaceManager.AddNamespace("payload", "loc2");
            namespaceManager.AddNamespace("a", "http://www.w3.org/2001/XMLSchema-instance");
            namespaceManager.AddNamespace("x", xmlDocument2.DocumentElement.NamespaceURI);
            string innerText3 = xmlDocument2.SelectNodes("//x:SEARCH-MESSAGE", nsmgr)[0].InnerText;
            string innerText4 = xmlDocument2.SelectNodes("//x:SEARCH-RESULT", nsmgr)[0].InnerText;
            if (innerText3 == "VALID" && innerText4 == "FOUND")
            {
              string lower1 = xmlDocument2.SelectNodes("//x:FIRSTNAMES", nsmgr)[0].InnerText.ToLower();
              string lower2 = xmlDocument2.SelectNodes("//x:SECONDNAMES", nsmgr)[0].InnerText.ToLower();
              string lower3 = xmlDocument2.SelectNodes("//x:SURNAME", nsmgr)[0].InnerText.ToLower();
              if (paFirstName.Contains(lower1) || paFirstName == lower1)
                flag1 = true;
              else if (lower2.Length > 0 && paFirstName.Contains(lower1))
                flag1 = true;
              if (paLastName.Contains(lower3) || paLastName == lower3)
                flag2 = true;
              if (flag1 & flag2)
              {
                idErrorMessage.Valid = true;
                idErrorMessage.ErrorMessage = "";
              }
              else if (!flag1 && !flag2)
              {
                idErrorMessage.Valid = false;
                idErrorMessage.ErrorMessage = "You First Name and Last Name do not match the ID Number entered.";
              }
              else if (!flag1)
              {
                idErrorMessage.Valid = false;
                idErrorMessage.ErrorMessage = "You First Name does not match the ID Number entered.";
              }
              else if (!flag2)
              {
                idErrorMessage.Valid = false;
                idErrorMessage.ErrorMessage = "You Last Name does not match the ID Number entered.";
              }
            }
            else if (innerText3 != "VALID")
            {
              idErrorMessage.Valid = false;
              idErrorMessage.ErrorMessage = "Your ID Number is invalid.";
            }
            else if (innerText4 != "FOUND")
            {
              idErrorMessage.Valid = false;
              idErrorMessage.ErrorMessage = "Your ID Number was not found.";
            }
            if (!idErrorMessage.Valid)
            {
              COML.Logging.Log("ID Verification Error: ", Enumerations.LogLevel.Error, (Exception) null);
              COML.Logging.Log("Found: " + innerText4, Enumerations.LogLevel.Error, (Exception) null);
              COML.Logging.Log("Valid: " + innerText3, Enumerations.LogLevel.Error, (Exception) null);
              COML.Logging.Log("Entered First Name: " + paFirstName, Enumerations.LogLevel.Error, (Exception) null);
              COML.Logging.Log("Entered Last Name: " + paLastName, Enumerations.LogLevel.Error, (Exception) null);
              COML.Logging.Log("Entered ID Number: " + paID, Enumerations.LogLevel.Error, (Exception) null);
            }
          }
        }
        catch (Exception ex)
        {
          idErrorMessage.Valid = true;
          idErrorMessage.ErrorMessage = "";
          COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        }
      }
      else
      {
        idErrorMessage.Valid = true;
        idErrorMessage.ErrorMessage = "";
      }
      return idErrorMessage;
    }

    [AllowAnonymous]
    public ActionResult ClearStudents()
    {
      try
      {
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.RedirectToAction("Login", "Account");
    }

    public string RegisterSponsor(string paEmail, string paPassword)
    {
      try
      {
        using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        {
          RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>((IRoleStore<IdentityRole, string>) new RoleStore<IdentityRole>((DbContext) applicationDbContext));
          Microsoft.AspNet.Identity.UserManager<ApplicationUser> manager = new Microsoft.AspNet.Identity.UserManager<ApplicationUser>((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) applicationDbContext));
          ApplicationUser applicationUser = new ApplicationUser();
          applicationUser.UserName = paEmail.ToLower();
          applicationUser.Email = paEmail.ToLower();
          ApplicationUser user = applicationUser;
          if (!manager.CreateAsync(user, paPassword).Result.Succeeded)
            return "";
          manager.AddToRole<ApplicationUser, string>(user.Id, "Sponsor");
          applicationDbContext.SaveChanges();
          return user.Id;
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return "";
      }
    }

    public bool UserExists(string paEmail)
    {
      List<ApplicationUser> applicationUserList = new List<ApplicationUser>();
      try
      {
        using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
          applicationUserList = applicationDbContext.Users.Where<ApplicationUser>((Expression<Func<ApplicationUser, bool>>) (x => x.Email.ToLower() == paEmail.ToLower())).ToList<ApplicationUser>();
        return applicationUserList.Count > 0;
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return false;
      }
    }

    internal class ChallengeResult : HttpUnauthorizedResult
    {
      public ChallengeResult(string provider, string redirectUri)
        : this(provider, redirectUri, (string) null)
      {
      }

      public ChallengeResult(string provider, string redirectUri, string userId)
      {
        this.LoginProvider = provider;
        this.RedirectUri = redirectUri;
        this.UserId = userId;
      }

      public string LoginProvider { get; set; }

      public string RedirectUri { get; set; }

      public string UserId { get; set; }

      public override void ExecuteResult(ControllerContext context)
      {
        AuthenticationProperties properties = new AuthenticationProperties()
        {
          RedirectUri = this.RedirectUri
        };
        if (this.UserId != null)
          properties.Dictionary["XsrfId"] = this.UserId;
        context.HttpContext.GetOwinContext().Authentication.Challenge(properties, this.LoginProvider);
      }
    }
  }
}
