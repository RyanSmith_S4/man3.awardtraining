﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.IO;
using System.Web.Mvc;

namespace Test.Controllers
{
  public class AdminController : Controller
  {
    public FileResult InstructorFileDownload(int paInstructorId, int paFileId)
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          try
          {
            UserFile instructorFile = BLL.Instructor.GetInstructorFile(paInstructorId, paFileId);
            if (instructorFile != null)
            {
              FileContentResult fileContentResult = new FileContentResult(File.ReadAllBytes(this.HttpContext.Server.MapPath("~\\" + instructorFile.Filepath)), "application/octet-stream");
              fileContentResult.FileDownloadName = "Download_" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "_").Replace(":", "") + instructorFile.Filepath.Substring(instructorFile.Filepath.Length - 4, 4);
              return (FileResult) fileContentResult;
            }
          }
          catch (Exception ex)
          {
            COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
          }
        }
      }
      return (FileResult) null;
    }
  }
}
