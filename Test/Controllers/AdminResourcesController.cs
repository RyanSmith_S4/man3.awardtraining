﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminResourcesController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using Test.Models.Admin;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  public class AdminResourcesController : Controller
  {
    public ActionResult Index(int paId = 1)
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          List<COML.Classes.Resource> resources = BLL.Core.Resource.GetResources();
          List<AdminResourceViewModel> allItems = new List<AdminResourceViewModel>();
          foreach (COML.Classes.Resource resource in resources)
          {
            AdminResourceViewModel resourceViewModel = new AdminResourceViewModel()
            {
              ResourceID = resource.ID,
              ContactPerson = resource.ContactPerson,
              Description = resource.Description,
              Email = resource.ContactPersonEmail,
              DateCreated = resource.DateTimeCreated
            };
            allItems.Add(resourceViewModel);
          }
          return (ActionResult) this.View((object) allItems.ToPagedList<AdminResourceViewModel>(paId, 10));
        }
        Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }

    public ActionResult Search(int paId = 1, string paWord = null)
    {
      List<COML.Classes.Resource> resourceList1 = new List<COML.Classes.Resource>();
      List<COML.Classes.Resource> resourceList2 = string.IsNullOrWhiteSpace(paWord) ? BLL.Core.Resource.GetResources() : BLL.Core.Resource.GetResourcesBySearch(paWord);
      List<AdminResourceViewModel> allItems = new List<AdminResourceViewModel>();
      foreach (COML.Classes.Resource resource in resourceList2)
      {
        AdminResourceViewModel resourceViewModel = new AdminResourceViewModel()
        {
          ResourceID = resource.ID,
          ContactPerson = resource.ContactPerson,
          Description = resource.Description,
          Email = resource.ContactPersonEmail,
          DateCreated = resource.DateTimeCreated
        };
        allItems.Add(resourceViewModel);
      }
      return (ActionResult) this.View("Index", (object) allItems.ToPagedList<AdminResourceViewModel>(paId, 10));
    }

    public ActionResult AddResource()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> AddResource(AddResourceViewModel loModel)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (this.User.Identity.IsAuthenticated)
        {
          if (new AccountController().DoesUserHaveRole(userId, "Admin"))
          {
            if (this.ModelState.IsValid)
            {
              if (loModel.Logo != null)
              {
                COML.Classes.Resource paResource = new COML.Classes.Resource()
                {
                  ContactPerson = loModel.ContactPerson,
                  ContactPersonCell = loModel.ContactCell,
                  ContactPersonEmail = loModel.ContactEmail,
                  DateTimeCreated = DateTime.Now,
                  Description = loModel.Description,
                  Link = loModel.Link
                };
                Guid guid = Guid.NewGuid();
                string contentType = loModel.Logo.ContentType;
                string str1 = "";
                if (!(contentType == "image/jpeg"))
                {
                  if (!(contentType == "image/png"))
                  {
                    if (contentType == "image/bmp")
                      str1 = "bmp";
                  }
                  else
                    str1 = "png";
                }
                else
                  str1 = "jpeg";
                string str2 = guid.ToString() + "." + str1;
                string filename = Path.Combine(this.Server.MapPath("~"), "Files", "Resources", str2);
                string str3 = Path.Combine("Files", "Resources", str2);
                loModel.Logo.SaveAs(filename);
                paResource.Logo = new COML.Classes.UserFile()
                {
                  DateCreated = DateTime.Now,
                  Filepath = str3,
                  Name = str2,
                  SystemFile = Enumerations.enumSystemFile.ResourceFile,
                  TypeFile = Enumerations.enumTypeFile.Pic
                };
                if (BLL.Core.Resource.SaveResource(paResource, userId))
                  return (ActionResult) this.View("AddResourceConfirmation");
                this.ModelState.AddModelError("", "Error processing your request.");
              }
              else
                this.ModelState.AddModelError("Logo", "Please select a logo.");
            }
          }
          else
          {
            Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction("Index", "Home");
          }
        }
        else
        {
          Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
      }
      catch (Exception ex)
      {
        this.ModelState.AddModelError("", "Error processing your request.");
        Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
      }
      return (ActionResult) this.View((object) loModel);
    }

    public ActionResult ResourceDetails(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Resource resource = BLL.Core.Resource.GetResource(paId);
      ResourceDetailsViewModel detailsViewModel = new ResourceDetailsViewModel();
      if (resource == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminResources");
      detailsViewModel.ContactPerson = resource.ContactPerson;
      detailsViewModel.ContactPersonCell = resource.ContactPersonCell;
      detailsViewModel.ContactPersonEmail = resource.ContactPersonEmail;
      detailsViewModel.DateCreated = resource.DateTimeCreated;
      detailsViewModel.Description = resource.Description;
      detailsViewModel.Link = resource.Link;
      detailsViewModel.ResourceID = resource.ID;
      detailsViewModel.Logo = "~/" + resource.Logo.Filepath.Replace("\\", "/");
      return (ActionResult) this.View((object) detailsViewModel);
    }

    public ActionResult RemoveResource(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (BLL.Core.Resource.RemoveResource(paId))
        return (ActionResult) this.View("RemoveResourceConfirmation");
      return (ActionResult) this.RedirectToAction("Index", "AdminResources");
    }
  }
}
