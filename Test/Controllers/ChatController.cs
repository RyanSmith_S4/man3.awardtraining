﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.ChatController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Test.Models.Chat;

namespace Test.Controllers
{
  public class ChatController : Controller
  {
    public ActionResult Index()
    {
      return (ActionResult) this.View("GeneralChat");
    }

    public ActionResult GeneralChat()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      string userId = this.User.Identity.GetUserId();
      LearnerChatViewModel learnerChatViewModel = new LearnerChatViewModel();
      List<string> userRoles = new AccountController().GetUserRoles(userId);
      if (userRoles.Count > 0)
      {
        if (userRoles.Contains("Admin"))
        {
          Enumerations.enumUserType enumUserType = Enumerations.enumUserType.Admin;
          learnerChatViewModel.UserID = userId;
          learnerChatViewModel.UserType = enumUserType;
          learnerChatViewModel.ChatName = "Administrator";
          return (ActionResult) this.View((object) learnerChatViewModel);
        }
        if (userRoles.Contains("Student"))
        {
          COML.Classes.Student student = BLL.Student.GetStudent(userId);
          Enumerations.enumUserType enumUserType = Enumerations.enumUserType.Learner;
          if (student != null)
          {
            if (!student.IsChatEnabled || student.IsLocked)
              return (ActionResult) this.RedirectToAction("Index", "Dashboard");
            learnerChatViewModel.ID = student.ID;
            learnerChatViewModel.UserID = userId;
            learnerChatViewModel.UserType = enumUserType;
            learnerChatViewModel.ChatName = student.FirstName + " " + student.LastName + " (Learner)";
            return (ActionResult) this.View((object) learnerChatViewModel);
          }
          new AccountController().LogOffWithoutRedirectAndPost();
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
        if (userRoles.Contains("Mentor") && userRoles.Contains("Assessor"))
        {
          Enumerations.enumUserType enumUserType = Enumerations.enumUserType.AssessorAndMentor;
          COML.Classes.Instructor instructorByUserId = BLL.Instructor.GetInstructorByUserID(userId);
          if (instructorByUserId != null)
          {
            if (!instructorByUserId.IsChatEnabled || instructorByUserId.IsLocked)
              return (ActionResult) this.RedirectToAction("Index", "InstructorBoth");
            learnerChatViewModel.ID = instructorByUserId.ID;
            learnerChatViewModel.UserID = userId;
            learnerChatViewModel.UserType = enumUserType;
            learnerChatViewModel.ChatName = instructorByUserId.FirstName + " " + instructorByUserId.LastName + " (Mentor)";
            return (ActionResult) this.View((object) learnerChatViewModel);
          }
          new AccountController().LogOffWithoutRedirectAndPost();
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
        if (userRoles.Contains("Mentor"))
        {
          Enumerations.enumUserType enumUserType = Enumerations.enumUserType.Mentor;
          COML.Classes.Instructor instructorByUserId = BLL.Instructor.GetInstructorByUserID(userId);
          if (instructorByUserId != null)
          {
            if (!instructorByUserId.IsChatEnabled || instructorByUserId.IsLocked)
              return (ActionResult) this.RedirectToAction("Index", "InstructorMentor");
            learnerChatViewModel.ID = instructorByUserId.ID;
            learnerChatViewModel.UserID = userId;
            learnerChatViewModel.UserType = enumUserType;
            learnerChatViewModel.ChatName = instructorByUserId.FirstName + " " + instructorByUserId.LastName + " (Mentor)";
            return (ActionResult) this.View((object) learnerChatViewModel);
          }
          new AccountController().LogOffWithoutRedirectAndPost();
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
        if (userRoles.Contains("Assessor"))
        {
          Enumerations.enumUserType enumUserType = Enumerations.enumUserType.Assessor;
          COML.Classes.Instructor instructorByUserId = BLL.Instructor.GetInstructorByUserID(userId);
          if (instructorByUserId != null)
          {
            if (!instructorByUserId.IsChatEnabled || instructorByUserId.IsLocked)
              return (ActionResult) this.RedirectToAction("Index", "InstructorAssessor");
            learnerChatViewModel.ID = instructorByUserId.ID;
            learnerChatViewModel.UserID = userId;
            learnerChatViewModel.UserType = enumUserType;
            learnerChatViewModel.ChatName = instructorByUserId.FirstName + " " + instructorByUserId.LastName + " (Assessor)";
            return (ActionResult) this.View((object) learnerChatViewModel);
          }
          new AccountController().LogOffWithoutRedirectAndPost();
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
        new AccountController().LogOffWithoutRedirectAndPost();
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
      new AccountController().LogOffWithoutRedirectAndPost();
      return (ActionResult) this.RedirectToAction("Index", "Home");
    }

    [HttpPost]
    public ActionResult SaveGeneralMessage(MessageViewModel paMessage)
    {
      string userId = this.User.Identity.GetUserId();
      if (!(paMessage.UserID == userId))
        return (ActionResult) this.Json((object) new
        {
          success = false
        }, JsonRequestBehavior.AllowGet);
      List<string> userRoles = new AccountController().GetUserRoles(userId);
      return (ActionResult) this.Json((object) new
      {
        success = BLL.Logging.SaveGeneralChatLog(userId, paMessage.Message, userRoles)
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult GetGeneralMessages(UserViewModel paUser)
    {
      string userId = this.User.Identity.GetUserId();
      if (!(paUser.UserID == userId))
        return (ActionResult) this.Json((object) new
        {
          success = false
        }, JsonRequestBehavior.AllowGet);
      DateTime now = DateTime.Now;
      return (ActionResult) this.Json((object) new
      {
        success = true,
        list = BLL.Logging.GetGeneralChatLogs(new DateTime(now.Year, now.Month, now.Day, 0, 1, 0))
      }, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult GetChatStates()
    {
      string userId = this.User.Identity.GetUserId();
      return (ActionResult) this.Json((object) new
      {
        GeneralChat = BLL.Logging.IsGeneralChatActive(userId),
        GraduateChat = BLL.Logging.IsGraduateChatActive(userId)
      }, JsonRequestBehavior.AllowGet);
    }
  }
}
