﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminStatsController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using System;
using System.Web.Mvc;
using Test.Models.Admin;

namespace Test.Controllers
{
  public class AdminStatsController : Controller
  {
    public ActionResult Index()
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          Stats stats = BLL.Core.Admin.GetStats();
          return (ActionResult) this.View((object) new AdminStatsViewModel()
          {
            LearnerActivityCount = stats.LearnersLately,
            LearnerCount = stats.TotalLearners,
            LearnerActiveCount = stats.TotalLearnersActive,
            LearnerInactiveCount = stats.TotalLearnersInactive,
            LearnerLockedCount = stats.TotalLearnersLocked
          });
        }
        Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }
  }
}
