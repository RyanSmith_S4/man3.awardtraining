﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.ReportController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Web.Mvc;
using Test.Models.Report;

namespace Test.Controllers
{
  [Authorize]
  public class ReportController : Controller
  {
    public ActionResult Generate()
    {
      return (ActionResult) this.View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Generate(ReportViewModel paModel)
    {
      try
      {
        DateTime paDateTimeStart = new DateTime();
        DateTime paDateTimeStop = new DateTime();
        if (paModel.ReportType == "" || paModel.ReportType == null)
        {
          this.ModelState.AddModelError("", "Please select a valid report type.");
          return (ActionResult) this.View((object) paModel);
        }
        string reportType = paModel.ReportType;
        if (!(reportType == "All Learners"))
        {
          if (!(reportType == "Learner Activity"))
          {
            if (!(reportType == "Active Learners"))
            {
              if (!(reportType == "Inactive Learners"))
              {
                if (reportType == "Locked Out Learners")
                {
                  List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetLockedStudentsDetailed();
                  if (studentsDetailed != null && studentsDetailed.Count > 0)
                  {
                    ExcelPackage excelPackage = new ExcelPackage();
                    ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
                    excelWorksheet.SetValue(1, 1, (object) ("LOCKED OUT LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
                    excelWorksheet.Cells["A1"].Style.Font.Bold = true;
                    excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
                    excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
                    excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
                    excelWorksheet.Cells["A1"].Style.WrapText = false;
                    excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    excelWorksheet.Column(1).Width = 40.0;
                    excelWorksheet.Column(2).Width = 30.0;
                    excelWorksheet.Column(3).Width = 30.0;
                    excelWorksheet.Column(4).Width = 30.0;
                    excelWorksheet.Column(5).Width = 30.0;
                    excelWorksheet.Column(6).Width = 30.0;
                    excelWorksheet.Column(7).Width = 30.0;
                    excelWorksheet.Column(8).Width = 30.0;
                    excelWorksheet.Column(9).Width = 30.0;
                    excelWorksheet.Column(10).Width = 30.0;
                    excelWorksheet.Column(11).Width = 30.0;
                    excelWorksheet.Column(12).Width = 30.0;
                    excelWorksheet.Column(13).Width = 30.0;
                    excelWorksheet.Column(14).Width = 30.0;
                    excelWorksheet.Column(15).Width = 30.0;
                    excelWorksheet.Column(16).Width = 30.0;
                    excelWorksheet.Column(17).Width = 30.0;
                    excelWorksheet.Column(18).Width = 30.0;
                    excelWorksheet.Column(19).Width = 30.0;
                    excelWorksheet.Column(20).Width = 30.0;
                    excelWorksheet.Column(21).Width = 30.0;
                    excelWorksheet.Column(22).Width = 30.0;
                    excelWorksheet.Column(23).Width = 30.0;
                    excelWorksheet.Column(24).Width = 30.0;
                    excelWorksheet.SetValue(2, 1, (object) "");
                    excelWorksheet.SetValue(3, 1, (object) "First Name");
                    excelWorksheet.SetValue(3, 2, (object) "Last Name");
                    excelWorksheet.SetValue(3, 3, (object) "Age");
                    excelWorksheet.SetValue(3, 4, (object) "Gender");
                    excelWorksheet.SetValue(3, 5, (object) "Race");
                    excelWorksheet.SetValue(3, 6, (object) "FSA Code");
                    excelWorksheet.SetValue(3, 7, (object) "ID Number");
                    excelWorksheet.SetValue(3, 8, (object) "Email");
                    excelWorksheet.SetValue(3, 9, (object) "Cell Number");
                    excelWorksheet.SetValue(3, 10, (object) "Postal Address 1");
                    excelWorksheet.SetValue(3, 11, (object) "Postal Address 2");
                    excelWorksheet.SetValue(3, 12, (object) "Postal Address 3");
                    excelWorksheet.SetValue(3, 13, (object) "Postal Address City");
                    excelWorksheet.SetValue(3, 14, (object) "Postal Address Code");
                    excelWorksheet.SetValue(3, 15, (object) "Province");
                    excelWorksheet.SetValue(3, 16, (object) "Home Language");
                    excelWorksheet.SetValue(3, 17, (object) "Highest Qualification");
                    excelWorksheet.SetValue(3, 18, (object) "Highest Qualification Title");
                    excelWorksheet.SetValue(3, 19, (object) "Currently Employed");
                    excelWorksheet.SetValue(3, 20, (object) "Date Enroled");
                    excelWorksheet.SetValue(3, 21, (object) "Last Activity");
                    excelWorksheet.SetValue(3, 22, (object) "Current Module");
                    excelWorksheet.SetValue(3, 23, (object) "Current Submodule");
                    excelWorksheet.SetValue(3, 24, (object) "Referred Friend");
                    using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
                    {
                      cell.Style.Font.Bold = true;
                      cell.Style.Font.Color.SetColor(Color.Black);
                      cell.Style.WrapText = false;
                      cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                      cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                      cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    for (int index = 0; index < studentsDetailed.Count; ++index)
                    {
                      StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
                      excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.FirstName);
                      excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.LastName);
                      excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.Age);
                      excelWorksheet.SetValue(index + 4, 4, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
                      excelWorksheet.SetValue(index + 4, 5, (object) studentSummaryDetailed.Race);
                      excelWorksheet.SetValue(index + 4, 6, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
                      excelWorksheet.SetValue(index + 4, 7, (object) studentSummaryDetailed.IdentityNumber);
                      excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.EmailAddress);
                      excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.TelephoneNumber);
                      excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.PostalAddress1);
                      excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress2);
                      excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress3);
                      excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalCity);
                      excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCode);
                      excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.Province);
                      excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.HomeLanguage);
                      excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HighestQualification);
                      excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualificationTitle);
                      excelWorksheet.SetValue(index + 4, 19, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
                      excelWorksheet.SetValue(index + 4, 20, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
                      excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
                      excelWorksheet.SetValue(index + 4, 22, (object) studentSummaryDetailed.CurrentModule);
                      excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentSubModule);
                      excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.ReferredFriend);
                    }
                    return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LockedOutLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
                  }
                  this.ModelState.AddModelError("", "No learners found.");
                }
              }
              else
              {
                List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetInactiveStudentsDetailed();
                if (studentsDetailed != null && studentsDetailed.Count > 0)
                {
                  ExcelPackage excelPackage = new ExcelPackage();
                  ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
                  excelWorksheet.SetValue(1, 1, (object) ("INACTIVE LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
                  excelWorksheet.Cells["A1"].Style.Font.Bold = true;
                  excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
                  excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
                  excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
                  excelWorksheet.Cells["A1"].Style.WrapText = false;
                  excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                  excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                  excelWorksheet.Column(1).Width = 40.0;
                  excelWorksheet.Column(2).Width = 30.0;
                  excelWorksheet.Column(3).Width = 30.0;
                  excelWorksheet.Column(4).Width = 30.0;
                  excelWorksheet.Column(5).Width = 30.0;
                  excelWorksheet.Column(6).Width = 30.0;
                  excelWorksheet.Column(7).Width = 30.0;
                  excelWorksheet.Column(8).Width = 30.0;
                  excelWorksheet.Column(9).Width = 30.0;
                  excelWorksheet.Column(10).Width = 30.0;
                  excelWorksheet.Column(11).Width = 30.0;
                  excelWorksheet.Column(12).Width = 30.0;
                  excelWorksheet.Column(13).Width = 30.0;
                  excelWorksheet.Column(14).Width = 30.0;
                  excelWorksheet.Column(15).Width = 30.0;
                  excelWorksheet.Column(16).Width = 30.0;
                  excelWorksheet.Column(17).Width = 30.0;
                  excelWorksheet.Column(18).Width = 30.0;
                  excelWorksheet.Column(19).Width = 30.0;
                  excelWorksheet.Column(20).Width = 30.0;
                  excelWorksheet.Column(21).Width = 30.0;
                  excelWorksheet.Column(22).Width = 30.0;
                  excelWorksheet.Column(23).Width = 30.0;
                  excelWorksheet.Column(24).Width = 30.0;
                  excelWorksheet.SetValue(2, 1, (object) "");
                  excelWorksheet.SetValue(3, 1, (object) "First Name");
                  excelWorksheet.SetValue(3, 2, (object) "Last Name");
                  excelWorksheet.SetValue(3, 3, (object) "Age");
                  excelWorksheet.SetValue(3, 4, (object) "Gender");
                  excelWorksheet.SetValue(3, 5, (object) "Race");
                  excelWorksheet.SetValue(3, 6, (object) "FSA Code");
                  excelWorksheet.SetValue(3, 7, (object) "ID Number");
                  excelWorksheet.SetValue(3, 8, (object) "Email");
                  excelWorksheet.SetValue(3, 9, (object) "Cell Number");
                  excelWorksheet.SetValue(3, 10, (object) "Postal Address 1");
                  excelWorksheet.SetValue(3, 11, (object) "Postal Address 2");
                  excelWorksheet.SetValue(3, 12, (object) "Postal Address 3");
                  excelWorksheet.SetValue(3, 13, (object) "Postal Address City");
                  excelWorksheet.SetValue(3, 14, (object) "Postal Address Code");
                  excelWorksheet.SetValue(3, 15, (object) "Province");
                  excelWorksheet.SetValue(3, 16, (object) "Home Language");
                  excelWorksheet.SetValue(3, 17, (object) "Highest Qualification");
                  excelWorksheet.SetValue(3, 18, (object) "Highest Qualification Title");
                  excelWorksheet.SetValue(3, 19, (object) "Currently Employed");
                  excelWorksheet.SetValue(3, 20, (object) "Date Enroled");
                  excelWorksheet.SetValue(3, 21, (object) "Last Activity");
                  excelWorksheet.SetValue(3, 22, (object) "Current Module");
                  excelWorksheet.SetValue(3, 23, (object) "Current Submodule");
                  excelWorksheet.SetValue(3, 24, (object) "Referred Friend");
                  using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
                  {
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Color.SetColor(Color.Black);
                    cell.Style.WrapText = false;
                    cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                  }
                  for (int index = 0; index < studentsDetailed.Count; ++index)
                  {
                    StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
                    excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.FirstName);
                    excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.LastName);
                    excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.Age);
                    excelWorksheet.SetValue(index + 4, 4, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
                    excelWorksheet.SetValue(index + 4, 5, (object) studentSummaryDetailed.Race);
                    excelWorksheet.SetValue(index + 4, 6, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
                    excelWorksheet.SetValue(index + 4, 7, (object) studentSummaryDetailed.IdentityNumber);
                    excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.EmailAddress);
                    excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.TelephoneNumber);
                    excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.PostalAddress1);
                    excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress2);
                    excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress3);
                    excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalCity);
                    excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCode);
                    excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.Province);
                    excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.HomeLanguage);
                    excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HighestQualification);
                    excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualificationTitle);
                    excelWorksheet.SetValue(index + 4, 19, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
                    excelWorksheet.SetValue(index + 4, 20, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
                    excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
                    excelWorksheet.SetValue(index + 4, 22, (object) studentSummaryDetailed.CurrentModule);
                    excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentSubModule);
                    excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.ReferredFriend);
                  }
                  return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("InactiveLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
                }
                this.ModelState.AddModelError("", "No learners found.");
              }
            }
            else
            {
              List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetStudentsDetailed();
              if (studentsDetailed != null && studentsDetailed.Count > 0)
              {
                ExcelPackage excelPackage = new ExcelPackage();
                ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
                excelWorksheet.SetValue(1, 1, (object) ("ACTIVE LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
                excelWorksheet.Cells["A1"].Style.Font.Bold = true;
                excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
                excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
                excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
                excelWorksheet.Cells["A1"].Style.WrapText = false;
                excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                excelWorksheet.Column(1).Width = 40.0;
                excelWorksheet.Column(2).Width = 30.0;
                excelWorksheet.Column(3).Width = 30.0;
                excelWorksheet.Column(4).Width = 30.0;
                excelWorksheet.Column(5).Width = 30.0;
                excelWorksheet.Column(6).Width = 30.0;
                excelWorksheet.Column(7).Width = 30.0;
                excelWorksheet.Column(8).Width = 30.0;
                excelWorksheet.Column(9).Width = 30.0;
                excelWorksheet.Column(10).Width = 30.0;
                excelWorksheet.Column(11).Width = 30.0;
                excelWorksheet.Column(12).Width = 30.0;
                excelWorksheet.Column(13).Width = 30.0;
                excelWorksheet.Column(14).Width = 30.0;
                excelWorksheet.Column(15).Width = 30.0;
                excelWorksheet.Column(16).Width = 30.0;
                excelWorksheet.Column(17).Width = 30.0;
                excelWorksheet.Column(18).Width = 30.0;
                excelWorksheet.Column(19).Width = 30.0;
                excelWorksheet.Column(20).Width = 30.0;
                excelWorksheet.Column(21).Width = 30.0;
                excelWorksheet.Column(22).Width = 30.0;
                excelWorksheet.Column(23).Width = 30.0;
                excelWorksheet.Column(24).Width = 30.0;
                excelWorksheet.SetValue(2, 1, (object) "");
                excelWorksheet.SetValue(3, 1, (object) "First Name");
                excelWorksheet.SetValue(3, 2, (object) "Last Name");
                excelWorksheet.SetValue(3, 3, (object) "Age");
                excelWorksheet.SetValue(3, 4, (object) "Gender");
                excelWorksheet.SetValue(3, 5, (object) "Race");
                excelWorksheet.SetValue(3, 6, (object) "FSA Code");
                excelWorksheet.SetValue(3, 7, (object) "ID Number");
                excelWorksheet.SetValue(3, 8, (object) "Email");
                excelWorksheet.SetValue(3, 9, (object) "Cell Number");
                excelWorksheet.SetValue(3, 10, (object) "Postal Address 1");
                excelWorksheet.SetValue(3, 11, (object) "Postal Address 2");
                excelWorksheet.SetValue(3, 12, (object) "Postal Address 3");
                excelWorksheet.SetValue(3, 13, (object) "Postal Address City");
                excelWorksheet.SetValue(3, 14, (object) "Postal Address Code");
                excelWorksheet.SetValue(3, 15, (object) "Province");
                excelWorksheet.SetValue(3, 16, (object) "Home Language");
                excelWorksheet.SetValue(3, 17, (object) "Highest Qualification");
                excelWorksheet.SetValue(3, 18, (object) "Highest Qualification Title");
                excelWorksheet.SetValue(3, 19, (object) "Currently Employed");
                excelWorksheet.SetValue(3, 20, (object) "Date Enroled");
                excelWorksheet.SetValue(3, 21, (object) "Last Activity");
                excelWorksheet.SetValue(3, 22, (object) "Current Module");
                excelWorksheet.SetValue(3, 23, (object) "Current Submodule");
                excelWorksheet.SetValue(3, 24, (object) "Referred Friend");
                using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
                {
                  cell.Style.Font.Bold = true;
                  cell.Style.Font.Color.SetColor(Color.Black);
                  cell.Style.WrapText = false;
                  cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                  cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                  cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }
                for (int index = 0; index < studentsDetailed.Count; ++index)
                {
                  StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
                  excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.FirstName);
                  excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.LastName);
                  excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.Age);
                  excelWorksheet.SetValue(index + 4, 4, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
                  excelWorksheet.SetValue(index + 4, 5, (object) studentSummaryDetailed.Race);
                  excelWorksheet.SetValue(index + 4, 6, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
                  excelWorksheet.SetValue(index + 4, 7, (object) studentSummaryDetailed.IdentityNumber);
                  excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.EmailAddress);
                  excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.TelephoneNumber);
                  excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.PostalAddress1);
                  excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress2);
                  excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress3);
                  excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalCity);
                  excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCode);
                  excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.Province);
                  excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.HomeLanguage);
                  excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HighestQualification);
                  excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualificationTitle);
                  excelWorksheet.SetValue(index + 4, 19, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
                  excelWorksheet.SetValue(index + 4, 20, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
                  excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
                  excelWorksheet.SetValue(index + 4, 22, (object) studentSummaryDetailed.CurrentModule);
                  excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentSubModule);
                  excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.ReferredFriend);
                }
                return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("ActiveLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
              }
              this.ModelState.AddModelError("", "No learners found.");
            }
          }
          else
          {
            paDateTimeStart = paModel.DateStart == null || paModel.DateStart == "" ? new DateTime(1994, 1, 1) : DateTime.ParseExact(paModel.DateStart, "dd/MM/yyyy", (IFormatProvider) CultureInfo.InvariantCulture);
            paDateTimeStop = paModel.DateStop != null ? DateTime.ParseExact(paModel.DateStop, "dd/MM/yyyy", (IFormatProvider) CultureInfo.InvariantCulture) : new DateTime(2099, 12, 31);
            List<StudentSummaryDetailed> studentsActiveInPeriod = BLL.Student.GetStudentsActiveInPeriod(paDateTimeStart, paDateTimeStop);
            if (studentsActiveInPeriod != null && studentsActiveInPeriod.Count > 0)
            {
              ExcelPackage excelPackage = new ExcelPackage();
              ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
              excelWorksheet.SetValue(1, 1, (object) ("LEARNER ACTIVITY EXPORT - " + DateTime.Now.ToShortDateString()));
              excelWorksheet.Cells["A1"].Style.Font.Bold = true;
              excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
              excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
              excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
              excelWorksheet.Cells["A1"].Style.WrapText = false;
              excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
              excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
              excelWorksheet.Column(1).Width = 40.0;
              excelWorksheet.Column(2).Width = 30.0;
              excelWorksheet.Column(3).Width = 30.0;
              excelWorksheet.Column(4).Width = 30.0;
              excelWorksheet.Column(5).Width = 30.0;
              excelWorksheet.Column(6).Width = 30.0;
              excelWorksheet.Column(7).Width = 30.0;
              excelWorksheet.Column(8).Width = 30.0;
              excelWorksheet.Column(9).Width = 30.0;
              excelWorksheet.Column(10).Width = 30.0;
              excelWorksheet.Column(11).Width = 30.0;
              excelWorksheet.Column(12).Width = 30.0;
              excelWorksheet.Column(13).Width = 30.0;
              excelWorksheet.Column(14).Width = 30.0;
              excelWorksheet.Column(15).Width = 30.0;
              excelWorksheet.Column(16).Width = 30.0;
              excelWorksheet.Column(17).Width = 30.0;
              excelWorksheet.Column(18).Width = 30.0;
              excelWorksheet.Column(19).Width = 30.0;
              excelWorksheet.Column(20).Width = 30.0;
              excelWorksheet.Column(21).Width = 30.0;
              excelWorksheet.Column(22).Width = 30.0;
              excelWorksheet.Column(23).Width = 30.0;
              excelWorksheet.Column(24).Width = 30.0;
              excelWorksheet.SetValue(2, 1, (object) "");
              excelWorksheet.SetValue(3, 1, (object) "First Name");
              excelWorksheet.SetValue(3, 2, (object) "Last Name");
              excelWorksheet.SetValue(3, 3, (object) "Age");
              excelWorksheet.SetValue(3, 4, (object) "Gender");
              excelWorksheet.SetValue(3, 5, (object) "Race");
              excelWorksheet.SetValue(3, 6, (object) "FSA Code");
              excelWorksheet.SetValue(3, 7, (object) "ID Number");
              excelWorksheet.SetValue(3, 8, (object) "Email");
              excelWorksheet.SetValue(3, 9, (object) "Cell Number");
              excelWorksheet.SetValue(3, 10, (object) "Postal Address 1");
              excelWorksheet.SetValue(3, 11, (object) "Postal Address 2");
              excelWorksheet.SetValue(3, 12, (object) "Postal Address 3");
              excelWorksheet.SetValue(3, 13, (object) "Postal Address City");
              excelWorksheet.SetValue(3, 14, (object) "Postal Address Code");
              excelWorksheet.SetValue(3, 15, (object) "Province");
              excelWorksheet.SetValue(3, 16, (object) "Home Language");
              excelWorksheet.SetValue(3, 17, (object) "Highest Qualification");
              excelWorksheet.SetValue(3, 18, (object) "Highest Qualification Title");
              excelWorksheet.SetValue(3, 19, (object) "Currently Employed");
              excelWorksheet.SetValue(3, 20, (object) "Date Enroled");
              excelWorksheet.SetValue(3, 21, (object) "Last Activity");
              excelWorksheet.SetValue(3, 22, (object) "Current Module");
              excelWorksheet.SetValue(3, 23, (object) "Current Submodule");
              excelWorksheet.SetValue(3, 24, (object) "Referred Friend");
              using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
              {
                cell.Style.Font.Bold = true;
                cell.Style.Font.Color.SetColor(Color.Black);
                cell.Style.WrapText = false;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
              }
              for (int index = 0; index < studentsActiveInPeriod.Count; ++index)
              {
                StudentSummaryDetailed studentSummaryDetailed = studentsActiveInPeriod[index];
                excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.FirstName);
                excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.LastName);
                excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.Age);
                excelWorksheet.SetValue(index + 4, 4, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
                excelWorksheet.SetValue(index + 4, 5, (object) studentSummaryDetailed.Race);
                excelWorksheet.SetValue(index + 4, 6, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
                excelWorksheet.SetValue(index + 4, 7, (object) studentSummaryDetailed.IdentityNumber);
                excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.EmailAddress);
                excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.TelephoneNumber);
                excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.PostalAddress1);
                excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress2);
                excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress3);
                excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalCity);
                excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCode);
                excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.Province);
                excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.HomeLanguage);
                excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HighestQualification);
                excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualificationTitle);
                excelWorksheet.SetValue(index + 4, 19, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
                excelWorksheet.SetValue(index + 4, 20, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
                excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
                excelWorksheet.SetValue(index + 4, 22, (object) studentSummaryDetailed.CurrentModule);
                excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentSubModule);
                excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.ReferredFriend);
              }
              return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LearnerActivityExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
            }
            this.ModelState.AddModelError("", "No learners found.");
          }
        }
        else
        {
          List<StudentSummaryDetailed> studentsDetailed = BLL.Student.GetAllStudentsDetailed();
          if (studentsDetailed != null && studentsDetailed.Count > 0)
          {
            ExcelPackage excelPackage = new ExcelPackage();
            ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
            excelWorksheet.SetValue(1, 1, (object) ("ALL LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
            excelWorksheet.Cells["A1"].Style.Font.Bold = true;
            excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
            excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
            excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
            excelWorksheet.Cells["A1"].Style.WrapText = false;
            excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            excelWorksheet.Column(1).Width = 40.0;
            excelWorksheet.Column(2).Width = 30.0;
            excelWorksheet.Column(3).Width = 30.0;
            excelWorksheet.Column(4).Width = 30.0;
            excelWorksheet.Column(5).Width = 30.0;
            excelWorksheet.Column(6).Width = 30.0;
            excelWorksheet.Column(7).Width = 30.0;
            excelWorksheet.Column(8).Width = 30.0;
            excelWorksheet.Column(9).Width = 30.0;
            excelWorksheet.Column(10).Width = 30.0;
            excelWorksheet.Column(11).Width = 30.0;
            excelWorksheet.Column(12).Width = 30.0;
            excelWorksheet.Column(13).Width = 30.0;
            excelWorksheet.Column(14).Width = 30.0;
            excelWorksheet.Column(15).Width = 30.0;
            excelWorksheet.Column(16).Width = 30.0;
            excelWorksheet.Column(17).Width = 30.0;
            excelWorksheet.Column(18).Width = 30.0;
            excelWorksheet.Column(19).Width = 30.0;
            excelWorksheet.Column(20).Width = 30.0;
            excelWorksheet.Column(21).Width = 30.0;
            excelWorksheet.Column(22).Width = 30.0;
            excelWorksheet.Column(23).Width = 30.0;
            excelWorksheet.Column(24).Width = 30.0;
            excelWorksheet.SetValue(2, 1, (object) "");
            excelWorksheet.SetValue(3, 1, (object) "First Name");
            excelWorksheet.SetValue(3, 2, (object) "Last Name");
            excelWorksheet.SetValue(3, 3, (object) "Age");
            excelWorksheet.SetValue(3, 4, (object) "Gender");
            excelWorksheet.SetValue(3, 5, (object) "Race");
            excelWorksheet.SetValue(3, 6, (object) "FSA Code");
            excelWorksheet.SetValue(3, 7, (object) "ID Number");
            excelWorksheet.SetValue(3, 8, (object) "Email");
            excelWorksheet.SetValue(3, 9, (object) "Cell Number");
            excelWorksheet.SetValue(3, 10, (object) "Postal Address 1");
            excelWorksheet.SetValue(3, 11, (object) "Postal Address 2");
            excelWorksheet.SetValue(3, 12, (object) "Postal Address 3");
            excelWorksheet.SetValue(3, 13, (object) "Postal Address City");
            excelWorksheet.SetValue(3, 14, (object) "Postal Address Code");
            excelWorksheet.SetValue(3, 15, (object) "Province");
            excelWorksheet.SetValue(3, 16, (object) "Home Language");
            excelWorksheet.SetValue(3, 17, (object) "Highest Qualification");
            excelWorksheet.SetValue(3, 18, (object) "Highest Qualification Title");
            excelWorksheet.SetValue(3, 19, (object) "Currently Employed");
            excelWorksheet.SetValue(3, 20, (object) "Date Enroled");
            excelWorksheet.SetValue(3, 21, (object) "Last Activity");
            excelWorksheet.SetValue(3, 22, (object) "Current Module");
            excelWorksheet.SetValue(3, 23, (object) "Current Submodule");
            excelWorksheet.SetValue(3, 24, (object) "Referred Friend");
            using (ExcelRange cell = excelWorksheet.Cells["A3:Y3"])
            {
              cell.Style.Font.Bold = true;
              cell.Style.Font.Color.SetColor(Color.Black);
              cell.Style.WrapText = false;
              cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
              cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
              cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            }
            for (int index = 0; index < studentsDetailed.Count; ++index)
            {
              StudentSummaryDetailed studentSummaryDetailed = studentsDetailed[index];
              excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.FirstName);
              excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.LastName);
              excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.Age);
              excelWorksheet.SetValue(index + 4, 4, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
              excelWorksheet.SetValue(index + 4, 5, (object) studentSummaryDetailed.Race);
              excelWorksheet.SetValue(index + 4, 6, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
              excelWorksheet.SetValue(index + 4, 7, (object) studentSummaryDetailed.IdentityNumber);
              excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.EmailAddress);
              excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.TelephoneNumber);
              excelWorksheet.SetValue(index + 4, 10, (object) studentSummaryDetailed.PostalAddress1);
              excelWorksheet.SetValue(index + 4, 11, (object) studentSummaryDetailed.PostalAddress2);
              excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.PostalAddress3);
              excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.PostalCity);
              excelWorksheet.SetValue(index + 4, 14, (object) studentSummaryDetailed.PostalCode);
              excelWorksheet.SetValue(index + 4, 15, (object) studentSummaryDetailed.Province);
              excelWorksheet.SetValue(index + 4, 16, (object) studentSummaryDetailed.HomeLanguage);
              excelWorksheet.SetValue(index + 4, 17, (object) studentSummaryDetailed.HighestQualification);
              excelWorksheet.SetValue(index + 4, 18, (object) studentSummaryDetailed.HighestQualificationTitle);
              excelWorksheet.SetValue(index + 4, 19, studentSummaryDetailed.CurrentlyEmployed ? (object) "Yes" : (object) "No");
              excelWorksheet.SetValue(index + 4, 20, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
              excelWorksheet.SetValue(index + 4, 21, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
              excelWorksheet.SetValue(index + 4, 22, (object) studentSummaryDetailed.CurrentModule);
              excelWorksheet.SetValue(index + 4, 23, (object) studentSummaryDetailed.CurrentSubModule);
              excelWorksheet.SetValue(index + 4, 24, (object) studentSummaryDetailed.ReferredFriend);
            }
            return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("AllLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
          }
          this.ModelState.AddModelError("", "No learners found.");
        }
      }
      catch (Exception ex)
      {
        COML.Logging.Log("Error running report", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Generate), "Report");
      }
      return (ActionResult) this.View((object) paModel);
    }
  }
}
