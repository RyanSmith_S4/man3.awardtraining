﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.SponsorController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using Test.Models.Sponsor;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  public class SponsorController : Controller
  {
    public ActionResult Index(int paId = 1)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (this.User.Identity.IsAuthenticated)
        {
          if (new AccountController().DoesUserHaveRole(userId, "Sponsor"))
          {
            int sponsorIdByUserId = BLL.Sponsor.GetSponsorIDByUserID(userId);
            if (sponsorIdByUserId > 0)
            {
              BLL.Logging.SaveSponsorPageLog(userId, "Learner overview page.");
              List<StudentSummary> studentsForSponsor = BLL.Student.GetStudentsForSponsor(sponsorIdByUserId);
              List<SponsorViewModel> allItems = new List<SponsorViewModel>();
              foreach (StudentSummary studentSummary in studentsForSponsor)
              {
                SponsorViewModel sponsorViewModel = new SponsorViewModel()
                {
                  UserID = studentSummary.UserID,
                  DateEnroled = studentSummary.DateTimeCreated,
                  IDNumber = studentSummary.IdentityNumber,
                  FirstName = studentSummary.FirstName,
                  LastName = studentSummary.LastName,
                  LatestActivity = studentSummary.LastActivity,
                  StudentID = studentSummary.StudentID
                };
                allItems.Add(sponsorViewModel);
              }
              return (ActionResult) this.View((object) allItems.ToPagedList<SponsorViewModel>(paId, 10));
            }
            COML.Logging.Log("Sponsor does not exist.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
          }
          COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
          return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
        }
        COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
    }

    public ActionResult Search(int paId = 1, string paWord = null, string paCode = null)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (this.User.Identity.IsAuthenticated)
        {
          if (new AccountController().DoesUserHaveRole(userId, "Sponsor"))
          {
            int sponsorIdByUserId = BLL.Sponsor.GetSponsorIDByUserID(userId);
            if (!string.IsNullOrWhiteSpace(paWord) && !string.IsNullOrWhiteSpace(paCode))
              BLL.Logging.SaveSponsorPageLog(userId, "Learner overview page. Searched on ID: " + paWord + " and FSA Code: " + paCode + ".");
            else if (!string.IsNullOrWhiteSpace(paWord))
              BLL.Logging.SaveSponsorPageLog(userId, "Learner overview page. Searched on ID: " + paWord + ".");
            else if (!string.IsNullOrWhiteSpace(paCode))
              BLL.Logging.SaveSponsorPageLog(userId, "Learner overview page. Searched on FSA Code: " + paWord + ".");
            if (sponsorIdByUserId > 0)
            {
              List<StudentSummary> studentSummaryList1 = new List<StudentSummary>();
              List<StudentSummary> studentSummaryList2 = !string.IsNullOrWhiteSpace(paWord) || !string.IsNullOrWhiteSpace(paCode) ? BLL.Student.GetStudentsByIDNumberAndCodeForSponsor(sponsorIdByUserId, paWord, paCode.ToUpper()) : BLL.Student.GetStudentsForSponsor(sponsorIdByUserId);
              List<SponsorViewModel> allItems = new List<SponsorViewModel>();
              foreach (StudentSummary studentSummary in studentSummaryList2)
              {
                SponsorViewModel sponsorViewModel = new SponsorViewModel()
                {
                  StudentID = studentSummary.StudentID,
                  UserID = studentSummary.UserID,
                  DateEnroled = studentSummary.DateTimeCreated,
                  IDNumber = studentSummary.IdentityNumber,
                  FirstName = studentSummary.FirstName,
                  LastName = studentSummary.LastName,
                  LatestActivity = studentSummary.LastActivity
                };
                allItems.Add(sponsorViewModel);
              }
              return (ActionResult) this.View("Index", (object) allItems.ToPagedList<SponsorViewModel>(paId, 10));
            }
            COML.Logging.Log("Sponsor does not exist.", Enumerations.LogLevel.Info, (Exception) null);
            return (ActionResult) this.RedirectToAction("Index", "Home");
          }
          COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
          return (ActionResult) this.RedirectToAction("Index", "Home");
        }
        COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
    }

    public ActionResult StudentProgress(int paId)
    {
      try
      {
        string userId = this.User.Identity.GetUserId();
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(userId, "Sponsor"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        int sponsorIdByUserId = BLL.Sponsor.GetSponsorIDByUserID(userId);
        if (sponsorIdByUserId > 0)
        {
          StudentProgressViewModel progressViewModel = new StudentProgressViewModel();
          List<COML.Classes.Module> userByIdforSponsor = BLL.Module.GetModulesForUserByIDforSponsor(paId, sponsorIdByUserId);
          COML.Classes.Student studentByIdForSponsor = BLL.Student.GetStudentByIDForSponsor(paId, sponsorIdByUserId);
          if (studentByIdForSponsor == null)
            return (ActionResult) this.RedirectToAction("Index", "Sponsor");
          BLL.Logging.SaveSponsorPageLog(userId, "Viewed progress of: " + studentByIdForSponsor.FirstName + " " + studentByIdForSponsor.LastName);
          if (userByIdforSponsor == null)
            return (ActionResult) this.RedirectToAction("Index", "Sponsor");
          progressViewModel.FirstName = studentByIdForSponsor.FirstName;
          progressViewModel.LastName = studentByIdForSponsor.LastName;
          progressViewModel.IsLocked = false;
          progressViewModel.Modules = new List<Test.Models.Sponsor.Module>();
          progressViewModel.IDNumber = studentByIdForSponsor.IdentityNumber;
          foreach (COML.Classes.Module module1 in userByIdforSponsor)
          {
            Test.Models.Sponsor.Module module2 = new Test.Models.Sponsor.Module()
            {
              ID = module1.ID,
              Code = module1.Code,
              Description = module1.Description,
              Name = module1.Name,
              Order = module1.Order,
              IsAvailable = module1.IsAvailable,
              IsComplete = module1.IsCompleted,
              IsLocked = false
            };
            module2.SubModules = new List<Test.Models.Sponsor.SubModule>();
            foreach (COML.Classes.SubModule subModule1 in module1.SubModules)
            {
              Test.Models.Sponsor.SubModule subModule2 = new Test.Models.Sponsor.SubModule()
              {
                ID = subModule1.ID,
                Code = subModule1.Code,
                Description = subModule1.Description,
                Name = subModule1.Name,
                IsComplete = subModule1.IsComplete,
                IsStarted = subModule1.IsStarted,
                IsLocked = subModule1.IsLocked,
                IsCurrent = subModule1.IsCurrent,
                IsAvailable = subModule1.IsAvailable,
                IsNotYetCompetent = subModule1.IsNotYetCompetent
              };
              if (subModule2.IsLocked)
              {
                module2.IsLocked = true;
                progressViewModel.IsLocked = true;
              }
              subModule2.Code = subModule2.Code == "" ? subModule2.Code : " - " + subModule2.Code;
              module2.SubModules.Add(subModule2);
            }
            progressViewModel.Modules.Add(module2);
          }
          return (ActionResult) this.View((object) progressViewModel);
        }
        COML.Logging.Log("Sponsor does not exist.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
    }

    public ActionResult SponsorContact()
    {
      return (ActionResult) this.View();
    }

    public ActionResult DownloadStudentsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      string userId = this.User.Identity.GetUserId();
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Sponsor"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<StudentSummaryDetailed> studentsBySponsor = BLL.Sponsor.GetStudentsBySponsor(userId);
        if (studentsBySponsor == null || studentsBySponsor.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("ACTIVE LEARNER EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 30.0;
        excelWorksheet.Column(3).Width = 30.0;
        excelWorksheet.Column(4).Width = 30.0;
        excelWorksheet.Column(5).Width = 30.0;
        excelWorksheet.Column(6).Width = 30.0;
        excelWorksheet.Column(7).Width = 30.0;
        excelWorksheet.Column(8).Width = 30.0;
        excelWorksheet.Column(9).Width = 30.0;
        excelWorksheet.Column(10).Width = 30.0;
        excelWorksheet.Column(11).Width = 30.0;
        excelWorksheet.Column(12).Width = 30.0;
        excelWorksheet.Column(13).Width = 30.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "First Name");
        excelWorksheet.SetValue(3, 2, (object) "Last Name");
        excelWorksheet.SetValue(3, 3, (object) "Age");
        excelWorksheet.SetValue(3, 4, (object) "Gender");
        excelWorksheet.SetValue(3, 5, (object) "Race");
        excelWorksheet.SetValue(3, 6, (object) "FSA Code");
        excelWorksheet.SetValue(3, 7, (object) "ID Number");
        excelWorksheet.SetValue(3, 8, (object) "Province");
        excelWorksheet.SetValue(3, 9, (object) "Home Language");
        excelWorksheet.SetValue(3, 10, (object) "Date Enroled");
        excelWorksheet.SetValue(3, 11, (object) "Last Activity");
        excelWorksheet.SetValue(3, 12, (object) "Current Module");
        excelWorksheet.SetValue(3, 13, (object) "Current Submodule");
        using (ExcelRange cell = excelWorksheet.Cells["A3:M3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < studentsBySponsor.Count; ++index)
        {
          StudentSummaryDetailed studentSummaryDetailed = studentsBySponsor[index];
          excelWorksheet.SetValue(index + 4, 1, (object) studentSummaryDetailed.FirstName);
          excelWorksheet.SetValue(index + 4, 2, (object) studentSummaryDetailed.LastName);
          excelWorksheet.SetValue(index + 4, 3, (object) studentSummaryDetailed.Age);
          excelWorksheet.SetValue(index + 4, 4, studentSummaryDetailed.Gender == Enumerations.enumGender.Male ? (object) "Male" : (object) "Female");
          excelWorksheet.SetValue(index + 4, 5, (object) studentSummaryDetailed.Race);
          excelWorksheet.SetValue(index + 4, 6, studentSummaryDetailed.FsaCode == "SYSTEM" ? (object) "DEFAULT" : (object) studentSummaryDetailed.FsaCode);
          excelWorksheet.SetValue(index + 4, 7, (object) studentSummaryDetailed.IdentityNumber);
          excelWorksheet.SetValue(index + 4, 8, (object) studentSummaryDetailed.Province);
          excelWorksheet.SetValue(index + 4, 9, (object) studentSummaryDetailed.HomeLanguage);
          excelWorksheet.SetValue(index + 4, 10, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.DateTimeCreated));
          excelWorksheet.SetValue(index + 4, 11, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) studentSummaryDetailed.LastActivity));
          excelWorksheet.SetValue(index + 4, 12, (object) studentSummaryDetailed.CurrentModule);
          excelWorksheet.SetValue(index + 4, 13, (object) studentSummaryDetailed.CurrentSubModule);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("ActiveLearnerExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "Sponsor");
      }
    }
  }
}
