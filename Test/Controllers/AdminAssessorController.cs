﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminAssessorController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using Test.Models.Admin;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  public class AdminAssessorController : Controller
  {
    public ActionResult AssessorUnlock(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.UnlockAssessor(paId);
      return (ActionResult) this.RedirectToAction("AssessorDetails", "AdminAssessor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult AssessorLock(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.LockAssessor(paId);
      return (ActionResult) this.RedirectToAction("AssessorDetails", "AdminAssessor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult AssessorApprove(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.ApproveAssessor(paId);
      return (ActionResult) this.RedirectToAction("AssessorDetails", "AdminAssessor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult Index(int paId = 1)
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          List<AssessorSummary> assessors = BLL.Instructor.GetAssessors();
          List<AdminAssessorViewModel> allItems = new List<AdminAssessorViewModel>();
          foreach (AssessorSummary assessorSummary in assessors)
          {
            AdminAssessorViewModel assessorViewModel = new AdminAssessorViewModel()
            {
              DateEnroled = assessorSummary.DateTimeCreated,
              IDNumber = assessorSummary.IdentityNumber,
              FirstName = assessorSummary.FirstName,
              LastName = assessorSummary.LastName,
              LatestActivity = assessorSummary.LastActivity,
              AssessorID = assessorSummary.AssessorID,
              IsLocked = assessorSummary.IsLocked,
              IsApproved = assessorSummary.IsApproved,
              UserID = assessorSummary.UserID
            };
            allItems.Add(assessorViewModel);
          }
          return (ActionResult) this.View((object) allItems.ToPagedList<AdminAssessorViewModel>(paId, 10));
        }
        COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }

    public ActionResult Search(int paId = 1, string paWord = null)
    {
      List<AssessorSummary> assessorSummaryList1 = new List<AssessorSummary>();
      List<AssessorSummary> assessorSummaryList2 = string.IsNullOrWhiteSpace(paWord) ? BLL.Instructor.GetAssessors() : BLL.Instructor.GetAssessorsByIDNumber(paWord);
      List<AdminAssessorViewModel> allItems = new List<AdminAssessorViewModel>();
      foreach (AssessorSummary assessorSummary in assessorSummaryList2)
      {
        AdminAssessorViewModel assessorViewModel = new AdminAssessorViewModel()
        {
          DateEnroled = assessorSummary.DateTimeCreated,
          IDNumber = assessorSummary.IdentityNumber,
          FirstName = assessorSummary.FirstName,
          LastName = assessorSummary.LastName,
          LatestActivity = assessorSummary.LastActivity,
          AssessorID = assessorSummary.AssessorID,
          IsLocked = assessorSummary.IsLocked,
          IsApproved = assessorSummary.IsApproved,
          UserID = assessorSummary.UserID
        };
        allItems.Add(assessorViewModel);
      }
      return (ActionResult) this.View("Index", (object) allItems.ToPagedList<AdminAssessorViewModel>(paId, 10));
    }

    public ActionResult DownloadAssessorsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<AssessorSummary> assessors = BLL.Instructor.GetAssessors();
        if (assessors == null || assessors.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("ASSESSOR EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.Column(6).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "First Name");
        excelWorksheet.SetValue(3, 2, (object) "Last Name");
        excelWorksheet.SetValue(3, 3, (object) "ID Number");
        excelWorksheet.SetValue(3, 4, (object) "Email");
        excelWorksheet.SetValue(3, 5, (object) "Telephone Number");
        excelWorksheet.SetValue(3, 6, (object) "Date Applied");
        using (ExcelRange cell = excelWorksheet.Cells["A3:F3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < assessors.Count; ++index)
        {
          AssessorSummary assessorSummary = assessors[index];
          excelWorksheet.SetValue(index + 4, 1, (object) assessorSummary.FirstName);
          excelWorksheet.SetValue(index + 4, 2, (object) assessorSummary.LastName);
          excelWorksheet.SetValue(index + 4, 3, (object) assessorSummary.IdentityNumber);
          excelWorksheet.SetValue(index + 4, 4, (object) assessorSummary.EmailAddress);
          excelWorksheet.SetValue(index + 4, 5, (object) assessorSummary.TelephoneCell);
          excelWorksheet.SetValue(index + 4, 6, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) assessorSummary.DateTimeCreated));
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("AssessorExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminAssessor");
      }
    }

    public ActionResult AssessorDetails(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Instructor assessorById = BLL.Instructor.GetAssessorByID(paId);
      AssessorDetailsViewModel detailsViewModel = new AssessorDetailsViewModel();
      if (assessorById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminAssessor");
      detailsViewModel.Age = assessorById.Age.ToString();
      detailsViewModel.CellNumber = assessorById.TelephoneCell;
      detailsViewModel.DateEnroled = assessorById.DateTimeCreated;
      detailsViewModel.Employed = assessorById.Employed ? "Yes" : "No";
      detailsViewModel.FirstName = assessorById.FirstName;
      detailsViewModel.LastName = assessorById.LastName;
      detailsViewModel.Nickname = assessorById.Nickname;
      detailsViewModel.Gender = assessorById.SelectGender.ToString();
      detailsViewModel.Race = assessorById.SelectedRace.ToString();
      detailsViewModel.HighestQualification = assessorById.HighestQualification;
      detailsViewModel.HighestQualificationTitle = assessorById.HighestQualificationTitle;
      detailsViewModel.HomeLanguage = assessorById.HomeLanguage;
      detailsViewModel.IDNumber = assessorById.IdentityNumber;
      detailsViewModel.PostalAddress1 = assessorById.PostalAddress1;
      detailsViewModel.PostalAddress2 = assessorById.PostalAddress2;
      detailsViewModel.PostalAddress3 = assessorById.PostalAddress3;
      detailsViewModel.PostalAddressCity = assessorById.PostalAddressCity;
      detailsViewModel.PostalAddressCode = assessorById.PostalAddressCode;
      detailsViewModel.PostalAddressProvince = assessorById.PostalAddressProvince;
      detailsViewModel.AssessorID = assessorById.ID;
      detailsViewModel.UserID = assessorById.UserID;
      detailsViewModel.IsLocked = assessorById.IsLocked;
      detailsViewModel.IsApproved = assessorById.IsApproved;
      detailsViewModel.Email = assessorById.EmailAddress;
      detailsViewModel.AssessorCode = assessorById.AssessorCode;
      detailsViewModel.DateApproved = assessorById.DateTimeApproved ?? new DateTime(1, 1, 1);
      detailsViewModel.UserFiles = new List<Test.Models.Admin.UserFile>();
      if (assessorById.UserFiles != null && assessorById.UserFiles.Count > 0)
      {
        foreach (COML.Classes.UserFile userFile in assessorById.UserFiles)
          detailsViewModel.UserFiles.Add(new Test.Models.Admin.UserFile()
          {
            ID = userFile.ID,
            Filepath = userFile.Filepath,
            Name = userFile.Name,
            SystemFile = userFile.SystemFile,
            TypeFile = userFile.TypeFile,
            DateCreated = userFile.DateCreated
          });
      }
      detailsViewModel.Students = new List<AssessorStudent>();
      if (assessorById.Students != null && assessorById.Students.Count > 0)
      {
        foreach (COML.Classes.Student student in assessorById.Students)
          detailsViewModel.Students.Add(new AssessorStudent()
          {
            StudentID = student.ID,
            Email = student.EmailAddress,
            FirstName = student.FirstName,
            IDNumber = student.IdentityNumber,
            LastName = student.LastName,
            TelephoneCell = student.TelephoneCell,
            UserID = student.UserID
          });
      }
      return (ActionResult) this.View((object) detailsViewModel);
    }

    public ActionResult UnassignStudent(int paInstructorId, int paStudentId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.UnassignAssessorStudent(paInstructorId, paStudentId);
      return (ActionResult) this.RedirectToAction("AssessorDetails", "AdminAssessor", (object) new
      {
        paId = paInstructorId
      });
    }

    public ActionResult AssessorAssignLearner(int paAssessorId, int paId = 1)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      List<COML.Classes.Student> assignedToAssessors = BLL.Instructor.GetStudentsNotAssignedToAssessors();
      List<AssessorAssignLearner> allItems = new List<AssessorAssignLearner>();
      foreach (COML.Classes.Student student in assignedToAssessors)
        allItems.Add(new AssessorAssignLearner()
        {
          UnassignedAssessorID = paAssessorId,
          FsaCode = student.FsaCode,
          StudentID = student.ID,
          Email = student.EmailAddress,
          FirstName = student.FirstName,
          IDNumber = student.IdentityNumber,
          LastName = student.LastName,
          TelephoneCell = student.TelephoneCell,
          UserID = student.UserID,
          DateEnroled = student.DateTimeCreated
        });
      return (ActionResult) this.View((object) allItems.ToPagedList<AssessorAssignLearner>(paId, 10));
    }

    public ActionResult AssessorLearnerSearch(
      int paAssessorId,
      int paId = 1,
      string paWord = null,
      string paCode = null)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      List<COML.Classes.Student> studentList = new List<COML.Classes.Student>();
      List<AssessorAssignLearner> allItems = new List<AssessorAssignLearner>();
      foreach (COML.Classes.Student student in !string.IsNullOrWhiteSpace(paWord) || !string.IsNullOrWhiteSpace(paCode) ? BLL.Instructor.GetStudentsByIDNumberAndCodeForAssessor(paWord, paCode) : BLL.Instructor.GetStudentsNotAssignedToAssessors())
        allItems.Add(new AssessorAssignLearner()
        {
          UnassignedAssessorID = paAssessorId,
          FsaCode = student.FsaCode,
          StudentID = student.ID,
          Email = student.EmailAddress,
          FirstName = student.FirstName,
          IDNumber = student.IdentityNumber,
          LastName = student.LastName,
          TelephoneCell = student.TelephoneCell,
          UserID = student.UserID,
          DateEnroled = student.DateTimeCreated
        });
      return (ActionResult) this.View("AssessorAssignLearner", (object) allItems.ToPagedList<AssessorAssignLearner>(paId, 10));
    }

    public ActionResult AssignLearnerToAssessor(int paAssessorId, int paStudentId)
    {
      try
      {
        if (!this.User.Identity.IsAuthenticated)
          return (ActionResult) this.RedirectToAction("Index", "Home");
        if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
          return (ActionResult) this.RedirectToAction("Index", "Home");
        BLL.Instructor.AssignLearnerToAssessor(paAssessorId, paStudentId);
        List<AssessorAssignLearner> assessorAssignLearnerList = new List<AssessorAssignLearner>();
        return (ActionResult) this.RedirectToAction("AssessorAssignLearner", "AdminAssessor", (object) new
        {
          paAssessorId = paAssessorId
        });
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "Home");
      }
    }

    public ActionResult AssessorLogs(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Instructor instructorById = BLL.Instructor.GetInstructorByID(paId);
      List<GenericLog> assessorLogs = BLL.Logging.GetAssessorLogs(paId);
      AssessorLogsViewModel assessorLogsViewModel = new AssessorLogsViewModel();
      if (instructorById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminAssessor");
      assessorLogsViewModel.FirstName = instructorById.FirstName;
      assessorLogsViewModel.LastName = instructorById.LastName;
      assessorLogsViewModel.InstructorID = instructorById.ID;
      assessorLogsViewModel.Logs = new List<Log>();
      foreach (GenericLog genericLog in assessorLogs)
        assessorLogsViewModel.Logs.Add(new Log()
        {
          Timestamp = genericLog.TimeStamp,
          Description = genericLog.Description,
          Type = genericLog.Type
        });
      return (ActionResult) this.View((object) assessorLogsViewModel);
    }

    public ActionResult DownloadAssessorLogReport(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        COML.Classes.Instructor instructorById = BLL.Instructor.GetInstructorByID(paId);
        List<GenericLog> assessorLogs = BLL.Logging.GetAssessorLogs(paId);
        if (assessorLogs == null || assessorLogs.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("ASSESSOR LOG EXPORT - " + instructorById.FirstName + " " + instructorById.LastName + " - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "Timestamp");
        excelWorksheet.SetValue(3, 2, (object) "Type");
        excelWorksheet.SetValue(3, 3, (object) "Description");
        using (ExcelRange cell = excelWorksheet.Cells["A3:C3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < assessorLogs.Count; ++index)
        {
          GenericLog genericLog = assessorLogs[index];
          string str;
          switch (genericLog.Type)
          {
            case Enumerations.enumLogType.GeneralChat:
              str = "General Chat";
              break;
            case Enumerations.enumLogType.GraduateChat:
              str = "Graduate Chat";
              break;
            default:
              str = genericLog.Type.ToString();
              break;
          }
          excelWorksheet.SetValue(index + 4, 1, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) genericLog.TimeStamp));
          excelWorksheet.SetValue(index + 4, 2, (object) str);
          excelWorksheet.SetValue(index + 4, 3, (object) genericLog.Description);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LogExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminAssessor");
      }
    }
  }
}
