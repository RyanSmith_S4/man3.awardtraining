﻿// Decompiled with JetBrains decompiler
// Type: Test.Controllers.AdminMentorController
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using COML;
using COML.Classes;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using Test.Models.Admin;
using Webdiyer.WebControls.Mvc;

namespace Test.Controllers
{
  public class AdminMentorController : Controller
  {
    public ActionResult MentorUnlock(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.UnlockMentor(paId);
      return (ActionResult) this.RedirectToAction("MentorDetails", "AdminMentor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult MentorLock(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.LockMentor(paId);
      return (ActionResult) this.RedirectToAction("MentorDetails", "AdminMentor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult MentorApprove(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      BLL.Instructor.ApproveMentor(paId);
      return (ActionResult) this.RedirectToAction("MentorDetails", "AdminMentor", (object) new
      {
        paId = paId
      });
    }

    public ActionResult Index(int paId = 1)
    {
      if (this.User.Identity.IsAuthenticated)
      {
        if (new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        {
          List<MentorSummary> mentors = BLL.Instructor.GetMentors();
          List<AdminMentorViewModel> allItems = new List<AdminMentorViewModel>();
          foreach (MentorSummary mentorSummary in mentors)
          {
            AdminMentorViewModel adminMentorViewModel = new AdminMentorViewModel()
            {
              DateEnroled = mentorSummary.DateTimeCreated,
              IDNumber = mentorSummary.IdentityNumber,
              FirstName = mentorSummary.FirstName,
              LastName = mentorSummary.LastName,
              LatestActivity = mentorSummary.LastActivity,
              MentorID = mentorSummary.MentorID,
              IsLocked = mentorSummary.IsLocked,
              IsApproved = mentorSummary.IsApproved
            };
            allItems.Add(adminMentorViewModel);
          }
          return (ActionResult) this.View((object) allItems.ToPagedList<AdminMentorViewModel>(paId, 10));
        }
        COML.Logging.Log("User does not have the required role.", Enumerations.LogLevel.Info, (Exception) null);
        return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
      }
      COML.Logging.Log("User is not authenticated.", Enumerations.LogLevel.Info, (Exception) null);
      return (ActionResult) this.RedirectToAction(nameof (Index), "Home");
    }

    public ActionResult Search(int paId = 1, string paWord = null)
    {
      List<MentorSummary> mentorSummaryList1 = new List<MentorSummary>();
      List<MentorSummary> mentorSummaryList2 = string.IsNullOrWhiteSpace(paWord) ? BLL.Instructor.GetMentors() : BLL.Instructor.GetMentorsByIDNumber(paWord);
      List<AdminMentorViewModel> allItems = new List<AdminMentorViewModel>();
      foreach (MentorSummary mentorSummary in mentorSummaryList2)
      {
        AdminMentorViewModel adminMentorViewModel = new AdminMentorViewModel()
        {
          DateEnroled = mentorSummary.DateTimeCreated,
          IDNumber = mentorSummary.IdentityNumber,
          FirstName = mentorSummary.FirstName,
          LastName = mentorSummary.LastName,
          LatestActivity = mentorSummary.LastActivity,
          MentorID = mentorSummary.MentorID,
          IsLocked = mentorSummary.IsLocked,
          IsApproved = mentorSummary.IsApproved
        };
        allItems.Add(adminMentorViewModel);
      }
      return (ActionResult) this.View("Index", (object) allItems.ToPagedList<AdminMentorViewModel>(paId, 10));
    }

    public ActionResult DownloadMentorsReport()
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        List<MentorSummary> mentors = BLL.Instructor.GetMentors();
        if (mentors == null || mentors.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("MENTOR EXPORT - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.Column(6).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "First Name");
        excelWorksheet.SetValue(3, 2, (object) "Last Name");
        excelWorksheet.SetValue(3, 3, (object) "ID Number");
        excelWorksheet.SetValue(3, 4, (object) "Email");
        excelWorksheet.SetValue(3, 5, (object) "Telephone Number");
        excelWorksheet.SetValue(3, 6, (object) "Date Applied");
        using (ExcelRange cell = excelWorksheet.Cells["A3:F3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < mentors.Count; ++index)
        {
          MentorSummary mentorSummary = mentors[index];
          excelWorksheet.SetValue(index + 4, 1, (object) mentorSummary.FirstName);
          excelWorksheet.SetValue(index + 4, 2, (object) mentorSummary.LastName);
          excelWorksheet.SetValue(index + 4, 3, (object) mentorSummary.IdentityNumber);
          excelWorksheet.SetValue(index + 4, 4, (object) mentorSummary.EmailAddress);
          excelWorksheet.SetValue(index + 4, 5, (object) mentorSummary.TelephoneCell);
          excelWorksheet.SetValue(index + 4, 6, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) mentorSummary.DateTimeCreated));
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("MentorExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("MentorIndex", "AdminMentor");
      }
    }

    public ActionResult DownloadMentorLogReport(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Login", "Account");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Login", "Account");
      try
      {
        COML.Classes.Instructor instructorById = BLL.Instructor.GetInstructorByID(paId);
        List<GenericLog> mentorLogs = BLL.Logging.GetMentorLogs(paId);
        if (mentorLogs == null || mentorLogs.Count <= 0)
          return (ActionResult) this.Content(string.Empty);
        ExcelPackage excelPackage = new ExcelPackage();
        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("Export");
        excelWorksheet.SetValue(1, 1, (object) ("MENTOR LOG EXPORT - " + instructorById.FirstName + " " + instructorById.LastName + " - " + DateTime.Now.ToShortDateString()));
        excelWorksheet.Cells["A1"].Style.Font.Bold = true;
        excelWorksheet.Cells["A1"].Style.Font.Size = 15f;
        excelWorksheet.Cells["A1"].Style.Font.UnderLine = true;
        excelWorksheet.Cells["A1"].Style.Font.Color.SetColor(Color.Black);
        excelWorksheet.Cells["A1"].Style.WrapText = false;
        excelWorksheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        excelWorksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        excelWorksheet.Column(1).Width = 40.0;
        excelWorksheet.Column(2).Width = 35.0;
        excelWorksheet.Column(3).Width = 35.0;
        excelWorksheet.Column(4).Width = 35.0;
        excelWorksheet.Column(5).Width = 35.0;
        excelWorksheet.SetValue(2, 1, (object) "");
        excelWorksheet.SetValue(3, 1, (object) "Timestamp");
        excelWorksheet.SetValue(3, 2, (object) "Type");
        excelWorksheet.SetValue(3, 3, (object) "Description");
        using (ExcelRange cell = excelWorksheet.Cells["A3:C3"])
        {
          cell.Style.Font.Bold = true;
          cell.Style.Font.Color.SetColor(Color.Black);
          cell.Style.WrapText = false;
          cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
          cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
          cell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        for (int index = 0; index < mentorLogs.Count; ++index)
        {
          GenericLog genericLog = mentorLogs[index];
          string str;
          switch (genericLog.Type)
          {
            case Enumerations.enumLogType.GeneralChat:
              str = "General Chat";
              break;
            case Enumerations.enumLogType.GraduateChat:
              str = "Graduate Chat";
              break;
            default:
              str = genericLog.Type.ToString();
              break;
          }
          excelWorksheet.SetValue(index + 4, 1, (object) string.Format("{0:d/M/yyyy HH:mm:ss}", (object) genericLog.TimeStamp));
          excelWorksheet.SetValue(index + 4, 2, (object) str);
          excelWorksheet.SetValue(index + 4, 3, (object) genericLog.Description);
        }
        return (ActionResult) this.File(excelPackage.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("LogExport-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", (object) DateTime.UtcNow));
      }
      catch (Exception ex)
      {
        COML.Logging.Log(ex.Message, Enumerations.LogLevel.Error, ex);
        return (ActionResult) this.RedirectToAction("Index", "AdminMentor");
      }
    }

    public ActionResult MentorDetails(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Instructor mentorById = BLL.Instructor.GetMentorByID(paId);
      MentorDetailsViewModel detailsViewModel = new MentorDetailsViewModel();
      if (mentorById == null)
        return (ActionResult) this.RedirectToAction("MentorIndex", "AdminMentor");
      detailsViewModel.Age = mentorById.Age.ToString();
      detailsViewModel.CellNumber = mentorById.TelephoneCell;
      detailsViewModel.DateEnroled = mentorById.DateTimeCreated;
      detailsViewModel.Employed = mentorById.Employed ? "Yes" : "No";
      detailsViewModel.FirstName = mentorById.FirstName;
      detailsViewModel.LastName = mentorById.LastName;
      detailsViewModel.Nickname = mentorById.Nickname;
      detailsViewModel.Gender = mentorById.SelectGender.ToString();
      detailsViewModel.Race = mentorById.SelectedRace.ToString();
      detailsViewModel.HighestQualification = mentorById.HighestQualification;
      detailsViewModel.HighestQualificationTitle = mentorById.HighestQualificationTitle;
      detailsViewModel.HomeLanguage = mentorById.HomeLanguage;
      detailsViewModel.IDNumber = mentorById.IdentityNumber;
      detailsViewModel.PostalAddress1 = mentorById.PostalAddress1;
      detailsViewModel.PostalAddress2 = mentorById.PostalAddress2;
      detailsViewModel.PostalAddress3 = mentorById.PostalAddress3;
      detailsViewModel.PostalAddressCity = mentorById.PostalAddressCity;
      detailsViewModel.PostalAddressCode = mentorById.PostalAddressCode;
      detailsViewModel.PostalAddressProvince = mentorById.PostalAddressProvince;
      detailsViewModel.MentorID = mentorById.ID;
      detailsViewModel.UserID = mentorById.UserID;
      detailsViewModel.IsLocked = mentorById.IsLocked;
      detailsViewModel.IsApproved = mentorById.IsApproved;
      detailsViewModel.MentorCode = mentorById.MentorCode;
      detailsViewModel.Email = mentorById.EmailAddress;
      detailsViewModel.DateApproved = mentorById.DateTimeApproved ?? new DateTime(1, 1, 1);
      detailsViewModel.UserFiles = new List<Test.Models.Admin.UserFile>();
      if (mentorById.UserFiles != null && mentorById.UserFiles.Count > 0)
      {
        foreach (COML.Classes.UserFile userFile in mentorById.UserFiles)
          detailsViewModel.UserFiles.Add(new Test.Models.Admin.UserFile()
          {
            ID = userFile.ID,
            Filepath = userFile.Filepath,
            Name = userFile.Name,
            SystemFile = userFile.SystemFile,
            TypeFile = userFile.TypeFile,
            DateCreated = userFile.DateCreated
          });
      }
      return (ActionResult) this.View((object) detailsViewModel);
    }

    public ActionResult MentorLogs(int paId)
    {
      if (!this.User.Identity.IsAuthenticated)
        return (ActionResult) this.RedirectToAction("Index", "Home");
      if (!new AccountController().DoesUserHaveRole(this.User.Identity.GetUserId(), "Admin"))
        return (ActionResult) this.RedirectToAction("Index", "Home");
      COML.Classes.Instructor instructorById = BLL.Instructor.GetInstructorByID(paId);
      List<GenericLog> mentorLogs = BLL.Logging.GetMentorLogs(paId);
      MentorLogsViewModel mentorLogsViewModel = new MentorLogsViewModel();
      if (instructorById == null)
        return (ActionResult) this.RedirectToAction("Index", "AdminMentor");
      mentorLogsViewModel.FirstName = instructorById.FirstName;
      mentorLogsViewModel.LastName = instructorById.LastName;
      mentorLogsViewModel.MentorID = instructorById.ID;
      mentorLogsViewModel.Logs = new List<Log>();
      foreach (GenericLog genericLog in mentorLogs)
        mentorLogsViewModel.Logs.Add(new Log()
        {
          Timestamp = genericLog.TimeStamp,
          Description = genericLog.Description,
          Type = genericLog.Type
        });
      return (ActionResult) this.View((object) mentorLogsViewModel);
    }
  }
}
