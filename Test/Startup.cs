﻿// Decompiled with JetBrains decompiler
// Type: Test.Startup
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Test.Models;

namespace Test
{
  public class Startup
  {
    public void ConfigureAuth(IAppBuilder app)
    {
      app.CreatePerOwinContext<ApplicationDbContext>(new Func<ApplicationDbContext>(ApplicationDbContext.Create));
      app.CreatePerOwinContext<ApplicationUserManager>(new Func<IdentityFactoryOptions<ApplicationUserManager>, IOwinContext, ApplicationUserManager>(ApplicationUserManager.Create));
      app.CreatePerOwinContext<ApplicationSignInManager>(new Func<IdentityFactoryOptions<ApplicationSignInManager>, IOwinContext, ApplicationSignInManager>(ApplicationSignInManager.Create));
      IAppBuilder app1 = app;
      CookieAuthenticationOptions options = new CookieAuthenticationOptions();
      options.AuthenticationType = "ApplicationCookie";
      options.LoginPath = new PathString("/Account/Login");
      options.SlidingExpiration = true;
      options.ExpireTimeSpan = TimeSpan.FromMinutes(20.0);
      options.Provider = (ICookieAuthenticationProvider) new CookieAuthenticationProvider()
      {
        OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(TimeSpan.FromMinutes(30.0), (Func<ApplicationUserManager, ApplicationUser, Task<ClaimsIdentity>>) ((manager, user) => user.GenerateUserIdentityAsync((UserManager<ApplicationUser>) manager)))
      };
      app1.UseCookieAuthentication(options);
      app.UseExternalSignInCookie("ExternalCookie");
    }

    public void Configuration(IAppBuilder app)
    {
      this.ConfigureAuth(app);
      app.MapSignalR();
    }
  }
}
