﻿// Decompiled with JetBrains decompiler
// Type: Test.ApplicationUserManager
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Data.Entity;
using Test.Models;

namespace Test
{
  public class ApplicationUserManager : UserManager<ApplicationUser>
  {
    public ApplicationUserManager(IUserStore<ApplicationUser> store)
      : base(store)
    {
    }

    public static ApplicationUserManager Create(
      IdentityFactoryOptions<ApplicationUserManager> options,
      IOwinContext context)
    {
      ApplicationUserManager applicationUserManager1 = new ApplicationUserManager((IUserStore<ApplicationUser>) new UserStore<ApplicationUser>((DbContext) context.Get<ApplicationDbContext>()));
      ApplicationUserManager applicationUserManager2 = applicationUserManager1;
      UserValidator<ApplicationUser> userValidator = new UserValidator<ApplicationUser>((UserManager<ApplicationUser, string>) applicationUserManager2);
      userValidator.AllowOnlyAlphanumericUserNames = false;
      userValidator.RequireUniqueEmail = true;
      applicationUserManager2.UserValidator = (IIdentityValidator<ApplicationUser>) userValidator;
      applicationUserManager1.PasswordValidator = (IIdentityValidator<string>) new PasswordValidator()
      {
        RequiredLength = 6,
        RequireNonLetterOrDigit = false,
        RequireDigit = false,
        RequireLowercase = false,
        RequireUppercase = false
      };
      applicationUserManager1.UserLockoutEnabledByDefault = true;
      applicationUserManager1.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5.0);
      applicationUserManager1.MaxFailedAccessAttemptsBeforeLockout = 5;
      ApplicationUserManager applicationUserManager3 = applicationUserManager1;
      PhoneNumberTokenProvider<ApplicationUser> numberTokenProvider = new PhoneNumberTokenProvider<ApplicationUser>();
      numberTokenProvider.MessageFormat = "Your security code is {0}";
      applicationUserManager3.RegisterTwoFactorProvider("Phone Code", (IUserTokenProvider<ApplicationUser, string>) numberTokenProvider);
      ApplicationUserManager applicationUserManager4 = applicationUserManager1;
      EmailTokenProvider<ApplicationUser> emailTokenProvider = new EmailTokenProvider<ApplicationUser>();
      emailTokenProvider.Subject = "Security Code";
      emailTokenProvider.BodyFormat = "Your security code is {0}";
      applicationUserManager4.RegisterTwoFactorProvider("Email Code", (IUserTokenProvider<ApplicationUser, string>) emailTokenProvider);
      applicationUserManager1.EmailService = (IIdentityMessageService) new EmailService();
      applicationUserManager1.SmsService = (IIdentityMessageService) new SmsService();
      IDataProtectionProvider protectionProvider = options.DataProtectionProvider;
      if (protectionProvider != null)
        applicationUserManager1.UserTokenProvider = (IUserTokenProvider<ApplicationUser, string>) new DataProtectorTokenProvider<ApplicationUser>(protectionProvider.Create("ASP.NET Identity"));
      return applicationUserManager1;
    }
  }
}
