﻿// Decompiled with JetBrains decompiler
// Type: Test.BundleConfig
// Assembly: Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E39E06F9-AA1B-48D9-B1CE-F3B39079F194
// Assembly location: C:\Users\S4\Desktop\man\bin\Test.dll

using System.Web.Optimization;

namespace Test
{
  public class BundleConfig
  {
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-1.10.2.min.js", "~/Scripts/jquery.knob.js"));
      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate*"));
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));
      bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js", "~/Scripts/bootstrap-datepicker.min.js"));
      bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.min.css", "~/Content/site.css"));
      bundles.Add(new ScriptBundle("~/bundles/pickerbootstrap").Include("~/Scripts/moment.js", "~/Scripts/bootstrap-datetimepicker.min.js", "~/Scripts/bootstrap-datepicker.js"));
      bundles.Add(new StyleBundle("~/Content/pickerscss").Include("~/Content/bootstrap-datepicker3.standalone.css", "~/Content/bootstrap-datetimepicker.min.css"));
      bundles.Add(new ScriptBundle("~/bundles/reports").Include("~/Scripts/reports.js"));
    }
  }
}
